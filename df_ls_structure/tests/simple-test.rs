use df_ls_core::ReferenceTo;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_structure::*;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn simple_test() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "descriptor_color_standard

            [OBJECT:DESCRIPTOR_COLOR]

            Simple test

            [COLOR:AMBER]
                [NAME:amber]
                [RGB:255:191:0]
                [WORD:AMBER]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DFRaw {
        header: "descriptor_color_standard".to_owned(),
        token_structure: vec![ObjectToken {
            color_tokens: vec![ColorToken {
                reference: Some(ReferenceTo::new("AMBER".to_owned())),
                name: Some("amber".to_owned()),
                word: vec![ReferenceTo::new("AMBER".to_owned())],
                rgb: Some((255, 191, 0)),
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
