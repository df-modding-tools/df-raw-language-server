#![allow(clippy::needless_update)]

use df_ls_core::{Reference, ReferenceTo};
use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_normal_case() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:PROFESSION]

            [PROFESSION:MONSTER]
                [NAME:TEST]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: vec![Reference("TEST".to_owned())],
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}

#[test]
fn test_alias() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:PROFESSION]

            [PROFESSION:MONSTER]
                [JOB:SLEEPER]
                [NAME:TEST]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            professions: vec![ProfessionToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: vec![
                    Reference("SLEEPER".to_owned()),
                    Reference("TEST".to_owned()),
                ],
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["alias"])
    .add_test_syntax_diagnostics_ranges(vec![Range {
        start: Position {
            line: 4,
            character: 17,
        },
        end: Position {
            line: 4,
            character: 20,
        },
    }])
    .run_test();
}
