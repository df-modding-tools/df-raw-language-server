use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum BiomeEnum {
    /// Mountain (ID: 0)
    #[df_token(token_name = "MOUNTAIN")]
    #[df_alias(token_name = "MOUNTAINS", discouraged)]
    Mountain,
    /// Glacier (ID: 1)
    #[df_token(token_name = "GLACIER")]
    Glacier,
    /// Tundra (ID: 2)
    #[df_token(token_name = "TUNDRA")]
    Tundra,
    /// Temperate Freshwater Swamp (ID: 3)
    #[df_token(token_name = "SWAMP_TEMPERATE_FRESHWATER")]
    SwampTemperateFreshwater,
    /// Temperate Saltwater Swamp (ID: 4)
    #[df_token(token_name = "SWAMP_TEMPERATE_SALTWATER")]
    SwampTemperateSaltwater,
    /// Temperate Freshwater Marsh (ID: 5)
    #[df_token(token_name = "MARSH_TEMPERATE_FRESHWATER")]
    MarshTemperateFreshwater,
    /// Temperate Saltwater Marsh (ID: 6)
    #[df_token(token_name = "MARSH_TEMPERATE_SALTWATER")]
    MarshTemperateSaltwater,
    /// Tropical Freshwater Swamp (ID: 7)
    #[df_token(token_name = "SWAMP_TROPICAL_FRESHWATER")]
    SwampTropicalFreshwater,
    /// Tropical Saltwater Swamp (ID: 8)
    #[df_token(token_name = "SWAMP_TROPICAL_SALTWATER")]
    SwampTropicalSaltwater,
    /// Mangrove Swamp (ID: 9)
    #[df_token(token_name = "SWAMP_MANGROVE")]
    SwampMangrove,
    /// Tropical Freshwater Marsh (ID: 10)
    #[df_token(token_name = "MARSH_TROPICAL_FRESHWATER")]
    MarshTropicalFreshwater,
    /// Tropical Saltwater Marsh (ID: 11)
    #[df_token(token_name = "MARSH_TROPICAL_SALTWATER")]
    MarshTropicalSaltwater,
    /// Taiga (ID: 12)
    #[df_token(token_name = "FOREST_TAIGA")]
    #[df_alias(token_name = "TAIGA", discouraged)]
    ForestTaiga,
    /// Temperate Coniferous Forest (ID: 13)
    #[df_token(token_name = "FOREST_TEMPERATE_CONIFER")]
    ForestTemperateConifer,
    /// Temperate Broadleaf Forest (ID: 14)
    #[df_token(token_name = "FOREST_TEMPERATE_BROADLEAF")]
    ForestTemperateBroadleaf,
    /// Tropical Coniferous Forest (ID: 15)
    #[df_token(token_name = "FOREST_TROPICAL_CONIFER")]
    ForestTropicalConifer,
    /// Tropical Dry Broadleaf Forest (ID: 16)
    #[df_token(token_name = "FOREST_TROPICAL_DRY_BROADLEAF")]
    ForestTropicalDryBroadleaf,
    /// Tropical Moist Broadleaf Forest (ID: 17)
    #[df_token(token_name = "FOREST_TROPICAL_MOIST_BROADLEAF")]
    ForestTropicalMoistBroadleaf,
    /// Temperate Grassland (ID: 18)
    #[df_token(token_name = "GRASSLAND_TEMPERATE")]
    GrasslandTemperate,
    /// Temperate Savanna (ID: 19)
    #[df_token(token_name = "SAVANNA_TEMPERATE")]
    SavannaTemperate,
    /// Temperate Shrubland (ID: 20)
    #[df_token(token_name = "SHRUBLAND_TEMPERATE")]
    ShrublandTemperate,
    /// Tropical Grassland (ID: 21)
    #[df_token(token_name = "GRASSLAND_TROPICAL")]
    GrasslandTropical,
    /// Tropical Savanna (ID: 22)
    #[df_token(token_name = "SAVANNA_TROPICAL")]
    SavannaTropical,
    /// Tropical Shrubland (ID: 23)
    #[df_token(token_name = "SHRUBLAND_TROPICAL")]
    ShrublandTropical,
    /// Badlands (ID: 24)
    #[df_token(token_name = "DESERT_BADLAND")]
    DesertBadland,
    /// Rocky Wasteland (ID: 25)
    #[df_token(token_name = "DESERT_ROCK")]
    DesertRock,
    /// Sand Desert (ID: 26)
    #[df_token(token_name = "DESERT_SAND")]
    DesertSand,
    /// Tropical Ocean (ID: 27)
    #[df_token(token_name = "OCEAN_TROPICAL")]
    OceanTropical,
    /// Temperate Ocean (ID: 28)
    #[df_token(token_name = "OCEAN_TEMPERATE")]
    OceanTemperate,
    /// Arctic Ocean (ID: 29)
    #[df_token(token_name = "OCEAN_ARCTIC")]
    OceanArctic,
    /// Temperate Freshwater Pool (ID: 30)
    #[df_token(token_name = "POOL_TEMPERATE_FRESHWATER")]
    PoolTemperateFreshwater,
    /// Temperate Brackish Pool (ID: 31)
    #[df_token(token_name = "POOL_TEMPERATE_BRACKISHWATER")]
    PoolTemperateBrackishwater,
    /// Temperate Saltwater Pool (ID: 32)
    #[df_token(token_name = "POOL_TEMPERATE_SALTWATER")]
    PoolTemperateSaltwater,
    /// Tropical Freshwater Pool (ID: 33)
    #[df_token(token_name = "POOL_TROPICAL_FRESHWATER")]
    PoolTropicalFreshwater,
    /// Tropical Brackish Pool (ID: 34)
    #[df_token(token_name = "POOL_TROPICAL_BRACKISHWATER")]
    PoolTropicalBrackishwater,
    /// Tropical Saltwater Pool (ID: 35)
    #[df_token(token_name = "POOL_TROPICAL_SALTWATER")]
    PoolTropicalSaltwater,
    /// Temperate Freshwater Lake (ID: 36)
    #[df_token(token_name = "LAKE_TEMPERATE_FRESHWATER")]
    LakeTemperateFreshwater,
    /// Temperate Brackish Lake (ID: 37)
    #[df_token(token_name = "LAKE_TEMPERATE_BRACKISHWATER")]
    LakeTemperateBrackishwater,
    /// Temperate Saltwater Lake (ID: 38)
    #[df_token(token_name = "LAKE_TEMPERATE_SALTWATER")]
    LakeTemperateSaltwater,
    /// Tropical Freshwater Lake (ID: 39)
    #[df_token(token_name = "LAKE_TROPICAL_FRESHWATER")]
    LakeTropicalFreshwater,
    /// Tropical Brackish Lake (ID: 40)
    #[df_token(token_name = "LAKE_TROPICAL_BRACKISHWATER")]
    LakeTropicalBrackishwater,
    /// Tropical Saltwater Lake (ID: 41)
    #[df_token(token_name = "LAKE_TROPICAL_SALTWATER")]
    LakeTropicalSaltwater,
    /// Temperate Freshwater River (ID: 42)
    #[df_token(token_name = "RIVER_TEMPERATE_FRESHWATER")]
    RiverTemperateFreshwater,
    /// Temperate Brackish River (ID: 43)
    #[df_token(token_name = "RIVER_TEMPERATE_BRACKISHWATER")]
    RiverTemperateBrackishwater,
    /// Temperate Saltwater River (ID: 44)
    #[df_token(token_name = "RIVER_TEMPERATE_SALTWATER")]
    RiverTemperateSaltwater,
    /// Tropical Freshwater River (ID: 45)
    #[df_token(token_name = "RIVER_TROPICAL_FRESHWATER")]
    RiverTropicalFreshwater,
    /// Tropical Brackish River (ID: 46)
    #[df_token(token_name = "RIVER_TROPICAL_BRACKISHWATER")]
    RiverTropicalBrackishwater,
    /// Tropical Saltwater River (ID: 47)
    #[df_token(token_name = "RIVER_TROPICAL_SALTWATER")]
    RiverTropicalSaltwater,
    /// Underground caverns (in water) (ID: 48)
    #[df_token(token_name = "SUBTERRANEAN_WATER")]
    SubterraneanWater,
    /// Underground caverns (out of water) (ID: 49)
    #[df_token(token_name = "SUBTERRANEAN_CHASM")]
    SubterraneanChasm,
    /// Magma sea (ID: 50)
    #[df_token(token_name = "SUBTERRANEAN_LAVA")]
    SubterraneanLava,
    /// All biomes excluding pools, rivers, and underground features (ID: 0-29, 36-41)
    #[df_token(token_name = "ALL_MAIN")]
    AllMain,
    /// All main biomes excluding oceans and lakes (ID: 0-26)
    #[df_token(token_name = "ANY_LAND")]
    AnyLand,
    /// All ocean biomes (ID: 27-29)
    #[df_token(token_name = "ANY_OCEAN")]
    AnyOcean,
    /// All lake biomes (ID: 36-41)
    #[df_token(token_name = "ANY_LAKE")]
    AnyLake,
    /// All temperate lake biomes (ID: 36-38)
    #[df_token(token_name = "ANY_TEMPERATE_LAKE")]
    AnyTemperateLake,
    /// All tropical lake biomes (ID: 39-41)
    #[df_token(token_name = "ANY_TROPICAL_LAKE")]
    AnyTropicalLake,
    /// All river biomes (ID: 42-47)
    #[df_token(token_name = "ANY_RIVER")]
    AnyRiver,
    /// All temperate river biomes (ID: 42-44)
    #[df_token(token_name = "ANY_TEMPERATE_RIVER")]
    AnyTemperateRiver,
    /// All tropical river biomes (ID: 45-47)
    #[df_token(token_name = "ANY_TROPICAL_RIVER")]
    AnyTropicalRiver,
    /// All pool biomes (ID: 30-35)
    #[df_token(token_name = "ANY_POOL")]
    AnyPool,
    /// All land biomes excluding Mountain, Glacier, and Tundra (ID: 3-26)
    #[df_token(token_name = "NOT_FREEZING")]
    NotFreezing,
    /// All Temperate land biomes - marshes, swamps, forests, grassland, savanna, and shrubland
    /// (ID: 3-6, 13-14, 18-20)
    #[df_token(token_name = "ANY_TEMPERATE")]
    AnyTemperate,
    /// All Tropical land biomes - marshes, swamps (including Mangrove), forests, grassland,
    /// savanna, and shrubland (ID: 7-11, 15-17, 21-23)
    #[df_token(token_name = "ANY_TROPICAL")]
    AnyTropical,
    /// All Forest biomes (excluding Taiga) (ID: 13-17)
    #[df_token(token_name = "ANY_FOREST")]
    AnyForest,
    /// Temperate and Tropical Shrubland (ID: 20, 23)
    #[df_token(token_name = "ANY_SHRUBLAND")]
    AnyShrubland,
    /// Temperate and Tropical Grassland (ID: 18, 21)
    #[df_token(token_name = "ANY_GRASSLAND")]
    AnyGrassland,
    /// Temperate and Tropical Savanna (ID: 19, 22)
    #[df_token(token_name = "ANY_SAVANNA")]
    AnySavanna,
    /// Temperate Coniferous and Broadleaf Forests (ID: 13-14)
    #[df_token(token_name = "ANY_TEMPERATE_FOREST")]
    AnyTemperateForest,
    /// Tropical Coniferous and Dry/Moist Broadleaf Forests (ID: 15-17)
    #[df_token(token_name = "ANY_TROPICAL_FOREST")]
    AnyTropicalForest,
    /// Temperate Broadleaf Forest, Grassland/Savanna/Shrubland, Swamps, and Marshes (ID: 3-6, 14, 18-20)
    #[df_token(token_name = "ANY_TEMPERATE_BROADLEAF")]
    AnyTemperateBroadleaf,
    /// Tropical Dry/Moist Broadleaf Forest, Grassland/Savanna/Shrubland, Swamps (including Mangrove),
    /// and Marshes (ID: 7-11, 16-17, 21-23)
    #[df_token(token_name = "ANY_TROPICAL_BROADLEAF")]
    AnyTropicalBroadleaf,
    /// All swamps and marshes (ID: 3-11)
    #[df_token(token_name = "ANY_WETLAND")]
    AnyWetland,
    /// All temperate swamps and marshes (ID: 3-6)
    #[df_token(token_name = "ANY_TEMPERATE_WETLAND")]
    AnyTemperateWetland,
    /// All tropical swamps and marshes (ID: 7-11)
    #[df_token(token_name = "ANY_TROPICAL_WETLAND")]
    AnyTropicalWetland,
    /// All tropical marshes (ID: 10-11)
    #[df_token(token_name = "ANY_TROPICAL_MARSH")]
    AnyTropicalMarsh,
    /// All temperate marshes (ID: 5-6)
    #[df_token(token_name = "ANY_TEMPERATE_MARSH")]
    AnyTemperateMarsh,
    /// All tropical swamps (including Mangrove) (ID: 7-9)
    #[df_token(token_name = "ANY_TROPICAL_SWAMP")]
    AnyTropicalSwamp,
    /// All temperate swamps (ID: 3-4)
    #[df_token(token_name = "ANY_TEMPERATE_SWAMP")]
    AnyTemperateSwamp,
    /// Badlands, Rocky Wasteland, and Sand Desert (ID: 24-26)
    #[df_token(token_name = "ANY_DESERT")]
    AnyDesert,
}

impl Default for BiomeEnum {
    fn default() -> Self {
        Self::Mountain
    }
}
