use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

#[test]
fn message_missing_header() {
    LexerTestBuilder::test_source(
        "

        ",
    )
    .add_test_s_exp(
        "(raw_file
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics(vec![Diagnostic {
        range: Range {
            start: Position {
                line: 0,
                character: 0,
            },
            end: Position {
                line: 0,
                character: 0,
            },
        },
        severity: Some(DiagnosticSeverity::ERROR),
        code: Some(NumberOrString::String("missing_header".to_owned())),
        source: Some("DF RAW Language Server".to_owned()),
        message: "Expected file header on first line.".to_owned(),
        ..Default::default()
    }])
    .run_test();
}

#[test]
fn message_missing_end_bracket() {
    LexerTestBuilder::test_source(
        "test
        [REF
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `test`)
  (comment)
  (token
    ([)
    (token_name: `REF`)
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics(vec![Diagnostic {
        range: Range {
            start: Position {
                line: 1,
                character: 12,
            },
            end: Position {
                line: 1,
                character: 12,
            },
        },
        severity: Some(DiagnosticSeverity::ERROR),
        code: Some(NumberOrString::String("missing_end_bracket".to_owned())),
        source: Some("DF RAW Language Server".to_owned()),
        message: "Expected `]`.".to_owned(),
        ..Default::default()
    }])
    .run_test();
}

#[test]
fn message_unexpected_end_bracket() {
    LexerTestBuilder::test_source(
        "test
        REF]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `test`)
  (comment)
  (ERROR: `]`)
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics(vec![Diagnostic {
        range: Range {
            start: Position {
                line: 1,
                character: 11,
            },
            end: Position {
                line: 1,
                character: 12,
            },
        },
        severity: Some(DiagnosticSeverity::WARNING),
        code: Some(NumberOrString::String("unexpected_end_bracket".to_owned())),
        source: Some("DF RAW Language Server".to_owned()),
        message: "Unexpected `]`. Did you forget a start bracket?".to_owned(),
        ..Default::default()
    }])
    .run_test();
}

#[test]
fn message_missing_token_name() {
    LexerTestBuilder::test_source(
        "test
        []
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `test`)
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics(vec![Diagnostic {
        range: Range {
            start: Position {
                line: 1,
                character: 9,
            },
            end: Position {
                line: 1,
                character: 9,
            },
        },
        severity: Some(DiagnosticSeverity::ERROR),
        code: Some(NumberOrString::String("missing_token_name".to_owned())),
        source: Some("DF RAW Language Server".to_owned()),
        message: "The token name is missing.".to_owned(),
        ..Default::default()
    }])
    .run_test();
}

#[test]
fn message_unexpected_characters() {
    LexerTestBuilder::test_source(
        "test
        [preREF]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `test`)
  (comment)
  (token
    ([)
    (token_name: `REF`)
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics(vec![Diagnostic {
        range: Range {
            start: Position {
                line: 1,
                character: 9,
            },
            end: Position {
                line: 1,
                character: 12,
            },
        },
        severity: Some(DiagnosticSeverity::ERROR),
        code: Some(NumberOrString::String("unexpected_characters".to_owned())),
        source: Some("DF RAW Language Server".to_owned()),
        message: "Unexpected characters found.".to_owned(),
        ..Default::default()
    }])
    .run_test();
}

#[test]
fn message_missing_newline() {
    LexerTestBuilder::test_source("test[REF]")
        .add_test_s_exp(
            "(raw_file
  (header: `test`)
  (token
    ([)
    (token_name: `REF`)
    (])
  )
  (EOF: ``)
)
",
        )
        .add_test_lexer_diagnostics(vec![Diagnostic {
            range: Range {
                start: Position {
                    line: 0,
                    character: 4,
                },
                end: Position {
                    line: 0,
                    character: 4,
                },
            },
            severity: Some(DiagnosticSeverity::ERROR),
            code: Some(NumberOrString::String("missing_newline".to_owned())),
            source: Some("DF RAW Language Server".to_owned()),
            message: "Expected a newline here.".to_owned(),
            ..Default::default()
        }])
        .run_test();
}
