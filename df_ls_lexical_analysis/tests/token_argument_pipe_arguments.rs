use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

#[test]
fn test_token_argument_pipe_arguments_permutations() {
    LexerTestBuilder::test_source(
        "Token Argument Pipe Arguments
        [A:string|string]
        [A:string|0]
        [A:0|string]
        [A:multiple|pipe|chars]
        [A:mixed:01:-5|are|OK:TOO]
        [A:>=<.-,+*|'&%$]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `Token Argument Pipe Arguments`)
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_pipe_arguments
        (token_argument_string: `string`)
        (|)
        (token_argument_string: `string`)
      )
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_pipe_arguments
        (token_argument_string: `string`)
        (|)
        (token_argument_integer: `0`)
      )
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_pipe_arguments
        (token_argument_integer: `0`)
        (|)
        (token_argument_string: `string`)
      )
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_pipe_arguments
        (token_argument_string: `multiple`)
        (|)
        (token_argument_string: `pipe`)
        (|)
        (token_argument_string: `chars`)
      )
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_string: `mixed`)
      (:)
      (token_argument_integer: `01`)
      (:)
      (token_argument_pipe_arguments
        (token_argument_integer: `-5`)
        (|)
        (token_argument_string: `are`)
        (|)
        (token_argument_reference: `OK`)
      )
      (:)
      (token_argument_reference: `TOO`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_pipe_arguments
        (token_argument_string: `>=<.-,+*`)
        (|)
        (token_argument_string: `'&%$`)
      )
    )
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}

#[test]
// See issue #116
fn test_token_argument_pipe_arguments_pipe_char() {
    LexerTestBuilder::test_source(
        "header
        [MAIN:DEBUG]
        [PIPE_ARG:string|'|'|32]",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `header`)
  (comment)
  (token
    ([)
    (token_name: `MAIN`)
    (token_arguments
      (:)
      (token_argument_reference: `DEBUG`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `PIPE_ARG`)
    (token_arguments
      (:)
      (token_argument_pipe_arguments
        (token_argument_string: `string`)
        (|)
        (token_argument_string: `'`)
        (|)
        (token_argument_string: `'`)
        (|)
        (token_argument_integer: `32`)
      )
    )
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}

#[test]
// See issue #117
// Combined with EOF error fix
fn test_token_argument_pipe_arguments_pipe_end_bracket_eof() {
    LexerTestBuilder::test_source(
        "header
        [MAIN:DEBUG]
        [PIPE_ARG:|'['",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `header`)
  (comment)
  (token
    ([)
    (token_name: `MAIN`)
    (token_arguments
      (:)
      (token_argument_reference: `DEBUG`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `PIPE_ARG`)
    (token_arguments
      (:)
      (token_argument_pipe_arguments
        (token_argument_empty: ``)
        (|)
        (token_argument_string: `'`)
      )
    )
    (])
  )
  (token
    ([)
    (token_name: ``)
    (ERROR: `'`)
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![
        "missing_end_bracket",
        "missing_token_name",
        "unexpected_characters",
        "missing_end_bracket",
    ])
    .add_test_lexer_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 2,
                character: 20,
            },
            end: Position {
                line: 2,
                character: 20,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 21,
            },
            end: Position {
                line: 2,
                character: 21,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 21,
            },
            end: Position {
                line: 2,
                character: 22,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 22,
            },
            end: Position {
                line: 2,
                character: 22,
            },
        },
    ])
    .run_test();
}
