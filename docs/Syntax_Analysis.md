# 2: Syntax Analysis

TODO

## Order of operations/types

When using types in `Choose<>` the ordering of the types is important.
The following order should be followed. If the item is higher in the list below,
the type should be put first in the `Choose` option.
This also applies to when multiple `Choose` types are nested.

- `AllowEmpty`
- `Any` (should not be combined with other types)
- `ArgN`
- `bool`
- `char`
- `Clamp`
- `DFChar`
- Integers (`u32`, `i32`,...)
- Enums
- (Most other types not listed.)
- `Reference`
- `ReferenceTo`
- `String`
- `PipeArguments`

Types that do not in itself effect the order.
(There nested types will might effect the order):
- `Option`
- `Choose`
- Tuple (`(char,char)`)
- `Vec`

Note: For some (mostly the top of the list) the order is less important.
But following this might still prevent future issues from occurring.
