#![cfg(debug_assertions)]

use df_ls_diagnostics::{lsp_types::*, DiagnosticsInfo};
use pretty_assertions::assert_eq;

pub fn test_diagnostics_codes(expected_diagnostics_codes: &[String], given_info: &DiagnosticsInfo) {
    let given_diagnostics_list = given_info.get_diagnostic_list();
    let given_diagnostics_codes = given_diagnostics_list
        .iter()
        .map(|d| {
            if let Some(code) = &d.code {
                match code {
                    NumberOrString::String(code) => code,
                    _ => panic!("Diagnostic code is not of type `String`."),
                }
            } else {
                panic!("One diagnostic message does not have a code");
            }
        })
        .collect();
    log::info!("Given Diagnostics Codes:\n{:#?}", given_diagnostics_codes);

    assert_diagnostic_codes(expected_diagnostics_codes, given_diagnostics_codes);
}

pub fn assert_diagnostic_codes(expected_codes: &[String], given_codes: Vec<&String>) {
    assert_eq!(
        expected_codes.len(),
        given_codes.len(),
        "Diagnostics codes lists are not the same size."
    );
    for (expected, given) in expected_codes.iter().zip(given_codes.into_iter()) {
        assert_eq!(expected, given);
    }
}
