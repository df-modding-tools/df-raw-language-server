mod get_material_from_reagent_arg;
mod local_creature_mat_arg;
mod local_plant_mat_arg;
mod material_token_arg;
mod material_type_enum;

use crate::NoneEnum;
use df_ls_core::Choose;
pub use get_material_from_reagent_arg::GetMaterialFromReagentArg;
pub use local_creature_mat_arg::LocalCreatureMatArg;
pub use local_plant_mat_arg::LocalPlantMatArg;
pub use material_token_arg::MaterialTokenArg;
pub use material_type_enum::{CoalMaterialEnum, MaterialTypeEnum};

/// Allow all Material Tokens and `NONE:NONE`.
pub type MaterialTokenArgWithNoneNone = Choose<MaterialTokenArg, (NoneEnum, NoneEnum)>;
/// Allow all Material Tokens and `LOCAL_CREATURE_MAT`.
pub type MaterialTokenArgWithLocalCreatureMat = Choose<MaterialTokenArg, LocalCreatureMatArg>;
/// Allow all Material Tokens and `LOCAL_PLANT_MAT`.
pub type MaterialTokenArgWithLocalPlantMat = Choose<MaterialTokenArg, LocalPlantMatArg>;
/// Allow all Material Tokens and `GET_MATERIAL_FROM_REAGENT`.
pub type MaterialTokenArgWithReagentMat = Choose<MaterialTokenArg, GetMaterialFromReagentArg>;

/// Allow `LOCAL_CREATURE_MAT` and `LOCAL_PLANT_MAT`.
pub type MaterialTokenArgWithCreaturePlantMat =
    Choose<MaterialTokenArg, Choose<LocalCreatureMatArg, LocalPlantMatArg>>;
/// Allow `LOCAL_CREATURE_MAT`, `LOCAL_PLANT_MAT` and `GET_MATERIAL_FROM_REAGENT`.
pub type MaterialTokenArgWithReagentCreaturePlantMat = Choose<
    MaterialTokenArg,
    Choose<LocalCreatureMatArg, Choose<LocalPlantMatArg, GetMaterialFromReagentArg>>,
>;
