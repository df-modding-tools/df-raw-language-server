#![allow(clippy::needless_update)]

use df_ls_core::{DFChar, ReferenceTo};
use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_df_char_value() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:PROFESSION]

            [PROFESSION:MONSTER]
                [SYMBOL:'M']
            [PROFESSION:MONSTER2]
                [SYMBOL:1]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            professions: vec![
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    symbol: Some(DFChar('M')),
                    ..Default::default()
                },
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER2".to_owned())),
                    symbol: Some(DFChar('☺')),
                    ..Default::default()
                },
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}

#[test]
fn test_df_char_error() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:PROFESSION]

            [PROFESSION:MONSTER]
                [SYMBOL:600]
            [PROFESSION:MONSTER2]
                [SYMBOL:-5]
            [PROFESSION:FIRE_MONSTER]
                [SYMBOL:'🔥']
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            professions: vec![
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    symbol: None,
                    ..Default::default()
                },
                ProfessionToken {
                    reference: Some(ReferenceTo::new("MONSTER2".to_owned())),
                    symbol: None,
                    ..Default::default()
                },
                ProfessionToken {
                    reference: Some(ReferenceTo::new("FIRE_MONSTER".to_owned())),
                    symbol: Some(DFChar('🔥')), //TODO: This is not an allowed character (see #67)
                    ..Default::default()
                },
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["too_large_int", "too_small_int"])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 4,
                character: 24,
            },
            end: Position {
                line: 4,
                character: 27,
            },
        },
        Range {
            start: Position {
                line: 6,
                character: 24,
            },
            end: Position {
                line: 6,
                character: 26,
            },
        },
    ])
    .run_test();
}
