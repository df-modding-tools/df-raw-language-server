#![cfg(debug_assertions)]

use crate::TokenDeserialize;
use pretty_assertions::assert_eq;
use std::fmt::Debug;

pub fn test_structure<S: TokenDeserialize + Debug + PartialEq>(
    expected_structure: &S,
    given_structure: &S,
) {
    log::info!("Given Structure:\n{:#?}", given_structure);

    assert_eq!(expected_structure, given_structure);
}
