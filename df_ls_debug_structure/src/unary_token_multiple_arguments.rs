use df_ls_core::Reference;
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_syntax_analysis::{Token, TokenDeserialize, TryFromArgumentGroup};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum DebugUnaryTokenEnum {
    Option1(Reference),
    Option2(Reference),
    Option3(Reference),
}
impl Default for DebugUnaryTokenEnum {
    fn default() -> Self {
        Self::Option1(Reference::default())
    }
}

// Deserialize a token with following pattern: `[REF:OPTION1:REF]`
df_ls_syntax_analysis::token_deserialize_unary_token!(DebugUnaryTokenEnum);

impl TryFromArgumentGroup for DebugUnaryTokenEnum {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument (is not token_name) for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        let reference_arg0 =
            Reference::try_from_argument_group(token, source, diagnostics, add_diagnostics_on_err)?;
        let options = match reference_arg0.0.as_ref() {
            "OPTION1" => {
                // Arg 1
                let opt = Reference::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                Self::Option1(opt)
            }
            "OPTION2" => {
                // Arg 1
                let opt = Reference::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                Self::Option2(opt)
            }
            "OPTION3" => {
                // Arg 1
                let opt = Reference::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?;
                Self::Option3(opt)
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec!["OPTION1", "OPTION2", "OPTION3"],
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(options)
    }
}
