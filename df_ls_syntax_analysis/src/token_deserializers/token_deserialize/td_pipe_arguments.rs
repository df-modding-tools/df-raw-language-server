use super::super::{Argument, Token, TokenArgument, TryFromArgument, TryFromArgumentGroup};
use super::TokenDeserialize;
use df_ls_core::PipeArguments;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};

// Deserialize a token with following pattern: `[REF:PIPE|arguments]`
crate::token_deserialize_unary_token!(PipeArguments);

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for PipeArguments {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for PipeArguments {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match &arg.value {
                TokenArgument::TVPipeArguments(v) => Ok(PipeArguments({
                    let mut list = Vec::new();
                    for item in v {
                        list.push(match item {
                            TokenArgument::TVEmpty => None,
                            _ => Some(super::td_any::from_token_arg_to_any(
                                arg,
                                item,
                                diagnostics,
                                add_diagnostics_on_err,
                            )?),
                        });
                    }
                    list
                })),
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "PipeArguments".to_owned()
    }
}

// -------------------------Convert from TokenArgument -----------------------

impl From<PipeArguments> for TokenArgument {
    fn from(item: PipeArguments) -> TokenArgument {
        TokenArgument::TVPipeArguments(item.0.into_iter().map(|v| v.into()).collect())
    }
}

impl From<Option<PipeArguments>> for TokenArgument {
    fn from(item: Option<PipeArguments>) -> TokenArgument {
        match item {
            Some(v) => TokenArgument::TVPipeArguments(v.0.into_iter().map(|v| v.into()).collect()),
            None => TokenArgument::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_tree_structure;
    use crate::test_utils::SyntaxTestBuilder;
    use df_ls_core::{Any, Reference};
    use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

    #[test]
    fn test_pipe_arguments_correct() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:string|string]
                [REF:s|2]
                [REF:2|s]
                [REF:2|0]
                [REF:'c'|0]
                [REF:-5|0|5]
                [REF:0|]
                [REF:||]
                [REF:a|a|a|a5|5|5|5|REF]
                [REF:|]",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec![])
        .add_test_syntax_diagnostics_ranges(vec![]);

        test_tree_structure!(
            test_builder,
            [
                PipeArguments => PipeArguments(vec![
                    Some(Any::String("string".to_owned())),
                    Some(Any::String("string".to_owned()))
                ]),
                (PipeArguments,) => (PipeArguments(vec![
                    Some(Any::String("s".to_owned())),
                    Some(Any::Integer(2))
                ]),),
                PipeArguments => PipeArguments(vec![
                    Some(Any::Integer(2)),
                    Some(Any::String("s".to_owned())),
                ]),
                PipeArguments => PipeArguments(vec![
                    Some(Any::Integer(2)),
                    Some(Any::Integer(0)),
                ]),
                PipeArguments => PipeArguments(vec![
                    Some(Any::Character('c')),
                    Some(Any::Integer(0))
                ]),
                PipeArguments => PipeArguments(vec![
                    Some(Any::Integer(-5)),
                    Some(Any::Integer(0)),
                    Some(Any::Integer(5))
                ]),
                PipeArguments => PipeArguments(vec![Some(Any::Integer(0)), None]),
                PipeArguments => PipeArguments(vec![None, None, None]),
                PipeArguments => PipeArguments(vec![
                    Some(Any::String("a".to_owned())),
                    Some(Any::String("a".to_owned())),
                    Some(Any::String("a".to_owned())),
                    Some(Any::String("a5".to_owned())),
                    Some(Any::Integer(5)),
                    Some(Any::Integer(5)),
                    Some(Any::Integer(5)),
                    Some(Any::Reference(Reference("REF".to_owned())))
                ]),
            ]
        );
    }
}
