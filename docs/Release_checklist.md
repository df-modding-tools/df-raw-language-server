# Release Checklist

## Pre-Merge

- [ ] Commit all open changes to repo (in `dev` branch) and GitLab.
- [ ] Check the milestone for any open changes and resolve them or move them to the next version.
- [ ] Update Docker container using CI pipeline from last commit.

## Language Server
- [ ] Check for rust updates: `rustup update`.
- [ ] Check `cargo +nightly udeps` for unused packages.
- [ ] Check `cargo update` for minor/patch updates.
- [ ] Check `cargo outdated` for outdated packages and major updates.
- - [ ] If package where updated make sure minor/patch updates are also updated again.
- [ ] Check `cargo deny check` for security issues.
- [ ] A manual quick check if everything works as expected.
- [ ] Run `cargo clean`.
- [ ] Run `cargo clippy`.
- [ ] Run `cargo +nightly fmt`.
- [ ] Run `cargo test`, all tests should be correct.

## VSCode Client
- [ ] Change directory: `cd df_ls_clients/df_ls_vscode`
- [ ] Run `npm update` to update package dependencies.
- [ ] Make sure `engines.vscode` matches `devDependencies.@types/vscode`.

- [ ] Clear GitLab runner caches.
- [ ] Check for files that should no longer be in repo.
- [ ] Update version number in:
- - [ ] `README.md` -> Latest version.
- - [ ] Use `./update_version.sh` to update files.
- - [ ] Make sure the `package.json` file has correct version. (requires `npm update` for VSCode client)
- [ ] Create a new entry in and update date:
- - [ ] `CHANGELOG.md`
- [ ] Commit the version number changes to GitRepo.
- [ ] Make sure CI starts running and finish without issues.

## Merge
- [ ] Merge the `dev` branch into `master`.
- [ ] Make sure CI starts running.
- - [ ] Start LS builds (Linux, Windows, MacOS)
- - [ ] Start Client builds (VS Code, ...)
- [ ] Create git Tag: `vX.X.X` (As Release).

## Publish
- [ ] Update clients in there respective stores:
- - [ ] VS Code:
- - - [ ] Download `.vsix` file from CI.
- - - [ ] Update to marketplace. [VSCode_Marketplace][VSCode_Marketplace]
- - - Update by going to https://marketplace.visualstudio.com/manage/publishers/df-modding-tools
      Then right-click on the highest version and select `Update` upload new `.vsix` here.
- [ ] Upload binaries to [virustotal](https://www.virustotal.com)
- [ ] Publish to `crates.io`
- - [ ] Order of publishing packages is important. This can be done in groups, depending on dependency relations.
- - - [ ] Group 1: `df_ls_derive` and `df_ls_diagnostics`
- - - [ ] Group 2: `df_ls_lexical_analysis` and `df_ls_core`
- - - [ ] Group 4: `df_ls_syntax_analysis`
- - - [ ] Group 3: `df_ls_debug_structure`
- - - [ ] Group 5: `df_ls_structure`
- - - [ ] Group 6: `df_ls_semantic_analysis`
- - - [ ] Group 7: `df_language_server`
- - [ ] Go into folder of crate
- - [ ] Run `cargo publish --dry-run` to do a test
- - [ ] Run `cargo publish` to publish crate
- [ ] Do quick test of published versions.

## Post-Publish
- [ ] Create a post in Discord `#release` channel.
- [ ] Create message on DF Forums.
- [ ] Create post on Reddit.

[VSCode_Marketplace]: https://marketplace.visualstudio.com/items?itemName=df-modding-tools.dwarf-fortress-raw-vscode
