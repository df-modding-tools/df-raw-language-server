use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Copy, Debug, PartialEq, Default, Eq, Hash)]
pub struct ArgN(pub u8);

impl ArgN {
    pub const MIN: u8 = u8::MIN;
    pub const MAX: u8 = u8::MAX;
}

impl From<u8> for ArgN {
    fn from(item: u8) -> Self {
        ArgN(item)
    }
}

impl From<ArgN> for u8 {
    fn from(item: ArgN) -> Self {
        item.0
    }
}
