# Profiling

I used [Flamegraph](https://github.com/flamegraph-rs/flamegraph), but there are [others](https://nnethercote.github.io/perf-book/profiling.html#profiling).

```bash
sudo apt install -y linux-tools-common linux-tools-generic
cargo install flamegraph
```

Run test:

```bash
cargo flamegraph --bin=df_language_server -- check debug-test.txt
```

Go to the `./Cargo.toml` and uncomment the lines marked for profiling in Flamegraph.

## Permission declined

If you have the error:

```
perf_event_open(..., PERF_FLAG_FD_CLOEXEC) failed with unexpected error 13 (Permission denied)
perf_event_open(..., 0) failed unexpectedly with error 13 (Permission denied)
Error:
You may not have permission to collect stats.

Consider tweaking /proc/sys/kernel/perf_event_paranoid,
which controls use of the performance events system by
unprivileged users (without CAP_SYS_ADMIN).

The current value is 3:

  -1: Allow use of (almost) all events by all users
      Ignore mlock limit after perf_event_mlock_kb without CAP_IPC_LOCK
>= 0: Disallow ftrace function tracepoint by users without CAP_SYS_ADMIN
      Disallow raw tracepoint access by users without CAP_SYS_ADMIN
>= 1: Disallow CPU event access by users without CAP_SYS_ADMIN
>= 2: Disallow kernel profiling by users without CAP_SYS_ADMIN
```

You need to change a setting, but make sure to revert back after you are done:

```bash
# Read current value
sudo sysctl -n kernel.perf_event_paranoid
# Update value to `2`
sudo sysctl -w kernel.perf_event_paranoid=2
# Put back original value (most likely `3`)
sudo sysctl -w kernel.perf_event_paranoid=3
```

