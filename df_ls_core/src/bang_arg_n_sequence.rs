use crate::BangArgN;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Default, Eq, Hash)]
pub struct BangArgNSequence(pub Vec<BangArgNOrValue>);

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq, Hash)]
pub enum BangArgNOrValue {
    BangArgN(BangArgN),
    Value(String),
}

impl Default for BangArgNOrValue {
    fn default() -> Self {
        Self::BangArgN(BangArgN::default())
    }
}

impl From<BangArgN> for BangArgNOrValue {
    fn from(item: BangArgN) -> Self {
        BangArgNOrValue::BangArgN(item)
    }
}
