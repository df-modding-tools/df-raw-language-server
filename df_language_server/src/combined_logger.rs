use chrono::Utc;
use directories::ProjectDirs;
use log::LevelFilter;
use simplelog::{
    ColorChoice, CombinedLogger, ConfigBuilder, LevelPadding, TermLogger, TerminalMode, WriteLogger,
};
use std::fs::File;
use std::path::PathBuf;

pub fn setup_logger(log_filter: LevelFilter) {
    // Create log configuration
    let mut log_config_builder = ConfigBuilder::new();

    log_config_builder
        .set_max_level(LevelFilter::Error) // Always show level
        .set_time_level(LevelFilter::Error) // Always show time
        .set_thread_level(LevelFilter::Trace) // Almost never show thread number
        .set_target_level(LevelFilter::Trace) // Almost never show target
        .set_location_level(LevelFilter::Debug) // On debug show location
        .set_level_padding(LevelPadding::Right) // Add padding so they all have same length
        .add_filter_ignore_str("ureq::unit") // Don't display `ureq` message (can be enabled for testing)
        .add_filter_ignore_str("hyper"); // Don't display hyper networking message (not useful in most cases)

    #[cfg(debug_assertions)]
    log_config_builder.set_location_level(LevelFilter::Error); // On error show location

    let log_config = log_config_builder.build();

    // Path to log file
    let log_file_name = format!(
        "df_language_server_log_{}.log",
        Utc::now().format("%Y-%m-%dT%H-%M-%S")
    );
    let log_file_path = if let Some(proj_dirs) =
        ProjectDirs::from("org", "DF_Modding_Tools", "DF_Language_Server")
    {
        // Lin: /home/alice/.local/share/df_language_server/
        // Win: C:\Users\Alice\AppData\Local\DF_Modding_Tools\DF_Language_Server\data\
        // Mac: /Users/Alice/Library/Application Support/org.DF_Modding_Tools.DF_Language_Server/
        proj_dirs.data_local_dir().join(log_file_name)
    } else {
        // Create next to binary
        PathBuf::from(log_file_name)
    };
    // Create missing folders
    if let Some(parent_folder) = log_file_path.parent() {
        std::fs::create_dir_all(parent_folder).unwrap_or_else(|_err| {
            panic!(
                "Could not create folders for storing logs. Path: `{}`",
                parent_folder.to_string_lossy()
            )
        });
    }
    CombinedLogger::init(vec![
        TermLogger::new(
            log_filter,
            log_config.clone(),
            TerminalMode::Stderr,
            ColorChoice::Auto,
        ),
        WriteLogger::new(
            LevelFilter::Info,
            log_config,
            File::create(&log_file_path).unwrap_or_else(|_err| {
                panic!(
                    "Could not create log file. File: `{}`",
                    log_file_path.to_string_lossy()
                )
            }),
        ),
    ])
    .expect("The logger failed to initialize.");
    log::info!("Logging file: `{}`", log_file_path.to_string_lossy());
}
