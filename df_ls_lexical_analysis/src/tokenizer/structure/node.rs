use super::{Point, Range, Tree, TreeCursor};
use df_ls_diagnostics::lsp_types;
use std::hash::{Hash, Hasher};
use std::rc::Weak;
use std::{fmt, hash};

#[derive(Clone)]
pub struct Node {
    tree: Weak<Tree>,
    node_id: u64,
}

#[derive(Clone, Debug)]
pub(crate) struct DataNode {
    pub id: u64,
    pub kind_id: u16,
    pub kind: String,
    pub name: Option<String>,
    pub start_byte: usize,
    pub end_byte: usize,
    pub start_point: Point,
    pub end_point: Point,
    pub children_ids: Vec<u64>,
    pub parent_id: Option<u64>,
    pub next_sibling_id: Option<u64>,
    pub prev_sibling_id: Option<u64>,
    pub tree: Weak<Tree>,
}

impl Hash for DataNode {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl PartialEq for DataNode {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl DataNode {
    pub(crate) fn finalize_tree(
        &self,
        parent_id: Option<u64>,
        next_sibling_id: Option<u64>,
        prev_sibling_id: Option<u64>,
    ) {
        let mut this = self.clone();
        this.parent_id = parent_id;
        this.next_sibling_id = next_sibling_id;
        this.prev_sibling_id = prev_sibling_id;
        if let Some(tree) = self.tree.upgrade() {
            tree.update_node(self.id, this);

            let mut prev_sibling_id = None;
            let mut next_sibling_id;

            for (i, child_id) in self.children_ids.iter().enumerate() {
                next_sibling_id = match self.children_ids.get(i + 1) {
                    Some(id) => Some(*id),
                    None => None,
                };
                if let Some(child) = tree.get_tsnode(*child_id) {
                    child.finalize_tree(Some(self.id), next_sibling_id, prev_sibling_id)
                } else {
                    println!("Did not find child: {}", self.id);
                }
                prev_sibling_id = Some(*child_id);
            }
        }
    }

    pub(crate) fn get_range(&self) -> lsp_types::Range {
        lsp_types::Range {
            start: lsp_types::Position {
                line: self.start_point.row as u32,
                character: self.start_point.column as u32,
            },
            end: lsp_types::Position {
                line: self.end_point.row as u32,
                character: self.end_point.column as u32,
            },
        }
    }

    pub fn utf8_text<'a>(&self, source: &'a [u8]) -> Result<&'a str, std::str::Utf8Error> {
        std::str::from_utf8(&source[self.start_byte..self.end_byte])
    }

    pub fn get_text(&self, source: &str) -> String {
        self.utf8_text(source.as_bytes())
            .expect("Non UTF-8 Characters found")
            .to_owned()
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.node_id == other.node_id
    }
}

impl Eq for Node {}

impl hash::Hash for Node {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.node_id.hash(state);
    }
}

impl fmt::Debug for Node {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(
            f,
            "{{Node {} {} - {}}}",
            self.kind(),
            self.start_position(),
            self.end_position()
        )
    }
}

impl Node {
    pub(crate) fn new(node: &DataNode) -> Self {
        Node {
            node_id: node.id,
            tree: node.tree.clone(),
        }
    }

    fn new_from_id(&self, id: u64) -> Self {
        Node {
            node_id: id,
            tree: self.tree.clone(),
        }
    }

    fn node(&self) -> DataNode {
        if let Some(tree) = self.tree.upgrade() {
            tree.get_tsnode(self.node_id)
                .expect("Node does not yet exist in tree")
        } else {
            panic!("Tree has been dropped can not get node.");
        }
    }

    /// Get a numeric id for this node that is unique.
    ///
    /// Within a given syntax tree, no two nodes have the same id. However, if
    /// a new tree is created based on an older tree, and a node from the old
    /// tree is reused in the process, then that node will have the same id in
    /// both trees.
    pub fn id(&self) -> usize {
        self.node_id as usize
    }

    /// Get this node's type as a numerical id.
    pub fn kind_id(&self) -> u16 {
        self.node().kind_id
    }

    /// Get this node's type as a string.
    pub fn kind(&self) -> String {
        self.node().kind
    }

    /// Check if this node is *named*.
    ///
    /// Named nodes correspond to named rules in the grammar, whereas *anonymous* nodes
    /// correspond to string literals in the grammar.
    pub fn is_named(&self) -> bool {
        self.node().name.is_some()
    }

    /// Check if this node has been edited.
    // pub fn has_changes(&self) -> bool {
    //     unsafe { ffi::ts_node_has_changes(self.0) }
    // }

    /// Get the byte offsets where this node starts.
    pub fn start_byte(&self) -> usize {
        self.node().start_byte
    }

    /// Get the byte offsets where this node end.
    pub fn end_byte(&self) -> usize {
        self.node().end_byte
    }

    /// Get the byte range of source code that this node represents.
    pub fn byte_range(&self) -> std::ops::Range<usize> {
        self.start_byte()..self.end_byte()
    }

    /// Get the range of source code that this node represents, both in terms of raw bytes
    /// and of row/column coordinates.
    pub fn range(&self) -> Range {
        Range {
            start_byte: self.start_byte(),
            end_byte: self.end_byte(),
            start_point: self.start_position(),
            end_point: self.end_position(),
        }
    }

    /// Get this node's start position in terms of rows and columns.
    pub fn start_position(&self) -> Point {
        self.node().start_point
    }

    /// Get this node's end position in terms of rows and columns.
    pub fn end_position(&self) -> Point {
        self.node().end_point
    }

    /// Get the node's child at the given index, where zero represents the first
    /// child.
    ///
    /// This method is fairly fast, but its cost is technically log(i), so you
    /// if you might be iterating over a long list of children, you should use
    /// [Node::children] instead.
    pub fn child(&self, i: usize) -> Option<Self> {
        if let Some(node_id) = self.node().children_ids.get(i) {
            Some(self.new_from_id(*node_id))
        } else {
            None
        }
    }

    /// Get this node's number of children.
    pub fn child_count(&self) -> usize {
        self.node().children_ids.len()
    }

    /// Get this node's *named* child at the given index.
    ///
    /// See also [Node::is_named].
    /// This method is fairly fast, but its cost is technically log(i), so you
    /// if you might be iterating over a long list of children, you should use
    /// [Node::named_children] instead.
    pub fn named_child(&self, i: usize) -> Option<Self> {
        if let Some(node_id) = self.node().children_ids.get(i) {
            let node = self.new_from_id(*node_id);
            if node.is_named() {
                Some(node)
            } else {
                None
            }
        } else {
            None
        }
    }

    /// Get this node's number of *named* children.
    ///
    /// See also [Node::is_named].
    pub fn named_child_count(&self) -> usize {
        self.node()
            .children_ids
            .iter()
            .map(|node_id| self.new_from_id(*node_id))
            .filter(|node| node.is_named())
            .count()
    }

    /// Get this node's immediate parent.
    pub fn parent(&self) -> Option<Self> {
        if let Some(parent_id) = self.node().parent_id {
            Some(self.new_from_id(parent_id))
        } else {
            None
        }
    }

    /// Get this node's next sibling.
    pub fn next_sibling(&self) -> Option<Self> {
        if let Some(sibling_id) = self.node().next_sibling_id {
            Some(self.new_from_id(sibling_id))
        } else {
            None
        }
    }

    /// Get this node's previous sibling.
    pub fn prev_sibling(&self) -> Option<Self> {
        if let Some(sibling_id) = self.node().prev_sibling_id {
            Some(self.new_from_id(sibling_id))
        } else {
            None
        }
    }

    /// Get this node's next named sibling.
    pub fn next_named_sibling(&self) -> Option<Self> {
        let mut current_sibling = self.next_sibling();
        while current_sibling.is_some() {
            if let Some(current) = &current_sibling {
                if current.is_named() {
                    break;
                }
                current_sibling = current.next_sibling();
            } else {
                break;
            }
        }
        current_sibling
    }

    /// Get this node's previous named sibling.
    pub fn prev_named_sibling(&self) -> Option<Self> {
        let mut current_sibling = self.prev_sibling();
        while current_sibling.is_some() {
            if let Some(current) = &current_sibling {
                if current.is_named() {
                    break;
                }
                current_sibling = current.prev_sibling();
            } else {
                break;
            }
        }
        current_sibling
    }

    pub fn to_sexp(&self, tree_depth: usize) -> String {
        if self.child_count() > 0 {
            format!(
                "{}({}\n{}{})\n",
                " ".repeat(tree_depth * 2),
                self.kind(),
                self.children_to_sexp(tree_depth + 1),
                " ".repeat(tree_depth * 2),
            )
        } else {
            // Has no children
            format!("{}({})\n", " ".repeat(tree_depth * 2), self.kind(),)
        }
    }

    pub fn get_range(&self) -> lsp_types::Range {
        self.node().get_range()
    }

    fn children_to_sexp(&self, tree_depth: usize) -> String {
        let mut output = "".to_owned();
        let children_ids = &self.node().children_ids;

        for child_id in children_ids {
            let child = self.new_from_id(*child_id);
            output.push_str(&child.to_sexp(tree_depth));
        }
        output
    }

    pub fn utf8_text<'a>(&self, source: &'a [u8]) -> Result<&'a str, std::str::Utf8Error> {
        std::str::from_utf8(&source[self.start_byte()..self.end_byte()])
    }

    pub fn get_text(&self, source: &str) -> String {
        self.utf8_text(source.as_bytes())
            .expect("Non UTF-8 Characters found")
            .to_owned()
    }

    /// Create a new [TreeCursor] starting from this node.
    pub fn walk(self) -> TreeCursor {
        TreeCursor::new(self)
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_node() {
        let source = "header
            [REF:name]";
        // Parse Source to AST
        let (tree, diagnostic_lexer) = crate::do_lexical_analysis(source);
        println!("Lexer: {:#?}", diagnostic_lexer.get_diagnostic_list());
        assert_eq!(diagnostic_lexer.extract_diagnostic_list(), vec![]);
        let root = tree.root_node();
        assert_eq!("{Node raw_file (0, 0) - (1, 22)}", format!("{:?}", root));
        assert_eq!(0, root.id());
        assert_eq!(0, root.kind_id());
        assert_eq!(std::ops::Range { start: 0, end: 29 }, root.byte_range());
        assert_eq!(
            Range {
                start_byte: 0,
                end_byte: 29,
                start_point: Point { row: 0, column: 0 },
                end_point: Point { row: 1, column: 22 },
            },
            root.range()
        );
        let named_child = root.named_child(0).expect("No Named Child found.");
        assert_eq!("header", named_child.kind());
        assert_eq!(1, named_child.id());

        let next_named_sibling = named_child
            .next_named_sibling()
            .expect("No Named Sibling found.");
        assert_eq!("comment", next_named_sibling.kind());
        assert_eq!(3, next_named_sibling.id());

        let prev_named_sibling = next_named_sibling
            .prev_named_sibling()
            .expect("No Named Sibling found.");
        assert_eq!("header", prev_named_sibling.kind());
        assert_eq!(prev_named_sibling, named_child);

        let prev_sibling = next_named_sibling
            .prev_sibling()
            .expect("No Named Sibling found.");
        assert_eq!("header", prev_sibling.kind());
        assert_eq!(prev_sibling, named_child);
    }
}
