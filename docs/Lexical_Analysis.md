# 1: Lexical Analysis

Note: We will use an example here with permanent link, so the code might be slightly out of date.
But this should still give you the info you need.
Links last updated on: 2022/07/11

Lets take the following code as an example:
```df_raw
descriptor_color_standard

[OBJECT:DESCRIPTOR_COLOR]

[COLOR:AMBER]
    [NAME:amber]
    [WORD:AMBER]
    [RGB:255:191:0]
```

The tokenizer is the first part of the LS that starts.
What it does is convert the list of bytes of the file and creates tokens depending
and the previous and next bytes/tokens.
Most of these rules are fairly simple, and are mostly expressed in Regex.
Regex can look very complicated, but they are well documented.

The program expects a "header" in the first line.
This can be seen [here](https://gitlab.com/df-modding-tools/df-raw-language-server/-/blob/a1945aa920922a03cbd2492586d71ad358e1369d/df_ls_lexical_analysis/src/tokenizer/df_raw.rs#L68):
```rust
let header = tok_help.get_next_match(&regex_list.header, "header", Some("header"), false, true);
```

The `tok_help` is a `TokenizerHelper` it will keep track of the current position in
the file and some other info. The `tok_help` also helps us keep track of the current tree.
The `tree` in the Lexical Analysis refers to an [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree).

The `regex_list.header` refers to the regex we will use to detect the header.
You can find all the full regex list [here](https://gitlab.com/df-modding-tools/df-raw-language-server/-/blob/master/df_ls_lexical_analysis/src/tokenizer/regex_list.rs).
The regex always looks complicated but usually comes down to:
- We expect/allow all the following characters (in this order).
- Or, we allow everything except these characters.

Take the header for example:
```rust
// Accepts a sequence of characters that has at least one character
// except `[`, `]`, `\r` or `\n`.
// Needs to be at least 1 character long.
header: Regex::new(r"[^\[\]\r\n]+").unwrap(),
```

We can see by the comments what characters are accepted and not accepted and in what order.
If these descriptions are not clear enough you can find more detailed
descriptions in the [Syntax_Highlighting.md](./Syntax_Highlighting.md) file.

Lets look a bit closer at how the `get_next_match` function works.

## Even Closer look

The function `get_next_match` is a very important part of the lexer.
So it might be worth taking a better look at it.
We can see the code of [this function here](https://gitlab.com/df-modding-tools/df-raw-language-server/-/blob/a1945aa920922a03cbd2492586d71ad358e1369d/df_ls_lexical_analysis/src/tokenizer/tokenizer_helper.rs#L45-L112).
We will just look at the most important parts here, so we will skip over some of the
checks in place to deal with special cases.

We can slit to file into to parts, everything we already parsed and everything we
still need to look at. We do that here:
```rust
let (_done, to_match) = self.source.split_at(self.index);
```
The `_done` is ignored, we already parsed this. The `to_match` is everything after the the
current cursor position.
We now look for the first and biggest part in the source that matches the regex.
```rust
if let Some(mat) = regex.find(to_match) {
    // ... snip ...
} else {
    TokenMatchStatus::NoMatch
}
```

Note that most of the regex rules we have require at least 1 character. But if the next character
does not match the rule it will just match to that character, nothing more.
In case we do not find anything at all, we return `TokenMatchStatus::NoMatch`.

So we found something we that matches our regex, but it might have skipped over some characters
that did not match. In that case we do some further checking on that to make sure we did
not skip to far forward.
If we found something, we mark that prefix part as an ERROR.
But in most cases this is not the case.

We then move the cursor to the new position, after the part we just tokenized/matched.
And we create a node for the token. This token is then returned.

In most cases the function will return ether `NoMatch` or `Ok`.
There is also some special casing in case the token we are looking for is optional.

Now lets go back out of the `get_next_match` and continue with the result.

We can see the following lines [below it](https://gitlab.com/df-modding-tools/df-raw-language-server/-/blob/a1945aa920922a03cbd2492586d71ad358e1369d/df_ls_lexical_analysis/src/tokenizer/df_raw.rs#L69-L92):
```rust
match header {
    TokenMatchStatus::Ok(result) => {
        tok_help.add_node_to_tree(result, ROOT_ID);
    }
    TokenMatchStatus::OkWithPrefixFound(prefix, result) => {
        handle_prefix(prefix, diagnostics);
        tok_help.add_node_to_tree(result, ROOT_ID);
    }
    _ => {
        diagnostics.add_message(
            DMExtraInfo::new(Range {
                start: Position {
                    line: tok_help.get_point().row as u32,
                    character: tok_help.get_point().column as u32,
                },
                end: Position {
                    line: tok_help.get_point().row as u32,
                    character: tok_help.get_point().column as u32,
                },
            }),
            "missing_header",
        );
    }
}
```

This is how the token is handled. There are 4 options (the last 2 are combined here).
- `TokenMatchStatus::Ok` we found the token in the next bytes.
- `TokenMatchStatus::OkWithPrefixFound` we found the token, but there was something in front of it.
- `TokenMatchStatus::EoF` we found the end of the file, there will be nothing more after this.
- `TokenMatchStatus::NoMatch` we could not find any match.

Depending on the status above we can take the appropriate action.
In some cases the creation of an error is wanted in other cases we don't want that.

You will see this pattern everywhere in the lexer.
After the header we basically expect a list of DF mod tokens and comments.

After we are done parsing the whole file we have a full AST of the file.
In the next step we will use this AST for further processing.

The AST is visualized below:
```
(raw_file
  (header: "descriptor_color_standard")
  (comment)
  (token
    ([)
    (token_name: "OBJECT")
    (token_arguments
      (:)
      (token_argument_reference: "DESCRIPTOR_COLOR")
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: "COLOR")
    (token_arguments
      (:)
      (token_argument_reference: "AMBER")
    )
    (])
  )
  ...snip...
  (comment)
  (EOF: "")
)
```

If there is a problem when creating the AST (aka there is an error in the DF mod file).
In those cases there might be `ERROR` node in the tree and or a diagnostics message added.
`ERROR` nodes will be handled further in the next stage.

During the Lexical Analysis it will try to recover the file as much as possible.
This might result in incorrect file, but these files will throw more errors in other stages.
There is not much we can do to prevent this. But should not result in massive amount of errors.

After this AST is completed the Lexical Analysis is over and we can continue to the Syntax Analysis.
