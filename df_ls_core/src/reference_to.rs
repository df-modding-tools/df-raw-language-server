use crate::Referenceable;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct ReferenceTo<T: Referenceable>(pub String, PhantomData<T>);

impl<T: Referenceable> fmt::Debug for ReferenceTo<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("ReferenceTo")
            .field(&self.0)
            .field(&format_args!("T: {}", T::get_ref_type()))
            .finish()
    }
}

impl<T: Referenceable> PartialEq for ReferenceTo<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}
impl<T: Referenceable> Eq for ReferenceTo<T> {}

impl<T: Referenceable> ReferenceTo<T> {
    pub fn new(reference: String) -> Self {
        ReferenceTo::<T>(reference, PhantomData)
    }

    pub fn get_type() -> &'static str {
        T::get_ref_type()
    }
}

impl<T: Referenceable> Hash for ReferenceTo<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl<T: Referenceable> From<String> for ReferenceTo<T> {
    fn from(item: String) -> ReferenceTo<T> {
        ReferenceTo::<T>(item, PhantomData)
    }
}

impl<T: Referenceable> From<ReferenceTo<T>> for String {
    fn from(item: ReferenceTo<T>) -> String {
        item.0
    }
}
