use super::super::{Argument, Token, TokenArgument, TryFromArgument, TryFromArgumentGroup};
use super::{LoopControl, TokenDeserialize};
use df_ls_core::{DfLsConfig, ReferenceTo, Referenceable};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use df_ls_lexical_analysis::TreeCursor;

/// Deserialize a token with following pattern: `[REF:REF]`
impl<T: Referenceable + Default> TokenDeserialize for ReferenceTo<T> {
    fn deserialize_tokens(
        cursor: &mut TreeCursor,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        config: &DfLsConfig,
    ) -> Result<Box<Self>, ()> {
        // Get arguments from token
        let mut token = match Token::deserialize_tokens(cursor, source, diagnostics, config) {
            Ok(token) => token,
            Err(err) => {
                // When token could not be parsed correctly.
                // Token could not be parsed, so we can consume it.
                // Because this will always fail.
                Token::consume_token(cursor)?;
                return Err(err);
            }
        };
        Token::consume_token(cursor)?;
        // Token Name
        token.check_token_name(source, diagnostics, true)?;
        // Arg 0
        let result = Self::try_from_argument_group(&mut token, source, diagnostics, true);
        if result.is_ok() {
            token.check_all_arg_consumed(source, diagnostics, true)?;
        } else {
            // In case of an error, mark other arguments as unchecked.
            crate::mark_rest_of_token_as_unchecked(diagnostics, &token);
        }
        Ok(Box::new(result?))
    }

    fn deserialize_general_token(
        _cursor: &mut TreeCursor,
        _source: &str,
        _diagnostics: &mut DiagnosticsInfo,
        _config: &DfLsConfig,
        new_self: Box<Self>,
    ) -> (LoopControl, Box<Self>) {
        (LoopControl::DoNothing, new_self)
    }

    fn get_vec_loopcontrol() -> LoopControl {
        LoopControl::DoNothing
    }

    fn get_allowed_tokens(_config: &DfLsConfig) -> Option<Vec<String>> {
        None
    }
}

// ------------------------- Convert a group of arguments to Self -----------------------

impl<T: Referenceable> TryFromArgumentGroup for ReferenceTo<T> {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl<T: Referenceable> TryFromArgument for ReferenceTo<T> {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match &arg.value {
                TokenArgument::TVReference(v) => Ok(ReferenceTo::new(v.clone())),
                TokenArgument::TVString(v) => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo::new(arg.node.get_range()),
                            "reference_is_string",
                        );
                    }
                    Ok(ReferenceTo::new(v.clone()))
                }
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        format!("Reference to {}", T::get_ref_type())
    }
}

// -------------------------Convert from TokenArgument -----------------------

impl<T: Referenceable> From<ReferenceTo<T>> for TokenArgument {
    fn from(item: ReferenceTo<T>) -> TokenArgument {
        TokenArgument::TVReference(item.0)
    }
}

impl<T: Referenceable> From<Option<ReferenceTo<T>>> for TokenArgument {
    fn from(item: Option<ReferenceTo<T>>) -> TokenArgument {
        match item {
            Some(v) => TokenArgument::TVReference(v.0),
            None => TokenArgument::TVEmpty,
        }
    }
}
