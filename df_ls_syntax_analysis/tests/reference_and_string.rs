#![allow(clippy::needless_update)]

use df_ls_core::{Reference, ReferenceTo};
use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_reference() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE2]

            [BIPEDAL:MONSTER]
                [NAME:test]
                [PROFESSIONS:SLEEPING:20]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                professions: vec![(ReferenceTo::new("SLEEPING".to_owned()), 20u8)],
                ..Default::default()
            })],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}

#[test]
fn test_reference_is_string() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE2]

            [BIPEDAL:MONSTER]
                [NAME:test]
                [PROFESSIONS:SLeePING:20]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                professions: vec![(ReferenceTo::new("SLeePING".to_owned()), 20u8)],
                ..Default::default()
            })],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["reference_is_string"])
    .add_test_syntax_diagnostics_ranges(vec![Range {
        start: Position {
            line: 5,
            character: 29,
        },
        end: Position {
            line: 5,
            character: 37,
        },
    }])
    .run_test();
}

#[test]
fn test_string() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE2]

            [BIPEDAL:MONSTER]
                [NAME:test]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                ..Default::default()
            })],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}

#[test]
fn test_string_is_reference() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE1]

            [TYPE1:DOG]
                [ITEM:T1]

            [MAIN:TYPE2]

            [BIPEDAL:MONSTER]
                [NAME:NO_NAME]
                [EDUCATION:inn:beer:5]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_1: vec![Type1Token {
                reference: Some(Reference("DOG".to_owned())),
                list: vec![Reference("T1".to_owned())],
                ..Default::default()
            }],
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("NO_NAME".to_owned()),
                educations: vec![("inn".to_owned(), "beer".to_owned(), 5)],
                ..Default::default()
            })],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["string_is_reference"])
    .add_test_syntax_diagnostics_ranges(vec![Range {
        start: Position {
            line: 9,
            character: 22,
        },
        end: Position {
            line: 9,
            character: 29,
        },
    }])
    .run_test();
}
