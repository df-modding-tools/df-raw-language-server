use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

#[test]
fn test_token_argument_arg_n_permutations() {
    LexerTestBuilder::test_source(
        "Token Argument Arg N
        [A:ARG0]
        [A:ARG1]
        [A:ARG3]
        [A:ARG5]
        [A:ARG10]
        [A:ARG123456]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `Token Argument Arg N`)
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_arg_n: `ARG0`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_arg_n: `ARG1`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_arg_n: `ARG3`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_arg_n: `ARG5`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_arg_n: `ARG10`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `A`)
    (token_arguments
      (:)
      (token_argument_arg_n: `ARG123456`)
    )
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}
