use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum UnitTypeEnum {
    /// https://dwarffortresswiki.org/index.php/Miner
    #[df_token(token_name = "MINER")]
    Miner,
    /// https://dwarffortresswiki.org/index.php/Woodworker
    #[df_token(token_name = "WOODWORKER")]
    Woodworker,
    /// https://dwarffortresswiki.org/index.php/Carpenter
    #[df_token(token_name = "CARPENTER")]
    Carpenter,
    /// https://dwarffortresswiki.org/index.php/Bowyer
    #[df_token(token_name = "BOWYER")]
    Bowyer,
    /// https://dwarffortresswiki.org/index.php/Woodcutter
    #[df_token(token_name = "WOODCUTTER")]
    Woodcutter,
    /// https://dwarffortresswiki.org/index.php/Stoneworker
    #[df_token(token_name = "STONEWORKER")]
    Stoneworker,
    /// https://dwarffortresswiki.org/index.php/Engraver
    #[df_token(token_name = "ENGRAVER")]
    Engraver,
    /// https://dwarffortresswiki.org/index.php/Mason
    #[df_token(token_name = "MASON")]
    Mason,
    /// https://dwarffortresswiki.org/index.php/Ranger
    #[df_token(token_name = "RANGER")]
    Ranger,
    /// https://dwarffortresswiki.org/index.php/Animal_caretaker
    #[df_token(token_name = "ANIMAL_CARETAKER")]
    AnimalCaretaker,
    /// https://dwarffortresswiki.org/index.php/Animal_trainer
    #[df_token(token_name = "ANIMAL_TRAINER")]
    AnimalTrainer,
    /// https://dwarffortresswiki.org/index.php/Hunter
    #[df_token(token_name = "HUNTER")]
    Hunter,
    /// https://dwarffortresswiki.org/index.php/Trapper
    #[df_token(token_name = "TRAPPER")]
    Trapper,
    /// https://dwarffortresswiki.org/index.php/Animal_dissector
    #[df_token(token_name = "ANIMAL_DISSECTOR")]
    AnimalDissector,
    /// https://dwarffortresswiki.org/index.php/Metalsmith
    #[df_token(token_name = "METALSMITH")]
    Metalsmith,
    /// https://dwarffortresswiki.org/index.php/Furnace_operator
    #[df_token(token_name = "FURNACE_OPERATOR")]
    FurnaceOperator,
    /// https://dwarffortresswiki.org/index.php/Weaponsmith
    #[df_token(token_name = "WEAPONSMITH")]
    Weaponsmith,
    /// https://dwarffortresswiki.org/index.php/Armorsmith
    #[df_token(token_name = "ARMORER")]
    Armorer,
    /// https://dwarffortresswiki.org/index.php/Blacksmith
    #[df_token(token_name = "BLACKSMITH")]
    Blacksmith,
    /// https://dwarffortresswiki.org/index.php/Metalcrafter
    #[df_token(token_name = "METALCRAFTER")]
    Metalcrafter,
    /// https://dwarffortresswiki.org/index.php/Jeweler
    #[df_token(token_name = "JEWELER")]
    Jeweler,
    /// https://dwarffortresswiki.org/index.php/Gem_cutter
    #[df_token(token_name = "GEM_CUTTER")]
    GemCutter,
    /// https://dwarffortresswiki.org/index.php/Gem_setter
    #[df_token(token_name = "GEM_SETTER")]
    GemSetter,
    /// https://dwarffortresswiki.org/index.php/Craftsdwarf
    #[df_token(token_name = "CRAFTSMAN")]
    Craftsman,
    /// https://dwarffortresswiki.org/index.php/Woodcrafter
    #[df_token(token_name = "WOODCRAFTER")]
    Woodcrafter,
    /// https://dwarffortresswiki.org/index.php/Stonecrafter
    #[df_token(token_name = "STONECRAFTER")]
    Stonecrafter,
    /// https://dwarffortresswiki.org/index.php/Leatherworker
    #[df_token(token_name = "LEATHERWORKER")]
    Leatherworker,
    /// https://dwarffortresswiki.org/index.php/Bone_carver
    #[df_token(token_name = "BONE_CARVER")]
    BoneCarver,
    /// https://dwarffortresswiki.org/index.php/Weaver
    #[df_token(token_name = "WEAVER")]
    Weaver,
    /// https://dwarffortresswiki.org/index.php/Clothier
    #[df_token(token_name = "CLOTHIER")]
    Clothier,
    /// https://dwarffortresswiki.org/index.php/Glassmaker
    #[df_token(token_name = "GLASSMAKER")]
    Glassmaker,
    /// https://dwarffortresswiki.org/index.php/Potter
    #[df_token(token_name = "POTTER")]
    Potter,
    /// https://dwarffortresswiki.org/index.php/Glazer
    #[df_token(token_name = "GLAZER")]
    Glazer,
    /// https://dwarffortresswiki.org/index.php/Wax_worker
    #[df_token(token_name = "WAX_WORKER")]
    WaxWorker,
    /// https://dwarffortresswiki.org/index.php/Strand_extractor
    #[df_token(token_name = "STRAND_EXTRACTOR")]
    StrandExtractor,
    /// https://dwarffortresswiki.org/index.php/Fishery_worker
    #[df_token(token_name = "FISHERY_WORKER")]
    FisheryWorker,
    /// https://dwarffortresswiki.org/index.php/Fisherdwarf
    #[df_token(token_name = "FISHERMAN")]
    Fisherman,
    /// https://dwarffortresswiki.org/index.php/Fish_dissector
    #[df_token(token_name = "FISH_DISSECTOR")]
    FishDissector,
    /// https://dwarffortresswiki.org/index.php/Fish_cleaner
    #[df_token(token_name = "FISH_CLEANER")]
    FishCleaner,
    /// https://dwarffortresswiki.org/index.php/Farmer
    #[df_token(token_name = "FARMER")]
    Farmer,
    /// https://dwarffortresswiki.org/index.php/Cheese_maker
    #[df_token(token_name = "CHEESE_MAKER")]
    CheeseMaker,
    /// https://dwarffortresswiki.org/index.php/Milker
    #[df_token(token_name = "MILKER")]
    Milker,
    /// https://dwarffortresswiki.org/index.php/Cook
    #[df_token(token_name = "COOK")]
    Cook,
    /// https://dwarffortresswiki.org/index.php/Thresher
    #[df_token(token_name = "THRESHER")]
    Thresher,
    /// https://dwarffortresswiki.org/index.php/Miller
    #[df_token(token_name = "MILLER")]
    Miller,
    /// https://dwarffortresswiki.org/index.php/Butcher
    #[df_token(token_name = "BUTCHER")]
    Butcher,
    /// https://dwarffortresswiki.org/index.php/Tanner
    #[df_token(token_name = "TANNER")]
    Tanner,
    /// https://dwarffortresswiki.org/index.php/Dyer
    #[df_token(token_name = "DYER")]
    Dyer,
    /// https://dwarffortresswiki.org/index.php/Grower
    #[df_token(token_name = "PLANTER")]
    Planter,
    /// https://dwarffortresswiki.org/index.php/Herbalist
    #[df_token(token_name = "HERBALIST")]
    Herbalist,
    /// https://dwarffortresswiki.org/index.php/Brewer
    #[df_token(token_name = "BREWER")]
    Brewer,
    /// https://dwarffortresswiki.org/index.php/Soaper
    #[df_token(token_name = "SOAP_MAKER")]
    SoapMaker,
    /// https://dwarffortresswiki.org/index.php/Potash_maker
    #[df_token(token_name = "POTASH_MAKER")]
    PotashMaker,
    /// https://dwarffortresswiki.org/index.php/Lye_maker
    #[df_token(token_name = "LYE_MAKER")]
    LyeMaker,
    /// https://dwarffortresswiki.org/index.php/Wood_burner
    #[df_token(token_name = "WOOD_BURNER")]
    WoodBurner,
    /// https://dwarffortresswiki.org/index.php/Shearer
    #[df_token(token_name = "SHEARER")]
    Shearer,
    /// https://dwarffortresswiki.org/index.php/Spinner
    #[df_token(token_name = "SPINNER")]
    Spinner,
    /// https://dwarffortresswiki.org/index.php/Presser
    #[df_token(token_name = "PRESSER")]
    Presser,
    /// https://dwarffortresswiki.org/index.php/Beekeeper
    #[df_token(token_name = "BEEKEEPER")]
    Beekeeper,
    /// https://dwarffortresswiki.org/index.php/Engineer
    #[df_token(token_name = "ENGINEER")]
    Engineer,
    /// https://dwarffortresswiki.org/index.php/Mechanic
    #[df_token(token_name = "MECHANIC")]
    Mechanic,
    /// https://dwarffortresswiki.org/index.php/Siege_engineer
    #[df_token(token_name = "SIEGE_ENGINEER")]
    SiegeEngineer,
    /// https://dwarffortresswiki.org/index.php/Siege_operator
    #[df_token(token_name = "SIEGE_OPERATOR")]
    SiegeOperator,
    /// https://dwarffortresswiki.org/index.php/Pump_operator
    #[df_token(token_name = "PUMP_OPERATOR")]
    PumpOperator,
    /// https://dwarffortresswiki.org/index.php/Clerk
    #[df_token(token_name = "CLERK")]
    Clerk,
    /// https://dwarffortresswiki.org/index.php/Administrator
    #[df_token(token_name = "ADMINISTRATOR")]
    Administrator,
    /// https://dwarffortresswiki.org/index.php/Trader
    #[df_token(token_name = "TRADER")]
    Trader,
    /// https://dwarffortresswiki.org/index.php/Architect
    #[df_token(token_name = "ARCHITECT")]
    Architect,
    /// https://dwarffortresswiki.org/index.php/Alchemist
    #[df_token(token_name = "ALCHEMIST")]
    Alchemist,
    /// https://dwarffortresswiki.org/index.php/Doctor
    #[df_token(token_name = "DOCTOR")]
    Doctor,
    /// https://dwarffortresswiki.org/index.php/Diagnostician
    #[df_token(token_name = "DIAGNOSER")]
    Diagnoser,
    /// https://dwarffortresswiki.org/index.php/Bone_doctor
    #[df_token(token_name = "BONE_SETTER")]
    BoneSetter,
    /// https://dwarffortresswiki.org/index.php/Suturer
    #[df_token(token_name = "SUTURER")]
    Suturer,
    /// https://dwarffortresswiki.org/index.php/Surgeon
    #[df_token(token_name = "SURGEON")]
    Surgeon,
    /// https://dwarffortresswiki.org/index.php/Merchant
    #[df_token(token_name = "MERCHANT")]
    Merchant,
    /// https://dwarffortresswiki.org/index.php/Hammerman
    #[df_token(token_name = "HAMMERMAN")]
    Hammerman,
    /// https://dwarffortresswiki.org/index.php/Hammer_lord
    #[df_token(token_name = "MASTER_HAMMERMAN")]
    MasterHammerman,
    /// https://dwarffortresswiki.org/index.php/Spearman
    #[df_token(token_name = "SPEARMAN")]
    Spearman,
    /// https://dwarffortresswiki.org/index.php/Spearmaster
    #[df_token(token_name = "MASTER_SPEARMAN")]
    MasterSpearman,
    /// https://dwarffortresswiki.org/index.php/Crossbowman
    #[df_token(token_name = "CROSSBOWMAN")]
    Crossbowman,
    /// https://dwarffortresswiki.org/index.php/Elite_crossbowman
    #[df_token(token_name = "MASTER_CROSSBOWMAN")]
    MasterCrossbowman,
    /// https://dwarffortresswiki.org/index.php/Wrestler
    #[df_token(token_name = "WRESTLER")]
    Wrestler,
    /// https://dwarffortresswiki.org/index.php/Elite_wrestler
    #[df_token(token_name = "MASTER_WRESTLER")]
    MasterWrestler,
    /// https://dwarffortresswiki.org/index.php/Axeman
    #[df_token(token_name = "AXEMAN")]
    Axeman,
    /// https://dwarffortresswiki.org/index.php/Axe_lord
    #[df_token(token_name = "MASTER_AXEMAN")]
    MasterAxeman,
    /// https://dwarffortresswiki.org/index.php/Swordsman
    #[df_token(token_name = "SWORDSMAN")]
    Swordsman,
    /// https://dwarffortresswiki.org/index.php/Swordmaster
    #[df_token(token_name = "MASTER_SWORDSMAN")]
    MasterSwordsman,
    /// https://dwarffortresswiki.org/index.php/Maceman
    #[df_token(token_name = "MACEMAN")]
    Maceman,
    /// https://dwarffortresswiki.org/index.php/Mace_lord
    #[df_token(token_name = "MASTER_MACEMAN")]
    MasterMaceman,
    /// https://dwarffortresswiki.org/index.php/Pikeman
    #[df_token(token_name = "PIKEMAN")]
    Pikeman,
    /// https://dwarffortresswiki.org/index.php/Pikemaster
    #[df_token(token_name = "MASTER_PIKEMAN")]
    MasterPikeman,
    /// https://dwarffortresswiki.org/index.php/Bowman
    #[df_token(token_name = "BOWMAN")]
    Bowman,
    /// https://dwarffortresswiki.org/index.php/Elite_bowman
    #[df_token(token_name = "MASTER_BOWMAN")]
    MasterBowman,
    /// https://dwarffortresswiki.org/index.php/Blowgunner
    #[df_token(token_name = "BLOWGUNMAN")]
    Blowgunman,
    /// https://dwarffortresswiki.org/index.php/Master_blowgunner
    #[df_token(token_name = "MASTER_BLOWGUNMAN")]
    MasterBlowgunman,
    /// https://dwarffortresswiki.org/index.php/Lasher
    #[df_token(token_name = "LASHER")]
    Lasher,
    /// https://dwarffortresswiki.org/index.php/Master_lasher
    #[df_token(token_name = "MASTER_LASHER")]
    MasterLasher,
    /// https://dwarffortresswiki.org/index.php/Recruit
    #[df_token(token_name = "RECRUIT")]
    Recruit,
    /// https://dwarffortresswiki.org/index.php/Hunting_animal
    #[df_token(token_name = "TRAINED_HUNTER")]
    TrainedHunter,
    /// https://dwarffortresswiki.org/index.php/War_animal
    #[df_token(token_name = "TRAINED_WAR")]
    TrainedWar,
    /// https://dwarffortresswiki.org/index.php/Master_thief
    #[df_token(token_name = "MASTER_THIEF")]
    MasterThief,
    /// https://dwarffortresswiki.org/index.php/Thief
    #[df_token(token_name = "THIEF")]
    Thief,
    /// https://dwarffortresswiki.org/index.php/Peasant
    #[df_token(token_name = "STANDARD")]
    Standard,
    /// https://dwarffortresswiki.org/index.php/Child
    #[df_token(token_name = "CHILD")]
    Child,
    /// https://dwarffortresswiki.org/index.php/Baby
    #[df_token(token_name = "BABY")]
    Baby,
    /// https://dwarffortresswiki.org/index.php/Drunk
    #[df_token(token_name = "DRUNK")]
    Drunk,
    /// https://dwarffortresswiki.org/index.php/Monster_slayer
    #[df_token(token_name = "MONSTER_SLAYER")]
    MonsterSlayer,
    /// https://dwarffortresswiki.org/index.php/Scout
    #[df_token(token_name = "SCOUT")]
    Scout,
    /// https://dwarffortresswiki.org/index.php/Beast_hunter
    #[df_token(token_name = "BEAST_HUNTER")]
    BeastHunter,
    /// https://dwarffortresswiki.org/index.php/Snatcher
    #[df_token(token_name = "SNATCHER")]
    Snatcher,
    /// https://dwarffortresswiki.org/index.php/Mercenary
    #[df_token(token_name = "MERCENARY")]
    Mercenary,
    /// https://dwarffortresswiki.org/index.php/Gelder
    #[df_token(token_name = "GELDER")]
    Gelder,
    /// https://dwarffortresswiki.org/index.php/Performer
    #[df_token(token_name = "PERFORMER")]
    Performer,
    /// https://dwarffortresswiki.org/index.php/Poet
    #[df_token(token_name = "POET")]
    Poet,
    /// https://dwarffortresswiki.org/index.php/Bard
    #[df_token(token_name = "BARD")]
    Bard,
    /// https://dwarffortresswiki.org/index.php/Dancer
    #[df_token(token_name = "DANCER")]
    Dancer,
    /// https://dwarffortresswiki.org/index.php/Sage
    #[df_token(token_name = "SAGE")]
    Sage,
    /// https://dwarffortresswiki.org/index.php/Scholar
    #[df_token(token_name = "SCHOLAR")]
    Scholar,
    /// https://dwarffortresswiki.org/index.php/Philosopher
    #[df_token(token_name = "PHILOSOPHER")]
    Philosopher,
    /// https://dwarffortresswiki.org/index.php/Mathematician
    #[df_token(token_name = "MATHEMATICIAN")]
    Mathematician,
    /// https://dwarffortresswiki.org/index.php/Historian
    #[df_token(token_name = "HISTORIAN")]
    Historian,
    /// https://dwarffortresswiki.org/index.php/Astronomer
    #[df_token(token_name = "ASTRONOMER")]
    Astronomer,
    /// https://dwarffortresswiki.org/index.php/Naturalist
    #[df_token(token_name = "NATURALIST")]
    Naturalist,
    /// https://dwarffortresswiki.org/index.php/Chemist
    #[df_token(token_name = "CHEMIST")]
    Chemist,
    /// https://dwarffortresswiki.org/index.php/Geographer
    #[df_token(token_name = "GEOGRAPHER")]
    Geographer,
    /// https://dwarffortresswiki.org/index.php/Scribe
    #[df_token(token_name = "SCRIBE")]
    Scribe,
    /// https://dwarffortresswiki.org/index.php/Papermaker
    #[df_token(token_name = "PAPERMAKER")]
    Papermaker,
    /// https://dwarffortresswiki.org/index.php/Bookbinder
    #[df_token(token_name = "BOOKBINDER")]
    Bookbinder,
    /// https://dwarffortresswiki.org/index.php/Tavern_keeper
    #[df_token(token_name = "TAVERN_KEEPER")]
    TavernKeeper,
    /// https://dwarffortresswiki.org/index.php/Criminal
    #[df_token(token_name = "CRIMINAL")]
    Criminal,
    /// https://dwarffortresswiki.org/index.php/Peddler
    #[df_token(token_name = "PEDDLER")]
    Peddler,
    /// https://dwarffortresswiki.org/index.php/Prophet
    #[df_token(token_name = "PROPHET")]
    Prophet,
    /// https://dwarffortresswiki.org/index.php/Pilgrim
    #[df_token(token_name = "PILGRIM")]
    Pilgrim,
    /// https://dwarffortresswiki.org/index.php/Monk
    #[df_token(token_name = "MONK")]
    Monk,
    /// https://dwarffortresswiki.org/index.php/Messenger
    #[df_token(token_name = "MESSENGER")]
    Messenger,
}

impl Default for UnitTypeEnum {
    fn default() -> Self {
        Self::Miner
    }
}
