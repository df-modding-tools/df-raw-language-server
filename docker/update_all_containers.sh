#!/bin/sh -e

# Clean all old builds
#docker system prune -a

# General

# NOTE: Containers are moved over to GitLab registry, not docker hub
# docker login

# Code check

docker build --no-cache -t dfmoddingtools/code_check:x86_64-unknown-linux-gnu \
    -f ./docker/Dockerfile.code_check .
docker push dfmoddingtools/code_check:x86_64-unknown-linux-gnu

# Linux

docker build --no-cache -t dfmoddingtools/builder:x86_64-unknown-linux-musl \
    -f ./docker/Dockerfile.x86_64-unknown-linux-musl .
docker push dfmoddingtools/builder:x86_64-unknown-linux-musl

# Windows

#docker build --no-cache -t dfmoddingtools/builder:x86_64-pc-windows-gnu ./docker/x86_64-pc-windows-gnu/
#docker push dfmoddingtools/builder:x86_64-pc-windows-gnu
