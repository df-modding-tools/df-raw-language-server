use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum WeaponSkillEnum {
    #[df_token(token_name = "AXE")]
    Axe,
    #[df_token(token_name = "SWORD")]
    Sword,
    #[df_token(token_name = "DAGGER")]
    Dagger,
    #[df_token(token_name = "MACE")]
    Mace,
    #[df_token(token_name = "HAMMER")]
    Hammer,
    #[df_token(token_name = "SPEAR")]
    Spear,
    #[df_token(token_name = "CROSSBOW")]
    Crossbow,
    #[df_token(token_name = "SHIELD")]
    Shield,
    #[df_token(token_name = "PIKE")]
    Pike,
    #[df_token(token_name = "WHIP")]
    Whip,
    #[df_token(token_name = "BOW")]
    Bow,
    #[df_token(token_name = "BLOWGUN")]
    Blowgun,
}
impl Default for WeaponSkillEnum {
    fn default() -> Self {
        Self::Axe
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum SkillEnum {
    #[df_token(token_name = "MINING")]
    Mining,
    #[df_token(token_name = "WOODCUTTING")]
    Woodcutting,
    #[df_token(token_name = "CARPENTRY")]
    Carpentry,
    #[df_token(token_name = "DETAILSTONE")]
    Detailstone,
    #[df_token(token_name = "MASONRY")]
    Masonry,
    #[df_token(token_name = "ANIMALTRAIN")]
    Animaltrain,
    #[df_token(token_name = "ANIMALCARE")]
    Animalcare,
    #[df_token(token_name = "DISSECT_FISH")]
    DissectFish,
    #[df_token(token_name = "DISSECT_VERMIN")]
    DissectVermin,
    #[df_token(token_name = "PROCESSFISH")]
    Processfish,
    #[df_token(token_name = "BUTCHER")]
    Butcher,
    #[df_token(token_name = "TRAPPING")]
    Trapping,
    #[df_token(token_name = "TANNER")]
    Tanner,
    #[df_token(token_name = "WEAVING")]
    Weaving,
    #[df_token(token_name = "BREWING")]
    Brewing,
    #[df_token(token_name = "ALCHEMY")]
    Alchemy,
    #[df_token(token_name = "CLOTHESMAKING")]
    Clothesmaking,
    #[df_token(token_name = "MILLING")]
    Milling,
    #[df_token(token_name = "PROCESSPLANTS")]
    Processplants,
    #[df_token(token_name = "CHEESEMAKING")]
    Cheesemaking,
    #[df_token(token_name = "MILK")]
    Milk,
    #[df_token(token_name = "COOK")]
    Cook,
    #[df_token(token_name = "PLANT")]
    Plant,
    #[df_token(token_name = "HERBALISM")]
    Herbalism,
    #[df_token(token_name = "FISH")]
    Fish,
    #[df_token(token_name = "SMELT")]
    Smelt,
    #[df_token(token_name = "EXTRACT_STRAND")]
    ExtractStrand,
    #[df_token(token_name = "FORGE_WEAPON")]
    ForgeWeapon,
    #[df_token(token_name = "FORGE_ARMOR")]
    ForgeArmor,
    #[df_token(token_name = "FORGE_FURNITURE")]
    ForgeFurniture,
    #[df_token(token_name = "CUTGEM")]
    Cutgem,
    #[df_token(token_name = "ENCRUSTGEM")]
    Encrustgem,
    #[df_token(token_name = "WOODCRAFT")]
    Woodcraft,
    #[df_token(token_name = "STONECRAFT")]
    Stonecraft,
    #[df_token(token_name = "METALCRAFT")]
    Metalcraft,
    #[df_token(token_name = "GLASSMAKER")]
    Glassmaker,
    #[df_token(token_name = "LEATHERWORK")]
    Leatherwork,
    #[df_token(token_name = "BONECARVE")]
    Bonecarve,
    #[df_token(token_name = "AXE")]
    Axe,
    #[df_token(token_name = "SWORD")]
    Sword,
    #[df_token(token_name = "DAGGER")]
    Dagger,
    #[df_token(token_name = "MACE")]
    Mace,
    #[df_token(token_name = "HAMMER")]
    Hammer,
    #[df_token(token_name = "SPEAR")]
    Spear,
    #[df_token(token_name = "CROSSBOW")]
    Crossbow,
    #[df_token(token_name = "SHIELD")]
    Shield,
    #[df_token(token_name = "ARMOR")]
    Armor,
    #[df_token(token_name = "SIEGECRAFT")]
    Siegecraft,
    #[df_token(token_name = "SIEGEOPERATE")]
    Siegeoperate,
    #[df_token(token_name = "BOWYER")]
    Bowyer,
    #[df_token(token_name = "PIKE")]
    Pike,
    #[df_token(token_name = "WHIP")]
    Whip,
    #[df_token(token_name = "BOW")]
    Bow,
    #[df_token(token_name = "BLOWGUN")]
    Blowgun,
    #[df_token(token_name = "THROW")]
    Throw,
    #[df_token(token_name = "MECHANICS")]
    Mechanics,
    #[df_token(token_name = "MAGIC_NATURE")]
    MagicNature,
    #[df_token(token_name = "SNEAK")]
    Sneak,
    #[df_token(token_name = "DESIGNBUILDING")]
    Designbuilding,
    #[df_token(token_name = "DRESS_WOUNDS")]
    DressWounds,
    #[df_token(token_name = "DIAGNOSE")]
    Diagnose,
    #[df_token(token_name = "SURGERY")]
    Surgery,
    #[df_token(token_name = "SET_BONE")]
    SetBone,
    #[df_token(token_name = "SUTURE")]
    Suture,
    #[df_token(token_name = "CRUTCH_WALK")]
    CrutchWalk,
    #[df_token(token_name = "WOOD_BURNING")]
    WoodBurning,
    #[df_token(token_name = "LYE_MAKING")]
    LyeMaking,
    #[df_token(token_name = "SOAP_MAKING")]
    SoapMaking,
    #[df_token(token_name = "POTASH_MAKING")]
    PotashMaking,
    #[df_token(token_name = "DYER")]
    Dyer,
    #[df_token(token_name = "OPERATE_PUMP")]
    OperatePump,
    #[df_token(token_name = "SWIMMING")]
    Swimming,
    #[df_token(token_name = "PERSUASION")]
    Persuasion,
    #[df_token(token_name = "NEGOTIATION")]
    Negotiation,
    #[df_token(token_name = "JUDGING_INTENT")]
    JudgingIntent,
    #[df_token(token_name = "APPRAISAL")]
    Appraisal,
    #[df_token(token_name = "ORGANIZATION")]
    Organization,
    #[df_token(token_name = "RECORD_KEEPING")]
    RecordKeeping,
    #[df_token(token_name = "LYING")]
    Lying,
    #[df_token(token_name = "INTIMIDATION")]
    Intimidation,
    #[df_token(token_name = "CONVERSATION")]
    Conversation,
    #[df_token(token_name = "COMEDY")]
    Comedy,
    #[df_token(token_name = "FLATTERY")]
    Flattery,
    #[df_token(token_name = "CONSOLE")]
    Console,
    #[df_token(token_name = "PACIFY")]
    Pacify,
    #[df_token(token_name = "TRACKING")]
    Tracking,
    #[df_token(token_name = "KNOWLEDGE_ACQUISITION")]
    KnowledgeAcquisition,
    #[df_token(token_name = "CONCENTRATION")]
    Concentration,
    #[df_token(token_name = "DISCIPLINE")]
    Discipline,
    #[df_token(token_name = "SITUATIONAL_AWARENESS")]
    SituationalAwareness,
    #[df_token(token_name = "WRITING")]
    Writing,
    #[df_token(token_name = "PROSE")]
    Prose,
    #[df_token(token_name = "POETRY")]
    Poetry,
    #[df_token(token_name = "READING")]
    Reading,
    #[df_token(token_name = "SPEAKING")]
    Speaking,
    #[df_token(token_name = "COORDINATION")]
    Coordination,
    #[df_token(token_name = "BALANCE")]
    Balance,
    #[df_token(token_name = "LEADERSHIP")]
    Leadership,
    #[df_token(token_name = "TEACHING")]
    Teaching,
    #[df_token(token_name = "MELEE_COMBAT")]
    MeleeCombat,
    #[df_token(token_name = "RANGED_COMBAT")]
    RangedCombat,
    #[df_token(token_name = "WRESTLING")]
    Wrestling,
    #[df_token(token_name = "BITE")]
    Bite,
    #[df_token(token_name = "GRASP_STRIKE")]
    GraspStrike,
    #[df_token(token_name = "STANCE_STRIKE")]
    StanceStrike,
    #[df_token(token_name = "DODGING")]
    Dodging,
    #[df_token(token_name = "MISC_WEAPON")]
    MiscWeapon,
    #[df_token(token_name = "KNAPPING")]
    Knapping,
    #[df_token(token_name = "MILITARY_TACTICS")]
    MilitaryTactics,
    #[df_token(token_name = "SHEARING")]
    Shearing,
    #[df_token(token_name = "SPINNING")]
    Spinning,
    #[df_token(token_name = "POTTERY")]
    Pottery,
    #[df_token(token_name = "GLAZING")]
    Glazing,
    #[df_token(token_name = "PRESSING")]
    Pressing,
    #[df_token(token_name = "BEEKEEPING")]
    Beekeeping,
    #[df_token(token_name = "WAX_WORKING")]
    WaxWorking,
    #[df_token(token_name = "CLIMBING")]
    Climbing,
    #[df_token(token_name = "GELD")]
    Geld,
    #[df_token(token_name = "DANCE")]
    Dance,
    #[df_token(token_name = "MAKE_MUSIC")]
    MakeMusic,
    #[df_token(token_name = "SING")]
    Sing,
    #[df_token(token_name = "PLAY_KEYBOARD_INSTRUMENT")]
    PlayKeyboardInstrument,
    #[df_token(token_name = "PLAY_STRINGED_INSTRUMENT")]
    PlayStringedInstrument,
    #[df_token(token_name = "PLAY_WIND_INSTRUMENT")]
    PlayWindInstrument,
    #[df_token(token_name = "PLAY_PERCUSSION_INSTRUMENT")]
    PlayPercussionInstrument,
    #[df_token(token_name = "CRITICAL_THINKING")]
    CriticalThinking,
    #[df_token(token_name = "LOGIC")]
    Logic,
    #[df_token(token_name = "MATHEMATICS")]
    Mathematics,
    #[df_token(token_name = "ASTRONOMY")]
    Astronomy,
    #[df_token(token_name = "CHEMISTRY")]
    Chemistry,
    #[df_token(token_name = "GEOGRAPHY")]
    Geography,
    #[df_token(token_name = "OPTICS_ENGINEER")]
    OpticsEngineer,
    #[df_token(token_name = "FLUID_ENGINEER")]
    FluidEngineer,
    #[df_token(token_name = "PAPERMAKING")]
    Papermaking,
    #[df_token(token_name = "BOOKBINDING")]
    Bookbinding,
    #[df_token(token_name = "INTRIGUE")]
    Intrigue,
    #[df_token(token_name = "RIDING")]
    Riding,
}
impl Default for SkillEnum {
    fn default() -> Self {
        Self::Mining
    }
}
