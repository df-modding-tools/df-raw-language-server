use crate::common;
use std::collections::HashMap;

#[derive(Clone, Debug, Default)]
pub struct TokenRefInfo {
    pub self_reference: bool,
}

pub fn get_token_ref_info(field: &syn::Field) -> TokenRefInfo {
    let mut token_info = TokenRefInfo::default();
    // Loop over all the line of attributes (`#[...]`).
    for (_i, attr) in field.attrs.iter().enumerate() {
        let token_attrs = get_token_attrs_variables(attr);
        for (key, val) in token_attrs {
            let mut string_quote = "".to_string();
            if let Some(val) = val {
                if let syn::Lit::Str(string_lit) = val {
                    string_quote = string_lit.value();
                }
                if string_quote.is_empty() {
                    continue;
                }
            }
            #[allow(clippy::single_match)]
            match key.as_ref() {
                "self_reference" => token_info.self_reference = true,
                _ => {}
            }
        }
    }
    token_info
}

fn get_token_attrs_variables(attr: &syn::Attribute) -> HashMap<String, Option<syn::Lit>> {
    let meta_items = common::get_meta_items(attr, "referenceable").unwrap();
    let mut lit_list: HashMap<String, Option<syn::Lit>> = HashMap::new();
    // If an different argument was found, skip this.
    if meta_items.is_none() {
        // return empty list
        return lit_list;
    }
    for meta_item in meta_items.unwrap() {
        match meta_item {
            // Parse `#[referenceable(self_reference)]`
            syn::NestedMeta::Meta(syn::Meta::Path(m)) if m.is_ident("self_reference") => {
                lit_list.insert("self_reference".to_owned(), None);
            }
            _ => (),
        };
    }
    lit_list
}
