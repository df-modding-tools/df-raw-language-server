use df_ls_core::{AllowEmpty, ReferenceTo};
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_structure::*;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn simple_test() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "language_words

            [OBJECT:LANGUAGE]

            [WORD:TARNISH]
                [NOUN:tarnish:]
                    [FRONT_COMPOUND_NOUN_SING]
                    [REAR_COMPOUND_NOUN_SING]
                    [THE_NOUN_SING]
                    [OF_NOUN_SING]
                [VERB:tarnish:tarnishes:tarnished:tarnished:tarnishing]
                    [STANDARD_VERB]

            [SYMBOL:FLOWERY]
                [S_WORD:BERRY]
                [S_WORD:BLOSSOM]
                [S_WORD:BRIDE]
                [S_WORD:BUTTERFLY]

            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DFRaw {
        header: "language_words".to_owned(),
        token_structure: vec![ObjectToken {
            language_tokens: vec![
                LanguageToken::WordToken(WordToken {
                    reference: Some(ReferenceTo::new("TARNISH".to_owned())),
                    nouns: vec![NounToken {
                        words: Some(("tarnish".to_owned(), AllowEmpty::None)),
                        front_compound_noun_sing: Some(()),
                        read_compound_noun_sing: Some(()),
                        the_compound_noun_sing: None,
                        the_noun_sing: Some(()),
                        of_noun_sing: Some(()),
                        front_compound_noun_plur: None,
                        read_compound_noun_plur: None,
                        the_compound_noun_plur: None,
                        the_noun_plur: None,
                        of_noun_plur: None,
                    }],
                    adj: vec![],
                    verb: vec![VerbToken {
                        words: Some((
                            "tarnish".to_owned(),
                            "tarnishes".to_owned(),
                            "tarnished".to_owned(),
                            "tarnished".to_owned(),
                            "tarnishing".to_owned(),
                        )),
                        standard_verb: Some(()),
                        front_compound_adj: None,
                        the_compound_adj: None,
                        rear_compound_adj: None,
                    }],
                    prefix: vec![],
                }),
                LanguageToken::SymbolToken(SymbolToken {
                    reference: Some(ReferenceTo::new("FLOWERY".to_owned())),
                    s_word: vec![
                        ReferenceTo::new("BERRY".to_owned()),
                        ReferenceTo::new("BLOSSOM".to_owned()),
                        ReferenceTo::new("BRIDE".to_owned()),
                        ReferenceTo::new("BUTTERFLY".to_owned()),
                    ],
                }),
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
