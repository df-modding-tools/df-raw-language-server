use df_ls_core::{Choose, Reference, ReferenceTo};
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_structure::*;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_tissue() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "tissue_template_default

            [OBJECT:TISSUE_TEMPLATE]

            [TISSUE_TEMPLATE:FAT_TEMPLATE]
                [TISSUE_NAME:fat:NP]
                [THICKENS_ON_ENERGY_STORAGE]
                [SCARS]
                [TISSUE_MATERIAL:LOCAL_CREATURE_MAT:FAT]
                [RELATIVE_THICKNESS:1]
                [HEALING_RATE:100]
                [VASCULAR:3]
                [PAIN_RECEPTORS:5]
                [CONNECTS]
                [INSULATION:100]
                [TISSUE_SHAPE:LAYER]

            [TISSUE_TEMPLATE:MUSCLE_TEMPLATE]
                [TISSUE_NAME:muscle:muscles]
                [THICKENS_ON_STRENGTH]
                [ARTERIES]
                [SCARS]
                [MUSCULAR]
                [TISSUE_MATERIAL:LOCAL_CREATURE_MAT:MUSCLE]
                [RELATIVE_THICKNESS:3]
                [HEALING_RATE:100]
                [VASCULAR:5]
                [PAIN_RECEPTORS:5]
                [CONNECTS]
                [TISSUE_SHAPE:LAYER]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DFRaw {
        header: "tissue_template_default".to_owned(),
        token_structure: vec![ObjectToken {
            tissue_template_tokens: vec![
                TissueToken {
                    reference: Some(ReferenceTo::new("FAT_TEMPLATE".to_owned())),
                    name: Some(("fat".to_owned(), Choose::Choice1(PluralEnum::Np))),
                    thickness_on_energy_storage: Some(()),
                    scars: Some(()),
                    material: Some(Choose::Choice2(LocalCreatureMatArg(Reference(
                        "FAT".to_owned(),
                    )))),
                    relative_thickness: Some(1),
                    healing_rate: Some(100),
                    vascular: Some(3),
                    pain_receptors: Some(5),
                    connects: Some(()),
                    insulation: Some(100),
                    tissue_shape: Some(TissueShapeEnum::Layer),
                    ..Default::default()
                },
                TissueToken {
                    reference: Some(ReferenceTo::new("MUSCLE_TEMPLATE".to_owned())),
                    name: Some(("muscle".to_owned(), Choose::Choice2("muscles".to_owned()))),
                    thickness_on_strength: Some(()),
                    arteries: Some(()),
                    scars: Some(()),
                    muscular: Some(()),
                    material: Some(Choose::Choice2(LocalCreatureMatArg(Reference(
                        "MUSCLE".to_owned(),
                    )))),
                    relative_thickness: Some(3),
                    healing_rate: Some(100),
                    vascular: Some(5),
                    pain_receptors: Some(5),
                    connects: Some(()),
                    tissue_shape: Some(TissueShapeEnum::Layer),
                    ..Default::default()
                },
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
