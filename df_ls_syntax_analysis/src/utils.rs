use crate::Token;
use df_ls_diagnostics::lsp_types::*;
use df_ls_diagnostics::{DMExtraInfo, DiagnosticsInfo};
use df_ls_lexical_analysis::{Node, TreeCursor};

pub fn mark_rest_of_file_as_unchecked(
    cursor: &mut TreeCursor,
    diagnostics: &mut DiagnosticsInfo,
    node: &Node,
) {
    // Get root node of AST
    let root_node = {
        while cursor.goto_parent() {}
        cursor.node()
    };
    let start_pos = Position {
        line: node.end_position().row as u32,
        character: node.end_position().column as u32,
    };
    let end_pos = Position {
        line: root_node.end_position().row as u32,
        character: root_node.end_position().column as u32,
    };
    // Check if there are characters left to not check
    if start_pos == end_pos {
        // EOF is here, so do not add message
        return;
    }
    diagnostics.add_message(
        DMExtraInfo::new(Range {
            start: start_pos,
            end: end_pos,
        }),
        "unchecked_code",
    );
    diagnostics.lock_message_list();
}

/// Everything after the previous argument will be marked as unchecked.
pub fn mark_rest_of_token_as_unchecked(diagnostics: &mut DiagnosticsInfo, token: &Token) {
    // If no arguments where found or there is no next argument
    // we can stop early because there is nothing to mark as unchecked
    let prev_arg = match token.get_previous_arg_opt() {
        Some(arg) => arg,
        None => return,
    };
    let last_arg = match token.get_last_arg_opt() {
        Some(arg) => arg,
        None => return,
    };

    let start_pos = Position {
        line: prev_arg.node.end_position().row as u32,
        character: prev_arg.node.end_position().column as u32,
    };
    let end_pos = Position {
        line: last_arg.node.end_position().row as u32,
        character: last_arg.node.end_position().column as u32,
    };
    // Check if there are characters left to not check
    if start_pos == end_pos {
        // End of token is here, so do not add message
        return;
    }

    diagnostics.add_message(
        DMExtraInfo::new(Range {
            start: start_pos,
            end: end_pos,
        }),
        "unchecked_code",
    );
    // Do not lock diagnostics, other tokens can still be checked.
}
