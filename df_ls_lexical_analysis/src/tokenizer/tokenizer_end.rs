use std::fmt::Display;

/// Something happened that prevents us from parsing the file further.
/// This does not mean the file was invalid.
/// This is used as the error case to stop the parsing of the file.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum TokenizerEnd {
    /// End of File was found, but it was NOT expected.
    /// The file can not be parsed further.
    UnexpectedEoF,
    /// End of File was found,
    /// although this does not make the file invalid.
    /// The file can not be parsed further.
    ExpectedEof,
}

impl Display for TokenizerEnd {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::UnexpectedEoF => "Unexpected EoF",
                Self::ExpectedEof => "Expected Eof",
            },
        )
    }
}
