"use strict";

import * as net from "net";
import * as path from "path";
import * as os from "os";
import { ChildProcess, execFile } from "child_process";
import { ServerOptions, StreamInfo } from "vscode-languageclient/node";
import { logger } from "./extension";

export function getServerOptions(): ServerOptions {
    // Describe how to start the server, it will not be started here, but later
    // The `Promise` is is the prospect of what will be have started.
    // So the code inside to promise can be executed more then once after
    // the extension was activated.
    const serverOptions: ServerOptions = () => {
        return new Promise((resolve, reject) => {
            logger.info("Starting Language server");
            // Start Language Server and get the port from it.
            if (!isStartedInDebugMode()) {
                start_connect_server_process(resolve);
            } else {
                connect_to_running_server(resolve);
            }
        });
    };
    return serverOptions;
}

// Start and Connect to the Language Server.
// This will start the language server from the given executable.
// This does NOT (re)compile the server.
function start_connect_server_process(resolve: (value: StreamInfo) => void) {
    // Start Language server as child process
    let server: ChildProcess | undefined = execFile(
        get_executable_path(), ['server', 'tcp']
    );
    if (server === undefined) {
        logger.error("DF Language Server is undefined.");
        return;
    }
    if (server.stdout === null || server.stderr === null) {
        logger.error("Could not start DF Language Server.");
        return;
    }
    let connected_to_server = false;
    // Stream stdout data
    server.stdout.on('data', function (data) {
        logger.log_server(data);
        // Check if already connected to server to prevent checking additional output
        if (!connected_to_server) {
            // Look for matching string and get port from string
            const regex = /<\[SERVER available on port: `([0-9]+)`\]>/;
            const match_found = data.match(regex);
            if (match_found !== null && match_found.length == 2) {
                // Parse port from string to int
                const port = parseInt(match_found[1], 10);
                logger.info('Port found: ' + port);
                // Start client part of the extension to connect to the server
                const clientSocket = new net.Socket();
                clientSocket.connect(port, "127.0.0.1", () => {
                    resolve({
                        reader: clientSocket,
                        writer: clientSocket,
                        detached: true,
                    });
                });
                handle_server_errors(clientSocket);
                connected_to_server = true;
            }
        }
    });
    // Stream stderr data to log
    server.stderr.on('data', function (data) {
        logger.log_server(data);
    });

    handle_server_errors(server);
}

// Connect to an already running server.
// This only starts the client and connect it to an existing server.
function connect_to_running_server(resolve: (value: StreamInfo) => void) {
    // Use fixed port (`2087`) for debugging and do not start server in here
    // This functions expects you to be running
    // the server already separate on your system.
    logger.info("Connection to running DF Language Server.");
    const clientSocket = new net.Socket();
    let server = clientSocket.connect(2087, "127.0.0.1", () => {
        resolve({
            reader: clientSocket,
            writer: clientSocket,
        });
    });

    logger.log_server("DF Language Server started separately, can not capture server output.\n");

    handle_server_errors(server);

}

// Handling events from:
// - https://nodejs.org/api/net.html#class-netsocket
// - https://nodejs.org/api/child_process.html#class-childprocess
function handle_server_errors(server: net.Socket | ChildProcess) {
    // Connected
    server.on('connect', function () {
        logger.error("DF Language Server and Client connected.");
    });

    // Log errors
    server.on('error', function (err: Error) {
        logger.error("DF Language Server started with an error.");
        logger.error(err);
    });

    // Log how the server was terminated
    server.on('exit', function (code: number, signal: string) {
        if (code == 0 || signal == "SIGINT" || signal == "SIGTERM") {
            logger.info("DF Language Server shutdown correctly.");
        } else {
            logger.error("DF Language Server shutdown incorrectly.");
            if (code != null) {
                logger.error("Exit code: ", code);
            }
            if (signal != null) {
                logger.error("Exit signal: ", signal);
            }
        }
    });

    // Log timeout
    server.on('timeout', function () {
        logger.error("DF Language Server and Client connection timed-out.");
    });

    // Log end
    server.on('close', function (hasError: boolean) {
        if (hasError) {
            logger.error("DF Language Server and Client connection was closed because of an error.");
        } else {
            logger.error("DF Language Server and Client connection was closed.");
        }
    });
}

function isStartedInDebugMode(): boolean {
    return process.env.VSCODE_DEBUG_MODE === "true";
}

function get_executable_path(): string {
    let executable_file: string = '';
    switch (os.platform()) {
        case 'linux':
            executable_file = 'df_language_server-x86_64-unknown-linux-musl';
            break;
        case 'win32':
            executable_file = 'df_language_server-x86_64-pc-windows-gnu.exe';
            break;
        // case 'darwin':
        //     executable_file = 'df_language_server-x86_64-apple-darwin';
        //     break;
        default:
            logger.info("This platform is not yet supported by us.");
            process.exit(1);
            break;
    }
    let executable_path: string = path.join(__dirname, '../', 'out', executable_file);
    return executable_path;
}
