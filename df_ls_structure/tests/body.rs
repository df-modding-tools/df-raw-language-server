use df_ls_core::{Choose, Reference, ReferenceTo};
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_structure::*;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_body_and_bodygloss() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "body

            [OBJECT:BODY]

            [BODY:THROAT]
                [BP:THROAT:throat:STP]
                    [CON_CAT:NECK]

            [BODY:TEETH]
                [BP:U_F_TOOTH:upper front tooth:upper front teeth]
                    [CONTYPE:HEAD]
                    [CATEGORY:TOOTH]
                    [NUMBER:6]
                    [INDIVIDUAL_NAME:first upper right incisor:STP]

            [BODY:BASIC_2PARTBODY]
                [BP:UB:upper body:upper bodies]
                    [UPPERBODY]
                    [CATEGORY:BODY_UPPER]
                    [DEFAULT_RELSIZE:1000]
                [BP:LB:lower body:lower bodies]
                    [CON:UB][LOWERBODY]
                    [CATEGORY:BODY_LOWER]

            [BODYGLOSS:PAW:foot:paw:feet:paws]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DFRaw {
        header: "body".to_owned(),
        token_structure: vec![ObjectToken {
            body_tokens: vec![
                BodyObjectToken::BodyToken(BodyToken {
                    reference: Some(ReferenceTo::new("THROAT".to_owned())),
                    bp: vec![BodyPartToken {
                        bp: Some((
                            Reference("THROAT".to_owned()),
                            "throat".to_owned(),
                            Choose::Choice1(StandardPluralEnum::Stp),
                        )),
                        con_cat: Some(Reference("NECK".to_owned())),
                        ..Default::default()
                    }],
                }),
                BodyObjectToken::BodyToken(BodyToken {
                    reference: Some(ReferenceTo::new("TEETH".to_owned())),
                    bp: vec![BodyPartToken {
                        bp: Some((
                            Reference("U_F_TOOTH".to_owned()),
                            "upper front tooth".to_owned(),
                            Choose::Choice2("upper front teeth".to_owned()),
                        )),
                        category: Some(Reference("TOOTH".to_owned())),
                        contype: Some(ConTypeEnum::Head),
                        individual_name: vec![(
                            "first upper right incisor".to_owned(),
                            Choose::Choice1(StandardPluralEnum::Stp),
                        )],
                        number: Some(6),
                        ..Default::default()
                    }],
                }),
                BodyObjectToken::BodyToken(BodyToken {
                    reference: Some(ReferenceTo::new("BASIC_2PARTBODY".to_owned())),
                    bp: vec![
                        BodyPartToken {
                            bp: Some((
                                Reference("UB".to_owned()),
                                "upper body".to_owned(),
                                Choose::Choice2("upper bodies".to_owned()),
                            )),
                            category: Some(Reference("BODY_UPPER".to_owned())),
                            default_relsize: Some(1000),
                            upperbody: Some(()),
                            ..Default::default()
                        },
                        BodyPartToken {
                            bp: Some((
                                Reference("LB".to_owned()),
                                "lower body".to_owned(),
                                Choose::Choice2("lower bodies".to_owned()),
                            )),
                            category: Some(Reference("BODY_LOWER".to_owned())),
                            con: Some(Reference("UB".to_owned())),
                            lowerbody: Some(()),
                            ..Default::default()
                        },
                    ],
                }),
                BodyObjectToken::BodyGlossToken(BodyGlossToken {
                    bodygloss: Some((
                        ReferenceTo::new("PAW".to_owned()),
                        "foot".to_owned(),
                        "paw".to_owned(),
                        "feet".to_owned(),
                        "paws".to_owned(),
                    )),
                }),
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
