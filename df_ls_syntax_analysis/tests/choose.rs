#![allow(clippy::needless_update)]

use df_ls_core::{Choose, DFChar, Reference, ReferenceTo};
use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_choose_value() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE2]

            [BIPEDAL:MONSTER]
                [NAME:test]
                [MAIN_PROFESSION:SLEEPING]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                main_profession: Some(Choose::Choice1(Reference("SLEEPING".to_owned()))),
                ..Default::default()
            })],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}

#[test]
fn test_choose_value_error() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE2]

            [BIPEDAL:MONSTER]
                [NAME:test]
                [MAIN_PROFESSION:666]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                main_profession: None,
                ..Default::default()
            })],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["wrong_arg_type"])
    .add_test_syntax_diagnostics_ranges(vec![Range {
        start: Position {
            line: 5,
            character: 33,
        },
        end: Position {
            line: 5,
            character: 36,
        },
    }])
    .run_test();
}

#[test]
fn test_choose_vec_with_error() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE2]

            [BIPEDAL:MONSTER]
                [NAME:test]
                [HOBBY:REF]
                [HOBBY:String]
                [HOBBY:666]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_2: vec![Type2Token::Bipedal(HumanToken {
                reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                name: Some("test".to_owned()),
                hobbies: vec![
                    Choose::Choice1(Reference("REF".to_owned())),
                    Choose::Choice1(Reference("String".to_owned())),
                ],
                ..Default::default()
            })],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["reference_is_string", "wrong_arg_type"])
    .add_test_syntax_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 6,
                character: 23,
            },
            end: Position {
                line: 6,
                character: 29,
            },
        },
        Range {
            start: Position {
                line: 7,
                character: 23,
            },
            end: Position {
                line: 7,
                character: 26,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_multi_choose() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "h
            [MAIN:TYPE2]

            [BIPEDAL:MONSTER]
                [MULTI:SLEEPING]
            [BIPEDAL:MONSTER2]
                [MULTI:5]
            [BIPEDAL:MONSTER3]
                [MULTI:-105]
            [BIPEDAL:MONSTER4]
                [MULTI:'a']
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            type_2: vec![
                Type2Token::Bipedal(HumanToken {
                    reference: Some(ReferenceTo::new("MONSTER".to_owned())),
                    multi_choose: Some(Choose::Choice1(Reference("SLEEPING".to_owned()))),
                    ..Default::default()
                }),
                Type2Token::Bipedal(HumanToken {
                    reference: Some(ReferenceTo::new("MONSTER2".to_owned())),
                    multi_choose: Some(Choose::Choice2(Choose::Choice1(Choose::Choice1(5)))),
                    ..Default::default()
                }),
                Type2Token::Bipedal(HumanToken {
                    reference: Some(ReferenceTo::new("MONSTER3".to_owned())),
                    multi_choose: Some(Choose::Choice2(Choose::Choice1(Choose::Choice2(-105)))),
                    ..Default::default()
                }),
                Type2Token::Bipedal(HumanToken {
                    reference: Some(ReferenceTo::new("MONSTER4".to_owned())),
                    multi_choose: Some(Choose::Choice2(Choose::Choice2(DFChar('a')))),
                    ..Default::default()
                }),
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
