use df_ls_syntax_analysis::DfLsConfig;
use serde_json::Value;
use tokio::net::TcpListener;
use tower_lsp::jsonrpc::Result;
use tower_lsp::lsp_types::*;
use tower_lsp::{Client, LanguageServer, LspService, Server};

pub async fn start_language_server_tcp(port: Option<u16>, debug: bool) {
    let listener = match port {
        Some(port) => TcpListener::bind(format!("127.0.0.1:{}", port)).await,
        // Port set to `0` to request port form OS
        None => TcpListener::bind("127.0.0.1:0").await,
    }
    .unwrap();
    let port = listener.local_addr().unwrap().port();
    // This string is parsed by the JS client, do not change!
    println!("<[SERVER available on port: `{}`]>", port);
    // Start waiting for new clients to connect (clients can reconnect)
    loop {
        match listener.accept().await {
            Ok((stream, addr)) => {
                log::info!("New client connected: {:?}", addr);
                // Spawn a new task (not necessarily a new thread)
                tokio::spawn(async move {
                    let (read, write) = tokio::io::split(stream);

                    let config = DfLsConfig::default();
                    let (service, socket) = LspService::new(|client| Backend {
                        client,
                        debug,
                        config,
                    });
                    // Start language server service
                    Server::new(read, write, socket).serve(service).await;
                    // If continuing the client disconnected or server stopped/crashes
                    log::info!("Client disconnected");
                });
            }
            Err(e) => log::error!("Client tried to connect, but failed: {:?}", e),
        }
    }
}

pub async fn start_language_server_stdio(debug: bool) {
    let config = DfLsConfig::default();
    let (service, socket) = LspService::new(|client| Backend {
        client,
        debug,
        config,
    });
    // Start language server service
    Server::new(tokio::io::stdin(), tokio::io::stdout(), socket)
        .serve(service)
        .await;
}

#[derive(Debug)]
pub struct Backend {
    pub client: Client,
    pub debug: bool,
    pub config: DfLsConfig,
}

#[tower_lsp::async_trait]
impl LanguageServer for Backend {
    async fn initialize(&self, _: InitializeParams) -> Result<InitializeResult> {
        Ok(InitializeResult {
            server_info: None,
            capabilities: ServerCapabilities {
                text_document_sync: Some(TextDocumentSyncCapability::Kind(
                    TextDocumentSyncKind::INCREMENTAL,
                )),
                completion_provider: Some(CompletionOptions {
                    resolve_provider: Some(false),
                    trigger_characters: Some(vec![".".to_string()]),
                    work_done_progress_options: Default::default(),
                    all_commit_characters: None,
                }),
                execute_command_provider: Some(ExecuteCommandOptions {
                    commands: vec!["dummy.do_something".to_string()],
                    work_done_progress_options: Default::default(),
                }),
                workspace: Some(WorkspaceServerCapabilities {
                    workspace_folders: Some(WorkspaceFoldersServerCapabilities {
                        supported: Some(true),
                        change_notifications: Some(OneOf::Left(true)),
                    }),
                    file_operations: None,
                }),
                ..ServerCapabilities::default()
            },
        })
    }

    async fn initialized(&self, _: InitializedParams) {
        log::info!("Initialized!");
    }

    async fn shutdown(&self) -> Result<()> {
        log::info!("Shutdown");
        Ok(())
    }

    async fn did_change_workspace_folders(&self, _: DidChangeWorkspaceFoldersParams) {
        // TODO
    }

    async fn did_change_configuration(&self, _: DidChangeConfigurationParams) {
        // TODO
    }

    async fn did_change_watched_files(&self, _: DidChangeWatchedFilesParams) {
        // TODO
    }

    async fn execute_command(&self, _: ExecuteCommandParams) -> Result<Option<Value>> {
        match self.client.apply_edit(WorkspaceEdit::default()).await {
            Ok(res) if res.applied => self.client.log_message(MessageType::INFO, "applied").await,
            Ok(_) => self.client.log_message(MessageType::INFO, "rejected").await,
            Err(err) => self.client.log_message(MessageType::ERROR, err).await,
        }

        Ok(None)
    }

    async fn did_open(&self, document: DidOpenTextDocumentParams) {
        log::info!("DF Raw file opened.");

        let uri = document.text_document.uri;
        // Get source from document
        let source = document.text_document.text;
        // Check source
        let diagnostic_list = crate::check_source(source.as_bytes(), self.debug, &self.config);
        self.client
            .publish_diagnostics(uri, diagnostic_list, None)
            .await;
    }

    async fn did_change(&self, _document: DidChangeTextDocumentParams) {
        // log::info!("file change");

        // let uri = document.text_document.uri;
        // // Read file from disk
        // let path = uri.to_file_path();
        // if let Ok(path) = path {
        //     // Get source from document
        //     let source = std::fs::read(path).expect("Something went wrong reading the file");
        //     // Check source
        //     let diagnostic_list = crate::check_source(&source, true);
        //     self.client
        //         .publish_diagnostics(uri, diagnostic_list, None)
        //         .await;

        //     self.client
        //         .log_message(MessageType::Info, "file changed!")
        //         .await;
        // } else {
        //     log::warn!("URI was not a file: {}", uri.to_string());
        // }
    }

    async fn did_save(&self, document: DidSaveTextDocumentParams) {
        log::info!("DF Raw file saved.");

        let uri = document.text_document.uri;
        // Get source from document
        let source = match document.text {
            Some(source) => source,
            None => {
                // Read file from disk
                let path = uri.to_file_path().expect("URI was not a file.");
                std::fs::read_to_string(path).expect("Something went wrong reading the file")
            }
        };
        // Check source
        let diagnostic_list = crate::check_source(source.as_bytes(), self.debug, &self.config);
        self.client
            .publish_diagnostics(uri, diagnostic_list, None)
            .await;

        log::info!("Finished checking saved file.");
    }

    async fn did_close(&self, _: DidCloseTextDocumentParams) {
        log::info!("DF Raw file closed.");
    }

    async fn completion(&self, _: CompletionParams) -> Result<Option<CompletionResponse>> {
        Ok(Some(CompletionResponse::Array(vec![
            CompletionItem::new_simple("Hello".to_string(), "Some detail".to_string()),
            CompletionItem::new_simple("Bye".to_string(), "More detail".to_string()),
        ])))
    }
}
