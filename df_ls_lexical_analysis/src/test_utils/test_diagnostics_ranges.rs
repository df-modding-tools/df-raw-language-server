#![cfg(debug_assertions)]

use df_ls_diagnostics::{lsp_types::*, DiagnosticsInfo};
use pretty_assertions::assert_eq;

pub fn test_diagnostics_ranges(
    expected_diagnostics_ranges: &[Range],
    given_info: &DiagnosticsInfo,
) {
    let given_diagnostics_list = given_info.get_diagnostic_list();
    let given_diagnostics_ranges = given_diagnostics_list.iter().map(|d| d.range).collect();
    log::info!("Given Diagnostics Ranges:\n{:#?}", given_diagnostics_ranges);

    assert_diagnostic_range(expected_diagnostics_ranges, given_diagnostics_ranges);
}

pub fn assert_diagnostic_range(expected_ranges: &[Range], given_ranges: Vec<Range>) {
    assert_eq!(
        given_ranges.len(),
        expected_ranges.len(),
        "Diagnostics ranges lists are not the same size."
    );
    for (expected, given) in expected_ranges.iter().zip(given_ranges.iter()) {
        assert_eq!(expected, given);
    }
}
