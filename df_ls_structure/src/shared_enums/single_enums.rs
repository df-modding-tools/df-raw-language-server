use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum AllEnum {
    #[df_token(token_name = "ALL")]
    All,
}

impl Default for AllEnum {
    fn default() -> Self {
        Self::All
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum NoneEnum {
    #[df_token(token_name = "NONE")]
    None,
}

impl Default for NoneEnum {
    fn default() -> Self {
        Self::None
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum StandardPluralEnum {
    #[df_token(token_name = "STP")]
    Stp,
}
impl Default for StandardPluralEnum {
    fn default() -> Self {
        Self::Stp
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum NoMatGlossEnum {
    #[df_token(token_name = "NO_MATGLOSS")]
    NoMatgloss,
}

impl Default for NoMatGlossEnum {
    fn default() -> Self {
        Self::NoMatgloss
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum NotApplicableEnum {
    #[df_token(token_name = "NA")]
    NotApplicable,
}
impl Default for NotApplicableEnum {
    fn default() -> Self {
        Self::NotApplicable
    }
}
