use df_ls_diagnostics::lsp_types::{Diagnostic, NumberOrString};

/// Output format for the diagnostics.
#[derive(Debug, Clone, Copy)]
pub enum DiagnosticFormat {
    /// Human readable text.
    Text,
    /// JSON format.
    Json,
}

/// Print the diagnostics list in the correct format.
pub(crate) fn print_diagnostics(
    diagnostics: Vec<Diagnostic>,
    format: DiagnosticFormat,
    pretty_print: bool,
) {
    if diagnostics.is_empty() {
        log::info!("No warnings or errors found in the file.");
    } else {
        match format {
            DiagnosticFormat::Text => {
                for diagnostic in diagnostics {
                    let range_string = format!(
                        "start: (line {}, char {}), end: (line {}, char {})",
                        diagnostic.range.start.line + 1, // `line` is zero based, convert to one based.
                        diagnostic.range.start.character,
                        diagnostic.range.end.line + 1, // `line` is zero based, convert to one based.
                        diagnostic.range.end.character
                    );
                    let code = match diagnostic.code {
                        Some(NumberOrString::String(string)) => string,
                        Some(NumberOrString::Number(number)) => number.to_string(),
                        None => "<no code>".to_owned(),
                    };
                    println!("`{}` at: {}", code, range_string);
                }
            }
            DiagnosticFormat::Json => {
                let output_json = match pretty_print {
                    true => serde_json::to_string_pretty(&diagnostics),
                    false => serde_json::to_string(&diagnostics),
                }
                .expect("The conversion to Json failed.");

                println!("{}", output_json);
            }
        }
    }
}
