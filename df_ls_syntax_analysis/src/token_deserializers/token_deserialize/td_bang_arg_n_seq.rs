use super::super::{Argument, Token, TokenArgument, TryFromArgument, TryFromArgumentGroup};
use super::TokenDeserialize;
use df_ls_core::{BangArgN, BangArgNOrValue, BangArgNSequence, Clamp};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use std::convert::TryFrom;

// Deserialize a token with following pattern: `[REF:!ARG1]`
crate::token_deserialize_unary_token!(BangArgNSequence);

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for BangArgNSequence {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for BangArgNSequence {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match &arg.value {
                TokenArgument::TVBangArgNSequence(v) => Ok(BangArgNSequence({
                    let mut list = Vec::new();
                    for item in v {
                        list.push(match item {
                            TokenArgument::TVBangArgN(n) => BangArgNOrValue::BangArgN(BangArgN(*n)),
                            TokenArgument::TVString(s) => BangArgNOrValue::Value(s.clone()),
                            _ => unreachable!("Only support BangArgN and String"),
                        });
                    }
                    list
                })),
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "BangArgNSequence".to_owned()
    }
}

// -------------------------Convert from TokenArgument -----------------------

impl From<BangArgNSequence> for TokenArgument {
    fn from(item: BangArgNSequence) -> TokenArgument {
        TokenArgument::TVBangArgNSequence(
            item.0
                .into_iter()
                .map(|v| match v {
                    BangArgNOrValue::BangArgN(n) => n.into(),
                    BangArgNOrValue::Value(v) => v.into(),
                })
                .collect(),
        )
    }
}

impl From<Option<BangArgNSequence>> for TokenArgument {
    fn from(item: Option<BangArgNSequence>) -> TokenArgument {
        match item {
            Some(v) => TokenArgument::TVBangArgNSequence(
                v.0.into_iter()
                    .map(|v| match v {
                        BangArgNOrValue::BangArgN(n) => n.into(),
                        BangArgNOrValue::Value(v) => v.into(),
                    })
                    .collect(),
            ),
            None => TokenArgument::TVEmpty,
        }
    }
}

impl From<BangArgNOrValue> for TokenArgument {
    fn from(item: BangArgNOrValue) -> TokenArgument {
        match item {
            BangArgNOrValue::BangArgN(n) => n.into(),
            BangArgNOrValue::Value(v) => v.into(),
        }
    }
}

impl From<Option<BangArgNOrValue>> for TokenArgument {
    fn from(item: Option<BangArgNOrValue>) -> TokenArgument {
        match item {
            Some(v) => match v {
                BangArgNOrValue::BangArgN(n) => n.into(),
                BangArgNOrValue::Value(v) => v.into(),
            },
            None => TokenArgument::TVEmpty,
        }
    }
}

// -------------------------Implement Clamp -----------------------

impl<const L: isize, const H: isize> TryFromArgument for Clamp<BangArgNSequence, L, H> {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // In debug mode warn user about when clamp exceeds allowable value range.
        #[cfg(debug_assertions)]
        {
            if (H as i64) > (<BangArgN>::MAX as i64) {
                panic!(
                    "Clamp exceeds range of type. High: {} > {}::MAX = {} \
                    for Clamp<{}, {}, {}>",
                    H,
                    "BangArgN",
                    <BangArgN>::MAX,
                    "BangArgNSequence",
                    L,
                    H
                );
            }
            if (L as i64) < (<BangArgN>::MIN as i64) {
                panic!(
                    "Clamp exceeds range of type. Low: {} < {}::MIN = {} \
                    for Clamp<{}, {}, {}>",
                    L,
                    "BangArgN",
                    <BangArgN>::MIN,
                    "BangArgNSequence",
                    L,
                    H
                );
            }
        }
        if let Some(arg) = arg_opt {
            match &arg.value {
                TokenArgument::TVBangArgNSequence(v) => Ok(Self::from(BangArgNSequence({
                    let mut list = Vec::new();
                    for item in v {
                        list.push(match item {
                            TokenArgument::TVBangArgN(value_u16) => {
                                // Make sure we do not go over the type limits.
                                let low = std::cmp::max(L as u16, <BangArgN>::MIN as u16);
                                let high = std::cmp::min(H as u16, <BangArgN>::MAX as u16);

                                if *value_u16 > high {
                                    if add_diagnostics_on_err {
                                        diagnostics.add_message(
                                            DMExtraInfo {
                                                range: arg.node.get_range(),
                                                message_template_data: hash_map! {
                                                    "max_value" => high.to_string(),
                                                },
                                            },
                                            "too_large_int",
                                        );
                                    }
                                    return Err(());
                                }
                                if *value_u16 < low {
                                    if add_diagnostics_on_err {
                                        diagnostics.add_message(
                                            DMExtraInfo {
                                                range: arg.node.get_range(),
                                                message_template_data: hash_map! {
                                                    "min_value" => low.to_string(),
                                                },
                                            },
                                            "too_small_int",
                                        );
                                    }
                                    return Err(());
                                }
                                BangArgNOrValue::from(match <BangArgN>::try_from(*value_u16) {
                                    Ok(v) => v,
                                    Err(err) => {
                                        log::error!("{}", err);
                                        panic!(
                                            "Number conversion failed, `{}` to `{}`",
                                            value_u16, "BangArgN"
                                        );
                                    }
                                })
                            }
                            TokenArgument::TVString(s) => BangArgNOrValue::Value(s.clone()),
                            _ => unreachable!("Only support BangArgN and String"),
                        });
                    }
                    list
                }))),
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        let low = std::cmp::max(L as i64, <BangArgN>::MIN as i64);
        let high = std::cmp::min(H as i64, <BangArgN>::MAX as i64);
        format!(
            "String with `!ARGx` ({}, limited to min: {}, max: {})",
            "BangArgNSequence", low, high
        )
    }
}

impl<const L: isize, const H: isize> From<Clamp<BangArgNSequence, L, H>> for TokenArgument {
    fn from(item: Clamp<BangArgNSequence, L, H>) -> TokenArgument {
        Self::from(item.value)
    }
}

impl<const L: isize, const H: isize> From<Option<Clamp<BangArgNSequence, L, H>>> for TokenArgument {
    fn from(item: Option<Clamp<BangArgNSequence, L, H>>) -> TokenArgument {
        match item {
            Some(v) => Self::from(v),
            None => TokenArgument::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_tree_structure;
    use crate::test_utils::SyntaxTestBuilder;
    use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

    #[test]
    fn test_bang_arg_n_sequence_correct() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [BANG_ARG_N_SEQ:!ARG5]
                [BANG_ARG_N_SEQ:!ARG5]
                [BANG_ARG_N_SEQ:aa!ARG9]
                [BANG_ARG_N_SEQ:!ARG5aaa]
                [BANG_ARG_N_SEQ:AA!ARG5RR]
                [BANG_ARG_N_SEQ:AA!ARG5AA]
                [BANG_ARG_N_SEQ:AA!ARG0BB!ARG1CC!ARG2DD!ARG3EE]
                [BANG_ARG_N_SEQ:!ARG5A]
                [BANG_ARG_N_SEQ:!ARG5!ARG59!ARG22]",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec![])
        .add_test_syntax_diagnostics_ranges(vec![]);

        test_tree_structure!(
            test_builder,
            [
                BangArgNSequence => BangArgNSequence(vec![BangArgNOrValue::BangArgN(BangArgN(5))]),
                (BangArgNSequence,) => (BangArgNSequence(vec![BangArgNOrValue::BangArgN(BangArgN(
                    5
                ))]),),
                BangArgNSequence => BangArgNSequence(vec![
                    BangArgNOrValue::Value("aa".to_owned()),
                    BangArgNOrValue::BangArgN(BangArgN(9))
                ]),
                BangArgNSequence => BangArgNSequence(vec![
                    BangArgNOrValue::BangArgN(BangArgN(5)),
                    BangArgNOrValue::Value("aaa".to_owned())
                ]),
                BangArgNSequence => BangArgNSequence(vec![
                    BangArgNOrValue::Value("AA".to_owned()),
                    BangArgNOrValue::BangArgN(BangArgN(5)),
                    BangArgNOrValue::Value("RR".to_owned())
                ]),
                BangArgNSequence => BangArgNSequence(vec![
                    BangArgNOrValue::Value("AA".to_owned()),
                    BangArgNOrValue::BangArgN(BangArgN(5)),
                    BangArgNOrValue::Value("AA".to_owned())
                ]),
                BangArgNSequence => BangArgNSequence(vec![
                    BangArgNOrValue::Value("AA".to_owned()),
                    BangArgNOrValue::BangArgN(BangArgN(0)),
                    BangArgNOrValue::Value("BB".to_owned()),
                    BangArgNOrValue::BangArgN(BangArgN(1)),
                    BangArgNOrValue::Value("CC".to_owned()),
                    BangArgNOrValue::BangArgN(BangArgN(2)),
                    BangArgNOrValue::Value("DD".to_owned()),
                    BangArgNOrValue::BangArgN(BangArgN(3)),
                    BangArgNOrValue::Value("EE".to_owned()),
                ]),
                BangArgNSequence => BangArgNSequence(vec![
                    BangArgNOrValue::BangArgN(BangArgN(5)),
                    BangArgNOrValue::Value("A".to_owned())
                ]),
                BangArgNSequence => BangArgNSequence(vec![
                    BangArgNOrValue::BangArgN(BangArgN(5)),
                    BangArgNOrValue::BangArgN(BangArgN(59)),
                    BangArgNOrValue::BangArgN(BangArgN(22)),
                ]),
            ]
        );
    }
}
