use crate::tokenizer::TokenizerHelper;
use df_ls_diagnostics::lsp_types::*;
use df_ls_diagnostics::{DMExtraInfo, DiagnosticsInfo};

pub(crate) fn mark_rest_of_file_as_unchecked_tok(
    tok_help: &mut TokenizerHelper,
    diagnostics: &mut DiagnosticsInfo,
) {
    let start_point = tok_help.get_point();
    let start_pos = Position {
        line: start_point.row as u32,
        character: start_point.column as u32,
    };
    // Move cursor to end of file
    tok_help.move_index(tok_help.get_eof_index());

    let end_point = tok_help.get_point();
    let end_pos = Position {
        line: end_point.row as u32,
        character: end_point.column as u32,
    };
    // Check if there are characters left to not check
    if start_pos == end_pos {
        // EOF is here, so do not add message
        return;
    }
    diagnostics.add_message(
        DMExtraInfo::new(Range {
            start: start_pos,
            end: end_pos,
        }),
        "unchecked_code",
    );
    diagnostics.lock_message_list();
}
