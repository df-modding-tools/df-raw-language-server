use super::super::{Argument, Token, TokenArgument, TryFromArgument, TryFromArgumentGroup};
use super::TokenDeserialize;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};

// Deserialize a token with following pattern: `[REF:char]` or `[REF:'c']`
crate::token_deserialize_unary_token!(char);

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for char {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for char {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match arg.value {
                TokenArgument::TVCharacter(v) => Ok(v),
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "char".to_owned()
    }
}

// -------------------------Convert from TokenArgument -----------------------

impl From<char> for TokenArgument {
    fn from(item: char) -> TokenArgument {
        TokenArgument::TVCharacter(item)
    }
}

impl From<Option<char>> for TokenArgument {
    fn from(item: Option<char>) -> TokenArgument {
        match item {
            Some(v) => TokenArgument::TVCharacter(v),
            None => TokenArgument::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_tree_structure;
    use crate::test_utils::SyntaxTestBuilder;
    use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

    #[test]
    fn test_char_correct() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:'c']
                [REF:''']
                [REF:'\"']
                [REF:'0']
                [REF:'P']
                [REF:'#']
                [REF:'*']
                [REF:'`']",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec![])
        .add_test_syntax_diagnostics_ranges(vec![]);

        test_tree_structure!(
            test_builder,
            [
                char => 'c',
                (char,) => ('\'',),
                char => '\"',
                char => '0',
                char => 'P',
                char => '#',
                char => '*',
                char => '`',
            ]
        );
    }
}
