use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum AllOrAllSolidEnum {
    /// All material states.
    #[df_token(token_name = "ALL")]
    All,
    /// All solid material states.
    #[df_token(token_name = "ALL_SOLID")]
    AllSolid,
}
impl Default for AllOrAllSolidEnum {
    fn default() -> Self {
        Self::All
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum MaterialStateEnum {
    /// Material is in a solid state.
    /// Like:
    /// - Iron at room temperature
    /// - Water blow freezing (ice)
    #[df_token(token_name = "SOLID")]
    Solid,
    /// Material is in a liquid state.
    /// Like:
    /// - Iron at very high temperatures (molten iron)
    /// - Water at room temperature
    #[df_token(token_name = "LIQUID")]
    Liquid,
    /// Material is in a gas state.
    /// Like:
    /// - Oxygen at room temperature (air)
    /// - Water above its boiling point (steam)
    #[df_token(token_name = "GAS")]
    Gas,
    /// Material is ground to a powder.
    #[df_token(token_name = "POWDER")]
    Powder,
    /// Unknown
    #[df_token(token_name = "SOLID_POWDER")]
    SolidPowder,
    /// Unknown
    #[df_token(token_name = "PASTE")]
    Paste,
    /// Unknown
    #[df_token(token_name = "SOLID_PASTE")]
    SolidPaste,
    /// Unknown
    #[df_token(token_name = "PRESSED")]
    Pressed,
    /// Unknown
    #[df_token(token_name = "SOLID_PRESSED")]
    SolidPressed,
}
impl Default for MaterialStateEnum {
    fn default() -> Self {
        Self::Solid
    }
}
