#![no_main]
use libfuzzer_sys::fuzz_target;

fuzz_target!(|data: &[u8]| {
    // fuzzed code goes here
    if let Ok(source) = std::str::from_utf8(data) {
        let (_tree, _diagnostics) =
            df_ls_lexical_analysis::tokenize_df_raw_file(source.to_owned(), false);
    }
});
