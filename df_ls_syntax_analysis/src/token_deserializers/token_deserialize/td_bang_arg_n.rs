use super::super::{Argument, Token, TokenArgument, TryFromArgument, TryFromArgumentGroup};
use df_ls_core::{BangArgN, Clamp};
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use std::convert::TryFrom;

// Deserialize a token with following pattern: `[REF:!ARG2]` or `[REF:!ARG10]`
// This should be done using `BangArgNSequence` not `BangArgN`

// ------------------------- Convert a group of arguments to Self -----------------------

impl TryFromArgumentGroup for BangArgN {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        token.check_as_required_argument(source, diagnostics, add_diagnostics_on_err)?;
        let arg = token.get_current_arg_opt();
        let result = Self::try_from_argument(arg, source, diagnostics, add_diagnostics_on_err);
        token.consume_argument();
        result
    }
}

// -------------------------Convert one argument to Self -----------------------

impl TryFromArgument for BangArgN {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        if let Some(arg) = arg_opt {
            match arg.value {
                TokenArgument::TVBangArgN(v) => Ok(BangArgN(v)),
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        "BangArgN".to_owned()
    }
}

// -------------------------Convert from TokenArgument -----------------------

impl From<BangArgN> for TokenArgument {
    fn from(item: BangArgN) -> TokenArgument {
        TokenArgument::TVBangArgN(item.0)
    }
}

impl From<Option<BangArgN>> for TokenArgument {
    fn from(item: Option<BangArgN>) -> TokenArgument {
        match item {
            Some(v) => TokenArgument::TVBangArgN(v.0),
            None => TokenArgument::TVEmpty,
        }
    }
}

// -------------------------Implement Clamp -----------------------

impl<const L: isize, const H: isize> TryFromArgument for Clamp<BangArgN, L, H> {
    fn try_from_argument(
        arg_opt: Option<&Argument>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // In debug mode warn user about when clamp exceeds allowable value range.
        #[cfg(debug_assertions)]
        {
            if (H as i64) > (<BangArgN>::MAX as i64) {
                panic!(
                    "Clamp exceeds range of type. High: {} > {}::MAX = {} \
                    for Clamp<{}, {}, {}>",
                    H,
                    "BangArgN",
                    <BangArgN>::MAX,
                    "BangArgN",
                    L,
                    H
                );
            }
            if (L as i64) < (<BangArgN>::MIN as i64) {
                panic!(
                    "Clamp exceeds range of type. Low: {} < {}::MIN = {} \
                    for Clamp<{}, {}, {}>",
                    L,
                    "BangArgN",
                    <BangArgN>::MIN,
                    "BangArgN",
                    L,
                    H
                );
            }
        }
        if let Some(arg) = arg_opt {
            match arg.value {
                TokenArgument::TVBangArgN(value_u16) => {
                    // Make sure we do not go over the type limits.
                    let low = std::cmp::max(L as u16, <BangArgN>::MIN as u16);
                    let high = std::cmp::min(H as u16, <BangArgN>::MAX as u16);

                    if value_u16 > high {
                        if add_diagnostics_on_err {
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: arg.node.get_range(),
                                    message_template_data: hash_map! {
                                        "max_value" => high.to_string(),
                                    },
                                },
                                "too_large_int",
                            );
                        }
                        return Err(());
                    }
                    if value_u16 < low {
                        if add_diagnostics_on_err {
                            diagnostics.add_message(
                                DMExtraInfo {
                                    range: arg.node.get_range(),
                                    message_template_data: hash_map! {
                                        "min_value" => low.to_string(),
                                    },
                                },
                                "too_small_int",
                            );
                        }
                        return Err(());
                    }
                    Ok(Self::from(match <BangArgN>::try_from(value_u16) {
                        Ok(v) => v,
                        Err(err) => {
                            log::error!("{}", err);
                            panic!(
                                "Number conversion failed, `{}` to `{}`",
                                value_u16, "BangArgN"
                            );
                        }
                    }))
                }
                _ => {
                    if add_diagnostics_on_err {
                        diagnostics.add_message(
                            DMExtraInfo {
                                range: arg.node.get_range(),
                                message_template_data: hash_map! {
                                    "expected_parameters" => Self::expected_argument_types(),
                                    "found_parameters" => arg.value.argument_to_token_type_name(),
                                },
                            },
                            "wrong_arg_type",
                        );
                    }
                    Err(())
                }
            }
        } else {
            Err(())
        }
    }

    fn expected_argument_types() -> String {
        let low = std::cmp::max(L as i64, <BangArgN>::MIN as i64);
        let high = std::cmp::min(H as i64, <BangArgN>::MAX as i64);
        format!(
            "`!ARGx` ({}, limited to min: {}, max: {})",
            "BangArgN", low, high
        )
    }
}

impl<const L: isize, const H: isize> From<Clamp<BangArgN, L, H>> for TokenArgument {
    fn from(item: Clamp<BangArgN, L, H>) -> TokenArgument {
        TokenArgument::TVBangArgN(item.value.0)
    }
}

impl<const L: isize, const H: isize> From<Option<Clamp<BangArgN, L, H>>> for TokenArgument {
    fn from(item: Option<Clamp<BangArgN, L, H>>) -> TokenArgument {
        match item {
            Some(v) => TokenArgument::TVBangArgN(v.value.0),
            None => TokenArgument::TVEmpty,
        }
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_tree_structure;
    use crate::test_utils::SyntaxTestBuilder;
    use df_ls_core::{BangArgNOrValue, BangArgNSequence};
    use df_ls_diagnostics::{Position, Range};
    use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

    // Test `BangArgNSequence` instead of `BangArgN`,
    // because `BangArgN` is not allowed on it own as an argument.
    #[test]
    fn test_bang_arg_n_correct() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:!ARG0]
                [REF:!ARG2]
                [REF:!ARG5]
                [REF:!ARG9]
                [REF:!ARG10]
                [REF:!ARG05]
                [REF:!ARG007]
                [REF:!ARG200]",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec![])
        .add_test_syntax_diagnostics_ranges(vec![]);

        test_tree_structure!(
            test_builder,
            [
                BangArgNSequence => BangArgNSequence(vec![BangArgNOrValue::BangArgN(BangArgN(0))]),
                (BangArgNSequence,) => (BangArgNSequence(vec![BangArgNOrValue::BangArgN(BangArgN(
                    2
                ))]),),
                BangArgNSequence => BangArgNSequence(vec![BangArgNOrValue::BangArgN(BangArgN(5))]),
                BangArgNSequence => BangArgNSequence(vec![BangArgNOrValue::BangArgN(BangArgN(9))]),
                BangArgNSequence => BangArgNSequence(vec![BangArgNOrValue::BangArgN(BangArgN(10))]),
                BangArgNSequence => BangArgNSequence(vec![
                    BangArgNOrValue::BangArgN(BangArgN(0)),
                    BangArgNOrValue::Value("5".to_owned())
                ]),
                BangArgNSequence => BangArgNSequence(vec![
                    BangArgNOrValue::BangArgN(BangArgN(0)),
                    BangArgNOrValue::Value("07".to_owned())
                ]),
                BangArgNSequence => BangArgNSequence(vec![BangArgNOrValue::BangArgN(BangArgN(200))]),
            ]
        );
    }

    #[test]
    fn test_u16_overflow() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:!ARG500000]
                [REF:!ARG65536]",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec!["expected_integer", "expected_integer"])
        .add_test_syntax_diagnostics_ranges(vec![
            Range {
                start: Position {
                    line: 1,
                    character: 21,
                },
                end: Position {
                    line: 1,
                    character: 31,
                },
            },
            Range {
                start: Position {
                    line: 2,
                    character: 21,
                },
                end: Position {
                    line: 2,
                    character: 30,
                },
            },
        ]);

        test_tree_structure!(
            test_builder,
            [
                Option<BangArgNSequence> =!,
                Option<BangArgNSequence> =!,
            ]
        );
    }
}
