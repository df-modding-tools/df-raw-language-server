use df_ls_core::{AllowEmpty, ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
pub enum LanguageToken {
    #[df_token(token_name = "WORD")]
    WordToken(WordToken),
    #[df_token(token_name = "SYMBOL")]
    SymbolToken(SymbolToken),
    #[df_token(token_name = "TRANSLATION")]
    TranslationToken(TranslationToken),
}

impl Default for LanguageToken {
    fn default() -> Self {
        Self::WordToken(WordToken::default())
    }
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct TranslationToken {
    /// Argument 1 of `TRANSLATION`
    /// The reference for a Translation in other RAW files and tokens
    #[df_token(token_name = "TRANSLATION", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    ///
    #[df_token(token_name = "T_WORD")]
    pub t_word: Vec<(ReferenceTo<WordToken>, String)>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct SymbolToken {
    /// Argument 1 of `SYMBOL`
    /// The reference for a Symbol in other RAW files and tokens
    #[df_token(token_name = "SYMBOL", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    ///
    #[df_token(token_name = "S_WORD")]
    pub s_word: Vec<ReferenceTo<WordToken>>,
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct WordToken {
    /// Argument 1 of `WORD`
    /// The reference for a Word in other RAW files and tokens
    #[df_token(token_name = "WORD", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    ///
    #[df_token(token_name = "NOUN")]
    pub nouns: Vec<NounToken>,
    ///
    #[df_token(token_name = "ADJ")]
    pub adj: Vec<AdjToken>,
    ///
    #[df_token(token_name = "VERB")]
    pub verb: Vec<VerbToken>,
    ///
    #[df_token(token_name = "PREFIX")]
    pub prefix: Vec<PrefixToken>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct NounToken {
    /// Argument 1 of `NOUN`
    ///
    #[df_token(token_name = "NOUN", on_duplicate_to_parent, primary_token)]
    pub words: Option<(String, AllowEmpty<String>)>,
    ///
    #[df_token(token_name = "FRONT_COMPOUND_NOUN_SING")]
    pub front_compound_noun_sing: Option<()>,
    ///
    #[df_token(token_name = "REAR_COMPOUND_NOUN_SING")]
    pub read_compound_noun_sing: Option<()>,
    ///
    #[df_token(token_name = "THE_COMPOUND_NOUN_SING")]
    pub the_compound_noun_sing: Option<()>,
    ///
    #[df_token(token_name = "THE_NOUN_SING")]
    pub the_noun_sing: Option<()>,
    ///
    #[df_token(token_name = "OF_NOUN_SING")]
    pub of_noun_sing: Option<()>,
    ///
    #[df_token(token_name = "FRONT_COMPOUND_NOUN_PLUR")]
    pub front_compound_noun_plur: Option<()>,
    ///
    #[df_token(token_name = "REAR_COMPOUND_NOUN_PLUR")]
    pub read_compound_noun_plur: Option<()>,
    ///
    #[df_token(token_name = "THE_COMPOUND_NOUN_PLUR")]
    pub the_compound_noun_plur: Option<()>,
    ///
    #[df_token(token_name = "THE_NOUN_PLUR")]
    pub the_noun_plur: Option<()>,
    ///
    #[df_token(token_name = "OF_NOUN_PLUR")]
    pub of_noun_plur: Option<()>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct VerbToken {
    /// Argument 1 of `VERB`
    #[df_token(token_name = "VERB", on_duplicate_to_parent, primary_token)]
    pub words: Option<(String, String, String, String, String)>,
    ///
    #[df_token(token_name = "STANDARD_VERB")]
    pub standard_verb: Option<()>,
    ///
    #[df_token(token_name = "FRONT_COMPOUND_ADJ")]
    pub front_compound_adj: Option<()>,
    ///
    #[df_token(token_name = "THE_COMPOUND_ADJ")]
    pub the_compound_adj: Option<()>,
    ///
    #[df_token(token_name = "REAR_COMPOUND_ADJ")]
    pub rear_compound_adj: Option<()>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct AdjToken {
    /// Argument 1 of `ADJ`
    #[df_token(token_name = "ADJ", on_duplicate_to_parent, primary_token)]
    pub words: Option<String>,
    ///
    #[df_token(token_name = "ADJ_DIST")]
    pub adj_dist: Option<i32>,
    ///
    #[df_token(token_name = "FRONT_COMPOUND_ADJ")]
    pub front_compound_adj: Option<()>,
    ///
    #[df_token(token_name = "THE_COMPOUND_ADJ")]
    pub the_compound_adj: Option<()>,
    ///
    #[df_token(token_name = "REAR_COMPOUND_ADJ")]
    pub rear_compound_adj: Option<()>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct PrefixToken {
    /// Argument 1 of `PREFIX`
    #[df_token(token_name = "PREFIX", on_duplicate_to_parent, primary_token)]
    pub words: Option<String>,
    ///
    #[df_token(token_name = "FRONT_COMPOUND_PREFIX")]
    pub front_compound_prefix: Option<()>,
    ///
    #[df_token(token_name = "THE_COMPOUND_PREFIX")]
    pub the_compound_prefix: Option<()>,
}
