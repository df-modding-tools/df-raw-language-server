use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

#[test]
fn test_multi_line_1() {
    LexerTestBuilder::test_source(
        "header
        [REF
        ]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `header`)
  (comment)
  (token
    ([)
    (token_name: `REF`)
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket", "unexpected_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 1,
                character: 12,
            },
            end: Position {
                line: 1,
                character: 12,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 8,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_multi_line_2() {
    LexerTestBuilder::test_source(
        "header
        [REF:
        ]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `header`)
  (comment)
  (token
    ([)
    (token_name: `REF`)
    (token_arguments
      (:)
      (token_argument_empty: ``)
    )
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket", "unexpected_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 1,
                character: 13,
            },
            end: Position {
                line: 1,
                character: 13,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 8,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_multi_line_3() {
    LexerTestBuilder::test_source(
        "header
        [REF:string
        ]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `header`)
  (comment)
  (token
    ([)
    (token_name: `REF`)
    (token_arguments
      (:)
      (token_argument_string: `string`)
    )
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket", "unexpected_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 1,
                character: 19,
            },
            end: Position {
                line: 1,
                character: 19,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 8,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_multi_line_4() {
    LexerTestBuilder::test_source(
        "header
        [REF:string
        [
        ]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `header`)
  (comment)
  (token
    ([)
    (token_name: `REF`)
    (token_arguments
      (:)
      (token_argument_string: `string`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![
        "missing_end_bracket",
        "missing_token_name",
        "missing_end_bracket",
        "unexpected_end_bracket",
    ])
    .add_test_lexer_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 1,
                character: 19,
            },
            end: Position {
                line: 1,
                character: 19,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 9,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 9,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 3,
                character: 8,
            },
            end: Position {
                line: 3,
                character: 9,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_multi_line_5() {
    LexerTestBuilder::test_source(
        "header
        [REF:string]
        [REF][POP:9]
        [TEST]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `header`)
  (comment)
  (token
    ([)
    (token_name: `REF`)
    (token_arguments
      (:)
      (token_argument_string: `string`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `REF`)
    (])
  )
  (token
    ([)
    (token_name: `POP`)
    (token_arguments
      (:)
      (token_argument_integer: `9`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `TEST`)
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}
#[test]
fn test_multi_line_6() {
    LexerTestBuilder::test_source(
        "header
        [REF:string][REF][POP:9]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `header`)
  (comment)
  (token
    ([)
    (token_name: `REF`)
    (token_arguments
      (:)
      (token_argument_string: `string`)
    )
    (])
  )
  (token
    ([)
    (token_name: `REF`)
    (])
  )
  (token
    ([)
    (token_name: `POP`)
    (token_arguments
      (:)
      (token_argument_integer: `9`)
    )
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![])
    .add_test_lexer_diagnostics_ranges(vec![])
    .run_test();
}
