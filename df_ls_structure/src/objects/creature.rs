use df_ls_core::{AllowEmpty, Any, Choose, Clamp, DFChar, Reference, ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

mod attack;
mod caste;
mod creature_enums;
mod gait;
mod local_mat;
mod misc_creature_nesting;
mod select_caste;

pub use attack::*;
pub use caste::*;
pub use creature_enums::*;
pub use gait::*;
pub use local_mat::*;
pub use misc_creature_nesting::*;
pub use select_caste::*;

use crate::{
    BiomeEnum, BodyAttributeEnum, BodyDetailPlanToken, BodyGlossToken, BodyToken,
    BpCriteriaTokenArg, ColorToken, CreatureVariationToken, ItemReferenceArg, LocalMaterialToken,
    MaleOrFemaleEnum, MaterialStateEnum, MaterialTokenArgWithLocalCreatureMat, NoneEnum,
    PersonalityTraitEnum, SkillEnum, SoulAttributeEnum, SphereEnum, UnitTypeEnum, UseMaterial,
    UseMaterialTemplate,
};

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct CreatureToken {
    /// argument 1 of `CREATURE`
    #[df_token(token_name = "CREATURE", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    // region: Creature-only tokens ===============================================================
    /// Nested token `CASTE`
    #[df_token(token_name = "CASTE")]
    pub castes: Vec<Caste>,
    /// Nested token `SELECT_CASTE`
    #[df_token(token_name = "SELECT_CASTE")]
    pub select_castes: Vec<SelectCaste>,
    /// A list of `TISSUE` tokens on this creature.
    #[df_token(token_name = "TISSUE")]
    pub tissue: Vec<LocalTissueToken>,
    /// A list of `USE_TISSUE` tokens on this creature.
    #[df_token(token_name = "USE_TISSUE")]
    pub use_tissue: Vec<UseTissue>,
    /// A list of `USE_TISSUE_TEMPLATE` tokens on this creature.
    #[df_token(token_name = "USE_TISSUE_TEMPLATE")]
    pub use_tissue_template: Vec<UseTissueTemplate>,
    /// A list of `SELECT_TISSUE` tokens on this creature.
    #[df_token(token_name = "SELECT_TISSUE")]
    pub select_tissue: Vec<SelectTissue>,
    /// Removes a tissue from the creature.
    #[df_token(token_name = "REMOVE_TISSUE")]
    pub remove_tissue: Vec<Reference>, // ref is a local mat
    /// A list of `MATERIAL` tokens on this creature.
    #[df_token(token_name = "MATERIAL")]
    pub material: Vec<LocalMaterialToken>,
    /// A list of `USE_MATERIAL` tokens on this creature.
    #[df_token(token_name = "USE_MATERIAL")]
    pub use_material: Vec<UseMaterial>,
    /// A list of `USE_MATERIAL_TEMPLATE` tokens on this creature.
    #[df_token(token_name = "USE_MATERIAL_TEMPLATE")]
    pub use_material_template: Vec<UseMaterialTemplate>,
    /// A list of `SELECT_MATERIAL` tokens on this creature.
    #[df_token(token_name = "SELECT_MATERIAL")]
    pub select_material: Vec<SelectMaterial>,
    /// Removes a material from the creature.
    #[df_token(token_name = "REMOVE_MATERIAL")]
    pub remove_material: Vec<Reference>, // ref is a local mat
    /// If set, the creature will blink between its `[TILE]` and its `ALTTILE`.
    #[df_token(token_name = "ALTTILE")]
    pub alttile: Option<DFChar>,
    /// Enables the creature to be kept in artificial hives by beekeepers.
    #[df_token(token_name = "ARTIFICIAL_HIVEABLE")]
    pub artificial_hiveable: Option<()>,
    /// Select a biome the creature may appear in.
    #[df_token(token_name = "BIOME")]
    pub biome: Vec<BiomeEnum>,
    /// Multiplies frequency by a factor of (integer)%.
    #[df_token(token_name = "CHANGE_FREQUENCY_PERC")]
    pub change_frequency_perc: Option<u32>,
    /// The minimum/maximum numbers of how many creatures per spawned cluster. Certain vermin fish
    /// use this token in combination with temperate ocean and river biome tokens to perform
    /// seasonal migrations. Defaults to 1:1 if not specified.
    #[df_token(token_name = "CLUSTER_NUMBER")]
    pub cluster_number: Option<(u32, u32)>,
    /// Color of the creature's tile.
    /// See [Color](https://dwarffortresswiki.org/index.php/Color) for usage.
    #[df_token(token_name = "COLOR")]
    pub color: Option<(u8, u8, u8)>,
    /// Creatures active in their civilization's military will use this tile instead.
    #[df_token(token_name = "CREATURE_SOLDIER_TILE")]
    pub creature_soldier_tile: Option<DFChar>,
    /// The symbol of the creature in ASCII mode.
    #[df_token(token_name = "CREATURE_TILE")]
    pub creature_tile: Option<DFChar>,
    /// Adding this token to a creature prevents it from appearing in generated worlds (unless it's
    /// marked as always present for a particular civilisation). For example, adding it to dogs will
    /// lead to worlds being generated without dogs in them. Also removes the creature from the
    /// object testing arena's spawn list. If combined with `[FANCIFUL]`, artistic depictions of the
    /// creature will occur regardless. Used by centaurs, chimeras and griffons in the vanilla game.
    ///
    /// Note: a creature tagged as `DOES_NOT_EXIST` can still be summoned successfully, as long as
    /// it has a body defined in its raws ([source](http://www.bay12forums.com/smf/index.php?topic=165213.msg8086938#msg8086938)).
    /// It's also possible for another creature to transform into it.
    #[df_token(token_name = "DOES_NOT_EXIST")]
    // TODO maybe add a warning for this token? like #84
    pub does_not_exist: Option<()>,
    /// Makes the creature appear as a large 3x3 wagon responsible for carrying trade goods, pulled
    /// by two `[WAGON_PULLER]` creatures and driven by a merchant.
    #[df_token(token_name = "EQUIPMENT_WAGON")]
    pub equipment_wagon: Option<()>,
    /// Creature is considered evil and will only show up in evil biomes. Civilizations with
    /// `[USE_EVIL_ANIMALS]` can domesticate them regardless of exotic status. Has no effect on
    /// cavern creatures except to restrict taming. A civilization with evil creatures can
    /// colonise evil areas.
    #[df_token(token_name = "EVIL")]
    pub evil: Option<()>,
    /// The creature is a thing of legend and known to all civilizations. Its materials cannot be
    /// requested or preferred. The tag also adds some art value modifiers. Used by a number of
    /// creatures. Conflicts with `[COMMON_DOMESTIC]`.
    #[df_token(token_name = "FANCIFUL")]
    pub fanciful: Option<()>,
    /// Determines the chances of a creature appearing within its environment, with higher values
    /// resulting in more frequent appearance. Also affects the chance of a creature being brought
    /// in a caravan for trading.
    ///
    /// The game effectively considers all creatures that can possibly appear and uses the
    /// `FREQUENCY` value as a weight - for example, if there are three creatures with frequencies
    /// 10/25/50, the creature with `[FREQUENCY:50]` will appear approximately 58.8% of the time.
    ///
    /// Defaults to 50 if not specified.
    ///
    /// Not to be confused with `[POP_RATIO]`.
    #[df_token(token_name = "FREQUENCY")]
    pub frequency: Option<u32>,
    /// Defines a new name for a creature in baby state, much like `[BABYNAME]`, but applied
    /// regardless of caste.
    #[df_token(token_name = "GENERAL_BABY_NAME")]
    pub general_baby_name: Option<(String, String)>,
    /// Defines a new name for a creature in child state, much like `[CHILDNAME]`, but applied
    /// regardless of caste.
    #[df_token(token_name = "GENERAL_CHILD_NAME")]
    pub general_child_name: Option<(String, String)>,
    /// Found on procedurally generated creatures like forgotten beasts, titans, demons, angels, and
    /// night creatures. Cannot be specified in user-defined raws.
    #[df_token(token_name = "GENERATED")] // TODO mark generated, see #84
    pub generated: Option<()>,
    /// The colour of the creature's `[GLOWTILE]`.
    #[df_token(token_name = "GLOWCOLOR")]
    pub glowcolor: Option<(u8, u8, u8)>,
    /// If present, the being glows in the dark (generally used for Adventurer Mode). The tile is
    /// what replaces the being's current tile when it is obscured from your sight by darkness. The
    /// default setting for kobolds (a yellow quotation mark) provides a nice "glowing eyes" effect.
    /// The game is also hardcoded to automatically convert quotation mark `GLOWTILES` into
    /// apostrophes if the creature has lost one eye. This token works at the generic creature level - for
    /// caste-specific glow tiles, use `[CASTE_GLOWTILE]` instead.
    #[df_token(token_name = "GLOWTILE")]
    pub glowtile: Option<DFChar>,
    /// Creature is considered good and will only show up in good biomes - unicorns, for example.
    /// Civilizations with `[USE_GOOD_ANIMALS]` can domesticate them regardless of exotic status.
    /// Has no effect on cavern creatures except to restrict taming. A civilization that has good
    /// creatures can colonise good areas in world-gen.
    #[df_token(token_name = "GOOD")]
    pub good: Option<()>,
    /// Found on generated angels. This is the historical figure ID of the deity to which the
    /// angel is associated.
    #[df_token(token_name = "HFID")]
    #[df_issue(
        severity = "INFO",
        note = "HFID's are generated during worldgen, so can not really be used in mods."
    )]
    pub hfid: Option<u32>,
    /// What product is harvested from beekeeping.
    #[df_token(token_name = "HIVE_PRODUCT")]
    pub hive_product: Vec<(
        u32,
        u32,
        ItemReferenceArg,
        MaterialTokenArgWithLocalCreatureMat,
    )>,
    /// Creature can spawn as a wild animal in the appropriate biomes.
    #[df_token(token_name = "LARGE_ROAMING")]
    pub large_roaming: Option<()>,
    /// Allows you to play as a wild animal of this species in adventurer mode. Prevents trading of
    /// (tame) instances of this creature in caravans.
    #[df_token(token_name = "LOCAL_POPS_CONTROLLABLE")]
    pub local_pops_controllable: Option<()>,
    /// The creatures will scatter if they have this tag, or form tight packs if they don't.
    #[df_token(token_name = "LOOSE_CLUSTERS")]
    pub loose_clusters: Option<()>,
    /// Marks if the creature is an actual real-life creature. Only used for worlfeb "age" names
    /// at present.
    #[df_token(token_name = "MUNDANE")]
    pub mundane: Option<()>,
    /// The generic name for any creature of this type - will be used when distinctions between
    /// caste are unimportant. For names for specific castes, use `[CASTE_NAME]` instead. If left
    /// undefined, the creature will be labeled as "nothing" by the game.
    #[df_token(token_name = "NAME")]
    pub name: Option<(String, String, String)>,
    /// Sets what other creatures prefer about this creature. "Urist likes dwarves for their
    /// beards." Multiple entries will be chosen from at random. Creatures lacking a `PREFSTRING`
    /// token will never appear under another's preferences.
    #[df_token(token_name = "PREFSTRING")]
    pub prefstring: Vec<String>,
    /// The generic name for members of this profession, at the creature level. In order to give
    /// members of specific castes different names for professions, use `[CASTE_PROFESSION_NAME]`
    /// instead.
    #[df_token(token_name = "PROFESSION_NAME")]
    pub profession_name: Vec<(UnitTypeEnum, String, String)>,
    /// The creature will only show up in "savage" biomes. Has no effect on cavern creatures. Cannot
    /// be combined with `[GOOD]` or `[EVIL]`.
    #[df_token(token_name = "SAVAGE")]
    pub savage: Option<()>,
    /// Determines how keen a creature's sense of smell is. Lower is better. At 10000, a creature
    /// cannot smell at all.
    #[df_token(token_name = "SMELL_TRIGGER")]
    pub smell_trigger: Option<u32>,
    /// If this creature is active in its civilization's military, it will blink between its default
    /// tile and this one.
    #[df_token(token_name = "SOLDIER_ALTTILE")]
    pub soldier_alttile: Option<DFChar>,
    /// Boasting speeches relating to killing this creature. Examples include `dwarf.txt` and `elf.txt`
    /// in `data\speech`. Must end in `.txt`.
    #[df_token(token_name = "SPEECH")]
    pub speech: Option<String>, // TODO: string is a txt path
    /// Boasting speeches relating to killing females of this creature. Must end in `.txt`
    #[df_token(token_name = "SPEECH_FEMALE")]
    pub speech_female: Option<String>, // TODO: string is a txt path
    /// Boasting speeches relating to killing males of this creature. Must end in `.txt`
    #[df_token(token_name = "SPEECH_MALE")]
    pub speech_male: Option<String>, // TODO: string is a txt path
    /// Sets what religious spheres the creature is aligned to, for purposes of being worshipped via
    /// the `[POWER]` token. Also affects the layout of hidden fun stuff, and the creature's name.
    #[df_token(token_name = "SPHERE")]
    pub sphere: Vec<SphereEnum>,
    /// Possibly means that a large swarm of this vermin can be disturbed, usually in adventurer mode.
    /// Needs verification.
    #[df_token(token_name = "TRIGGERABLE_GROUP")]
    pub triggerable_group: Option<(u32, u32)>,
    /// Creature will occur in every region with the correct biome. Does not apply to `[EVIL]`/`[GOOD]`
    /// tags.
    #[df_token(token_name = "UBIQUITOUS")]
    pub ubiquitous: Option<()>,
    /// Depth that the creature appears underground. Numbers can be from 0 to 5. 0 is actually
    /// 'above ground' and can be used if the creature is to appear both above and below ground.
    ///
    /// Values from 1 to 3 are the respective cavern levels, 4 is the magma sea and 5 is the `HFS`.
    /// A single argument may be used instead of min and max. Demons use only 5:5; user-defined
    /// creatures with both this depth and `[FLIER]` will take part in the initial wave from the
    /// HFS alongside generated demons. Without `[FLIER]`, they will only spawn from the map
    /// edges.
    ///
    /// Civilizations that can use underground plants or animals will only export (via the embark
    /// screen or caravans) things that are available at depth 1.
    #[df_token(token_name = "UNDERGROUND_DEPTH")]
    pub underground_depth: Option<(u32, Option<u32>)>,
    /// The vermin creature will attempt to eat exposed food. See `[PENETRATEPOWER]`. Distinct from
    /// `[VERMIN_ROTTER]`.
    #[df_token(token_name = "VERMIN_EATER")]
    pub vermin_eater: Option<()>,
    /// The vermin appears in water and will attempt to swim around.
    #[df_token(token_name = "VERMIN_FISH")]
    pub vermin_fish: Option<()>,
    /// The creature appears in "general" surface ground locations. Note that this doesn't stop the
    /// creature from flying if it can (most vermin birds have this tag).
    #[df_token(token_name = "VERMIN_GROUNDER")]
    pub vermin_grounder: Option<()>,
    /// The vermin are attracted to rotting stuff and loose food left in the open and cause unhappy
    /// thoughts to dwarves who encounter them. Present on flies, knuckle worms, acorn flies, and
    /// blood gnats. May speed up decay, but this is not verified.
    #[df_token(token_name = "VERMIN_ROTTER")]
    pub vermin_rotter: Option<()>,
    /// The creature randomly appears near dirt or mud, and may be uncovered by creatures that have
    /// the `[ROOT_AROUND]` interaction such as geese and chickens. Dwarves will ignore the creature
    /// when given the "Capture live land animal" task.
    #[df_token(token_name = "VERMIN_SOIL")]
    pub vermin_soil: Option<()>,
    /// The vermin will appear in a single tile cluster of many vermin, such as a colony of ants.
    #[df_token(token_name = "VERMIN_SOIL_COLONY")]
    pub vermin_soil_colony: Option<()>,
    // endregion ==================================================================================
    // region: Caste Tokens =======================================================================
    /// A list of `[ATTACK:...]` tokens this creature caste has.
    #[df_token(token_name = "ATTACK")]
    pub attacks: Vec<Attack>,
    /// List of interactions the creature can perform.
    #[df_token(token_name = "CAN_DO_INTERACTION")]
    pub can_do_interaction: Vec<CanDoInteraction>,
    /// A list of `SET_BP_GROUP` tokens on this creature.
    #[df_token(token_name = "SET_BP_GROUP")]
    pub set_bp_group: Vec<SetBpGroup>,
    /// A list of `SET_TL_GROUP` tokens on this creature.
    #[df_token(token_name = "SET_TL_GROUP")]
    pub set_tl_group: Vec<SetTlGroup>,
    /// A list of `SElECT_TISSUE_LAYER` tokens on this creature.
    #[df_token(token_name = "SELECT_TISSUE_LAYER")]
    pub select_tissue_layer: Vec<SelectTissueLayer>,
    /// A list of `TISSUE_LAYER` tokens on this creature.
    #[df_token(token_name = "TISSUE_LAYER")]
    pub tissue_layer: Vec<TissueLayer>,
    /// Presumably a counterpart to `TISSUE_LAYER_UNDER`, which adds a tissue layer over a given part.
    #[df_token(token_name = "TISSUE_LAYER_OVER")]
    // TODO: the optional reference here is weird and needs some research;
    // look for this token in `creature_masterwork_fish.txt` to see what I mean.
    pub tissue_layer_over: Vec<(BpCriteriaTokenArg, Reference, Option<Reference>)>,
    /// Adds the tissue layer under a given part.
    ///
    /// For example an Iron Man has a gaseous poison within and this tissue (`GAS` is its name) has
    /// the token `[TISSUE_LEAKS]` and its state is `GAS`, so when you puncture the iron outside and
    /// damage this tissue it leaks gas (can have a syndrome by using a previous one in the creature
    /// sample).
    #[df_token(token_name = "TISSUE_LAYER_UNDER")]
    pub tissue_layer_under: Vec<(BpCriteriaTokenArg, Reference)>,
    /// Prevents tamed creature from being made available for adoption, instead allowing it to
    /// automatically adopt whoever it wants. The basic requirements for adoption are intact, and
    /// the creature will only adopt individuals who have a preference for their species. Used by
    /// cats in the vanilla game.
    #[df_token(token_name = "ADOPTS_OWNER")]
    pub adopts_owner: Option<()>,
    /// Makes the creature need alcohol to get through the working day; it will choose to drink
    /// booze instead of water if possible. Going sober for too long reduces speed.
    #[df_token(token_name = "ALCOHOL_DEPENDENT")]
    pub alcohol_dependent: Option<()>,
    /// Sets the creature to be active during the day, night, and twilight in Adventurer Mode. Seems
    /// to be a separate value from `[DIURNAL]`/`[NOCTURNAL]`/`[CREPUSCULAR]`, rather than implying them.
    #[df_token(token_name = "ALL_ACTIVE")]
    pub all_active: Option<()>,
    /// Found on `[LARGE_PREDATOR]`s who ambush their prey. Instead of charging relentlessly at prey,
    /// the predator will wait till the prey is within a few squares before charging. May or may not
    /// work on other creatures (unverified).
    #[df_token(token_name = "AMBUSHPREDATOR")]
    pub ambushpredator: Option<()>,
    /// Allows a creature to breathe both inside and outside of water (unlike `[AQUATIC]`) - does
    /// not prevent drowning in magma.
    #[df_token(token_name = "AMPHIBIOUS")]
    pub amphibious: Option<()>,
    /// Applies the specified creature variation.
    ///
    /// In addition to the required `CREATURE_VARIATION` object ID, you may give extra arguments;
    /// These extra arguments will replace instances of `!ARGn` in the creature variation, where
    /// `n` is the index of the argument given to `APPLY_CREATURE_VARIATION`; the first argument
    /// will replace any "`!ARG1`", the second any "`!ARG2`" and so on. You may use any number of arguments.
    ///
    /// If you have an `!ARGn` of a higher number than the arguments given, the replacements will
    /// act oddly, depending on if `n` has more digits than the number of arguments given.
    /// For example, `[APPLY_CREATURE_VARIATION:EXAMPLE_CV:one:two:three]`, which has three
    /// arguments given, will leave all instances of `!ARG5` in `EXAMPLE_CV` intact as `!ARG5`,
    /// but `!ARG10` will become `one0`.
    ///
    /// The pipe character `|` is turned into `:` when inserted, so single arguments in
    /// `APPLY_CREATURE_VARIATION` may be turned into multi-argument segments in the output.
    ///  For example, with this creature variation:
    ///
    /// ```df_raw
    /// [CREATURE_VARIATION:DIMORPHIC_FEATHER_COLORS]
    ///     [CV_NEW_TAG:SELECT_CASTE:FEMALE]
    ///         [CV_NEW_TAG:SET_TL_GROUP:BY_CATEGORY:ALL:FEATHER]
    ///             [CV_NEW_TAG:TL_COLOR_MODIFIER:!ARG1]
    ///                 [CV_NEW_TAG:TLCM_NOUN:feathers:PLURAL]
    ///     [CV_NEW_TAG:SELECT_CASTE:MALE]
    ///         [CV_NEW_TAG:SET_TL_GROUP:BY_CATEGORY:ALL:FEATHER]
    ///             [CV_NEW_TAG:TL_COLOR_MODIFIER:!ARG2]
    ///                 [CV_NEW_TAG:TLCM_NOUN:feathers:PLURAL]
    /// ```
    /// If you use `[APPLY_CREATURE_VARIATION:DIMORPHIC_FEATHER_COLORS:BROWN|1:BLUE|10|GREEN|1|BLACK|1]`,
    /// the following is what will be added to the creature:
    ///
    /// ```df_raw
    /// [SELECT_CASTE:FEMALE]
    ///     [SET_TL_GROUP:BY_CATEGORY:ALL:FEATHER]
    ///     [TL_COLOR_MODIFIER:BROWN:1]
    ///         [TLCM_NOUN:feathers:PLURAL]
    /// [SELECT_CASTE:MALE]
    ///     [SET_TL_GROUP:BY_CATEGORY:ALL:FEATHER]
    ///     [TL_COLOR_MODIFIER:BLUE:10:GREEN:1:BLACK:1]
    ///         [TLCM_NOUN:feathers:PLURAL]
    /// ```
    #[df_token(token_name = "APPLY_CREATURE_VARIATION")]
    pub apply_creature_variation: Vec<(
        ReferenceTo<CreatureVariationToken>,
        Option<(Vec<AllowEmpty<Any>>,)>,
    )>,
    /// Enables the creature to breathe in water, but causes it to air-drown on dry land.
    #[df_token(token_name = "AQUATIC")]
    pub aquatic: Option<()>,
    /// Causes the creature to be excluded from the object testing arena's creature spawning list.
    ///
    /// Typically applied to spoileriffic creatures.
    #[df_token(token_name = "ARENA_RESTRICTED")]
    pub arena_restricted: Option<()>,
    /// Prevents the creature from attacking or frighten creatures with the `[NATURAL]` tag.
    #[df_token(token_name = "AT_PEACE_WITH_WILDLIFE")]
    pub at_peace_with_wildlife: Option<()>,
    /// Specifies when a megabeast or semi-megabeast will attack the fortress. The attacks will
    /// start occuring when at least one of the requirements is met. Setting a value to 0 disables
    /// the trigger.
    ///
    /// Arguments: population, exported wealth, created wealth.
    #[df_token(token_name = "ATTACK_TRIGGER")]
    pub attack_trigger: Option<(u32, u32, u32)>,
    /// Age at which creature is considered a child, the default is zero. One can think of this as
    /// the duration of the baby stage.
    #[df_token(token_name = "BABY")]
    pub baby: Option<u32>,
    /// Defines a new name for a creature in baby state at the caste level. For non-caste-specific
    /// baby names, see `[GENERAL_BABY_NAME]`.
    ///
    /// Arguments: singular, plural
    #[df_token(token_name = "BABYNAME")]
    pub babyname: Option<(String, String)>,
    /// Creature may be subject to beaching, becoming stranded on shores, where they will eventually
    /// air-drown. The number indicates the frequency of the occurrence. Presumably requires the
    /// creature to be `[AQUATIC]`. Used by orcas, sperm whales and sea nettle jellyfish in the
    /// vanilla game.
    #[df_token(token_name = "BEACH_FREQUENCY")]
    pub beach_frequency: Option<u32>,
    /// The creature is non-aggressive by default, and will never automatically be engaged by
    /// companions or soldiers. It will run away from any creatures that are not friendly to it, and
    /// will only defend itself if it becomes enraged. Can be thought of as the counterpoint of the
    /// `[LARGE_PREDATOR]` tag. When tamed, animals with this tag will be useless for fortress
    /// defense.
    #[df_token(token_name = "BENIGN")]
    pub benign: Option<()>,
    /// Specifies what the creature's blood is made of.
    #[df_token(token_name = "BLOOD")]
    pub blood: Option<(MaterialTokenArgWithLocalCreatureMat, MaterialStateEnum)>,
    /// Causes vampire-like behaviour; the creature will suck the blood of unconscious victims when
    /// its thirst for blood grows sufficiently large. When controlling the creature in adventure
    /// mode, this can be done at will. Seems to be required to make the creature denouncable as a
    /// creature of the night.
    #[df_token(token_name = "BLOODSUCKER")]
    pub bloodsucker: Option<()>,
    /// Draws body parts from `OBJECT:BODY` files (such as `body_default.txt`)
    ///
    /// For example:
    ///
    /// `[BODY:BODY_WITH_HEAD_FLAG:HEART:GUTS:BRAIN:MOUTH]`
    ///
    /// This is the body from a purring maggot. It creates a body with head, a heart, some guts, a
    /// brain, and a mouth. That's all a maggot needs.
    ///
    /// **If the body is left undefined, the creature will cause a crash whenever it spawns.**
    #[df_token(token_name = "BODY")]
    pub body: Option<(Vec<ReferenceTo<BodyToken>>,)>,
    /// A list of `BODY_APPEARANCE_MODIFIER` tokens on this creature.
    #[df_token(token_name = "BODY_APPEARANCE_MODIFIER")]
    pub body_appearance_modifier: Vec<BodyAppearanceModifier>,
    /// Loads a plan listed in `OBJECT:BODY_DETAIL_PLAN` files, such as `b_detail_plan_default.txt`. Mass
    /// applies `USE_MATERIAL_TEMPLATE`, mass alters `RELSIZE`, alters body part positions, and will
    /// allow tissue layers to be defined. Tissue layers are defined in order of skin to bone here.
    ///
    /// For example:
    ///
    /// `[BODY_DETAIL_PLAN:VERTEBRATE_TISSUE_LAYERS:SKIN:FAT:MUSCLE:BONE:CARTILAGE]`
    ///
    /// This creates the detailed body of a fox, the skin, fat, muscle, bones and cartilage out of
    /// the vertebrate tissues.
    ///
    /// A maggot would only need:
    ///
    /// `[BODY_DETAIL_PLAN:EXOSKELETON_TISSUE_LAYERS:SKIN:FAT:MUSCLE]`
    #[df_token(token_name = "BODY_DETAIL_PLAN")]
    pub body_detail_plan: Vec<(
        ReferenceTo<BodyDetailPlanToken>,
        Option<Reference>,
        Option<Reference>,
        Option<Reference>,
        Option<Reference>,
        Option<Reference>,
    )>,
    /// Sets size at a given age. Size is in cubic centimeters, and for normal body materials, is
    /// roughly equal to the creature's average weight in grams.
    ///
    /// For example, this is the size of a minotaur:
    ///
    /// `[BODY_SIZE:0:0:10000]`
    ///
    /// `[BODY_SIZE:1:168:50000]`
    ///
    /// `[BODY_SIZE:12:0:220000]`
    ///
    /// Its birth size would be 10,000 cm3 (~10 kg). At 1 year and 168 days old it would be
    /// 50,000 cm3 (~50 kg). And as an adult (at 12 years old) it would be 220,000 cm3 and weigh
    /// roughly 220 kg.
    #[df_token(token_name = "BODY_SIZE")]
    pub body_size: Vec<(u32, u32, u32)>,
    /// Substitutes body part text with replacement text. Draws gloss information from `OBJECT:BODY`
    /// files (such as `body_default.txt`)
    #[df_token(token_name = "BODYGLOSS")]
    pub bodygloss: Vec<(Vec<ReferenceTo<BodyGlossToken>>,)>,
    /// Creature eats bones. Implies `[CARNIVORE]`.
    #[df_token(token_name = "BONECARN")]
    #[df_issue(
        since = "0.44.12",
        link = "https://www.bay12games.com/dwarves/mantisbt/view.php?id=11069",
        note = "Adventurers with this token are currently unable to eat bones.",
        severity = "INFO"
    )]
    pub bonecarn: Option<()>,
    /// Allows a creature to destroy furniture and buildings. Value `[1]` targets mostly doors,
    /// hatches, furniture and the like. Value `[2]` targets any building/structure other than
    /// floors, walls, and stairs.
    #[df_token(token_name = "BUILDINGDESTROYER")]
    pub buildingdestroyer: Option<u8>,
    /// The creature gains skills and can have professions. If a member of a civilization (even a
    /// pet) has this token, it'll need to eat, drink and sleep. Note that this token makes the
    /// creature unable to be eaten by an adventurer, so it is not recommended for uncivilized
    /// monsters. Adventurers lacking this token can allocate but not increase attributes and
    /// skills. Skills allocated will disappear on start.
    #[df_token(token_name = "CAN_LEARN")]
    pub can_learn: Option<()>,
    /// Can talk. Note that this is not necessary for a creature to gain social skills.
    #[df_token(token_name = "CAN_SPEAK")]
    pub can_speak: Option<()>,
    /// Creature cannot climb, even if it has free grasp parts.
    #[df_token(token_name = "CANNOT_CLIMB")]
    pub cannot_climb: Option<()>,
    /// Creature cannot jump.
    #[df_token(token_name = "CANNOT_JUMP")]
    pub cannot_jump: Option<()>,
    /// Acts like `[NOT_LIVING]`, except that `[OPPOSED_TO_LIFE]` creatures will attack them.
    #[df_token(token_name = "CANNOT_UNDEAD")]
    pub cannot_undead: Option<()>,
    /// Allows the creature to open doors that are set to be unpassable for pets. In adventure mode,
    /// creatures lacking this token will be unable to pass through door tiles except whilst these
    /// are occupied by other creatures.
    #[df_token(token_name = "CANOPENDOORS")]
    pub canopendoors: Option<()>,
    /// Creature only eats meat. If the creature goes on rampages in worldgen, it will often devour
    /// the people/animals it kills.
    #[df_token(token_name = "CARNIVORE")]
    pub carnivore: Option<()>,
    /// Caste-specific `[ALTTILE]`; if set, the creature of this caste will blink between its
    /// `[CASTE_TILE]` and its `CASTE_ALTTILE`.
    #[df_token(token_name = "CASTE_ALTTILE")]
    pub caste_alttile: Option<DFChar>,
    /// Caste-specific `[COLOR]`. Color of the creature's tile.
    /// See [Color](https://dwarffortresswiki.org/index.php/Color) for usage.
    #[df_token(token_name = "CASTE_COLOR")]
    pub caste_color: Option<(u8, u8, u8)>,
    /// Caste-specific `[GLOWCOLOR]`; The colour of the creature/caste's `[CASTE_GLOWTILE]`.
    #[df_token(token_name = "CASTE_GLOWCOLOR")]
    pub caste_glowcolor: Option<(u8, u8, u8)>,
    /// Caste-specific `[GLOWTILE]`; if present, the being glows in the dark (generally used for
    /// Adventurer Mode). The tile is what replaces the being's current tile when it is obscured
    /// from your sight by darkness.
    #[df_token(token_name = "CASTE_GLOWTILE")]
    pub caste_glowtile: Option<DFChar>,
    /// Caste-specific `[NAME]`; the generic name for any creature of this type and caste.
    #[df_token(token_name = "CASTE_NAME")]
    pub caste_name: Option<(String, String, String)>,
    /// Caste-specific `[PROFESSION_NAME]`; the generic name for members of this profession,
    /// of this caste.
    #[df_token(token_name = "CASTE_PROFESSION_NAME")]
    pub caste_profession_name: Vec<(UnitTypeEnum, String, String)>,
    /// Caste-specific `[SOLDIER_ALTTILE]`; if this creature of this caste is active in its
    /// civilization's military, it will blink between its default tile and this one.
    /// Requires `[CASTE_SOLDIER_TILE]`.
    #[df_token(token_name = "CASTE_SOLDIER_ALTTILE")]
    pub caste_soldier_alttile: Option<DFChar>,
    /// Caste-specific `[CREATURE_SOLDIER_TILE]`; creatures of this caste active in their
    /// civilization's military will use this tile instead.
    #[df_token(token_name = "CASTE_SOLDIER_TILE")]
    pub caste_soldier_tile: Option<DFChar>,
    /// Caste-specific version of `[SPEECH]`; boasting speeches relating to killing this creature
    /// of this caste. Examples of `SPEECH` include `dwarf.txt` and `elf.txt` in `data\speech`.
    /// Must end in `.txt`.
    #[df_token(token_name = "CASTE_SPEECH")]
    pub caste_speech: Option<String>, // TODO: string is a txt path
    /// Caste-specific `[CREATURE_TILE]`; the symbol of the creature of this caste in ASCII mode.
    #[df_token(token_name = "CASTE_TILE")]
    pub caste_tile: Option<DFChar>,
    /// Gives the creature a bonus in caves. Also causes cave adaptation.
    #[df_token(token_name = "CAVE_ADAPT")]
    pub cave_adapt: Option<()>,
    /// Multiplies body size by a factor of (integer)%. 50 halves size, 200 doubles.
    #[df_token(token_name = "CHANGE_BODY_SIZE_PERC")]
    pub change_body_size_perc: Option<u32>,
    /// Age at which creature is considered an adult. One can think of this as the duration of the
    /// child stage. Allows the creature's offspring to be rendered fully tame if trained during
    /// their childhood.
    #[df_token(token_name = "CHILD")]
    pub child: Option<u32>,
    /// Defines a new name for a creature in child state at the caste level. For non-caste-specific
    /// child names, see `[GENERAL_CHILD_NAME]`.
    #[df_token(token_name = "CHILDNAME")]
    pub childname: Option<(String, String)>,
    /// Number of eggs laid in one sitting.
    #[df_token(token_name = "CLUTCH_SIZE")]
    pub clutch_size: Option<(u32, u32)>,
    /// Caste hovers around colony.
    #[df_token(token_name = "COLONY_EXTERNAL")]
    pub colony_external: Option<()>,
    /// When combined with any of `[PET]`, `[PACK_ANIMAL]`, `[WAGON_PULLER]` and/or `[MOUNT]`, the
    /// creature is guaranteed to be domesticated by any civilization with `[COMMON_DOMESTIC_PET]`,
    /// `[COMMON_DOMESTIC_PACK]`.
    ///
    /// `[COMMON_DOMESTIC_PULL]` and/or `[COMMON_DOMESTIC_MOUNT]` respectively. Such civilizations
    /// will always have access to the creature, even in the absence of wild populations. This token
    /// is invalid on `[FANCIFUL]` creatures.
    #[df_token(token_name = "COMMON_DOMESTIC")]
    pub common_domestic: Option<()>,
    /// Creatures of this caste's species with the `[SPOUSE_CONVERTER]` and
    /// `[NIGHT_CREATURE_HUNTER]` tokens will kidnap `[SPOUSE_CONVERSION_TARGET]`s of an appropriate
    /// sex and convert them into castes with `CONVERTED_SPOUSE`.
    #[df_token(token_name = "CONVERTED_SPOUSE")]
    pub converted_spouse: Option<()>,
    /// Set this to allow the creature to be cooked in meals without first being butchered/cleaned.
    /// Used by some water-dwelling vermin such as mussels, nautiluses and oysters.
    #[df_token(token_name = "COOKABLE_LIVE")]
    pub cookable_live: Option<()>,
    /// Creature is 'berserk' and will attack all other creatures, except members of its own species
    /// that also have the `CRAZED` tag. It will show "Berserk" in the unit list. Berserk creatures go
    /// on rampages during worldgen much more frequently than non-berserk ones.
    #[df_token(token_name = "CRAZED")]
    pub crazed: Option<()>,
    /// An arbitrary creature classification. Can be set to anything, but only existing uses are
    /// `GENERAL_POISON` (used in syndromes), `EDIBLE_GROUND_BUG` (valid targets for `GOBBLE_VERMIN_x`
    /// tokens), and `MAMMAL` (self-explanatory). A single creature can have multiple classes.
    /// Eligibility for certain entity positions can also be permitted or restricted by this tag.
    #[df_token(token_name = "CREATURE_CLASS")]
    pub creature_class: Vec<Reference>,
    /// Sets the creature to be active at twilight in adventurer mode.
    #[df_token(token_name = "CREPUSCULAR")]
    pub crepuscular: Option<()>,
    /// Allows a creature to steal and eat edible items from a site. It will attempt to grab a food
    /// item and immediately make its way to the map's edge, where it will disappear with it. If the
    /// creature goes on rampages during worldgen, it will often steal food instead of attacking.
    /// Trained and tame instances of the creature will no longer display this behavior.
    #[df_token(token_name = "CURIOUSBEAST_EATER")]
    pub curiousbeast_eater: Option<()>,
    /// Allows a creature to (very quickly) drink your alcohol. Or spill the barrel to the ground.
    /// Also affects undead versions of the creature. Unlike food or item thieves, drink thieves
    /// will consume your alcohol on the spot rather than run away with one piece of it. Trained and
    /// tame instances of the creature will no longer display this behavior.
    #[df_token(token_name = "CURIOUSBEAST_GUZZLER")]
    pub curiousbeast_guzzler: Option<()>,
    /// Allows a creature to steal things (apparently, of the highest value it can find). It will
    /// attempt to grab an item of value and immediately make its way to the map's edge, where it
    /// will disappear with it. If the creature goes on rampages in worldgen, it will often steal
    /// items instead of attacking - kea birds are infamous for this. Trained and tame instances of
    /// the creature will no longer display this behavior. Also, makes the creature unable to drop
    /// hauled items until it enters combat.
    #[df_token(token_name = "CURIOUSBEAST_ITEM")]
    pub curiousbeast_item: Option<()>,
    /// Found on generated demons. Marks the caste to be used in the initial wave after breaking
    /// into the underworld, and by the demon civilizations created during world-gen breachings.
    /// Could not be specified in user-defined raws until version `0.47.01`.
    #[df_token(token_name = "DEMON")]
    pub demon: Option<()>,
    /// A brief description of the creature type, as displayed when viewing the creature's
    /// description/thoughts & preferences screen.
    #[df_token(token_name = "DESCRIPTION")]
    pub description: Option<String>,
    /// Dies upon attacking. Used by honey bees to simulate them dying after using their stingers.
    #[df_token(token_name = "DIE_WHEN_VERMIN_BITE")]
    pub die_when_vermin_bite: Option<()>,
    /// Increases experience gain during adventure mode. Creatures with 11 or higher are not
    /// assigned for quests in adventure mode.
    #[df_token(token_name = "DIFFICULTY")]
    pub difficulty: Option<u32>,
    /// Sets the creature to be active during the day in Adventurer Mode.
    #[df_token(token_name = "DIURNAL")]
    pub diurnal: Option<()>,
    /// The creature hunts vermin by diving from the air. On tame creatures it works the same as
    /// normal `[HUNTS_VERMIN]`. Found on peregrine falcons.
    #[df_token(token_name = "DIVE_HUNTS_VERMIN")]
    pub dive_hunts_vermin: Option<()>,
    /// Defines the material composition of eggs laid by the creature. Egg-laying creatures in the
    /// default game define this 3 times, using `LOCAL_CREATURE_MAT:EGGSHELL`,
    /// `LOCAL_CREATURE_MAT:EGG_WHITE`, and then `LOCAL_CREATURE_MAT:EGG_YOLK`. Eggs will be made out of
    /// eggshell. Edibility is determined by tags on whites or yolk, but they otherwise do not
    /// exist.
    #[df_token(token_name = "EGG_MATERIAL")]
    pub egg_material: Vec<(MaterialTokenArgWithLocalCreatureMat, MaterialStateEnum)>,
    /// Determines the size of laid eggs. Doesn't affect hatching or cooking, but bigger eggs will
    /// be heavier, and may take longer to be hauled depending on the hauler's strength.
    #[df_token(token_name = "EGG_SIZE")]
    pub egg_size: Option<u32>,
    /// Allows the creature to wear or wield items.
    #[df_token(token_name = "EQUIPS")]
    pub equips: Option<()>,
    /// A list of `EXTRA_BUTCHER_OBJECT` tokens on this creature.
    #[df_token(token_name = "EXTRA_BUTCHER_OBJECT")]
    pub extra_butcher_object: Vec<ExtraButcherObject>,
    /// Defines a creature extract which can be obtained via small animal dissection.
    #[df_token(token_name = "EXTRACT")]
    pub extract: Option<MaterialTokenArgWithLocalCreatureMat>,
    /// Creature can see regardless of whether it has working eyes and has full 360 degree vision,
    /// making it impossible to strike the creature from a blind spot in combat.
    #[df_token(token_name = "EXTRAVISION")]
    pub extravision: Option<()>,
    /// Found on subterranean animal-man tribals. Currently defunct. In previous versions, it caused
    /// these creatures to crawl out of chasms and underground rivers.
    #[df_token(token_name = "FEATURE_ATTACK_GROUP")]
    pub feature_attack_group: Option<()>,
    /// Found on forgotten beasts. Presumably makes it act as such, initiating underground attacks on
    /// fortresses, or leads to the pop-up message upon encountering one.
    ///
    /// Hides the creature from displaying in a world_sites_and_pops file, and does not create historical
    /// figures like generated forgotten beasts do.
    ///
    /// Requires specifying a `[BIOME]` in which the creature will live, and both surface and subterranean
    /// biomes are allowed. Does not stack with `[LARGE_ROAMING]` and if both are used the creature will
    /// not spawn. Appears to be incompatible with `[DEMON]` even if used in separate castes.
    ///
    /// Could not be specified in user-defined raws until version `0.47.01`.
    #[df_token(token_name = "FEATURE_BEAST")]
    pub feature_beast: Option<()>,
    /// Makes the creature biologically female, enabling her to bear young. Usually specified inside
    /// a caste.
    #[df_token(token_name = "FEMALE")]
    pub female: Option<()>,
    /// Makes the creature immune to `FIREBALL` and `FIREJET` attacks, and allows it to path through
    /// high temperature zones, like lava or fires. Does not, by itself, make the creature immune to
    /// the damaging effects of burning in fire, and does not prevent general heat damage or melting
    /// on its own (this would require adjustments to be made to the creature's body materials - see
    /// the dragon raws for an example).
    #[df_token(token_name = "FIREIMMUNE")]
    pub fireimmune: Option<()>,
    /// Like `[FIREIMMUNE]`, but also renders the creature immune to `DRAGONFIRE` attacks.
    #[df_token(token_name = "FIREIMMUNE_SUPER")]
    pub fireimmune_super: Option<()>,
    /// The creature's corpse is a single `FISH_RAW` food item that needs to be cleaned (into a
    /// `FISH` item) at a fishery to become edible. Before being cleaned the item is referred to as
    /// "raw". The food item is categorized under "fish" on the food and stocks screens, and when
    /// uncleaned it is sorted under "raw fish" in the stocks (but does not show up on the food
    /// screen).
    ///
    /// Without this or `[COOKABLE_LIVE]`, fished vermin will turn into food the same way as non-
    /// vermin creatures, resulting in multiple units of food (meat, brain, lungs, eyes, spleen
    /// etc.) from a single fished vermin. These units of food are categorized as meat by the game.
    #[df_token(token_name = "FISHITEM")]
    pub fishitem: Option<()>,
    /// The creature's body is constantly at this temperature, heating up or cooling the surrounding
    /// area. Alters the temperature of the creature's inventory and all adjacent tiles, with all
    /// the effects that this implies. May trigger wildfires at high enough values. Also makes the
    /// creature immune to extreme heat or cold as long as the temperature set is not harmful to the
    /// materials that the creature is made from.
    ///
    /// Note that temperatures of 12000 and higher may cause pathfinding issues in fortress mode.
    #[df_token(token_name = "FIXED_TEMP")]
    pub fixed_temp: Option<u32>,
    /// If engaged in combat, the creature will flee at the first sign of resistance. Used by
    /// kobolds in the vanilla game.
    #[df_token(token_name = "FLEEQUICK")]
    pub fleequick: Option<()>,
    /// Allows a creature to fly, independent of it having wings or not. Fortress Mode pathfinding
    /// only partially incorporates flying - flying creatures need a land path to exist between them
    /// and an area in order to access it, but as long as one such path exists, they do not need to
    /// use it, instead being able to fly over intervening obstacles. Winged creatures with this
    /// token can lose their ability to fly if their wings are crippled or severed. Winged creatures
    /// without this token will be unable to fly. (A 'wing' in this context refers to any body part
    /// with its own `FLIER` token).
    #[df_token(token_name = "FLIER")]
    pub flier: Option<()>,
    /// Defines a gait by which the creature can move.
    /// See [Gait](https://dwarffortresswiki.org/index.php/Gait) for more information.
    ///
    /// - `<max speed>` indicates the maximum speed achievable by a creature using this gait.
    ///
    /// - `<build up time>` indicates how long it will take for a creature using this gait to go from
    /// `<start speed>` to `<max speed>`. For example, a value of 10 means that it should be able to
    /// reach the maximum speed by moving 10 tiles in a straight line over even terrain.
    ///
    /// - `<max turning speed>` indicates the maximum speed permissible when the creature suddenly
    /// changes its direction of motion. The creature's speed will be reduced to `<max turning speed>`
    /// if travelling at a higher speed than this before turning.
    ///
    /// - `<start speed>` indicates the creature's speed when it starts moving using this gait.
    ///
    /// - `<energy expenditure>` indicates how energy-consuming the gait is. Higher values cause the
    /// creature to tire out faster. Persistent usage of a high-intensity gait will eventually lead
    /// to exhaustion and collapse.
    ///
    /// You may use `NO_BUILD_UP` instead of `<build up time>`, `<max turning speed>` and `<start speed>`.
    ///
    /// It's possible to specify a `<start speed>` greater than the `<max speed>`; the moving creature
    /// will decelerate towards its `<max speed>` in this case.
    #[df_token(token_name = "GAIT")]
    pub gait: Vec<(
        GaitTypeEnum,
        String,
        u32,
        Choose<NoBuildUpEnum, (u32, u32, u32)>,
        u32,
        Option<GaitFlagTokenArg>,
    )>,
    /// Has the same function as `[MATERIAL_FORCE_MULTIPLIER]`, but applies to all attacks instead
    /// of just those involving a specific material. Appears to be overridden by
    /// `MATERIAL_FORCE_MULTIPLIER` (werebeasts, for example, use both tokens to provide resistance
    /// to all materials, with one exception to which they are especially vulnerable).
    #[df_token(token_name = "GENERAL_MATERIAL_FORCE_MULTIPLIER")]
    pub general_material_force_multiplier: Option<(u32, u32)>,
    /// Makes the creature get infections from necrotic tissue.
    #[df_token(token_name = "GETS_INFECTIONS_FROM_ROT")]
    pub gets_infections_from_rot: Option<()>,
    /// Makes the creature's wounds become infected if left untreated for too long.
    #[df_token(token_name = "GETS_WOUND_INFECTIONS")]
    pub gets_wound_infections: Option<()>,
    /// The creature can and will gnaw its way out of animal traps and cages using the specified
    /// verb, depending on the material from which it is made (normally wood).
    #[df_token(token_name = "GNAWER")]
    pub gnawer: Option<String>,
    /// The creature eats vermin of the specified class.
    #[df_token(token_name = "GOBBLE_VERMIN_CLASS")]
    pub gobble_vermin_class: Vec<Reference>, // TODO: ref is creature class
    /// The creature eats a specified vermin.
    #[df_token(token_name = "GOBBLE_VERMIN_CREATURE")]
    pub gobble_vermin_creature: Vec<(ReferenceTo<CreatureToken>, Reference)>,
    /// The value determines how rapidly grass is trampled when a creature steps on it - a value of
    /// 0 causes the creature to never damage grass, while a value of 100 causes grass to be
    /// trampled as rapidly as possible. Defaults to 5.
    #[df_token(token_name = "GRASSTRAMPLE")]
    pub grasstrample: Option<u32>,
    /// Used in Creature Variants. This token changes the adult body size to the average of the old
    /// adult body size and the target value and scales all intermediate growth stages by the same
    /// factor.
    #[df_token(token_name = "GRAVITATE_BODY_SIZE")]
    pub gravitate_body_size: Option<u32>,
    /// The creature is a grazer. If tamed in Fortress mode, it needs a pasture to survive. The
    /// higher the number, the less frequently it needs to eat in order to live. See
    /// [Pasture](https://dwarffortresswiki.org/index.php/Pasture) for details on its issues.
    #[df_token(token_name = "GRAZER")]
    pub grazer: Option<u32>,
    /// Defines certain behaviors for the creature.
    ///
    /// These habits require the creature to have a `[LAIR]` to work properly, and also don't seem
    /// to work on creatures who are not a `[SEMIMEGABEAST]`, `[MEGABEAST]` or
    /// `[NIGHT_CREATURE_HUNTER]`.
    #[df_token(token_name = "HABIT")]
    pub habit: Vec<(HabitTypeEnum, u32)>,
    /// "If you set `HABIT_NUM` to a number, it should give you that exact number of habits
    /// according to the weights". All lists of `HABIT`s are preceded by `[HABIT_NUM:TEST_ALL]`.
    #[df_token(token_name = "HABIT_NUM")]
    pub habit_num: Option<Choose<u32, TestAllEnum>>,
    /// The creature has nerves in its muscles. Cutting the muscle tissue can sever motor and
    /// sensory nerves.
    #[df_token(token_name = "HAS_NERVES")]
    pub has_nerves: Option<()>,
    /// The creature has a shell. Seemingly no longer used - holdover from previous versions.
    #[df_token(token_name = "HASSHELL")]
    pub hasshell: Option<()>,
    /// Default 'NONE'. The creature's normal body temperature. Creature ceases maintaining
    /// temperature on death unlike fixed material temperatures. Provides minor protection from
    /// environmental temperature to the creature.
    #[df_token(token_name = "HOMEOTHERM")]
    pub homeotherm: Option<Choose<u32, NoneEnum>>,
    /// Creature hunts and kills nearby vermin.
    #[df_token(token_name = "HUNTS_VERMIN")]
    pub hunts_vermin: Option<()>,
    /// The creature cannot move. Found on sponges. Will also stop a creature from breeding in
    /// fortress mode (MALE and `FEMALE` are affected, if one is `IMMOBILE` no breeding will
    /// happen).
    #[df_token(token_name = "IMMOBILE")]
    pub immobile: Option<()>,
    /// The creature is immobile while on land. Only works on `[AQUATIC]` creatures which can't
    /// breathe on land.
    #[df_token(token_name = "IMMOBILE_LAND")]
    pub immobile_land: Option<()>,
    /// The creature radiates fire. It will ignite, and potentially completely destroy, items the
    /// creature is standing on. Keep booze away from critters with this tag. Also gives the vermin
    /// a high chance of escaping from animal traps and cages made of certain materials.
    #[df_token(token_name = "IMMOLATE")]
    pub immolate: Option<()>,
    /// Alias for `[CAN_SPEAK]` + `[CAN_LEARN]` but additionally keeps creatures from being
    /// butchered by the AI during worldgen and post-gen. In fortress mode, `[CAN_LEARN]` is
    /// enough.
    #[df_token(token_name = "INTELLIGENT")]
    pub intelligent: Option<()>,
    /// Determines if the creature leaves behind a non-standard corpse (i.e. wood, statue, bars,
    /// stone, pool of liquid, etc.).
    #[df_token(token_name = "ITEMCORPSE")]
    pub itemcorpse: Option<(ItemReferenceArg, MaterialTokenArgWithLocalCreatureMat)>,
    /// The quality of an item-type corpse left behind. Valid values are:
    /// - 0: ordinary
    /// - 1: well-crafted
    /// - 2: finely-crafted
    /// - 3: superior
    /// - 4: exceptional
    /// - 5: masterpiece.
    #[df_token(token_name = "ITEMCORPSE_QUALITY")]
    pub itemcorpse_quality: Option<u8>, // TODO maybe nest under itemcorpse?
    /// Found on megabeasts, semimegabeasts, and night creatures. The creature will seek out sites
    /// of this type and take them as lairs.
    #[df_token(token_name = "LAIR")]
    pub lair: Vec<(LairTypeEnum, Clamp<u8, 0, 100>)>,
    /// Defines certain features of the creature's lair. The only valid characteristic is
    /// `HAS_DOORS`.
    #[df_token(token_name = "LAIR_CHARACTERISTIC")]
    pub lair_characteristic: Option<(LairCharacteristicEnum, Clamp<u8, 0, 100>)>,
    /// This creature will actively hunt adventurers in its lair.
    #[df_token(token_name = "LAIR_HUNTER")]
    pub lair_hunter: Option<()>,
    /// What this creature says while hunting adventurers in its lair. Requires a `.txt` on the end.
    #[df_token(token_name = "LAIR_HUNTER_SPEECH")]
    pub lair_hunter_speech: Option<String>, // TODO: string is a txt path
    /// Will attack things that are smaller than it (like dwarves). Only one group of "large
    /// predators" (possibly two groups on "savage" maps) will appear on any given map. In adventure
    /// mode, large predators will try to ambush and attack you (and your party will attack them
    /// back).
    ///
    /// When tamed, large predators tend to be much more aggressive to enemies than non-large
    /// predators, making them a good choice for an animal army.
    ///
    /// They may go on rampages in worldgen, and adventurers may receive quests to kill them.
    /// Also, they can be mentioned in the intro paragraph when starting a fortress e.g.
    /// "ere the wolves get hungry."
    #[df_token(token_name = "LARGE_PREDATOR")]
    pub large_predator: Option<()>,
    /// Creature lays eggs instead of giving birth to live young.
    #[df_token(token_name = "LAYS_EGGS")]
    pub lays_eggs: Option<()>,
    /// Creature lays the specified item instead of regular eggs.
    #[df_token(token_name = "LAYS_UNUSUAL_EGGS")]
    pub lays_unusual_eggs: Option<(ItemReferenceArg, MaterialTokenArgWithLocalCreatureMat)>,
    /// The creature has ligaments in its `[CONNECTIVE_TISSUE_ANCHOR]` tissues (bone or chitin by
    /// default). Cutting the bone/chitin tissue severs the ligaments, disabling motor function if
    /// the target is a limb.
    #[df_token(token_name = "LIGAMENTS")]
    pub ligaments: Option<(MaterialTokenArgWithLocalCreatureMat, u32)>,
    /// The creature will generate light, such as in adventurer mode at night.
    #[df_token(token_name = "LIGHT_GEN")]
    pub light_gen: Option<()>,
    /// The creature will attack enemies rather than flee from them. This tag has the same effect on
    /// player-controlled creatures - including modded dwarves. Retired as of version `0.40.14`
    /// in favor of `[LARGE_PREDATOR]`.
    #[df_token(token_name = "LIKES_FIGHTING")]
    pub likes_fighting: Option<()>,
    /// Creature uses "sssssnake talk" (multiplies 'S' when talking - "My name isss Recisssiz.").
    /// Used by serpent men and reptile men in the vanilla game. C's with the same pronunciation
    /// (depending on the word) are not affected by this token.
    #[df_token(token_name = "LISP")]
    pub lisp: Option<()>,
    /// Determines the number of offspring per one birth.
    #[df_token(token_name = "LITTERSIZE")]
    pub littersize: Option<(u32, u32)>,
    /// Wild animals of this species may occasionally join a civilization. Prevents trading of
    /// (tame) instances of this creature in caravans.
    #[df_token(token_name = "LOCAL_POPS_PRODUCE_HEROES")]
    pub local_pops_produce_heroes: Option<()>,
    /// Lets a creature open doors that are set to forbidden in Fortress Mode.
    #[df_token(token_name = "LOCKPICKER")]
    pub lockpicker: Option<()>,
    /// Determines how well a creature can see in the dark - higher is better. Dwarves have 10,000,
    /// which amounts to perfect nightvision.
    #[df_token(token_name = "LOW_LIGHT_VISION")]
    pub low_light_vision: Option<Clamp<u16, 0, 10_000>>, // TODO: find out if it can be negative
    /// No function, presumably a placeholder.
    #[df_token(token_name = "MAGICAL")]
    pub magical: Option<()>,
    /// The creature is able to see while submerged in magma.
    #[df_token(token_name = "MAGMA_VISION")]
    pub magma_vision: Option<()>,
    /// Makes the creature biologically male. Usually declared inside a caste.
    #[df_token(token_name = "MALE")]
    pub male: Option<()>,
    // region: Mannerisms =========================================================================
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_ARMS")]
    pub mannerism_arms: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_BREATH")]
    pub mannerism_breath: Option<()>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_CHEEK")]
    pub mannerism_cheek: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_EAR")]
    pub mannerism_ear: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_EYELIDS")]
    pub mannerism_eyelids: Option<()>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_EYES")]
    pub mannerism_eyes: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_FEET")]
    pub mannerism_feet: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_FINGERS")]
    pub mannerism_fingers: Option<(String, String)>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_HAIR")]
    pub mannerism_hair: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_HANDS")]
    pub mannerism_hands: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_HEAD")]
    pub mannerism_head: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_KNUCKLES")]
    pub mannerism_knuckles: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_LAUGH")]
    pub mannerism_laugh: Option<()>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_LEG")]
    pub mannerism_leg: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_LIPS")]
    pub mannerism_lips: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_MOUTH")]
    pub mannerism_mouth: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_NAILS")]
    pub mannerism_nails: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_NOSE")]
    pub mannerism_nose: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_POSTURE")]
    pub mannerism_posture: Option<()>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_SIT")]
    pub mannerism_sit: Option<()>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_SMILE")]
    pub mannerism_smile: Option<()>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_STRETCH")]
    pub mannerism_stretch: Option<()>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_TONGUE")]
    pub mannerism_tongue: Option<String>,
    /// Adds a possible mannerism to the creature's profile.
    ///
    /// Mannerisms give a possibility of a personality quirk being included in each member of a
    /// caste.
    ///
    /// Mannerisms are hardcoded and not otherwise placed in the raws.
    ///
    /// Words placed with the mannerism token are the strings used in the mannerisms for that
    /// particular part (e.g. replacing "finger" with "toe" will give you "when she gets
    /// exasperated, she often points and shakes her toe").
    #[df_token(token_name = "MANNERISM_WALK")]
    pub mannerism_walk: Option<()>,
    // endregion ==================================================================================
    /// When struck with a weapon made of the specified material, the force exerted will be
    /// multiplied by A/B, thus making the creature more or less susceptible to this material.
    /// For example, if A is 2 and B is 1, the force exerted by the defined material will be
    /// doubled. If A is 1 and B is 2, it will be halved instead.
    ///
    /// See also `[GENERAL_MATERIAL_FORCE_MULTIPLIER]`, which can be used to make this sort of
    /// effect applicable to all materials.
    #[df_token(token_name = "MATERIAL_FORCE_MULTIPLIER")]
    pub material_force_multiplier: Vec<(MaterialTokenArgWithLocalCreatureMat, u32, u32)>,
    /// Sets the creature to be active at dawn in adventurer mode.
    #[df_token(token_name = "MATUTINAL")]
    pub matutinal: Option<()>,
    /// Determines the creature's natural lifespan, using the specified minimum and maximum age
    /// values (in years). Each individual creature with this token is generated with a
    /// predetermined date (calculated down to the exact tick!) between these values, at which it is
    /// destined to die of old age, should it live long enough.
    ///
    /// Note that the probability of death at any given age does not increase as the creature gets
    /// older ([source](https://i.imgur.com/A1A4aA9.png)).
    ///
    /// Creatures which lack this token are naturally immortal. The `NO_AGING` syndrome tag will
    /// prevent death by old age from occurring. Also note that, among civilized creatures, castes
    /// which lack this token will refuse to marry others with it, and vice versa.
    #[df_token(token_name = "MAXAGE")]
    pub maxage: Option<(u32, u32)>,
    /// Makes the creature slowly stroll around, unless it's in combat or performing a job. If
    /// combined with `[CAN_LEARN]`, will severely impact their pathfinding and lead the creature to
    /// move extremely slowly when not performing any task.
    #[df_token(token_name = "MEANDERER")]
    pub meanderer: Option<()>,
    /// A 'boss' creature. A small number of the creatures are created during worldgen, their
    /// histories and descendants (if any) will be tracked in worldgen (as opposed to simply
    /// 'spawning'), and they will occasionally go on rampages, potentially leading to worship if
    /// they attack the same place multiple times. Their presence and number will also influence age
    /// names. When appearing in fortress mode, they will have a pop-up message announcing their
    /// arrival.
    ///
    /// See the wiki [megabeast](https://dwarffortresswiki.org/index.php/Megabeast) page for more details.
    ///
    /// Requires specifying a `[BIOME]` in which the creature will live. Subterranean biomes appear
    /// to not be allowed.
    #[df_token(token_name = "MEGABEAST")]
    #[df_issue(
        since = "0.44.09",
        link = "https://www.bay12games.com/dwarves/mantisbt/view.php?id=10731",
        note = "They will remain hostile to military even after being tamed.",
        severity = "INFO"
    )]
    pub megabeast: Option<()>,
    /// This means you can increase your attribute to a given percentage of its starting value
    /// (or the average value + your starting value if that is higher).
    ///
    /// Default is 200, which would increase the attribute by either 200%, or by a flat +200
    /// if that is higher.
    #[df_token(token_name = "MENT_ATT_CAP_PERC")]
    pub ment_att_cap_perc: Vec<(SoulAttributeEnum, u32)>,
    /// Sets up a mental attribute's range of values (0-5000). All mental attribute ranges default
    /// to `200:800:900:1000:1100:1300:2000`.
    #[df_token(token_name = "MENT_ATT_RANGE")]
    pub ment_att_range: Vec<(SoulAttributeEnum, u32, u32, u32, u32, u32, u32, u32)>,
    /// Mental attribute gain/decay rates. Lower numbers in the last three slots make decay occur
    /// faster. Defaults are `500:4:5:4`.
    #[df_token(token_name = "MENT_ATT_RATES")]
    pub ment_att_rates: Vec<(
        SoulAttributeEnum,
        Choose<u32, NoneEnum>,
        Choose<u32, NoneEnum>,
        Choose<u32, NoneEnum>,
        Choose<u32, NoneEnum>,
    )>,
    /// Allows the creature to be milked in the farmer's workshop. The frequency is the amount of
    /// ticks the creature needs to "recharge" (i.e. how much time needs to pass before it can be
    /// milked again). Does not work on sentient creatures, regardless of ethics.
    #[df_token(token_name = "MILKABLE")]
    pub milkable: Option<(MaterialTokenArgWithLocalCreatureMat, u32)>,
    /// The creature spawns stealthed and will attempt to path into the fortress, pulling any levers
    /// it comes across. It will be invisible on the map and unit list until spotted by a citizen,
    /// at which point the game will pause and recenter on the creature. Used by gremlins in the
    /// vanilla game. "They go on little missions to mess with various fortress buildings, not just
    /// levers."
    #[df_token(token_name = "MISCHIEVOUS")]
    #[df_alias(token_name = "MISCHIEVIOUS", discouraged)]
    pub mischievous: Option<()>,
    /// Seemingly no longer used.
    #[df_token(token_name = "MODVALUE")]
    pub modvalue: Option<()>,
    /// Creature may be used as a mount. No use for the player in fortress mode, but enemy sieging
    /// forces may arrive with cavalry. Mounts are usable in adventure mode.
    #[df_token(token_name = "MOUNT")]
    pub mount: Option<()>,
    /// Creature may be used as a mount, but civilizations cannot domesticate it in worldgen without
    /// certain exceptions.
    #[df_token(token_name = "MOUNT_EXOTIC")]
    pub mount_exotic: Option<()>,
    /// Allows the creature to have all-around vision as long as it has multiple heads that can see.
    #[df_token(token_name = "MULTIPART_FULL_VISION")]
    pub multipart_full_vision: Option<()>,
    /// Makes the species usually produce a single offspring per birth, occasionally producing twins
    /// or triplets using typical real-world human probabilities. Requires `[FEMALE]`.
    #[df_token(token_name = "MULTIPLE_LITTER_RARE")]
    pub multiple_litter_rare: Option<()>,
    /// Animal is considered to be natural. `NATURAL` animals will not engage creatures tagged with
    /// `[AT_PEACE_WITH_WILDLIFE]` in combat unless they are members of a hostile entity and vice-
    /// versa.
    #[df_token(token_name = "NATURAL")]
    #[df_alias(token_name = "NATURAL_ANIMAL", discouraged)]
    pub natural: Option<()>,
    /// The creature possesses the specified skill at this level inherently - that is, it begins
    /// with the skill at this level, and the skill may never rust below that. A value of 15 is
    /// legendary.
    #[df_token(token_name = "NATURAL_SKILL")]
    pub natural_skill: Vec<(SkillEnum, u32)>,
    /// Creatures with this token can appear in bogeyman ambushes in adventure mode, where they
    /// adopt classical bogeyman traits such as stalking the adventurer and vaporising when dawn
    /// breaks. Such traits do not manifest if the creature is encountered outside of a bogeyman
    /// ambush (for instance, as a megabeast or a civilised being). In addition, their corpses and
    /// severed body parts turn into smoke after a short while. Note that setting the "Number of
    /// Bogeyman Types" in advanced world generation to 0 will only remove randomly-generated
    /// bogeymen.
    #[df_token(token_name = "NIGHT_CREATURE_BOGEYMAN")]
    pub night_creature_bogeyman: Option<()>,
    /// Found on some necromancers. Creatures with this tag will experiment and create strange
    /// hybrid creatures. This tag appears to be all that is needed to allow necromancers or modded
    /// secrets to create every category of procedural experimental creature
    /// ([Though more testing is required to fully confirm this](http://www.bay12forums.com/smf/index.php?topic=175437.msg8085255#msg8085255)).
    #[df_token(token_name = "NIGHT_CREATURE_EXPERIMENTER")]
    pub night_creature_experimenter: Option<()>,
    /// Found on night trolls and werebeasts. Implies that the creature is a night creature, and
    /// shows its description in legends mode entry. The creature is always hostile and will start
    /// no quarter combat with any nearby creatures, except for members of its own race. Note that
    /// this tag does not override the creature's normal behavior in fortress mode except for the
    /// aforementioned aggression, and doesn't prevent the creature from fleeing the battles it
    /// started. It also removes the creature's materials from stockpile settings list, making them
    /// be stored there regardless of settings.
    ///
    /// This tag causes the usual behaviour of werebeasts in worldgen, that is, fleeing towns upon
    /// being cursed and conducting raids from a lair. If this tag is absent from a deity curse, the
    /// accursed will simply be driven out of towns in a similar manner to vampires. When paired
    /// with `SPOUSE_CONVERTER`, a very small population of the creature will be created during
    /// worldgen (sometimes only a single individual will be created), and their histories will be
    /// tracked (that is, they will not spawn spontaneously later, they must either have children or
    /// convert other creatures to increase their numbers). The creature will settle in a lair and
    /// go on rampages during worldgen. It will actively attempt to seek out potential conversion
    /// targets to abduct, convert, and have children with (if possible).
    #[df_token(token_name = "NIGHT_CREATURE_HUNTER")]
    pub night_creature_hunter: Option<()>,
    /// Found on nightmares. Corpses and severed body parts derived from creatures with this token
    /// turn into smoke after a short while.
    #[df_token(token_name = "NIGHT_CREATURE_NIGHTMARE")]
    pub night_creature_nightmare: Option<()>,
    /// The creature caste does not appear in autumn.
    #[df_token(token_name = "NO_AUTUMN")]
    pub no_autumn: Option<()>,
    /// Creature doesn't require connected body parts to move (presumably); generally used on undead
    /// creatures with connections that have rotted away.
    #[df_token(token_name = "NO_CONNECTIONS_FOR_MOVEMENT")]
    pub no_connections_for_movement: Option<()>,
    /// Creature cannot become dizzy.
    #[df_token(token_name = "NO_DIZZINESS")]
    pub no_dizziness: Option<()>,
    /// Creature does not need to drink.
    #[df_token(token_name = "NO_DRINK")]
    pub no_drink: Option<()>,
    /// Creature does not need to eat.
    #[df_token(token_name = "NO_EAT")]
    pub no_eat: Option<()>,
    /// Creature cannot suffer fevers.
    #[df_token(token_name = "NO_FEVERS")]
    pub no_fevers: Option<()>,
    /// The creature is biologically sexless. Makes the creature unable to breed.
    #[df_token(token_name = "NO_GENDER")]
    pub no_gender: Option<()>,
    /// The creature cannot raise any physical attributes.
    #[df_token(token_name = "NO_PHYS_ATT_GAIN")]
    pub no_phys_att_gain: Option<()>,
    /// The creature cannot lose any physical attributes.
    #[df_token(token_name = "NO_PHYS_ATT_RUST")]
    pub no_phys_att_rust: Option<()>,
    /// Creature does not need to sleep. Can still be rendered unconscious by other means.
    #[df_token(token_name = "NO_SLEEP")]
    pub no_sleep: Option<()>,
    /// The creature caste does not appear in spring.
    #[df_token(token_name = "NO_SPRING")]
    pub no_spring: Option<()>,
    /// The creature caste does not appear in summer.
    #[df_token(token_name = "NO_SUMMER")]
    pub no_summer: Option<()>,
    /// Creature doesn't require an organ with the `[THOUGHT]` tag to survive or attack; generally
    /// used on creatures that don't have brains.
    #[df_token(token_name = "NO_THOUGHT_CENTER_FOR_MOVEMENT")]
    pub no_thought_center_for_movement: Option<()>,
    /// Prevents creature from selecting its color based on its profession (e.g. Miner, Hunter,
    /// Wrestler).
    #[df_token(token_name = "NO_UNIT_TYPE_COLOR")]
    pub no_unit_type_color: Option<()>,
    /// Likely prevents the creature from leaving broken vegetation tracks (unverified).
    #[df_token(token_name = "NO_VEGETATION_PERTURB")]
    pub no_vegetation_perturb: Option<()>,
    /// The creature caste does not appear in winter.
    #[df_token(token_name = "NO_WINTER")]
    pub no_winter: Option<()>,
    /// Creature has no bones.
    #[df_token(token_name = "NOBONES")]
    pub nobones: Option<()>,
    /// Creature doesn't need to breathe or have `[BREATHE]` parts in body, nor can it drown or be
    /// strangled. Creatures living in magma must have this tag, otherwise they will drown.
    #[df_token(token_name = "NOBREATHE")]
    pub nobreathe: Option<()>,
    /// Sets the creature to be active at night in adventure mode.
    #[df_token(token_name = "NOCTURNAL")]
    pub nocturnal: Option<()>,
    /// Creature has no emotions. It is immune to the effects of stress and unable to rage, and its
    /// needs cannot be fulfilled in any way. Used on undead in the vanilla game.
    #[df_token(token_name = "NOEMOTION")]
    pub noemotion: Option<()>,
    /// Creature can't become tired or over-exerted from taking too many combat actions or moving at
    /// full speed for extended periods of time.
    #[df_token(token_name = "NOEXERT")]
    pub noexert: Option<()>,
    /// Creature doesn't feel fear and will never flee from battle. Additionally, it causes bogeymen
    /// and nightmares to become friendly towards the creature.
    #[df_token(token_name = "NOFEAR")]
    pub nofear: Option<()>,
    /// Creature will not drop meat when butchered.
    #[df_token(token_name = "NOMEAT")]
    pub nomeat: Option<()>,
    /// Creature isn't nauseated by gut hits and cannot vomit.
    #[df_token(token_name = "NONAUSEA")]
    pub nonausea: Option<()>,
    /// Creature doesn't feel pain.
    #[df_token(token_name = "NOPAIN")]
    pub nopain: Option<()>,
    /// Creature will not drop a hide when butchered.
    #[df_token(token_name = "NOSKIN")]
    pub noskin: Option<()>,
    /// Creature will not drop a skull on butchering, rot, or decay of severed head.
    #[df_token(token_name = "NOSKULL")]
    pub noskull: Option<()>,
    /// Does not produce miasma when rotting.
    #[df_token(token_name = "NOSMELLYROT")]
    pub nosmellyrot: Option<()>,
    /// Weapons can't get stuck in the creature.
    #[df_token(token_name = "NOSTUCKINS")]
    pub nostuckins: Option<()>,
    /// Creature can't be stunned and knocked unconscious by pain or head injuries. Creatures with
    /// this tag never wake up from sleep in Fortress Mode. If this creature needs to sleep while
    /// playing, it will die.
    #[df_token(token_name = "NOSTUN")]
    pub nostun: Option<()>,
    /// Cannot be butchered.
    #[df_token(token_name = "NOT_BUTCHERABLE")]
    pub not_butcherable: Option<()>,
    /// Cannot be raised from the dead by necromancers or evil clouds. Implies the creature is not a
    /// normal living being. Used by vampires, mummies and inorganic creatures like the amethyst man
    /// and bronze colossus. Creatures who are `[OPPOSED_TO_LIFE]` (undead) will be docile towards
    /// creatures with this token.
    #[df_token(token_name = "NOT_LIVING")]
    pub not_living: Option<()>,
    /// Creature doesn't require a `[THOUGHT]` body part to survive. Has the added effect of
    /// preventing speech, though directly controlling creatures that would otherwise be capable of
    /// speaking allows them to engage in conversation.
    #[df_token(token_name = "NOTHOUGHT")]
    pub nothought: Option<()>,
    /// How easy the creature is to smell. The higher the number, the easier the creature can be
    /// smelt. Zero is odorless. Default is 50.
    #[df_token(token_name = "ODOR_LEVEL")]
    pub odor_level: Option<u32>,
    /// What the creature smells like. If no odor string is defined, the creature name (not the
    /// caste name) is used.
    #[df_token(token_name = "ODOR_STRING")]
    pub odor_string: Option<String>,
    /// Is hostile to all creatures except undead and other non-living ones and will show Opposed to
    /// life in the unit list. Used by undead in the vanilla game. Functions without the
    /// `[NOT_LIVING]` token, and seems to imply said token as well. Undead will not be hostile to
    /// otherwise-living creatures given this token. Living creatures given this token will attack
    /// living creatures that lack it, while ignoring other living creatures that also have this
    /// token.
    #[df_token(token_name = "OPPOSED_TO_LIFE")]
    pub opposed_to_life: Option<()>,
    /// Determines caste's likelihood of having sexual attraction to certain sexes. Values default
    /// to `75:20:5` for the same sex and `5:20:75` for the opposite sex. The first value indicates how
    /// likely to be entirely uninterested in the sex, the second decides if the creature will
    /// become lovers but not marry, the third decides if the creature will marry this sex. The
    /// values themselves are simply ratios and do not need to add up to any particular value.
    #[df_token(token_name = "ORIENTATION")]
    pub orientation: Vec<(MaleOrFemaleEnum, u32, u32, u32)>,
    /// Lets you play as an outsider of this species in adventure mode.
    #[df_token(token_name = "OUTSIDER_CONTROLLABLE")]
    pub outsider_controllable: Option<()>,
    /// Allows the creature to be used as a pack animal. Currently only used by merchants without
    /// wagons. Also prevents creature from dropping hauled items on its own -- do not use for
    /// player-controllable creatures!
    #[df_token(token_name = "PACK_ANIMAL")]
    pub pack_animal: Option<()>,
    /// The creature is immune to all paralyzing special attacks.
    #[df_token(token_name = "PARALYZEIMMUNE")]
    pub paralyzeimmune: Option<()>,
    /// Used to control the bat riders with paralyze-dart blowguns that flew through the `2D` chasm.
    /// Doesn't do anything now.
    #[df_token(token_name = "PATTERNFLIER")]
    pub patternflier: Option<()>,
    /// In earlier versions, creature would generate pearls. Apparently does nothing in the current
    /// version.
    #[df_token(token_name = "PEARL")]
    pub pearl: Option<()>,
    /// Controls the ability of vermin to find a way into containers when they are eating food from
    /// your stockpiles. The value for vermin in the game's current version ranges from 1-3. A
    /// higher value is better at penetrating containers.
    #[df_token(token_name = "PENETRATEPOWER")]
    pub penetratepower: Option<u32>,
    /// Determines the range and chance of personality traits. Standard is `0:50:100`.
    ///
    /// See [Personality trait](https://dwarffortresswiki.org/index.php/Personality_trait)
    /// for more info.
    #[df_token(token_name = "PERSONALITY")]
    pub personality: Vec<(PersonalityTraitEnum, u8, u8, u8)>,
    /// Allows the creature to be tamed in Fortress mode. Prerequisite for all other working animal
    /// roles. Civilizations that encounter it in worldgen will tame and domesticate it for their
    /// own use. Adding this to civilization members will classify them as pets instead of citizens,
    /// with all the problems that entails. However, you can solve these problems using the popular
    /// plugin Dwarf Therapist, which is completely unaffected by the tag.
    #[df_token(token_name = "PET")]
    pub pet: Option<()>,
    /// Allows the creature to be tamed in Fortress mode. Prequisite for all other working animal
    /// roles. Civilizations cannot domesticate it in worldgen, with certain exceptions.
    /// Adding this to civilization members will classify them as pets instead of citizens, with
    /// all the problems that entails. ([Example](https://dwarffortresswiki.org/index.php/Gremlin)).
    ///
    /// May make them more difficult to tame, but this needs verification.
    #[df_token(token_name = "PET_EXOTIC")]
    pub pet_exotic: Option<()>,
    /// How valuable a tamed animal is. Actual cost in points in the embarking screen is
    /// `1+(PETVALUE/2)` for an untrained animal, `1+PETVALUE` for a war/hunting one.
    #[df_token(token_name = "PETVALUE")]
    pub petvalue: Option<u32>,
    /// Divides the creature's `[PETVALUE]` by the specified number. Used by honey bees to prevent a
    /// single hive from being worth a fortune.
    #[df_token(token_name = "PETVALUE_DIVISOR")]
    pub petvalue_divisor: Option<u32>,
    /// This means you can increase your attribute to a given percentage of its starting value
    /// (or the average value + your starting value if that is higher).
    ///
    /// Default is 200, which would increase the attribute by either 200%, or by a flat +200
    /// if that is higher.
    #[df_token(token_name = "PHYS_ATT_CAP_PERC")]
    pub phys_att_cap_perc: Vec<(BodyAttributeEnum, u32)>,
    /// Sets up a physical attribute's range of values (0-5000). All physical attribute ranges
    /// default to `200:700:900:1000:1100:1300:2000`.
    #[df_token(token_name = "PHYS_ATT_RANGE")]
    pub phys_att_range: Vec<(BodyAttributeEnum, u32, u32, u32, u32, u32, u32, u32)>,
    /// Physical attribute gain/decay rates. Lower numbers in the last three slots make decay occur
    /// faster. Defaults for `STRENGTH`, `AGILITY`, `TOUGHNESS`, and `ENDURANCE` are `500:3:4:3`,
    /// while `RECUPERATION` and `DISEASE_RESISTANCE` default to `500:NONE:NONE:NONE`.
    #[df_token(token_name = "PHYS_ATT_RATES")]
    pub phys_att_rates: Vec<(
        BodyAttributeEnum,
        Choose<u32, NoneEnum>,
        Choose<u32, NoneEnum>,
        Choose<u32, NoneEnum>,
        Choose<u32, NoneEnum>,
    )>,

    /// Weighted population of caste; Lower is rarer. Not to be confused with `[FREQUENCY]`.
    #[df_token(token_name = "POP_RATIO")]
    pub pop_ratio: Option<u32>,
    /// The minimum/maximum numbers of how many of these creatures are present in each world map
    /// tile of the appropriate region. Defaults to 1:1 if not specified.
    #[df_token(token_name = "POPULATION_NUMBER")]
    pub population_number: Option<(u32, u32)>,
    /// Allows the being to represent itself as a deity, allowing it to become the leader of a
    /// civilized group. Used by unique demons in the vanilla game. Requires `[CAN_SPEAK]` to
    /// actually do anything more than settle at a location (e.g. write books, lead armies, profane
    /// temples). Doesn't appear to do anything for creatures that are already civilized. Once the
    /// creature ascends to a position of leadership, it will proceed to act as a standard ruler for
    /// their entity and fulfill the same functions (hold tournaments, tame creatures, etc).
    #[df_token(token_name = "POWER")]
    pub power: Option<()>,
    /// Creature has a percentage chance to flip out at visible non-friendly creatures. Enraged
    /// creatures attack anything regardless of timidity and get a strength bonus to their hits.
    /// This is what makes badgers so hardcore.
    #[df_token(token_name = "PRONE_TO_RAGE")]
    pub prone_to_rage: Option<Clamp<u8, 0, 100>>,
    /// The creature has pus. Specifies the stuff secreted by infected wounds.
    #[df_token(token_name = "PUS")]
    pub pus: Option<(MaterialTokenArgWithLocalCreatureMat, MaterialStateEnum)>,
    /// Specifies a new relative size for a part than what is stated in the body plan. For example,
    /// dwarves have larger livers.
    #[df_token(token_name = "RELSIZE")]
    pub relsize: Vec<(BpCriteriaTokenArg, u32)>,
    /// What creature's remains are called.
    #[df_token(token_name = "REMAINS")]
    pub remains: Option<(String, String)>,
    /// What color the creature's remains are.
    #[df_token(token_name = "REMAINS_COLOR")]
    pub remains_color: Option<ColorToken>,
    /// Goes with `[VERMIN_BITE]` and `[DIE_WHEN_VERMIN_BITE]`, the vermin creature will leave
    /// remains on death when biting. Leaving this tag out will cause the creature to disappear
    /// entirely after it bites.
    #[df_token(token_name = "REMAINS_ON_VERMIN_BITE_DEATH")]
    pub remains_on_vermin_bite_death: Option<()>,
    /// Unknown.
    #[df_token(token_name = "REMAINS_UNDETERMINED")]
    pub remains_undetermined: Option<()>,
    /// The creature will retract into a body part when threatened. It will be unable to move or
    /// attack, but enemies will only be able to attack the specified body part. (eg. Turtle,
    /// Hedgehog)
    ///
    /// Second-person descriptions are used for adventurer mode natural ability. "<pro_pos>" can be
    /// used in the descriptions, being replaced with the proper pronoun (or lack thereof) in-game.
    #[df_token(token_name = "RETRACT_INTO_BP")]
    #[df_issue(
        since = "0.44.04",
        link = "https://www.bay12games.com/dwarves/mantisbt/view.php?id=10519",
        note = "Undead curled up creatures are buggy.",
        severity = "INFO"
    )]
    pub retract_into_bp: Option<(BpCriteriaTokenArg, String, String, String, String)>,
    /// Cat behavior. If it kills a vermin creature and has an owner, it carries the remains in its
    /// mouth and drops them at their feet. Requires `[HUNTS_VERMIN]`, obviously.
    #[df_token(token_name = "RETURNS_VERMIN_KILLS_TO_OWNER")]
    pub returns_vermin_kills_to_owner: Option<()>,
    /// Creature will occasionally root around in the grass, looking for insects.
    ///
    /// Used for flavor in Adventurer Mode, spawns vermin edible for this creature in Fortress Mode.
    /// Creatures missing the specified body part will be unable to perform this action. The action
    /// produces a message (visible in adventure mode) in the form:
    ///
    /// `[creature]` `[verb text]` the `[description of creature's location]`
    ///
    /// In adventure mode, the "rooting around" ability will be included in the "natural abilities"
    /// menu, represented by its second person verb text.
    #[df_token(token_name = "ROOT_AROUND")]
    pub root_around: Option<(BpCriteriaTokenArg, String, String)>,
    /// Causes the specified tissue layer(s) of the indicated body part(s) to secrete the designated
    /// material. A size 100 ('covering') contaminant is created over the affected body part(s) in
    /// its specified material state (and at the temperature appropriate to this state) when the
    /// trigger condition is met, as long as one of the secretory tissue layers is still intact.
    #[df_token(token_name = "SECRETION")]
    pub secretion: Vec<(
        MaterialTokenArgWithLocalCreatureMat,
        MaterialStateEnum,
        BpCriteriaTokenArg,
        Reference,
        // only an Option<> because of `creature_next_underground.txt` in vanilla:
        // TODO: find out why this is allowed to be an Option<>
        Option<SecretionTriggerEnum>,
    )>,
    /// Essentially the same as `[MEGABEAST]`, but more of them are created during worldgen.
    ///
    /// See the [semi-megabeast page](https://dwarffortresswiki.org/index.php/Semi-megabeast)
    /// for more info.
    #[df_token(token_name = "SEMIMEGABEAST")]
    pub semimegabeast: Option<()>,
    /// Gives the creature the ability to sense creatures belonging to the specified creature class
    /// even when they lie far beyond line of sight, including through walls and floors. It also
    /// appears to reduce or negate the combat penalty of blind units when fighting creatures they
    /// can sense. In adventure mode, the specified tile will be used to represent sensed creatures
    /// when they cannot be seen directly.
    #[df_token(token_name = "SENSE_CREATURE_CLASS")]
    pub sense_creature_class: Vec<(Reference, DFChar, u32, u32, u32)>, // TODO: ref is creature class
    /// The rate at which this creature learns this skill. Requires `[CAN_LEARN]` or `[INTELLIGENT]`
    /// to function.
    #[df_token(token_name = "SKILL_LEARN_RATE")]
    pub skill_learn_rate: Vec<(SkillEnum, u32)>,
    /// The rate at which this creature learns all skills. Requires `[CAN_LEARN]` or `[INTELLIGENT]`
    /// to function.
    #[df_token(token_name = "SKILL_LEARN_RATES")]
    pub skill_learn_rates: Option<u32>,
    /// Like `[SKILL_RATES]`, but applies to individual skills instead. Requires `[CAN_LEARN]` or
    /// `[INTELLIGENT]` to function.
    #[df_token(token_name = "SKILL_RATE")]
    pub skill_rate: Vec<(SkillEnum, u32, u32, u32, u32)>,
    /// Affects skill gain and decay. Lower numbers in the last three slots make decay occur faster
    /// (`[SKILL_RATES:100:1:1:1]` would cause rapid decay). The counter rates may also be replaced
    /// with `NONE`.
    ///
    /// Default is `[SKILL_RATES:100:8:16:16]`. Requires `[CAN_LEARN]` or `[INTELLIGENT]` to
    /// function.
    #[df_token(token_name = "SKILL_RATES")]
    pub skill_rates: Option<(
        Choose<u32, NoneEnum>,
        Choose<u32, NoneEnum>,
        Choose<u32, NoneEnum>,
        Choose<u32, NoneEnum>,
    )>,
    /// The rate at which this skill decays. Lower values cause the skill to decay faster. Requires
    /// `[CAN_LEARN]` or `[INTELLIGENT]` to function.
    #[df_token(token_name = "SKILL_RUST_RATE")]
    pub skill_rust_rate: Vec<(SkillEnum, u32, u32, u32)>,
    /// The rate at which all skills decay. Lower values cause the skills to decay faster. Requires
    /// `[CAN_LEARN]` or `[INTELLIGENT]` to function.
    #[df_token(token_name = "SKILL_RUST_RATES")]
    pub skill_rust_rates: Option<(u32, u32, u32)>,
    /// Shorthand for `[CAN_LEARN]` + `[SKILL_LEARN_RATES:50]` (unverified). Used by a number of
    /// 'primitive' creatures (like ogres, giants and troglodytes) in the vanilla game. Applicable
    /// to player races. Prevents a player from recruiting nobility, even basic ones. Subterranean
    /// creatures with this token combined with `[EVIL]` will become servants of goblins in their
    /// civilizations, in the style of trolls.
    #[df_token(token_name = "SLOW_LEARNER")]
    pub slow_learner: Option<()>,
    /// Creature leaves "remains" instead of a corpse. Used by vermin.
    #[df_token(token_name = "SMALL_REMAINS")]
    pub small_remains: Option<()>,
    /// Creature makes sounds periodically, which can be heard in Adventure mode.
    ///
    /// First-person reads "You bark"
    ///
    /// Third-person reads "The capybara barks"
    ///
    /// Out of sight reads "You hear a loud bark"
    ///
    /// with the text in bold being the description arguments of the token.
    #[df_token(token_name = "SOUND")]
    pub sound: Vec<(
        AlertOrPeacefulIntermittentEnum,
        u32,
        u32,
        Choose<VocalizationEnum, NoneEnum>,
        String,
        String,
        String,
    )>,
    /// Creature will only appear in biomes with this plant or creature available. Grazers given a
    /// specific type of grass (such as pandas and bamboo) will only eat that grass and nothing
    /// else, risking starvation if there's none available.
    #[df_token(token_name = "SPECIFIC_FOOD")]
    pub specific_food: Vec<PlantOrCreatureTokenArg>,
    /// This creature can be converted by a night creature with `[SPOUSE_CONVERTER]`.
    #[df_token(token_name = "SPOUSE_CONVERSION_TARGET")]
    pub spouse_conversion_target: Option<()>,
    /// If the creature has the `[NIGHT_CREATURE_HUNTER]` tag, it will kidnap
    /// `[SPOUSE_CONVERSION_TARGET]`s and transform them into the caste of its species with the
    /// `[CONVERTED_SPOUSE]` tag during worldgen. It may also start families this way.
    #[df_token(token_name = "SPOUSE_CONVERTER")]
    pub spouse_converter: Option<()>,
    /// If the creature rules over a site, it will cause the local landscape to be corrupted into
    /// evil surroundings associated with the creature's spheres. The creature must have at least
    /// one of the following spheres for this to take effect: `BLIGHT`, `DEATH`, `DISEASE`,
    /// `DEFORMITY`, `NIGHTMARES`. The first three kill vegetation, while the others sometimes do.
    ///
    /// The last two get evil plants and evil animals sometimes. `NIGHTMARES` gets bogeymen
    /// ([source](http://www.bay12forums.com/smf/index.php?topic=169696.msg8162224#msg8162224)).
    /// Used by demons in the vanilla game.
    #[df_token(token_name = "SPREAD_EVIL_SPHERES_IF_RULER")]
    pub spread_evil_spheres_if_ruler: Option<()>,
    /// Caste does not require `[GRASP]` body parts to climb -- it can climb with `[STANCE]` parts
    /// instead.
    #[df_token(token_name = "STANCE_CLIMBER")]
    pub stance_climber: Option<()>,
    /// Acts as `[GRAZER]` but set to `20000*G*(max size)^(-3/4)`, where `G` defaults to 100 but can
    /// be set in d_init, and the whole thing is trapped between 150 and 3 million. Used for all
    /// grazers in the default creature raws.
    #[df_token(token_name = "STANDARD_GRAZER")]
    pub standard_grazer: Option<()>,
    /// The creature will get strange moods in fortress mode and can produce artifacts.
    #[df_token(token_name = "STRANGE_MOODS")]
    pub strange_moods: Option<()>,
    /// Gives the creature knowledge of any secrets with `[SUPERNATURAL_LEARNING_POSSIBLE]` that
    /// match its spheres. Also prevents it from becoming a vampire or a werebeast. Other effects
    /// are unknown.
    #[df_token(token_name = "SUPERNATURAL")]
    pub supernatural: Option<()>,
    /// The creature naturally knows how to swim perfectly and does not use the swimmer skill, as
    /// opposed to `[SWIMS_LEARNED]`. However, Fortress mode AI never paths into water anyway,
    /// so it's less useful there.
    #[df_token(token_name = "SWIMS_INNATE")]
    pub swims_innate: Option<()>,
    /// The creature swims only as well as their present swimming skill allows them to.
    #[df_token(token_name = "SWIMS_LEARNED")]
    pub swims_learned: Option<()>,
    /// Dilutes the effects of syndromes which have the specified identifier. A percentage of 100
    /// is equal to the regular syndrome effect severity, higher percentages reduce severity.
    #[df_token(token_name = "SYNDROME_DILUTION_FACTOR")]
    pub syndrome_dilution_factor: Vec<(Reference, u32)>, // ref is a syndrome identifier
    /// The creature has tendons in its `[CONNECTIVE_TISSUE_ANCHOR]` tissues (bone or chitin by
    /// default). Cutting the bone/chitin tissue severs the tendons, disabling motor function if the
    /// target is a limb.
    #[df_token(token_name = "TENDONS")]
    pub tendons: Option<(MaterialTokenArgWithLocalCreatureMat, u32)>,
    /// The creature's webs can catch larger creatures.
    #[df_token(token_name = "THICKWEB")]
    pub thickweb: Option<()>,
    /// Found on titans. Cannot be specified in user-defined raws.
    #[df_token(token_name = "TITAN")] // TODO mark generated, see #84
    pub titan: Option<()>,
    /// How much the creature can carry when used by merchants.
    #[df_token(token_name = "TRADE_CAPACITY")]
    pub trade_capacity: Option<u32>,
    /// Shortcut for `[TRAINABLE_HUNTING]` + `[TRAINABLE_WAR]`.
    #[df_token(token_name = "TRAINABLE")]
    pub trainable: Option<()>,
    /// Can be trained as a hunting beast, increasing speed.
    #[df_token(token_name = "TRAINABLE_HUNTING")]
    pub trainable_hunting: Option<()>,
    /// Can be trained as a war beast, increasing strength and endurance.
    #[df_token(token_name = "TRAINABLE_WAR")]
    pub trainable_war: Option<()>,
    /// Allows the creature to go into [martial trances](https://dwarffortresswiki.org/index.php/Martial_trance).
    /// Used by dwarves in the vanilla game.
    #[df_token(token_name = "TRANCES")]
    pub trances: Option<()>,
    /// The creature will never trigger traps it steps on. Used by a number of creatures. Doesn't
    /// make the creature immune to remotely activated traps (like retractable spikes being
    /// triggered while the creature is standing over them). `TRAPAVOID` creatures lose this power
    /// if they're immobilized while standing in a trap, be it by stepping on thick web, being
    /// paralyzed or being knocked unconscious.
    #[df_token(token_name = "TRAPAVOID")]
    pub trapavoid: Option<()>,
    /// The creature is displayed as blue when in 7/7 water. Used on fish and amphibious creatures
    /// which swim under the water.
    #[df_token(token_name = "UNDERSWIM")]
    pub underswim: Option<()>,
    /// Found on generated demons; causes the game to create a single named instance of the demon
    /// which will emerge from the underworld and take over civilizations during worldgen. Could not
    /// be specified in user-defined raws prior to version `0.47.01`.
    #[df_token(token_name = "UNIQUE_DEMON")]
    pub unique_demon: Option<()>,
    /// Changes the language of the creature into unintelligible 'kobold-speak', which creatures of
    /// other species will be unable to understand. If a civilized creature has this and is not part
    /// of a `[SKULKING]` civ, it will tend to start wars with all nearby civilizations and will be
    /// unable to make peace treaties due to 'inability to communicate'.
    #[df_token(token_name = "UTTERANCES")]
    pub utterances: Option<()>,
    /// The creature is made of swampstuff. Doesn't appear to do anything in particular. Used by
    /// grimelings in the vanilla game.
    #[df_token(token_name = "VEGETATION")]
    pub vegetation: Option<()>,
    /// Enables vermin to bite other creatures, injecting the specified material. See
    /// `[SPECIALATTACK_INJECT_EXTRACT]` for details about injection - this token presumably works
    /// in a similar manner (unverified).
    #[df_token(token_name = "VERMIN_BITE")] // TODO research; can this be a Vec?
    pub vermin_bite: Option<(
        u32,
        String,
        MaterialTokenArgWithLocalCreatureMat,
        MaterialStateEnum,
    )>,
    /// Some dwarves will hate the creature and get unhappy thoughts when around it, and show a negative
    /// preference towards them. Being hated by some dwarves does not prevent the vermin from
    /// appearing as a positive preference for other dwarves.
    #[df_token(token_name = "VERMIN_HATEABLE")]
    pub vermin_hateable: Option<()>,
    /// This makes the creature move in a swarm of creatures of the same race as it (e.g. swarm of
    /// flies, swarm of ants).
    #[df_token(token_name = "VERMIN_MICRO")]
    pub vermin_micro: Option<()>,
    /// The creature cannot be caught by fishing.
    #[df_token(token_name = "VERMIN_NOFISH")]
    pub vermin_nofish: Option<()>,
    /// The creature will not be observed randomly roaming about the map.
    #[df_token(token_name = "VERMIN_NOROAM")]
    pub vermin_noroam: Option<()>,
    /// The creature cannot be caught in baited animal traps; however, a "catch live land animal"
    /// task may still be able to capture one if a dwarf finds one roaming around.
    #[df_token(token_name = "VERMIN_NOTRAP")]
    pub vermin_notrap: Option<()>,
    /// Old shorthand for "does cat stuff". Contains `[AT_PEACE_WITH_WILDLIFE]` +
    /// `[RETURNS_VERMIN_KILLS_TO_OWNER]` + `[HUNTS_VERMIN]` + `[ADOPTS_OWNER]`.
    #[df_token(token_name = "VERMINHUNTER")]
    pub verminhunter: Option<()>,
    /// Sets the creature to be active during the evening in adventurer mode.
    #[df_token(token_name = "VESPERTINE")]
    pub vespertine: Option<()>,
    /// Value should determine how close you have to get to a critter before it attacks (or prevents
    /// adv mode travel etc.) Default is 20.
    #[df_token(token_name = "VIEWRANGE")]
    pub viewrange: Option<u32>,
    /// The width of the creature's vision arcs, in degrees (i.e. 0 to 360). The first number is
    /// binocular vision, the second is non-binocular vision.
    ///
    /// Binocular vision has a minimum of about 10 degrees, monocular, a maximum of about 350
    /// degrees. Values past these limits will be accepted, but will default to ~10 degrees and ~350
    /// degrees respectively.
    #[df_token(token_name = "VISION_ARC")]
    pub vision_arc: Option<(Clamp<u16, 10, 360>, Clamp<u16, 0, 350>)>,
    /// Allows the creature to pull caravan wagons. If a civilization doesn't have access to any, it
    /// is restricted to trading with pack animals.
    #[df_token(token_name = "WAGON_PULLER")]
    pub wagon_puller: Option<()>,
    /// Allows the creature to create webs, and defines what the webs are made of.
    #[df_token(token_name = "WEBBER")]
    pub webber: Option<MaterialTokenArgWithLocalCreatureMat>,
    /// The creature will not get caught in thick webs. Used by creatures who can shoot thick webs
    /// (such as giant cave spiders) in order to make them immune to their own attacks.
    #[df_token(token_name = "WEBIMMUNE")]
    pub webimmune: Option<()>,
    // endregion ==================================================================================
    /// Applies the effects of all pending `[CV_ADD_TAG]` and `[CV_REMOVE_TAG]` tokens that have
    /// been defined in the current creature.
    #[df_token(token_name = "APPLY_CURRENT_CREATURE_VARIATION")]
    pub apply_current_creature_variation: Vec<()>,
    /// Copies creature tags, but not caste tags, from another specified creature. Used when making
    /// creature variations (i.e. giant animals and animal people). Often used in combination with
    /// `[APPLY_CREATURE_VARIATION]` to import standard variations from a file. Variations are
    /// documented in `c_variation_default.txt`, which also contains the code for giant animals and
    /// animal people.
    #[df_token(token_name = "COPY_TAGS_FROM")]
    pub copy_tags_from: Option<ReferenceTo<CreatureToken>>,
    // TODO: properly implement the 3 below tokens; ADD_TAG, REMOVE_TAG, and GO_TO_TAG:
    // --------------------------------------------------------------------------------------------
    /// **Warning: Incomplete token. This token is not yet properly implemented, and so you will
    /// not get any hover text information from the arguments, autocomplete will not work, and
    /// you will not be alerted to any errors.**
    ///
    /// ---
    /// Adds the given token to the creature.
    ///
    /// `APPLY_CREATURE_VARIATION` is the only token that cannot be added using this token.
    #[df_token(token_name = "CV_ADD_TAG")]
    #[df_alias(token_name = "CV_NEW_TAG")]
    pub cv_add_tag: Vec<(Reference, Option<(Vec<Any>,)>)>,
    /// **Warning: Incomplete token. This token is not yet properly implemented, and so you will
    /// not get any hover text information from the arguments, autocomplete will not work, and
    /// you will not be alerted to any errors.**
    ///
    /// ---
    /// Removes from the creature all tokens which start with the given chain of token arguments.
    ///
    /// For example, `[CV_REMOVE_TAG:BODY:HUMANOID_SIMPLE]` would remove
    /// `[BODY:HUMANOID_SIMPLE:3_EYES]`, but `[CV_REMOVE_TAG:BODY:3_EYES]`, or `[CV_REMOVE_TAG:3_EYES]`
    /// would not. Neither would `[CV_REMOVE_TAG:BODY:HUMANOID_SIMPLE]` be able to remove
    /// `[BODY:HUMANOID_SIMPLE_NECK]`, as it looks for whole arguments.
    ///
    /// `[CV_REMOVE_TAG:BODY]` would remove both examples above, as they both start with `BODY`.
    #[df_token(token_name = "CV_REMOVE_TAG")]
    pub cv_remove_tag: Vec<(Reference, Option<(Vec<Any>,)>)>,
    /// **Warning: Incomplete token. This token is not yet properly implemented, and so you will
    /// not get any hover text information from the arguments, autocomplete will not work, and
    /// you will not be alerted to any errors.**
    ///
    /// ---
    /// When using tags from an existing creature, inserts new tags after the specified tag.
    #[df_token(token_name = "GO_TO_TAG")]
    pub go_to_tag: Vec<(Reference, Option<(Vec<Any>,)>)>,
    /// When using tags from an existing creature, inserts new tags at the end of the creature.
    #[df_token(token_name = "GO_TO_END")]
    pub go_to_end: Vec<()>,
    /// When using tags from an existing creature, inserts new tags at the beginning of the
    /// creature.
    #[df_token(token_name = "GO_TO_START")]
    pub go_to_start: Vec<()>,
    /// List of `[CV_CONVERT_TAG]` tokens.
    #[df_token(token_name = "CV_CONVERT_TAG")]
    pub cv_convert_tag: Vec<CreatureCvConvertTag>,
}
