use crate::Any;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Default, Eq, Hash)]
pub struct PipeArguments(pub Vec<Option<Any>>);
