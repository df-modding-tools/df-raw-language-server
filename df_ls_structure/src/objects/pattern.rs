use df_ls_core::{ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct PatternToken {
    /// Argument 1 of `[COLOR_PATTERN:...]`
    #[df_token(token_name = "COLOR_PATTERN", on_duplicate_to_parent, primary_token)]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    #[df_token(token_name = "PATTERN")]
    pub pattern: Option<PatternEnum>,
    #[df_token(token_name = "CP_COLOR")]
    pub cp_color: Vec<ReferenceTo<crate::ColorToken>>,
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum PatternEnum {
    #[df_token(token_name = "SPOTS")]
    Spots,
    #[df_token(token_name = "STRIPES")]
    Stripes,
    #[df_token(token_name = "MOTTLED")]
    Mottled,
    #[df_token(token_name = "IRIS_EYE")]
    IrisEye,
    #[df_token(token_name = "PUPIL_EYE")]
    PupilEye,
}

impl Default for PatternEnum {
    fn default() -> Self {
        Self::Spots
    }
}
