use df_ls_core::Reference;
use df_ls_diagnostics::DiagnosticsInfo;
use df_ls_syntax_analysis::{Token, TryFromArgumentGroup};
use serde::{Deserialize, Serialize};

/// Alias for `CREATURE_MAT:CREATURE_ID:MATERIAL_NAME`,
/// where `CREATURE_ID` is the creature currently being defined;
/// as such, it can only be used in creature definitions.
// #[df_token(token_name = "LOCAL_CREATURE_MAT")]
#[derive(Serialize, Deserialize, Clone, Debug, Default, PartialEq, Eq)]
pub struct LocalCreatureMatArg(pub Reference);

impl TryFromArgumentGroup for LocalCreatureMatArg {
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()> {
        // Safe first argument (is not token_name) for error case
        let arg0 = match token.get_current_arg() {
            Ok(arg) => Ok(arg.clone()),
            Err(err) => Err(err),
        };
        // Arg 0
        let reference_arg0 =
            Reference::try_from_argument_group(token, source, diagnostics, add_diagnostics_on_err)?;
        // Match on Arg 0
        let material_type = match reference_arg0.0.as_ref() {
            "LOCAL_CREATURE_MAT" => {
                // Arg 1
                Reference::try_from_argument_group(
                    token,
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                )?
            }
            _ => {
                Self::diagnostics_wrong_enum_type(
                    &arg0?,
                    vec!["LOCAL_CREATURE_MAT"],
                    source,
                    diagnostics,
                    add_diagnostics_on_err,
                );
                return Err(());
            }
        };
        Ok(Self(material_type))
    }
}

// ---------------------------- TESTS --------------------------
#[cfg(test)]
mod tests {
    use super::*;
    use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
    use df_ls_syntax_analysis::test_tree_structure;
    use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

    #[test]
    fn test_material_correct() {
        let test_builder = SyntaxTestBuilder::from_lexer_test_builder(
            LexerTestBuilder::test_source(
                "header
                [REF:LOCAL_CREATURE_MAT:SKIN]
                ",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
        )
        .add_test_syntax_diagnostics_codes(vec![])
        .add_test_syntax_diagnostics_ranges(vec![]);

        test_tree_structure!(
            test_builder,
            [
                (LocalCreatureMatArg,) => (LocalCreatureMatArg(Reference("SKIN".to_owned())),),
            ]
        );
    }
}
