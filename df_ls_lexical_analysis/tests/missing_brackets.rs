use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

#[test]
fn test_missing_brackets_start() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        NAME:Amber]
        [NAME:Amber]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (ERROR: `]`)
  (comment)
  (token
    ([)
    (token_name: `NAME`)
    (token_arguments
      (:)
      (token_argument_string: `Amber`)
    )
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["unexpected_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 2,
            character: 18,
        },
        end: Position {
            line: 2,
            character: 19,
        },
    }])
    .run_test();
}

#[test]
fn test_missing_brackets_start_2() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        [NAME:']']
        [NAME:Amber]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: `NAME`)
    (token_arguments
      (:)
      (token_argument_string: `'`)
    )
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (token
    ([)
    (token_name: `NAME`)
    (token_arguments
      (:)
      (token_argument_string: `Amber`)
    )
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["unexpected_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 2,
            character: 17,
        },
        end: Position {
            line: 2,
            character: 18,
        },
    }])
    .run_test();
}

#[test]
fn test_missing_brackets_end() {
    LexerTestBuilder::test_source(
        "descriptor_color_standard

        [NAME:Amber
        [NAME:Amber]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `descriptor_color_standard`)
  (comment)
  (token
    ([)
    (token_name: `NAME`)
    (token_arguments
      (:)
      (token_argument_string: `Amber`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `NAME`)
    (token_arguments
      (:)
      (token_argument_string: `Amber`)
    )
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 2,
            character: 19,
        },
        end: Position {
            line: 2,
            character: 19,
        },
    }])
    .run_test();
}

#[test]
fn test_wrong_missing_brackets() {
    LexerTestBuilder::test_source(
        "creature_domestic

        [[]
        ]
        [[
        ][[][

        [CREAT:DOG]
            [CASTE:FEMALE]
                [FA[LE]
                [FEMALE]
        ",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `creature_domestic`)
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (ERROR: `]`)
  (comment)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (ERROR: `]`)
  (token
    ([)
    (token_name: ``)
    (])
  )
  (token
    ([)
    (token_name: ``)
    (])
  )
  (token
    ([)
    (token_name: ``)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `CREAT`)
    (token_arguments
      (:)
      (token_argument_reference: `DOG`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `CASTE`)
    (token_arguments
      (:)
      (token_argument_reference: `FEMALE`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `FA`)
    (])
  )
  (token
    ([)
    (token_name: `LE`)
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `FEMALE`)
    (])
  )
  (comment)
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![
        "missing_token_name",
        "missing_end_bracket",
        "missing_token_name",
        "unexpected_end_bracket",
        "missing_token_name",
        "missing_end_bracket",
        "missing_token_name",
        "missing_end_bracket",
        "unexpected_end_bracket",
        "missing_token_name",
        "missing_end_bracket",
        "missing_token_name",
        "missing_token_name",
        "missing_end_bracket",
        "missing_end_bracket",
    ])
    .add_test_lexer_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 2,
                character: 9,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 9,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 10,
            },
            end: Position {
                line: 2,
                character: 10,
            },
        },
        Range {
            start: Position {
                line: 3,
                character: 8,
            },
            end: Position {
                line: 3,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 9,
            },
            end: Position {
                line: 4,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 9,
            },
            end: Position {
                line: 4,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 10,
            },
            end: Position {
                line: 4,
                character: 10,
            },
        },
        Range {
            start: Position {
                line: 4,
                character: 10,
            },
            end: Position {
                line: 4,
                character: 10,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 8,
            },
            end: Position {
                line: 5,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 10,
            },
            end: Position {
                line: 5,
                character: 10,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 10,
            },
            end: Position {
                line: 5,
                character: 10,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 11,
            },
            end: Position {
                line: 5,
                character: 11,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 13,
            },
            end: Position {
                line: 5,
                character: 13,
            },
        },
        Range {
            start: Position {
                line: 5,
                character: 13,
            },
            end: Position {
                line: 5,
                character: 13,
            },
        },
        Range {
            start: Position {
                line: 9,
                character: 19,
            },
            end: Position {
                line: 9,
                character: 19,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_missing_brackets_end_2() {
    LexerTestBuilder::test_source(
        "creature_domestic

        [999",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `creature_domestic`)
  (comment)
  (token
    ([)
    (token_name: ``)
    (ERROR: `999`)
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec![
        "missing_token_name",
        "unexpected_characters",
        "missing_end_bracket",
    ])
    .add_test_lexer_diagnostics_ranges(vec![
        Range {
            start: Position {
                line: 2,
                character: 9,
            },
            end: Position {
                line: 2,
                character: 9,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 9,
            },
            end: Position {
                line: 2,
                character: 12,
            },
        },
        Range {
            start: Position {
                line: 2,
                character: 12,
            },
            end: Position {
                line: 2,
                character: 12,
            },
        },
    ])
    .run_test();
}

#[test]
fn test_missing_brackets_end_3() {
    LexerTestBuilder::test_source(
        "creature_domestic
        [REF",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `creature_domestic`)
  (comment)
  (token
    ([)
    (token_name: `REF`)
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 1,
            character: 12,
        },
        end: Position {
            line: 1,
            character: 12,
        },
    }])
    .run_test();
}

#[test]
fn test_missing_brackets_extra_bracket() {
    LexerTestBuilder::test_source(
        "creature_domestic
        [MAIN:TYPE1]

        [TYPE1:DOG]
            [ITE[M:T1]",
    )
    .add_test_s_exp(
        "(raw_file
  (header: `creature_domestic`)
  (comment)
  (token
    ([)
    (token_name: `MAIN`)
    (token_arguments
      (:)
      (token_argument_reference: `TYPE1`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `TYPE1`)
    (token_arguments
      (:)
      (token_argument_reference: `DOG`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `ITE`)
    (])
  )
  (token
    ([)
    (token_name: `M`)
    (token_arguments
      (:)
      (token_argument_reference: `T1`)
    )
    (])
  )
  (EOF: ``)
)
",
    )
    .add_test_lexer_diagnostics_codes(vec!["missing_end_bracket"])
    .add_test_lexer_diagnostics_ranges(vec![Range {
        start: Position {
            line: 4,
            character: 16,
        },
        end: Position {
            line: 4,
            character: 16,
        },
    }])
    .run_test();
}
