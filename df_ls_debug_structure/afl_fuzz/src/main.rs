#[macro_use]
extern crate afl;

use df_ls_debug_structure::MainToken;

// Fuzz all the Source files that can be given to the application by the user.
fn main() {
    fuzz_debug_structure_source();
}

#[allow(dead_code)]
fn fuzz_debug_structure_source() {
    fuzz!(|data: &[u8]| {
        if let Ok(source) = std::str::from_utf8(data) {
            let (tree, _) = df_ls_lexical_analysis::tokenize_df_raw_file(source.to_owned(), false);
            df_ls_syntax_analysis::do_syntax_analysis_direct::<MainToken>(&tree, &source, false);
        }
    });
}
