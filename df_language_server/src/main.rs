#![forbid(unsafe_code)]
#![deny(clippy::all)]

mod check_source;
mod combined_logger;
mod print_diagnostics;
mod server;
mod simple_logger;

use check_source::check_source;
use clap::Parser;
use df_ls_syntax_analysis::DfLsConfig;
use log::LevelFilter;
use print_diagnostics::DiagnosticFormat;
use std::{path::PathBuf, str::FromStr};

/// Provide custom error with links for when application panics (unrecoverable error).
#[macro_use]
mod panic_hook;

/// The available command line parameters.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Opts {
    /// Activates debug mode.
    /// This will make the application more verbose and might do additional checks.
    #[arg(short, long)]
    debug: bool,

    /// Activates quiet mode, no log message in std out.
    #[arg(short, long)]
    quiet: bool,

    /// Use simple logger
    ///
    /// This logger only logs to std_out/err, not to a file.
    #[arg(long)]
    simple_logger: bool,

    /// Verbose mode (-v, -vv)
    #[arg(short, long, action = clap::ArgAction::Count)]
    verbose: u8,

    /// All available commands
    #[command(subcommand)]
    cmd: Commands,
}

/// All available commands in DF Language server.
#[derive(Parser, Debug)]
enum Commands {
    /// Start the language server.
    Server {
        #[command(subcommand)]
        sub_cmd: ServerSubcommand,
    },
    /// Analyze the current file/mod/project and report errors.
    ///
    /// Check a file/folder for warnings and errors.
    ///
    /// Note: All log messages are printed to stderr, only output data is written to stdout.
    /// You can use the `-q` or `--quite` option to no print the log data to stderr.
    Check {
        /// Specify the file to check for warnings and errors.
        ///
        /// If no file is specified it will check the current directory.
        file: Option<PathBuf>,

        /// Specify the format the output will be printed as.
        ///
        /// Supported formats: {n}
        /// - `Text`: Human readable text. (default) {n}
        /// - `Json`: JSON format. (zero-based line numbers)
        ///
        /// Note: Line number are zero-based in some cases. This means that the first line the file
        /// is line `0` not line `1`.
        #[arg(short, long)]
        format: Option<DiagnosticFormat>,

        /// Print output in a more human friendly way.
        /// This will add newlines and indentations.
        ///
        /// Not all formats support pretty print.
        #[arg(long)]
        pretty_print: bool,

        /// Allow checker to use the debug structure.
        /// This is useful when debugging the lexer or syntax analyzer.
        /// This is also useful when fixing or debugging tests.
        ///
        /// This option is only available in debug builds.
        #[cfg(debug_assertions)]
        #[arg(long)]
        debug_structure: bool,
    },
    // /// Create a new mod or other project.
    // New { },
}

/// All available subcommands in DF Language server.
#[derive(Parser, Debug)]
enum ServerSubcommand {
    /// Start Language server on a TCP socket
    Tcp {
        /// Specify the port to start the server.
        ///
        /// The default debug port is `2087`.
        /// If no port is specified it will use an available port requested from the OS.
        #[arg(short, long)]
        port: Option<u16>,
    },
    /// Start language server on stdin/stdout
    Stdio {},
}

impl FromStr for DiagnosticFormat {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lowercase_s = s.to_lowercase();

        match lowercase_s.as_ref() {
            "text" => Ok(DiagnosticFormat::Text),
            "json" => Ok(DiagnosticFormat::Json),
            _ => Err("Unknown format.".to_owned()),
        }
    }
}

// This is the main functions where the language server starts its execution path.
// Tokio allows for multi-threading (job scheduler).
#[tokio::main]
async fn main() {
    setup_panic!();
    // Get command line arguments
    let opts: Opts = Opts::parse();
    // Get log settings
    initialize_logger(&opts);

    log::debug!("Command arguments: {:?}", opts);
    log_system_info();

    // Check what subcommand we want to run
    match opts.cmd {
        Commands::Server { sub_cmd } => match sub_cmd {
            ServerSubcommand::Tcp { port } => {
                log::info!("Start Language server via TCP");
                server::start_language_server_tcp(port, opts.debug).await;
            }
            ServerSubcommand::Stdio {} => {
                log::info!("Start Language server on stdin/stdout");
                server::start_language_server_stdio(opts.debug).await;
            }
        },
        Commands::Check {
            file,
            format,
            pretty_print,
            #[cfg(debug_assertions)]
            debug_structure,
        } => {
            if let Some(file_path) = file {
                // Checking only 1 file.
                log::info!("Checking file.");
                // Check if the path points to a file.
                if !file_path.is_file() {
                    panic!(
                        "The given path is not a file, or the application \
                        does not have access to this file."
                    );
                }
                let source_code = std::fs::read(file_path).expect("Could not read test file");
                let config = DfLsConfig::default();

                #[cfg(debug_assertions)]
                let diagnostics = if debug_structure {
                    check_source::check_source_structure::<df_ls_debug_structure::DebugRaw>(
                        &source_code,
                        opts.debug,
                        &config,
                    )
                } else {
                    check_source::check_source(&source_code, opts.debug, &config)
                };

                #[cfg(not(debug_assertions))]
                let diagnostics = check_source::check_source(&source_code, opts.debug, &config);

                // Use `Text` as default.
                let format = format.unwrap_or(DiagnosticFormat::Text);
                print_diagnostics::print_diagnostics(diagnostics, format, pretty_print);
            } else {
                // Checking multiple files.
                log::info!("Checking mod/project.");
                panic!(
                    "Checking a full project is not yet supported. \
                    For more info see: \
                    https://gitlab.com/df-modding-tools/df-raw-language-server/-/issues/107"
                );
            }
        }
    }
    log::info!("Stopping application.");
}

/// Setup logger. This will select where to print the log message and how many.
fn initialize_logger(opts: &Opts) {
    let log_filter: LevelFilter = if opts.debug {
        if opts.verbose >= 2 {
            LevelFilter::Trace
        } else {
            LevelFilter::Debug
        }
    } else if opts.quiet {
        LevelFilter::Off
    } else {
        match opts.verbose {
            0 => LevelFilter::Info,
            1 => LevelFilter::Debug,
            _ => LevelFilter::Trace,
        }
    };
    // Setup logger and log level
    if opts.simple_logger {
        log::set_logger(&simple_logger::LOGGER).unwrap();
        log::set_max_level(log_filter);
    } else {
        combined_logger::setup_logger(log_filter);
    }
}

fn log_system_info() {
    // Log basic info
    log::info!("---- System Info ----");
    // Example: `Bin: df_language_server: v0.2.0`
    log::info!(
        "Bin: {}: v{}",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );
    // Example: `OS: linux`
    log::info!("OS: {}", std::env::consts::OS);
    // Example: `OS Arch: x86_64`
    log::info!("OS Arch: {}", std::env::consts::ARCH);
    log::info!("---------------------");
}
