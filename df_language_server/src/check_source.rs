use colored::*;
use df_ls_diagnostics::lsp_types::{Diagnostic, NumberOrString, Range};
use df_ls_lexical_analysis::AstFormatSettings;
use df_ls_structure::DFRaw;
use df_ls_syntax_analysis::{DfLsConfig, TokenDeserialize};
use std::fmt::Debug;

/// Set to `true` to print additional output useful for creating or fixing tests.
static OUTPUT_FOR_TESTS: bool = false;

/// Check the source of errors and warnings
pub fn check_source(source: &[u8], debug: bool, config: &DfLsConfig) -> Vec<Diagnostic> {
    check_source_structure::<DFRaw>(source, debug, config)
}

pub(crate) fn check_source_structure<S: TokenDeserialize + Debug>(
    source: &[u8],
    debug: bool,
    config: &DfLsConfig,
) -> Vec<Diagnostic> {
    use std::time::Instant;
    let now = Instant::now();
    // Convert source from CP437 to UTF-8
    let source_bytes = df_cp437::convert_cp437_to_utf8(source);
    let source = String::from_utf8(source_bytes).expect("Non UTF-8 characters found");
    // 1: Do Lexical Analysis (Tokenizer)
    print_in_hline("Start Lexical Analysis", debug);
    let (tree, diagnostic_lexer) = df_ls_lexical_analysis::do_lexical_analysis(&source);
    if debug {
        let diagnostic_list_lexer = diagnostic_lexer.get_diagnostic_list();
        df_ls_lexical_analysis::print_ast(
            &tree,
            &source,
            &AstFormatSettings {
                color: true,
                tab_width: 2,
                print_formatted_code: true,
                show_comments: true,
                replace_comment_whitespace: true,
                print_comment_content: false,
                print_s_exp: true,
                show_unnamed: true,
                print_token_content: true,
                use_old_format: false,
            },
        );
        println!("Diagnostics Lexer: {:#?}", diagnostic_list_lexer);
        if OUTPUT_FOR_TESTS {
            println!(
                "Diagnostics Lexer Range: {:#?}",
                diagnostic_list_lexer
                    .iter()
                    .map(|item| item.range)
                    .collect::<Vec<Range>>()
            );
            println!(
                "Diagnostics Lexer Code: {:#?}",
                diagnostic_list_lexer
                    .iter()
                    .map(|item| {
                        let code = item
                            .code
                            .clone()
                            .unwrap_or_else(|| NumberOrString::String("".to_owned()));
                        match code {
                            NumberOrString::String(s) => s,
                            NumberOrString::Number(_) => "".to_owned(),
                        }
                    })
                    .collect::<Vec<String>>()
            );
        }
        df_ls_syntax_analysis::print_source_with_diagnostics(&source, diagnostic_list_lexer);
    }
    print_in_hline("End Lexical Analysis", debug);
    // If we reached a message limit or lock, stop further parsing.
    if diagnostic_lexer.check_message_limit_reached() | diagnostic_lexer.get_message_lock_state() {
        log::info!("Message limit reached, ending checks.");
        return diagnostic_lexer.extract_diagnostic_list();
    }
    // 2: Do Syntax Analysis
    print_in_hline("Start Syntax Analysis", debug);
    let (structure, diagnostic_syntax): (S, _) =
        df_ls_syntax_analysis::do_syntax_analysis(&tree, &source, config);
    if debug {
        let diagnostic_list_syntax = diagnostic_syntax.get_diagnostic_list();
        println!("\nStruct: {:#?}\n", structure);
        println!("Diagnostics syntax: {:#?}", &diagnostic_list_syntax);
        df_ls_syntax_analysis::print_source_with_diagnostics(&source, diagnostic_list_syntax);
    }
    print_in_hline("End Syntax Analysis", debug);

    // If we reached a message limit or lock, stop further parsing.
    if diagnostic_syntax.check_message_limit_reached() | diagnostic_syntax.get_message_lock_state()
    {
        log::info!("Message limit reached, ending checks.");
        return vec![
            diagnostic_lexer.extract_diagnostic_list(),
            diagnostic_syntax.extract_diagnostic_list(),
        ]
        .concat();
    }

    // Disabled for initial release
    // 3: Do Semantic Analysis
    // print_in_hline("Start Semantic Analysis", debug);
    // let diagnostic_list_semantics = df_ls_semantic_analysis::do_semantic_analysis(&structure);
    // if debug {
    //     println!("Diagnostics semantics: {:#?}", diagnostic_list_semantics);
    // }
    // print_in_hline("End Semantic Analysis", debug);

    log::info!("This took: {} millisec", now.elapsed().as_millis());
    vec![
        diagnostic_lexer.extract_diagnostic_list(),
        diagnostic_syntax.extract_diagnostic_list(),
        // diagnostic_list_semantics,
    ]
    .concat()
}

fn print_in_hline(text: &str, debug: bool) {
    if debug {
        println!(
            "{}",
            format!("-------------------{}-------------------", text).bright_cyan()
        );
    }
}
#[cfg(test)]
mod tests {
    use super::*;
    use crate::simple_logger;
    use log::LevelFilter;

    #[test]
    fn test_basic_df_raw() {
        use pretty_assertions::assert_eq;
        // Setup logger and log level
        log::set_logger(&simple_logger::LOGGER).unwrap();
        log::set_max_level(LevelFilter::Trace);

        let source = "creature_domestic

        [OBJECT:CREATURE]

        [CREATURE:DOG]
            [CASTE:FEMALE]
                [FEMALE]
            [CASTE:MALE]
                [MALE]
            [SELECT_CASTE:ALL]
                [NATURAL]
        ";
        let config = DfLsConfig::default();
        let diagnostic_list = check_source(source.as_bytes(), true, &config);
        println!("Diagnostics: {:#?}", diagnostic_list);
        assert_eq!(diagnostic_list, vec![]);
    }

    #[test]
    fn test_basic_cp437_df_raw() {
        use pretty_assertions::assert_eq;
        let source = b"descriptor_color_standard

        [OBJECT:DESCRIPTOR_COLOR]

        Simple test\x9ctest

        [COLOR:AMBER]
            [NAME:ambe\x8cr]
            [RGB:255:191:0]
            [WORD:AMBER]
        ";
        let config = DfLsConfig::default();
        let diagnostic_list = check_source(source, true, &config);
        println!("Diagnostics: {:#?}", diagnostic_list);
        assert_eq!(diagnostic_list, vec![]);
    }
}
