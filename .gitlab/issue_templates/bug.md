# Bug Report

## How to reproduce
<!-- Show us the raw file code that caused the bug, or if that's not enough to cause the bug, explain how to reproduce it. -->
```
Code goes here:

```

How often does this bug happen?: 
<!-- (Always, sometimes, almost never etc...) -->

## Result
<!-- Add a screenshot of the bug/result you got from the code/steps above, or explain the result instead; ideally both. -->


## Expected
<!-- Explain what should be happening instead. -->


## Additional information
<!-- 
Include the log, if you have any, they can be found here:
- Linux: /home/username/.local/share/df_language_server/
- Windows: C:\Users\username\AppData\Local\DF_Modding_Tools\DF_Language_Server\data\
- Mac: /Users/username/Library/Application Support/org.DF_Modding_Tools.DF_Language_Server/
-->


## Version
<!-- Which version of DF Language Sever are you using, and what OS? -->
- DF Language Server version:
- OS: (Windows, Linux, Mac)
- OS Version: 

<!-- Don't remove the following line! -->
/label ~Bug