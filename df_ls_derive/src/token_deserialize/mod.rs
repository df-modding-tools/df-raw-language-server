mod enum_value;
mod struct_de_info;
mod token_de_info;

use crate::common;
use enum_value::*;
use quote::quote;
pub use struct_de_info::*;
use syn::Ident;
pub use token_de_info::*;

pub fn impl_token_deserialize_macro(ast: &syn::DeriveInput) -> proc_macro2::TokenStream {
    let name = &ast.ident;
    let fields = common::get_struct_enum_fields(ast);
    let (struct_info, struct_err) = get_struct_enum_de_info(ast);
    // If this had the `enum_value` set, use different function.
    if struct_info.enum_value {
        return impl_token_deserialize_macro_enum_value(ast);
    }

    let parameter_checks = if struct_info.second_par_check {
        let token_name = struct_info.token_name.clone();
        let match_second_par = list_match_second_par(&fields);
        let allowed_second_args = list_allowed_tokens_strings(&fields, ast)
            .iter()
            .map(|item| format!("`{}`", item))
            .collect::<Vec<String>>()
            .join(", ");
        quote! {
            let mut token = match Token::deserialize_tokens(&mut cursor, source, &mut diagnostics, config){
                Ok(token) => token,
                Err(err) => {
                    // When token could not be parsed correctly.
                    // Token could not be parsed, so we can consume it.
                    // Because this will always fail.
                    Token::consume_token(&mut cursor).unwrap();
                    return (LoopControl::ErrBreak, new_self);
                }
            };
            // Token is consumed lower down.
            // Token Name
            match token.check_token_name(source, &mut diagnostics, false){
                Ok(_) => {},
                Err(_) => {
                    // Go back up the stack
                    if *new_self == Self::default() {
                        return (LoopControl::ErrBreak, new_self);
                    }
                    return (LoopControl::Break, new_self);
                }
            };
            let token_name = match token.checked_get_token_name(source, &mut diagnostics, true){
                Ok(name) => name,
                Err(_) => {
                    return (LoopControl::ErrBreak, new_self);
                }
            };
            // Check if token name is expected name
            if &token_name.value != &#token_name {
                // If nothing changed
                if *new_self == Self::default() {
                    return (LoopControl::ErrBreak, new_self);
                }
                return (LoopControl::Break, new_self);
            }
            if let Err(_) = Token::consume_token(&mut cursor) {
                return (LoopControl::Break, new_self);
            }
            // Arg 0
            let arg0 = match token.checked_get_current_arg(source, &mut diagnostics, true){
                Ok(arg) => arg,
                Err(_) => {
                    return (LoopControl::ErrBreak, new_self);
                }
            };
            token.consume_argument();
            if let Err(_) = token.check_all_arg_consumed(source, &mut diagnostics, true) {
                return (LoopControl::Break, new_self);
            }
            match &arg0.value {
                TokenArgument::TVReference(value) => {
                    log::debug!("Matching {} in {}", value, stringify!(#name));
                    match value.as_ref() as &str {
                        #match_second_par
                        object_type => {
                            diagnostics.add_message(DMExtraInfo {
                                range: arg0.node.get_range(),
                                message_template_data: hash_map! {
                                    "found_parameter" => format!("`{}`", object_type),
                                    "token_name" => format!("`{}`", #token_name),
                                    "valid_types" => #allowed_second_args.to_owned(),
                                },
                            }, "invalid_second_par_type");
                        }
                    }
                }
                _ => {
                    log::error!("LS Error: arguments not the same type but was already checked.");
                }
            }
        }
    } else {
        let match_first_par = list_match_first_par(&fields, ast);
        // Primary token is the main token that has to be set before adding data. (issue #36)
        let primary_token = get_primary_token(&fields);
        let primary_token_check = if let Some((primary_token, primary_token_name)) = primary_token {
            quote! {
                let primary_token_filled = new_self.#primary_token.is_some();
                let primary_token_name: Option<String> = Some(#primary_token_name.to_owned());
            }
        } else {
            quote! {
                let primary_token_filled = true;
                let primary_token_name: Option<String> = None;
            }
        };
        quote! {
            let mut token = match Token::deserialize_tokens(&mut cursor, source, &mut diagnostics, config){
                Ok(token) => token,
                Err(err) => {
                    // When token could not be parsed correctly
                    // Token could not be parsed, so we can consume it.
                    // Because this will always fail.
                    Token::consume_token(&mut cursor).unwrap();
                    return (LoopControl::ErrBreak, new_self);
                }
            };
            // No `Token::consume_token(&mut cursor)?;` here because the token will be consumed when
            // all arguments can be stored inside of object
            #primary_token_check
            // Token name
            if let Err(_) = token.check_token_name(source, &mut diagnostics, true) {
                return (LoopControl::Break, new_self);
            }
            let token_name = match token.checked_get_token_name(source, &mut diagnostics, true){
                Ok(name) => name,
                Err(_) => {
                    return (LoopControl::ErrBreak, new_self);
                }
            };
            log::debug!("Matching {} in {}", token_name.value, stringify!(#name));
            match token_name.value.as_ref() as &str {
                #match_first_par
                _ => {
                    // If nothing changed
                    if *new_self == Self::default() {
                        return (LoopControl::ErrBreak, new_self);
                    }
                    // Go back up to parent
                    return (LoopControl::Break, new_self);
                }
            }
        }
    };
    // Create the list of allowed tokens
    // This is used for checking in `Vec<>`
    let allowed_tokens = if struct_info.second_par_check {
        let token_name = struct_info.token_name;
        quote! { vec![#token_name.to_owned()] }
    } else {
        let list = list_allowed_tokens(&fields, ast);
        quote! { vec![#list] }
    };

    quote! {
        #struct_err
        impl TokenDeserialize for #name {
            #[allow(unused_variables)]
            fn deserialize_general_token(
                mut cursor: &mut df_ls_syntax_analysis::TreeCursor,
                source: &str,
                mut diagnostics: &mut df_ls_diagnostics::DiagnosticsInfo,
                config: &df_ls_core::DfLsConfig,
                mut new_self: Box<Self>,
            ) -> (df_ls_syntax_analysis::LoopControl, Box<Self>) {
                use df_ls_syntax_analysis::{LoopControl, TreeCursor,
                    TokenArgument, Token};
                use df_ls_diagnostics::{DiagnosticsInfo, DMExtraInfo, hash_map};

                let node = cursor.node();
                #parameter_checks
                #[allow(unreachable_code)]
                (LoopControl::DoNothing, new_self)
            }

            fn get_allowed_tokens(_config: &df_ls_core::DfLsConfig) -> Option<Vec<String>> {
                Some(#allowed_tokens)
            }
        }
        // ------------------------- Convert a group of arguments to Self -----------------------

        // TryFromArgumentGroup not implemented because it is not used
        // This struct is used to deserialize 1 or more tokens, not token arguments

        // -------------------------Convert one argument to Self -----------------------

        // TryFromArgument not implemented because it is not used
        // This struct is used to deserialize 1 or more tokens, not a token argument

    }
}

fn list_match_first_par(
    struct_fields: &[syn::Field],
    ast: &syn::DeriveInput,
) -> proc_macro2::TokenStream {
    let mut parse_gen = quote! {};
    for (_i, field) in struct_fields.iter().enumerate() {
        let ident = field.ident.as_ref().unwrap();
        let ident_type = &field.ty;
        let ident_inner_type = common::get_inner_type(&field.ty);
        let (token_info, token_err) = get_token_de_info(field);
        let ident_is_vec = common::check_type_is_vec(&field.ty);
        let token_name = token_info.token.token_name;
        // Change what to do when duplicate is reached
        let on_duplicate = if token_info.token.on_duplicate_to_parent {
            quote! {
                // Go back up to parent
                return (LoopControl::Break, new_self);
            }
        } else {
            let primary_token = match get_primary_token(struct_fields) {
                Some((_, token_name)) => token_name,
                _ => "<unknown>".to_owned(),
            };
            if token_info.token.on_duplicate_error {
                quote! {
                    diagnostics.add_message(DMExtraInfo {
                        range: node.get_range(),
                        message_template_data: hash_map! {
                            "token_name" => format!("`{}`", #token_name),
                            "parent_token" => format!("`{}`", #primary_token),
                        },
                    }, "duplicate_token_error");
                    if let Err(_) = Token::consume_token(&mut cursor) {
                        return (LoopControl::Break, new_self);
                    }
                    return (LoopControl::DoNothing, new_self);
                }
            } else {
                quote! {
                    diagnostics.add_message(DMExtraInfo {
                        range: node.get_range(),
                        message_template_data: hash_map! {
                            "token_name" => format!("`{}`", #token_name),
                            "parent_token" => format!("`{}`", #primary_token),
                        },
                    }, "duplicate_token_warn");
                    if let Err(_) = Token::consume_token(&mut cursor) {
                        return (LoopControl::Break, new_self);
                    }
                    return (LoopControl::DoNothing, new_self);
                }
            }
        };
        // If this token is not the primary token, add check if that is filled. (issue #36)
        let primary_token_check = if !token_info.token.primary_token {
            quote! {
                if !primary_token_filled {
                    if let Some(primary_token_name) = primary_token_name{
                        diagnostics.add_message(DMExtraInfo {
                            range: node.get_range(),
                            message_template_data: hash_map! {
                                "expected_tokens" => format!("`{}`", primary_token_name),
                            },
                        }, "token_is_missing");
                    } else {
                        diagnostics.add_message(DMExtraInfo {
                            range: node.get_range(),
                            message_template_data: hash_map! {
                                "expected_tokens" => "<Unknown>".to_owned(),
                            },
                        }, "token_is_missing");
                    }
                    return (LoopControl::ErrBreak, new_self);
                }
            }
        } else {
            quote! {}
        };
        // Add token_name to list of options.
        // Add main token_name
        let mut token_and_alias_names: Vec<DfAliasInfo> = vec![DfAliasInfo {
            token_name: token_name.clone(),
            discouraged: false,
            note: None,
        }];
        // Add all aliases
        token_and_alias_names.append(&mut token_info.aliases.clone());

        // Token Version check: Added
        let min_df_version = token_info.added.as_ref().map(|t| t.since.clone());
        let min_df_version_note =
            if let Some(note) = token_info.added.as_ref().and_then(|t| t.note.clone()) {
                format!(" {}", note)
            } else {
                "".to_owned()
            };
        // TODO move out of derive and into static function
        let min_df_version_message = if let Some(min_df_version) = min_df_version {
            quote! {
                if config.target_df_version.as_str() < #min_df_version {
                    diagnostics.add_message(
                        DMExtraInfo {
                            range: token_name.node.get_range(),
                            message_template_data: hash_map! {
                                "token_name" => format!("`{}`", #token_name),
                                "min_df_version" => format!("`{}`", #min_df_version),
                                "target_df_version" => format!("`{}`", config.target_df_version),
                                "note" => #min_df_version_note.to_string(),
                            },
                        },
                        "added_in_later_version",
                    );
                }
            }
        } else {
            quote! {}
        };

        // Token Version check: Deprecated
        let max_df_version = token_info.deprecated.as_ref().map(|t| t.since.clone());
        let max_df_version_note =
            if let Some(note) = token_info.deprecated.as_ref().and_then(|t| t.note.clone()) {
                format!(" {}", note)
            } else {
                "".to_owned()
            };
        // TODO move out of derive and into static function
        let max_df_version_message = if let Some(max_df_version) = max_df_version {
            quote! {
                if config.target_df_version.as_str() >= #max_df_version {
                    diagnostics.add_message(
                        DMExtraInfo {
                            range: token_name.node.get_range(),
                            message_template_data: hash_map! {
                                "token_name" => format!("`{}`", #token_name),
                                "max_df_version" => format!("`{}`", #max_df_version),
                                "target_df_version" => format!("`{}`", config.target_df_version),
                                "note" => #max_df_version_note.to_string(),
                            },
                        },
                        "deprecated_in_earlier_version",
                    );
                }
            }
        } else {
            quote! {}
        };

        // Token Version check: Issues
        let mut issue_messages = quote! {};
        for issue_info in token_info.issues {
            let severity = match issue_info.severity {
                SeverityIssue::Error => {
                    quote! { df_ls_diagnostics::lsp_types::DiagnosticSeverity::ERROR }
                }
                SeverityIssue::Warn => {
                    quote! { df_ls_diagnostics::lsp_types::DiagnosticSeverity::WARNING }
                }
                SeverityIssue::Info => {
                    quote! { df_ls_diagnostics::lsp_types::DiagnosticSeverity::INFORMATION }
                }
                SeverityIssue::Hint => {
                    quote! { df_ls_diagnostics::lsp_types::DiagnosticSeverity::HINT }
                }
            };
            let since = match &issue_info.since {
                Some(version) => format!(" since DF version `{}`", version),
                None => "".to_owned(),
            };
            let fixed_in = match &issue_info.fixed_in {
                Some(version) => format!(" and was fixed in `{}`", version),
                None => "".to_owned(),
            };
            let affected = if !since.is_empty() || !fixed_in.is_empty() {
                "is affected".to_owned()
            } else {
                "is likely affected".to_owned()
            };
            let link = match issue_info.link {
                Some(link) => format!(" Issue link: {} .", link),
                None => "".to_owned(),
            };
            let note = match issue_info.note {
                Some(note) => format!(" {}", note),
                None => "".to_owned(),
            };
            // Create message depending on info given.
            let message = quote! {
                diagnostics.add_message_with_severity(
                    DMExtraInfo {
                        range: token_name.node.get_range(),
                        message_template_data: hash_map! {
                            "token_name" => format!("`{}`", #token_name),
                            "since" => format!("{}", #since),
                            "fixed_in" => format!("{}", #fixed_in),
                            "affected" => format!(", your target version (`{}`) {}.", config.target_df_version, #affected),
                            "link" => format!("{}", #link),
                            "note" => #note.to_string(),
                        },
                    },
                    "token_issue",
                    Some(#severity)
                );
            };
            // Check Since version
            let message = if let Some(since_version) = &issue_info.since {
                quote! {
                    if config.target_df_version.as_str() >= #since_version {
                        #message
                    }
                }
            } else {
                message
            };
            // Check Fixed_in version
            let message = if let Some(fixed_in_version) = &issue_info.fixed_in {
                quote! {
                    if config.target_df_version.as_str() < #fixed_in_version {
                        #message
                    }
                }
            } else {
                message
            };
            issue_messages = quote! {
                #issue_messages
                #message
            };
        }

        // Loop over all options for the names used to set this value.
        for token in token_and_alias_names {
            let temp_token_name = token.token_name;
            let temp_note = if let Some(note) = token.note {
                format!(" {}", note)
            } else {
                "".to_owned()
            };
            let alias_message = if token.discouraged {
                quote! {
                    diagnostics.add_message(
                        DMExtraInfo {
                            range: token_name.node.get_range(),
                            message_template_data: hash_map! {
                                "alias_name" => format!("`{}`", #temp_token_name),
                                "suggested_name" => format!("`{}`", #token_name),
                                "note" => #temp_note.to_string(),
                            },
                        },
                        "alias",
                    );
                }
            } else {
                quote! {}
            };
            // Add option to list of options.
            // They change depending on the type of data structure they are in
            // - Struct with vec
            // - Struct without vec
            // - Enum
            match &ast.data {
                syn::Data::Struct(_x) => {
                    if ident_is_vec {
                        parse_gen = quote! {
                            #parse_gen
                            #temp_token_name => {
                                #token_err
                                #primary_token_check
                                #alias_message
                                #min_df_version_message
                                #max_df_version_message
                                #issue_messages
                                // `#ident_type` = `Vec<...>`
                                let value: Result<Box<#ident_type>, _> = TokenDeserialize::deserialize_tokens(
                                    &mut cursor,
                                    source,
                                    &mut diagnostics,
                                    config,
                                );
                                if let Ok(mut value) = value {
                                    // Append because we want to parse a whole list, not just 1 item
                                    new_self.#ident.append(value.as_mut());
                                }
                                // Don't go to next token, we are still matching this token.
                                return (LoopControl::Continue, new_self);
                            }
                        };
                    } else {
                        parse_gen = quote! {
                            #parse_gen
                            #temp_token_name => {
                                #token_err
                                if new_self.#ident.is_some() {
                                    #on_duplicate
                                }
                                #primary_token_check
                                #alias_message
                                #min_df_version_message
                                #max_df_version_message
                                #issue_messages
                                // `#ident_type` = `Option<T>` so `#ident_inner_type` will be `T`
                                let value: Result<Box<#ident_inner_type>, _> = TokenDeserialize::deserialize_tokens(
                                    &mut cursor,
                                    source,
                                    &mut diagnostics,
                                    config,
                                );
                                match value {
                                    Ok(value) => {
                                        new_self.#ident = Some(*value);
                                    }
                                    Err(_) => { /* Error is already in diagnostics */ }
                                }
                            }
                        };
                    }
                }
                syn::Data::Enum(_x) => {
                    parse_gen = quote! {
                        #parse_gen
                        #temp_token_name => {
                            #token_err
                            #primary_token_check
                            #alias_message
                            #min_df_version_message
                            #max_df_version_message
                            #issue_messages
                            let value: Result<Box<#ident_type>, _> = TokenDeserialize::deserialize_tokens(
                                &mut cursor,
                                source,
                                &mut diagnostics,
                                config,
                            );
                            match value {
                                Ok(value) => {
                                    new_self = Box::new(Self::#ident(*value));
                                    return (LoopControl::Break, new_self);
                                }
                                Err(_) => { /* Error is already in diagnostics */ }
                            }
                            return (LoopControl::Continue, new_self);
                        }
                    };
                }
                syn::Data::Union(_x) => {
                    unimplemented!("Union is not implemented for TokenDeserialize.");
                }
            }
        }
    }
    parse_gen
}

fn list_match_second_par(struct_fields: &[syn::Field]) -> proc_macro2::TokenStream {
    let mut parse_gen = quote! {};
    for (_i, field) in struct_fields.iter().enumerate() {
        let ident = field.ident.as_ref().unwrap();
        let ident_type = &field.ty;
        let (token_info, _) = get_token_de_info(field);
        let ident_is_vec = common::check_type_is_vec(&field.ty);
        let token_name = token_info.token.token_name;
        if ident_is_vec {
            parse_gen = quote! {
                #parse_gen
                #token_name => {
                    // `#ident_type` = `Vec<...>`
                    let value: Result<Box<#ident_type>, _> = TokenDeserialize::deserialize_tokens(
                        &mut cursor,
                        source,
                        &mut diagnostics,
                        config,
                    );
                    if let Ok(mut value) = value {
                        // Append because we want to parse a whole list, not just 1 item
                        new_self.#ident.append(value.as_mut());
                    }
                    // Don't go to next token, we are still matching this token.
                    return (LoopControl::Continue, new_self);
                }
            };
        } else {
            panic!(
                "Only `Vec<>` types are supported for member \
                field of a struct that uses `token_de(second_par_check)`."
            );
        }
    }
    parse_gen
}

pub(super) fn list_allowed_tokens(
    struct_fields: &[syn::Field],
    ast: &syn::DeriveInput,
) -> proc_macro2::TokenStream {
    let mut parse_gen = quote! {};
    for (_i, field) in struct_fields.iter().enumerate() {
        let (token_info, _) = get_token_de_info(field);
        let token_name = token_info.token.token_name;

        match &ast.data {
            syn::Data::Struct(_x) => {
                parse_gen = quote! {
                    #parse_gen #token_name.to_owned(),
                };
                for alias in token_info.aliases {
                    let alias_name = alias.token_name;
                    parse_gen = quote! {
                        #parse_gen #alias_name.to_owned(),
                    };
                }
            }
            syn::Data::Enum(_x) => {
                parse_gen = quote! {
                    #parse_gen #token_name.to_owned(),
                };
                for alias in token_info.aliases {
                    let alias_name = alias.token_name;
                    parse_gen = quote! {
                        #parse_gen #alias_name.to_owned(),
                    };
                }
            }
            syn::Data::Union(_x) => {
                unimplemented!("Union is not implemented for TokenDeserialize.");
            }
        }
    }
    parse_gen
}

pub(super) fn list_allowed_tokens_strings(
    struct_fields: &[syn::Field],
    ast: &syn::DeriveInput,
) -> Vec<String> {
    let mut list = vec![];
    for (_i, field) in struct_fields.iter().enumerate() {
        let (token_info, _) = get_token_de_info(field);
        let token_name = token_info.token.token_name;

        match &ast.data {
            syn::Data::Struct(_x) => {
                list.push(token_name);
            }
            syn::Data::Enum(_x) => {
                list.push(token_name);
            }
            syn::Data::Union(_x) => {
                unimplemented!("Union is not implemented for TokenDeserialize.");
            }
        }
    }
    list
}

fn get_primary_token(struct_fields: &[syn::Field]) -> Option<(Ident, String)> {
    let mut result = None;
    for (_i, field) in struct_fields.iter().enumerate() {
        let ident = field.ident.as_ref().unwrap();
        let (token_info, _) = get_token_de_info(field);
        if token_info.token.primary_token {
            let token_name = token_info.token.token_name;
            if result.is_none() {
                result = Some((ident.clone(), token_name));
            } else {
                panic!("ERROR: Only one primary_token can be set.");
            }
        }
    }
    result
}
