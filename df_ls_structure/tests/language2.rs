use df_ls_core::{AllowEmpty, ReferenceTo};
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_structure::*;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn simple_test() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "language_words

            [OBJECT:LANGUAGE]

            [WORD:ABBEY]
                [NOUN:abbey:abbeys]
                    [FRONT_COMPOUND_NOUN_SING]
                    [REAR_COMPOUND_NOUN_SING]
                    [THE_NOUN_SING]
                    [REAR_COMPOUND_NOUN_PLUR]
                    [OF_NOUN_PLUR]

            [WORD:ACE]
                [NOUN:ace:aces]
                    [FRONT_COMPOUND_NOUN_SING]
                    [REAR_COMPOUND_NOUN_SING]
                    [THE_NOUN_SING]
                    [REAR_COMPOUND_NOUN_PLUR]
                    [OF_NOUN_PLUR]
                [ADJ:ace]
                    [ADJ_DIST:1]
                    [FRONT_COMPOUND_ADJ]
                    [THE_COMPOUND_ADJ]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DFRaw {
        header: "language_words".to_owned(),
        token_structure: vec![ObjectToken {
            language_tokens: vec![
                LanguageToken::WordToken(WordToken {
                    reference: Some(ReferenceTo::new("ABBEY".to_owned())),
                    nouns: vec![NounToken {
                        words: Some(("abbey".to_owned(), AllowEmpty::Some("abbeys".to_owned()))),
                        front_compound_noun_sing: Some(()),
                        read_compound_noun_sing: Some(()),
                        the_compound_noun_sing: None,
                        the_noun_sing: Some(()),
                        of_noun_sing: None,
                        front_compound_noun_plur: None,
                        read_compound_noun_plur: Some(()),
                        the_compound_noun_plur: None,
                        the_noun_plur: None,
                        of_noun_plur: Some(()),
                    }],
                    adj: vec![],
                    verb: vec![],
                    prefix: vec![],
                }),
                LanguageToken::WordToken(WordToken {
                    reference: Some(ReferenceTo::new("ACE".to_owned())),
                    nouns: vec![NounToken {
                        words: Some(("ace".to_owned(), AllowEmpty::Some("aces".to_owned()))),
                        front_compound_noun_sing: Some(()),
                        read_compound_noun_sing: Some(()),
                        the_compound_noun_sing: None,
                        the_noun_sing: Some(()),
                        of_noun_sing: None,
                        front_compound_noun_plur: None,
                        read_compound_noun_plur: Some(()),
                        the_compound_noun_plur: None,
                        the_noun_plur: None,
                        of_noun_plur: Some(()),
                    }],
                    adj: vec![AdjToken {
                        words: Some("ace".to_owned()),
                        adj_dist: Some(1),
                        front_compound_adj: Some(()),
                        the_compound_adj: Some(()),
                        rear_compound_adj: None,
                    }],
                    verb: vec![],
                    prefix: vec![],
                }),
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
