use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum MaleOrFemaleEnum {
    #[df_token(token_name = "MALE")]
    Male,
    #[df_token(token_name = "FEMALE")]
    Female,
}

impl Default for MaleOrFemaleEnum {
    fn default() -> Self {
        Self::Male
    }
}
