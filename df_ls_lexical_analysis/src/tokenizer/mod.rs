mod df_raw;
mod header;
mod regex_list;
mod structure;
mod token;
mod token_argument;
mod token_arguments;
mod token_body;
mod token_name;
mod token_pipe_arguments;
mod tokenizer_end;
mod tokenizer_helper;
mod utils;

pub use df_raw::tokenize_df_raw_file;
pub(crate) use regex_list::RegexList;
pub use structure::*;
use tokenizer_end::TokenizerEnd;
pub(crate) use tokenizer_helper::{TokenMatchStatus, TokenizerHelper};

type TokenizerResult = Result<(), TokenizerEnd>;
