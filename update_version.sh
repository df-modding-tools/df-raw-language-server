#!/bin/bash

# exit when any command fails
set -e
# Debug this script
# set -x

# This tool is for updating all the version numbers in 1 go

# New version that should be inserted
# Always use [Semantic Versioning numbers](https://semver.org/)
# Examples: 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-alpha.beta < 1.0.0-beta < 1.0.0-beta.2
#           < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0
NEW_VER="0.4.1"

echo "Changing version number to version: '$NEW_VER'"

# List of all the files to update
FILE=(
  "./df_language_server/Cargo.toml"
  "./df_ls_core/Cargo.toml"
  "./df_ls_debug_structure/Cargo.toml"
  "./df_ls_derive/Cargo.toml"
  "./df_ls_diagnostics/Cargo.toml"
  "./df_ls_lexical_analysis/Cargo.toml"
  "./df_ls_semantic_analysis/Cargo.toml"
  "./df_ls_structure/Cargo.toml"
  "./df_ls_syntax_analysis/Cargo.toml"
)

# This code asumes you have `sd` installed:
# https://github.com/chmln/sd
# `cargo install sd`

# Allow `>=` pre-fix and keep the prefix
for file in ${FILE[@]}
do
  sd "version = \"(>=)?([0-9.a-z-+])+\" #:version" \
     "version = \"\${1}$NEW_VER\" #:version" $file
done

# ---- Update json -----
# List of all the files to update
#
# Note: VSCode does has some odd rules about version numbering, pre-release and release
# See for more info: https://code.visualstudio.com/api/working-with-extensions/publishing-extension#prerelease-extensions
FILE=(
  "./df_ls_clients/df_ls_vscode/package.json"
)
# update json
for file in ${FILE[@]}
do
  sd "\"version\": \"([0-9.a-z-+])+\"," \
     "\"version\": \"$NEW_VER\"," $file
done
