use df_ls_core::{Choose, DFChar, Reference, ReferenceTo};
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_structure::*;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_plant() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "plant_header

            [OBJECT:PLANT]

            [PLANT:MEADOW_GRASS]
                [ALL_NAMES:meadow-grass]
                [USE_MATERIAL_TEMPLATE:STRUCTURAL:STRUCTURAL_PLANT_TEMPLATE]
                [BASIC_MAT:LOCAL_PLANT_MAT:STRUCTURAL]
                [GRASS]
                [GRASS_TILES:'.':',':'`':''']
                [GRASS_COLORS:2:0:1:2:0:0:6:0:1:6:0:0]
                [WET]
                [DRY]
                [BIOME:GRASSLAND_TEMPERATE]
                [BIOME:SAVANNA_TEMPERATE]
                [BIOME:SHRUBLAND_TEMPERATE]
                [BIOME:ANY_TEMPERATE_FOREST]
                [BIOME:MOUNTAIN]
                [BIOME:TUNDRA]

            [PLANT:RED_SPINACH]
                [NAME:red spinach][NAME_PLURAL:red spinach][ADJ:red spinach]
                [USE_MATERIAL_TEMPLATE:STRUCTURAL:STRUCTURAL_PLANT_TEMPLATE]
                    [MATERIAL_REACTION_PRODUCT:SEED_MAT:LOCAL_PLANT_MAT:SEED]
                [BASIC_MAT:LOCAL_PLANT_MAT:STRUCTURAL]
                [DRY][BIOME:NOT_FREEZING]
                [VALUE:2]
                [SPRING][SUMMER][AUTUMN][WINTER]
                [USE_MATERIAL_TEMPLATE:FLOWER:FLOWER_TEMPLATE]
                    [STATE_COLOR:ALL:GREEN]
                    [DISPLAY_COLOR:2:0:1]
                [USE_MATERIAL_TEMPLATE:SEED:SEED_TEMPLATE]
                    [MATERIAL_VALUE:1]
                    [EDIBLE_VERMIN]
                    [EDIBLE_COOKED]
                [SEED:red spinach seed:red spinach seeds:0:0:1:LOCAL_PLANT_MAT:SEED]
                [USE_MATERIAL_TEMPLATE:LEAF:LEAF_TEMPLATE]
                    [STATE_COLOR:ALL:GREEN]
                    [DISPLAY_COLOR:2:0:0]
                    [EDIBLE_VERMIN]
                    [EDIBLE_RAW]
                    [EDIBLE_COOKED]
                    [STOCKPILE_PLANT_GROWTH]
                [FREQUENCY:50]
                [CLUSTERSIZE:5]
                [PREFSTRING:leaves]
                [GROWTH:LEAVES]
                    [GROWTH_NAME:red spinach leaf:red spinach leaves]
                    [GROWTH_ITEM:PLANT_GROWTH:NONE:LOCAL_PLANT_MAT:LEAF]
                    [GROWTH_DENSITY:1000]
                    [GROWTH_PRINT:0:6:2:0:0:ALL:1]
                [GROWTH:FLOWERS]
                    [GROWTH_NAME:red spinach inflorescence:STP]
                    [GROWTH_ITEM:PLANT_GROWTH:NONE:LOCAL_PLANT_MAT:FLOWER]
                    [GROWTH_DENSITY:1000]
                    [GROWTH_TIMING:60000:119999]
                    [GROWTH_PRINT:5:5:2:0:1:60000:119999:2]
            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DFRaw {
        header: "plant_header".to_owned(),
        token_structure: vec![ObjectToken {
            plant_tokens: vec![
                PlantToken {
                    reference: Some(ReferenceTo::new("MEADOW_GRASS".to_owned())),
                    use_material_template: vec![UseMaterialTemplate {
                        reference: Some((
                            Reference("STRUCTURAL".to_owned()),
                            ReferenceTo::new("STRUCTURAL_PLANT_TEMPLATE".to_owned()),
                        )),
                        ..Default::default()
                    }],
                    all_names: Some("meadow-grass".to_owned()),
                    basic_mat: Some(BasicMat {
                        basic_mat: Some(Choose::Choice2(LocalPlantMatArg(Reference(
                            "STRUCTURAL".to_owned(),
                        )))),
                        ..Default::default()
                    }),
                    wet: Some(()),
                    dry: Some(()),
                    biome: vec![
                        BiomeEnum::GrasslandTemperate,
                        BiomeEnum::SavannaTemperate,
                        BiomeEnum::ShrublandTemperate,
                        BiomeEnum::AnyTemperateForest,
                        BiomeEnum::Mountain,
                        BiomeEnum::Tundra,
                    ],
                    grass: Some(()),
                    grass_tiles: Some((DFChar('.'), DFChar(','), DFChar('`'), DFChar('\''))),
                    grass_colors: Some((2, 0, 1, 2, 0, 0, 6, 0, 1, 6, 0, 0)),
                    ..Default::default()
                },
                PlantToken {
                    reference: Some(ReferenceTo::new("RED_SPINACH".to_owned())),
                    growth: vec![
                        Growth {
                            reference: Some(Reference("LEAVES".to_owned())),
                            growth_name: Some((
                                "red spinach leaf".to_owned(),
                                Choose::Choice2("red spinach leaves".to_owned()),
                            )),
                            growth_item: Some((
                                Reference("PLANT_GROWTH".to_owned()),
                                Reference("NONE".to_owned()),
                                Choose::Choice2(LocalPlantMatArg(Reference("LEAF".to_owned()))),
                            )),
                            growth_density: Some(1000),
                            growth_print: vec![(
                                DFChar('\u{0}'),
                                DFChar('♠'),
                                2,
                                0,
                                0,
                                Choose::Choice2(Choose::Choice1(AllEnum::All)), // Ewww
                                Some(1),
                            )],
                            ..Default::default()
                        },
                        Growth {
                            reference: Some(Reference("FLOWERS".to_owned())),
                            growth_name: Some((
                                "red spinach inflorescence".to_owned(),
                                Choose::Choice1(StandardPluralEnum::Stp),
                            )),
                            growth_item: Some((
                                Reference("PLANT_GROWTH".to_owned()),
                                Reference("NONE".to_owned()),
                                Choose::Choice2(LocalPlantMatArg(Reference("FLOWER".to_owned()))),
                            )),
                            growth_density: Some(1000),
                            growth_timing: Some((60000, 119999)),
                            growth_print: vec![(
                                DFChar('♣'),
                                DFChar('♣'),
                                2,
                                0,
                                1,
                                Choose::Choice1((60000, 119999)),
                                Some(2),
                            )],
                            ..Default::default()
                        },
                    ],

                    use_material_template: vec![
                        UseMaterialTemplate {
                            reference: Some((
                                Reference("STRUCTURAL".to_owned()),
                                ReferenceTo::new("STRUCTURAL_PLANT_TEMPLATE".to_owned()),
                            )),
                            material_reaction_product: vec![(
                                ReferenceTo::new("SEED_MAT".to_owned()),
                                Choose::Choice2(Choose::Choice2(LocalPlantMatArg(Reference(
                                    "SEED".to_owned(),
                                )))),
                            )],
                            ..Default::default()
                        },
                        UseMaterialTemplate {
                            reference: Some((
                                Reference("FLOWER".to_owned()),
                                ReferenceTo::new("FLOWER_TEMPLATE".to_owned()),
                            )),
                            display_color: Some((2, 0, 1)),
                            state_color: vec![(
                                Choose::Choice2(AllOrAllSolidEnum::All),
                                ReferenceTo::new("GREEN".to_owned()),
                            )],
                            ..Default::default()
                        },
                        UseMaterialTemplate {
                            reference: Some((
                                Reference("SEED".to_owned()),
                                ReferenceTo::new("SEED_TEMPLATE".to_owned()),
                            )),
                            material_value: Some(1),
                            edible_vermin: Some(()),
                            edible_cooked: Some(()),
                            ..Default::default()
                        },
                        UseMaterialTemplate {
                            reference: Some((
                                Reference("LEAF".to_owned()),
                                ReferenceTo::new("LEAF_TEMPLATE".to_owned()),
                            )),
                            display_color: Some((2, 0, 0)),
                            state_color: vec![(
                                Choose::Choice2(AllOrAllSolidEnum::All),
                                ReferenceTo::new("GREEN".to_owned()),
                            )],
                            stockpile_plant_growth: Some(()),
                            edible_vermin: Some(()),
                            edible_raw: Some(()),
                            edible_cooked: Some(()),
                            ..Default::default()
                        },
                    ],
                    name: Some("red spinach".to_owned()),
                    name_plural: Some("red spinach".to_owned()),
                    adj: Some("red spinach".to_owned()),
                    prefstring: vec!["leaves".to_owned()],
                    basic_mat: Some(BasicMat {
                        basic_mat: Some(Choose::Choice2(LocalPlantMatArg(Reference(
                            "STRUCTURAL".to_owned(),
                        )))),
                        ..Default::default()
                    }),
                    frequency: Some(50),
                    dry: Some(()),
                    biome: vec![BiomeEnum::NotFreezing],
                    spring: Some(()),
                    summer: Some(()),
                    autumn: Some(()),
                    winter: Some(()),
                    value: Some(2),
                    clustersize: Some(5),
                    seed: Some((
                        "red spinach seed".to_owned(),
                        "red spinach seeds".to_owned(),
                        0,
                        0,
                        1,
                        Choose::Choice2(LocalPlantMatArg(Reference("SEED".to_owned()))),
                    )),
                    ..Default::default()
                },
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
