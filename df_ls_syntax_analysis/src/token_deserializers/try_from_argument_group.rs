use super::Token;
use crate::Argument;
use df_ls_diagnostics::{hash_map, DMExtraInfo, DiagnosticsInfo};
use std::fmt::Display;

pub trait TryFromArgumentGroup: Sized {
    /// Convert 0 or more arguments into `Self`
    /// This will consume the arguments used
    fn try_from_argument_group(
        token: &mut Token,
        source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) -> Result<Self, ()>;

    fn diagnostics_wrong_enum_type<S: AsRef<str> + Display>(
        arg: &Argument,
        expected_values: Vec<S>,
        _source: &str,
        diagnostics: &mut DiagnosticsInfo,
        add_diagnostics_on_err: bool,
    ) {
        if add_diagnostics_on_err {
            let expected_values_string = expected_values
                .iter()
                .map(|item| format!("`{}`", item))
                .collect::<Vec<String>>()
                .join(", ");
            diagnostics.add_message(
                DMExtraInfo {
                    range: arg.node.get_range(),
                    message_template_data: hash_map! {
                        "found_value" => arg.value.to_string(),
                        "valid_values" => expected_values_string,
                    },
                },
                "wrong_enum_value",
            );
        }
    }
}
