# Development

## Intro
So you want to build the project or contribute to this project.
If you want to contribute just in documentation, like the `README.md`,
you might not need to do all the steps below.

NOTE: Following steps are mainly for Linux. Similar steps exist for Windows and Mac OS.
Building the project from source is not difficult.
But requires a bit knowledge about how to use a terminal (or VS Code terminal).

We will use commands to do certain things like installing, compiling and testing.
Depending on your systems there are a few different terminals.
On Linux and MacOS just use the terminal already installed.
On Windows we recommend getting: [Windows Terminal](https://aka.ms/terminal)
You can also use the terminal build into VSCode.

Some basic terminal info. The terminal always has a current location.
Please make sure you run the commands in the right folder (in some cases).
You can usually see this in front of the cursor. You can change folder using the `cd` command.
`ls` lists all the folders and file in the current directory/folder.

If you have ANY questions, please aks them in our [Discord][Discord].
We will help you get started and will help you learn.
Everybody has to start somewhere, so there are no stupid questions.

## Pre-requirement
To compile and use this project you need the following:

* **Rust** (rustup + cargo) ([Linux](https://www.rust-lang.org/tools/install),
[Windows](https://www.rust-lang.org/tools/install?platform_override=win)) 
* **Node.js/NPM** [Node.js instructions](https://nodejs.org/en/)
* **TypeScript** [TypeScript intructions](https://www.npmjs.com/package/typescript)

In this guide we will give some extra recommendations for new contributors.
If you know more about programming or have your own tools, please feel free to use them instead.

Optionally you can also use [VSCode](https://code.visualstudio.com/) to make things a bit easier.
In VSCode we recommend installing the following plugins:
- [Rust Analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer)
- [Better TOML](https://marketplace.visualstudio.com/items?itemName=bungcip.better-toml)
- [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)
(helps with visualizing Git Branches and commits)

## Download and Build

Once you have the requirements follow the steps:
- Clone the repo (see blue "Clone" button in GitLab).
There are a few ways to do this. But if you want to contribute, we recommend "Clone with SSH".
  - SSH (GitLab account needed, it is free)
    - You need to setup a SSH key once. 
    ([GitLab guide](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair),
    [GitHub guide](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent))
        - Generate a new key pair.
        - Add the SSH public key to GitLab
  - You can also use the "Download source code" button next to it.
- Select a location where you want to code to be stored.
  - Open the folder in your file explorer.
  - Open a terminal window in this location.
  - Clone repo using: `git clone git@gitlab.com:df-modding-tools/df-raw-language-server.git`
- Open the new folder `df-raw-language-server`
  - Open it in the file explorer. You should see the files including `README.md`.
  - Move the terminal here: `cd df-raw-language-server`.
- Build the DF Language server: `cargo build`
  - This should finish without any problems.
  - If you do have problems, please contact us.
- Open project in VSCode:
  - Run `code .`
  - Or drag `df-raw-language-server` folder into the 'File Explorer'
- Go to debug/run tab in VSCode.
- Run `Server + Client`
- A new VSCode window will open.
- Open a DF Raw File in the new window.

If it did not work, please tell us why. So we can update the guide.

## Get started

The first time the code builds it will take a longer time. In the future this will be quicker.
Rust does incremental updates. These are stored inside the `target` folder.
This might get big over time and take up a bit of disk space. You can solve this by doing `cargo clean`.
But you will then have to rebuild again like the first time.
It is worth doing this once in a while (every few weeks).

Basic commands:
```bash
# Clean up repo
cargo clean

# Build code (debug build)
cargo build

# Build release build
cargo build --release

# Run server (TCP mode)
cargo run server tcp

# Format code
cargo fmt

# Run tests (make sure everything still works)
cargo test

# Test a specific file `debug-test.txt`
cargo run -- -d check debug-test.txt
# This is useful for debugging 1 file with known problems. This will print a lot of extra info.
# If you open big files this way it might print so much to the terminal and will be hard to filter through.

# Print help info for commands allowed in DF LS
cargo run -- --help
```

## Project parts

The project has 2 sub components. Together these parts will create a functional IDE integration.
- **DF Language server**: This is the actual Language server and will create a server 
that follows the [Language Server Protocol (LSP)](https://microsoft.github.io/language-server-protocol/).
- **DF Language server Clients**: This is a collection of clients for the different 
[IDEs](https://en.wikipedia.org/wiki/Integrated_development_environment).
If more IDEs want to be supported this is the place to add them.

### DF Language server Clients

The clients are very light weight and don't do much by them selfs.
The main thing they do is start the server when the IDE is opened (and DF raw file is detected).

The client just communicates with the local server it started over a TCP or IO connection.
It will then feed the data of the file into the server and receive the diagnostics data and
return it to the IDE. Think of the client as the glue that sticks the Server to the IDE.

#### Syntax Highlighting

The clients also into the Syntax Highlighting code.
Because each IDE does this slightly different we have documented the rules in the document linked here.

[This is documented here.](./Syntax_Highlighting.md)

### DF Language server

The server is the biggest component of the project.
It is broadly divided into 3 parts.
Each part has its own part to play and will feed in to the next step:
- **1: Lexical Analysis**: This takes the source file and split the file into tokens. 
This step will output a `Tree` objects.
The `Tree` object is also referred to as an [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree).
The Tokenizer will also report problems about the AST.
- **2: Syntax Analysis**: Also referred to as the first pass. 
This will take in the `Tree` object and the source and use this data to create a data structure. 
During this step more problems will be add to the diagnostic messages list.
- **3: Semantic Analysis**: Also referred to as the second pass. (still in development)
This step will take the data structure and check the structure so everything makes sense.
Some checks can only happen in this stage as it requires the full structure to be known.
This stage will return more diagnostic messages.

#### 1: Lexical Analysis

[This is documented here.](./Lexical_Analysis.md)

#### 2: Syntax Analysis

[This is documented here.](./Syntax_Analysis.md)

#### 3: Semantic Analysis

[This is documented here.](./Semantic_Analysis.md)

[Discord]: https://discord.gg/6eKf5ZY