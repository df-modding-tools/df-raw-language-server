use crate::{
    AllEnum, AllOrAllSolidEnum, ColorToken, DietInfoEnum, InorganicToken, ItemReferenceArg,
    MaterialStateEnum, MaterialTokenArgWithLocalCreatureMat, NoneEnum, OverwriteSolidEnum,
    PluralEnum, ReactionToken, StandardPluralEnum, SyndromeToken, TissueShapeEnum, TissueToken,
};
use df_ls_core::{Choose, DFChar, Reference, ReferenceTo};
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

/// Begins defining a new local tissue in the creature.
#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct LocalTissueToken {
    /// Argument 1 of `[TISSUE:...]`
    #[df_token(token_name = "TISSUE", on_duplicate_to_parent, primary_token)]
    pub reference: Option<Reference>,
    // region: Tissue Tokens ======================================================================
    /// Name of the tissue.
    ///
    /// Arguments are: `<name>:<plural>`
    ///
    /// Plural can alternatively be `NP` (No Plural) or `STP (Standard Plural,
    /// adds an 's' on the end).
    #[df_token(token_name = "TISSUE_NAME")]
    pub name: Option<(String, Choose<PluralEnum, String>)>,
    /// Defines the tissue material.
    #[df_token(token_name = "TISSUE_MATERIAL")]
    pub material: Option<MaterialTokenArgWithLocalCreatureMat>,
    /// The relative thickness of the tissue.
    /// A higher thickness is harder to penetrate, but raising a tissue's relative thickness
    /// decreases the thickness of all other tissues.
    #[df_token(token_name = "RELATIVE_THICKNESS")]
    pub relative_thickness: Option<u32>,
    /// Speed at which the tissue heals itself; lower is faster. Common values are `100` and `1000`
    ///
    /// Omitting the token will result in a tissue that never heals.
    #[df_token(token_name = "HEALING_RATE")]
    pub healing_rate: Option<u32>,
    /// How many arteries/veins are in the tissue.
    /// Related to how much said tissue bleeds.
    ///
    /// Higher = More bleeding (Which is why the heart has the highest value.)
    /// - Default heart = `10`
    /// - Default skin = `1`
    ///
    /// Also see: `MAJOR_ARTERIES` and `ARTERIES`
    #[df_token(token_name = "VASCULAR")]
    pub vascular: Option<i32>,
    /// Related to how much pain your character will suffer when said tissue is damaged.
    /// Higher = More pain when damaged (which is why the bone tissue has a much higher value
    /// than other tissues; a broken bone hurts a lot more than a flesh cut).
    /// - Default bones = `50`
    /// - Default skin = `5`
    #[df_token(token_name = "PAIN_RECEPTORS")]
    pub pain_receptors: Option<i32>,
    /// The thickness of the tissue increases when character strength increases.
    /// Used for muscles in vanilla.
    #[df_token(token_name = "THICKENS_ON_STRENGTH")]
    pub thickness_on_strength: Option<()>,
    /// Thickness of said tissue increases when the
    /// character eats and doesn't exercise sufficiently.
    /// Used for fat in vanilla.
    #[df_token(token_name = "THICKENS_ON_ENERGY_STORAGE")]
    pub thickness_on_energy_storage: Option<()>,
    /// The tissue contains arteries.
    /// Edged attacks have the chance to break an artery, increasing blood loss.
    /// Used for muscles in vanilla.
    ///
    /// Also see: `MAJOR_ARTERIES` and `VASCULAR`
    #[df_token(token_name = "ARTERIES")]
    pub arteries: Option<()>,
    /// Denotes whether or not the tissue will be scarred once healed.
    #[df_token(token_name = "SCARS")]
    pub scars: Option<()>,
    /// Holds the body part together.
    /// A cut or a fracture will disable the body part it's in.
    #[df_token(token_name = "STRUCTURAL")]
    pub structural: Option<()>,
    /// Any ligaments or tendons are part of this tissue.
    /// Vulnerable to edged attacks, damage disables the limb.
    ///
    /// Used for bones and chitin in vanilla.
    #[df_token(token_name = "CONNECTIVE_TISSUE_ANCHOR")]
    pub connective_tissue_anchor: Option<()>,
    /// The tissue will not heal, or heals slower, until it is set by a bone doctor.
    /// Used for bones, shell and chitin in vanilla.
    #[df_token(token_name = "SETTABLE")]
    pub settable: Option<()>,
    /// The broken tissue can be fixed with a cast or a splint to restore function while it heals.
    /// Used for bones, shell and chitin in vanilla.
    #[df_token(token_name = "SPLINTABLE")]
    pub splintable: Option<()>,
    /// The tissue performs some sort of special function (e.g. sight, hearing, breathing, etc.)
    /// An organ with such a function will stop working if a sufficient amount of damage is
    /// sustained by its `FUNCTIONAL` tissues. If an organ has no `FUNCTIONAL` tissues,
    /// it will stop working only if it is severed or destroyed entirely by heat or cold.
    #[df_token(token_name = "FUNCTIONAL")]
    pub functional: Option<()>,
    /// Nervous function - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "NERVOUS")]
    pub nervous: Option<()>,
    /// If a creature has no functioning parts with the `THOUGHT` token, it will be unable to move
    /// or breathe; `NO_THOUGHT_CENTER_FOR_MOVEMENT` bypasses this limitation.
    /// Mostly used in `[OBJECT:BODY]`.
    #[df_token(token_name = "THOUGHT")]
    pub though: Option<()>,
    /// Seems to affect where sensory or motor nerves are located,
    /// and whether damage to this tissue will render a limb useless.
    #[df_token(token_name = "MUSCULAR")]
    pub muscular: Option<()>,
    /// Used to smell - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "SMELL")]
    pub smell: Option<()>,
    /// Used to hearing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "HEAR")]
    pub hear: Option<()>,
    /// Unknown - not used.
    /// Most likely related to flying.
    #[df_token(token_name = "FLIGHT")]
    pub flight: Option<()>,
    /// Used to breathing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "BREATHE")]
    pub breathe: Option<()>,
    /// Used to seeing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "SIGHT")]
    pub sight: Option<()>,
    /// Holds body parts together.
    /// A body part will not be severed unless all of its component tissues with the
    /// `CONNECTS` tag are severed.
    #[df_token(token_name = "CONNECTS")]
    pub connects: Option<()>,
    /// Causes tissue to sometimes severely bleed when damaged.
    /// This is independent of its `VASCULAR` value.
    ///
    /// Also see: `ARTERIES`
    #[df_token(token_name = "MAJOR_ARTERIES")]
    pub major_arteries: Option<()>,
    /// Tissue supplies the creature with heat insulation.
    /// Higher values result in more insulation.
    #[df_token(token_name = "INSULATION")]
    pub insulation: Option<u32>,
    /// Unknown - not used.
    /// Maybe makes the tissue have no direct purpose?
    ///
    /// Also see: `STYLEABLE`
    #[df_token(token_name = "COSMETIC")]
    pub cosmetic: Option<()>,
    /// The tissue can be styled as per a tissue style (defined in an entity entry)
    ///
    /// Also see: `COSMETIC`
    #[df_token(token_name = "STYLEABLE")]
    pub styleable: Option<()>,
    /// The shape of the tissue, like if it is a layer or feathers.
    #[df_token(token_name = "TISSUE_SHAPE")]
    pub tissue_shape: Option<TissueShapeEnum>,
    /// Tissue is implicitly attached to another tissue and will fall off if that tissue
    /// layer is destroyed. Used for hair and feathers in vanilla, which are subordinate to skin.
    #[df_token(token_name = "SUBORDINATE_TO_TISSUE")]
    pub subordinate_to_tissue: Option<ReferenceTo<TissueToken>>,
    /// Sets/forces a default material state for the selected tissue.
    #[df_token(token_name = "TISSUE_MAT_STATE")]
    pub tissue_mat_state: Option<MaterialStateEnum>,
    /// The selected tissue leaks out of the creature when the layers above it are pierced.
    #[df_token(token_name = "TISSUE_LEAKS")]
    pub tissue_leaks: Option<()>,
    // endregion ==================================================================================
}

/// Defines a new local creature tissue and populates it with all properties defined in the
/// local tissue specified in the second argument.
#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct UseTissue {
    /// Argument 1 of `[USE_TISSUE:...]`
    #[df_token(token_name = "USE_TISSUE", on_duplicate_to_parent, primary_token)]
    pub reference: Option<(Reference, Reference)>,
    // region: Tissue Tokens ======================================================================
    /// Name of the tissue.
    ///
    /// Arguments are: `<name>:<plural>`
    ///
    /// Plural can alternatively be `NP` (No Plural) or `STP (Standard Plural,
    /// adds an 's' on the end).
    #[df_token(token_name = "TISSUE_NAME")]
    pub name: Option<(String, Choose<PluralEnum, String>)>,
    /// Defines the tissue material.
    #[df_token(token_name = "TISSUE_MATERIAL")]
    pub material: Option<MaterialTokenArgWithLocalCreatureMat>,
    /// The relative thickness of the tissue.
    /// A higher thickness is harder to penetrate, but raising a tissue's relative thickness
    /// decreases the thickness of all other tissues.
    #[df_token(token_name = "RELATIVE_THICKNESS")]
    pub relative_thickness: Option<u32>,
    /// Speed at which the tissue heals itself; lower is faster. Common values are `100` and `1000`
    ///
    /// Omitting the token will result in a tissue that never heals.
    #[df_token(token_name = "HEALING_RATE")]
    pub healing_rate: Option<u32>,
    /// How many arteries/veins are in the tissue.
    /// Related to how much said tissue bleeds.
    ///
    /// Higher = More bleeding (Which is why the heart has the highest value.)
    /// - Default heart = `10`
    /// - Default skin = `1`
    ///
    /// Also see: `MAJOR_ARTERIES` and `ARTERIES`
    #[df_token(token_name = "VASCULAR")]
    pub vascular: Option<i32>,
    /// Related to how much pain your character will suffer when said tissue is damaged.
    /// Higher = More pain when damaged (which is why the bone tissue has a much higher value
    /// than other tissues; a broken bone hurts a lot more than a flesh cut).
    /// - Default bones = `50`
    /// - Default skin = `5`
    #[df_token(token_name = "PAIN_RECEPTORS")]
    pub pain_receptors: Option<i32>,
    /// The thickness of the tissue increases when character strength increases.
    /// Used for muscles in vanilla.
    #[df_token(token_name = "THICKENS_ON_STRENGTH")]
    pub thickness_on_strength: Option<()>,
    /// Thickness of said tissue increases when the
    /// character eats and doesn't exercise sufficiently.
    /// Used for fat in vanilla.
    #[df_token(token_name = "THICKENS_ON_ENERGY_STORAGE")]
    pub thickness_on_energy_storage: Option<()>,
    /// The tissue contains arteries.
    /// Edged attacks have the chance to break an artery, increasing blood loss.
    /// Used for muscles in vanilla.
    ///
    /// Also see: `MAJOR_ARTERIES` and `VASCULAR`
    #[df_token(token_name = "ARTERIES")]
    pub arteries: Option<()>,
    /// Denotes whether or not the tissue will be scarred once healed.
    #[df_token(token_name = "SCARS")]
    pub scars: Option<()>,
    /// Holds the body part together.
    /// A cut or a fracture will disable the body part it's in.
    #[df_token(token_name = "STRUCTURAL")]
    pub structural: Option<()>,
    /// Any ligaments or tendons are part of this tissue.
    /// Vulnerable to edged attacks, damage disables the limb.
    ///
    /// Used for bones and chitin in vanilla.
    #[df_token(token_name = "CONNECTIVE_TISSUE_ANCHOR")]
    pub connective_tissue_anchor: Option<()>,
    /// The tissue will not heal, or heals slower, until it is set by a bone doctor.
    /// Used for bones, shell and chitin in vanilla.
    #[df_token(token_name = "SETTABLE")]
    pub settable: Option<()>,
    /// The broken tissue can be fixed with a cast or a splint to restore function while it heals.
    /// Used for bones, shell and chitin in vanilla.
    #[df_token(token_name = "SPLINTABLE")]
    pub splintable: Option<()>,
    /// The tissue performs some sort of special function (e.g. sight, hearing, breathing, etc.)
    /// An organ with such a function will stop working if a sufficient amount of damage is
    /// sustained by its `FUNCTIONAL` tissues. If an organ has no `FUNCTIONAL` tissues,
    /// it will stop working only if it is severed or destroyed entirely by heat or cold.
    #[df_token(token_name = "FUNCTIONAL")]
    pub functional: Option<()>,
    /// Nervous function - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "NERVOUS")]
    pub nervous: Option<()>,
    /// If a creature has no functioning parts with the `THOUGHT` token, it will be unable to move
    /// or breathe; `NO_THOUGHT_CENTER_FOR_MOVEMENT` bypasses this limitation.
    /// Mostly used in `[OBJECT:BODY]`.
    #[df_token(token_name = "THOUGHT")]
    pub though: Option<()>,
    /// Seems to affect where sensory or motor nerves are located,
    /// and whether damage to this tissue will render a limb useless.
    #[df_token(token_name = "MUSCULAR")]
    pub muscular: Option<()>,
    /// Used to smell - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "SMELL")]
    pub smell: Option<()>,
    /// Used to hearing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "HEAR")]
    pub hear: Option<()>,
    /// Unknown - not used.
    /// Most likely related to flying.
    #[df_token(token_name = "FLIGHT")]
    pub flight: Option<()>,
    /// Used to breathing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "BREATHE")]
    pub breathe: Option<()>,
    /// Used to seeing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "SIGHT")]
    pub sight: Option<()>,
    /// Holds body parts together.
    /// A body part will not be severed unless all of its component tissues with the
    /// `CONNECTS` tag are severed.
    #[df_token(token_name = "CONNECTS")]
    pub connects: Option<()>,
    /// Causes tissue to sometimes severely bleed when damaged.
    /// This is independent of its `VASCULAR` value.
    ///
    /// Also see: `ARTERIES`
    #[df_token(token_name = "MAJOR_ARTERIES")]
    pub major_arteries: Option<()>,
    /// Tissue supplies the creature with heat insulation.
    /// Higher values result in more insulation.
    #[df_token(token_name = "INSULATION")]
    pub insulation: Option<u32>,
    /// Unknown - not used.
    /// Maybe makes the tissue have no direct purpose?
    ///
    /// Also see: `STYLEABLE`
    #[df_token(token_name = "COSMETIC")]
    pub cosmetic: Option<()>,
    /// The tissue can be styled as per a tissue style (defined in an entity entry)
    ///
    /// Also see: `COSMETIC`
    #[df_token(token_name = "STYLEABLE")]
    pub styleable: Option<()>,
    /// The shape of the tissue, like if it is a layer or feathers.
    #[df_token(token_name = "TISSUE_SHAPE")]
    pub tissue_shape: Option<TissueShapeEnum>,
    /// Tissue is implicitly attached to another tissue and will fall off if that tissue
    /// layer is destroyed. Used for hair and feathers in vanilla, which are subordinate to skin.
    #[df_token(token_name = "SUBORDINATE_TO_TISSUE")]
    pub subordinate_to_tissue: Option<ReferenceTo<TissueToken>>,
    /// Sets/forces a default material state for the selected tissue.
    #[df_token(token_name = "TISSUE_MAT_STATE")]
    pub tissue_mat_state: Option<MaterialStateEnum>,
    /// The selected tissue leaks out of the creature when the layers above it are pierced.
    #[df_token(token_name = "TISSUE_LEAKS")]
    pub tissue_leaks: Option<()>,
    // endregion ==================================================================================
}

/// Loads a tissue template listed in `OBJECT:TISSUE_TEMPLATE` files, such as
/// `tissue_template_default.txt`.
#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct UseTissueTemplate {
    /// Argument 1 of `[USE_TISSUE_TEMPLATE:...]`
    #[df_token(
        token_name = "USE_TISSUE_TEMPLATE",
        on_duplicate_to_parent,
        primary_token
    )]
    pub reference: Option<(Reference, ReferenceTo<TissueToken>)>,
    // region: Tissue Tokens ======================================================================
    /// Name of the tissue.
    ///
    /// Arguments are: `<name>:<plural>`
    ///
    /// Plural can alternatively be `NP` (No Plural) or `STP (Standard Plural,
    /// adds an 's' on the end).
    #[df_token(token_name = "TISSUE_NAME")]
    pub name: Option<(String, Choose<PluralEnum, String>)>,
    /// Defines the tissue material.
    #[df_token(token_name = "TISSUE_MATERIAL")]
    pub material: Option<MaterialTokenArgWithLocalCreatureMat>,
    /// The relative thickness of the tissue.
    /// A higher thickness is harder to penetrate, but raising a tissue's relative thickness
    /// decreases the thickness of all other tissues.
    #[df_token(token_name = "RELATIVE_THICKNESS")]
    pub relative_thickness: Option<u32>,
    /// Speed at which the tissue heals itself; lower is faster. Common values are `100` and `1000`
    ///
    /// Omitting the token will result in a tissue that never heals.
    #[df_token(token_name = "HEALING_RATE")]
    pub healing_rate: Option<u32>,
    /// How many arteries/veins are in the tissue.
    /// Related to how much said tissue bleeds.
    ///
    /// Higher = More bleeding (Which is why the heart has the highest value.)
    /// - Default heart = `10`
    /// - Default skin = `1`
    ///
    /// Also see: `MAJOR_ARTERIES` and `ARTERIES`
    #[df_token(token_name = "VASCULAR")]
    pub vascular: Option<i32>,
    /// Related to how much pain your character will suffer when said tissue is damaged.
    /// Higher = More pain when damaged (which is why the bone tissue has a much higher value
    /// than other tissues; a broken bone hurts a lot more than a flesh cut).
    /// - Default bones = `50`
    /// - Default skin = `5`
    #[df_token(token_name = "PAIN_RECEPTORS")]
    pub pain_receptors: Option<i32>,
    /// The thickness of the tissue increases when character strength increases.
    /// Used for muscles in vanilla.
    #[df_token(token_name = "THICKENS_ON_STRENGTH")]
    pub thickness_on_strength: Option<()>,
    /// Thickness of said tissue increases when the
    /// character eats and doesn't exercise sufficiently.
    /// Used for fat in vanilla.
    #[df_token(token_name = "THICKENS_ON_ENERGY_STORAGE")]
    pub thickness_on_energy_storage: Option<()>,
    /// The tissue contains arteries.
    /// Edged attacks have the chance to break an artery, increasing blood loss.
    /// Used for muscles in vanilla.
    ///
    /// Also see: `MAJOR_ARTERIES` and `VASCULAR`
    #[df_token(token_name = "ARTERIES")]
    pub arteries: Option<()>,
    /// Denotes whether or not the tissue will be scarred once healed.
    #[df_token(token_name = "SCARS")]
    pub scars: Option<()>,
    /// Holds the body part together.
    /// A cut or a fracture will disable the body part it's in.
    #[df_token(token_name = "STRUCTURAL")]
    pub structural: Option<()>,
    /// Any ligaments or tendons are part of this tissue.
    /// Vulnerable to edged attacks, damage disables the limb.
    ///
    /// Used for bones and chitin in vanilla.
    #[df_token(token_name = "CONNECTIVE_TISSUE_ANCHOR")]
    pub connective_tissue_anchor: Option<()>,
    /// The tissue will not heal, or heals slower, until it is set by a bone doctor.
    /// Used for bones, shell and chitin in vanilla.
    #[df_token(token_name = "SETTABLE")]
    pub settable: Option<()>,
    /// The broken tissue can be fixed with a cast or a splint to restore function while it heals.
    /// Used for bones, shell and chitin in vanilla.
    #[df_token(token_name = "SPLINTABLE")]
    pub splintable: Option<()>,
    /// The tissue performs some sort of special function (e.g. sight, hearing, breathing, etc.)
    /// An organ with such a function will stop working if a sufficient amount of damage is
    /// sustained by its `FUNCTIONAL` tissues. If an organ has no `FUNCTIONAL` tissues,
    /// it will stop working only if it is severed or destroyed entirely by heat or cold.
    #[df_token(token_name = "FUNCTIONAL")]
    pub functional: Option<()>,
    /// Nervous function - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "NERVOUS")]
    pub nervous: Option<()>,
    /// If a creature has no functioning parts with the `THOUGHT` token, it will be unable to move
    /// or breathe; `NO_THOUGHT_CENTER_FOR_MOVEMENT` bypasses this limitation.
    /// Mostly used in `[OBJECT:BODY]`.
    #[df_token(token_name = "THOUGHT")]
    pub though: Option<()>,
    /// Seems to affect where sensory or motor nerves are located,
    /// and whether damage to this tissue will render a limb useless.
    #[df_token(token_name = "MUSCULAR")]
    pub muscular: Option<()>,
    /// Used to smell - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "SMELL")]
    pub smell: Option<()>,
    /// Used to hearing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "HEAR")]
    pub hear: Option<()>,
    /// Unknown - not used.
    /// Most likely related to flying.
    #[df_token(token_name = "FLIGHT")]
    pub flight: Option<()>,
    /// Used to breathing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "BREATHE")]
    pub breathe: Option<()>,
    /// Used to seeing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "SIGHT")]
    pub sight: Option<()>,
    /// Holds body parts together.
    /// A body part will not be severed unless all of its component tissues with the
    /// `CONNECTS` tag are severed.
    #[df_token(token_name = "CONNECTS")]
    pub connects: Option<()>,
    /// Causes tissue to sometimes severely bleed when damaged.
    /// This is independent of its `VASCULAR` value.
    ///
    /// Also see: `ARTERIES`
    #[df_token(token_name = "MAJOR_ARTERIES")]
    pub major_arteries: Option<()>,
    /// Tissue supplies the creature with heat insulation.
    /// Higher values result in more insulation.
    #[df_token(token_name = "INSULATION")]
    pub insulation: Option<u32>,
    /// Unknown - not used.
    /// Maybe makes the tissue have no direct purpose?
    ///
    /// Also see: `STYLEABLE`
    #[df_token(token_name = "COSMETIC")]
    pub cosmetic: Option<()>,
    /// The tissue can be styled as per a tissue style (defined in an entity entry)
    ///
    /// Also see: `COSMETIC`
    #[df_token(token_name = "STYLEABLE")]
    pub styleable: Option<()>,
    /// The shape of the tissue, like if it is a layer or feathers.
    #[df_token(token_name = "TISSUE_SHAPE")]
    pub tissue_shape: Option<TissueShapeEnum>,
    /// Tissue is implicitly attached to another tissue and will fall off if that tissue
    /// layer is destroyed. Used for hair and feathers in vanilla, which are subordinate to skin.
    #[df_token(token_name = "SUBORDINATE_TO_TISSUE")]
    pub subordinate_to_tissue: Option<ReferenceTo<TissueToken>>,
    /// Sets/forces a default material state for the selected tissue.
    #[df_token(token_name = "TISSUE_MAT_STATE")]
    pub tissue_mat_state: Option<MaterialStateEnum>,
    /// The selected tissue leaks out of the creature when the layers above it are pierced.
    #[df_token(token_name = "TISSUE_LEAKS")]
    pub tissue_leaks: Option<()>,
    // endregion ==================================================================================
}

/// Selects a tissue for editing.
#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct SelectTissue {
    /// Argument 1 of `[SELECT_TISSUE:...]`
    #[df_token(token_name = "SELECT_TISSUE", on_duplicate_to_parent, primary_token)]
    pub reference: Option<Reference>,
    // region: Tissue Tokens ======================================================================
    /// Name of the tissue.
    ///
    /// Arguments are: `<name>:<plural>`
    ///
    /// Plural can alternatively be `NP` (No Plural) or `STP (Standard Plural,
    /// adds an 's' on the end).
    #[df_token(token_name = "TISSUE_NAME")]
    pub name: Option<(String, Choose<PluralEnum, String>)>,
    /// Defines the tissue material.
    #[df_token(token_name = "TISSUE_MATERIAL")]
    pub material: Option<MaterialTokenArgWithLocalCreatureMat>,
    /// The relative thickness of the tissue.
    /// A higher thickness is harder to penetrate, but raising a tissue's relative thickness
    /// decreases the thickness of all other tissues.
    #[df_token(token_name = "RELATIVE_THICKNESS")]
    pub relative_thickness: Option<u32>,
    /// Speed at which the tissue heals itself; lower is faster. Common values are `100` and `1000`
    ///
    /// Omitting the token will result in a tissue that never heals.
    #[df_token(token_name = "HEALING_RATE")]
    pub healing_rate: Option<u32>,
    /// How many arteries/veins are in the tissue.
    /// Related to how much said tissue bleeds.
    ///
    /// Higher = More bleeding (Which is why the heart has the highest value.)
    /// - Default heart = `10`
    /// - Default skin = `1`
    ///
    /// Also see: `MAJOR_ARTERIES` and `ARTERIES`
    #[df_token(token_name = "VASCULAR")]
    pub vascular: Option<i32>,
    /// Related to how much pain your character will suffer when said tissue is damaged.
    /// Higher = More pain when damaged (which is why the bone tissue has a much higher value
    /// than other tissues; a broken bone hurts a lot more than a flesh cut).
    /// - Default bones = `50`
    /// - Default skin = `5`
    #[df_token(token_name = "PAIN_RECEPTORS")]
    pub pain_receptors: Option<i32>,
    /// The thickness of the tissue increases when character strength increases.
    /// Used for muscles in vanilla.
    #[df_token(token_name = "THICKENS_ON_STRENGTH")]
    pub thickness_on_strength: Option<()>,
    /// Thickness of said tissue increases when the
    /// character eats and doesn't exercise sufficiently.
    /// Used for fat in vanilla.
    #[df_token(token_name = "THICKENS_ON_ENERGY_STORAGE")]
    pub thickness_on_energy_storage: Option<()>,
    /// The tissue contains arteries.
    /// Edged attacks have the chance to break an artery, increasing blood loss.
    /// Used for muscles in vanilla.
    ///
    /// Also see: `MAJOR_ARTERIES` and `VASCULAR`
    #[df_token(token_name = "ARTERIES")]
    pub arteries: Option<()>,
    /// Denotes whether or not the tissue will be scarred once healed.
    #[df_token(token_name = "SCARS")]
    pub scars: Option<()>,
    /// Holds the body part together.
    /// A cut or a fracture will disable the body part it's in.
    #[df_token(token_name = "STRUCTURAL")]
    pub structural: Option<()>,
    /// Any ligaments or tendons are part of this tissue.
    /// Vulnerable to edged attacks, damage disables the limb.
    ///
    /// Used for bones and chitin in vanilla.
    #[df_token(token_name = "CONNECTIVE_TISSUE_ANCHOR")]
    pub connective_tissue_anchor: Option<()>,
    /// The tissue will not heal, or heals slower, until it is set by a bone doctor.
    /// Used for bones, shell and chitin in vanilla.
    #[df_token(token_name = "SETTABLE")]
    pub settable: Option<()>,
    /// The broken tissue can be fixed with a cast or a splint to restore function while it heals.
    /// Used for bones, shell and chitin in vanilla.
    #[df_token(token_name = "SPLINTABLE")]
    pub splintable: Option<()>,
    /// The tissue performs some sort of special function (e.g. sight, hearing, breathing, etc.)
    /// An organ with such a function will stop working if a sufficient amount of damage is
    /// sustained by its `FUNCTIONAL` tissues. If an organ has no `FUNCTIONAL` tissues,
    /// it will stop working only if it is severed or destroyed entirely by heat or cold.
    #[df_token(token_name = "FUNCTIONAL")]
    pub functional: Option<()>,
    /// Nervous function - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "NERVOUS")]
    pub nervous: Option<()>,
    /// If a creature has no functioning parts with the `THOUGHT` token, it will be unable to move
    /// or breathe; `NO_THOUGHT_CENTER_FOR_MOVEMENT` bypasses this limitation.
    /// Mostly used in `[OBJECT:BODY]`.
    #[df_token(token_name = "THOUGHT")]
    pub though: Option<()>,
    /// Seems to affect where sensory or motor nerves are located,
    /// and whether damage to this tissue will render a limb useless.
    #[df_token(token_name = "MUSCULAR")]
    pub muscular: Option<()>,
    /// Used to smell - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "SMELL")]
    pub smell: Option<()>,
    /// Used to hearing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "HEAR")]
    pub hear: Option<()>,
    /// Unknown - not used.
    /// Most likely related to flying.
    #[df_token(token_name = "FLIGHT")]
    pub flight: Option<()>,
    /// Used to breathing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "BREATHE")]
    pub breathe: Option<()>,
    /// Used to seeing - not used.
    /// This token is used in `[OBJECT:BODY]` tokens.
    #[df_token(token_name = "SIGHT")]
    pub sight: Option<()>,
    /// Holds body parts together.
    /// A body part will not be severed unless all of its component tissues with the
    /// `CONNECTS` tag are severed.
    #[df_token(token_name = "CONNECTS")]
    pub connects: Option<()>,
    /// Causes tissue to sometimes severely bleed when damaged.
    /// This is independent of its `VASCULAR` value.
    ///
    /// Also see: `ARTERIES`
    #[df_token(token_name = "MAJOR_ARTERIES")]
    pub major_arteries: Option<()>,
    /// Tissue supplies the creature with heat insulation.
    /// Higher values result in more insulation.
    #[df_token(token_name = "INSULATION")]
    pub insulation: Option<u32>,
    /// Unknown - not used.
    /// Maybe makes the tissue have no direct purpose?
    ///
    /// Also see: `STYLEABLE`
    #[df_token(token_name = "COSMETIC")]
    pub cosmetic: Option<()>,
    /// The tissue can be styled as per a tissue style (defined in an entity entry)
    ///
    /// Also see: `COSMETIC`
    #[df_token(token_name = "STYLEABLE")]
    pub styleable: Option<()>,
    /// The shape of the tissue, like if it is a layer or feathers.
    #[df_token(token_name = "TISSUE_SHAPE")]
    pub tissue_shape: Option<TissueShapeEnum>,
    /// Tissue is implicitly attached to another tissue and will fall off if that tissue
    /// layer is destroyed. Used for hair and feathers in vanilla, which are subordinate to skin.
    #[df_token(token_name = "SUBORDINATE_TO_TISSUE")]
    pub subordinate_to_tissue: Option<ReferenceTo<TissueToken>>,
    /// Sets/forces a default material state for the selected tissue.
    #[df_token(token_name = "TISSUE_MAT_STATE")]
    pub tissue_mat_state: Option<MaterialStateEnum>,
    /// The selected tissue leaks out of the creature when the layers above it are pierced.
    #[df_token(token_name = "TISSUE_LEAKS")]
    pub tissue_leaks: Option<()>,
    // endregion ==================================================================================
}

/// Selects a locally defined material for editing. You can select `ALL` to modify all materials
/// in the creature.
#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct SelectMaterial {
    /// Arguments of `[SELECT_MATERIAL:...]`
    #[df_token(token_name = "SELECT_MATERIAL", on_duplicate_to_parent, primary_token)]
    //#[referenceable(self_reference)]
    pub reference: Option<Choose<AllEnum, Reference>>,
    /// Adds a material to selected materials. Used immediately after `[SELECT_MATERIAL]`.
    #[df_token(token_name = "PLUS_MATERIAL")] // TODO nest
    pub plus_material: Vec<Reference>,
    // region: Not Permitted in MatDef ============================================================
    /// Applies a prefix to all items made from the material. For `PLANT` and `CREATURE` materials,
    /// this defaults to the plant/creature name. Not permitted in material template definitions.
    #[df_token(token_name = "PREFIX")]
    pub prefix: Option<Choose<NoneEnum, String>>,
    /// Multiplies the value of the material. Not permitted in material template definitions.
    #[df_token(token_name = "MULTIPLY_VALUE")]
    pub multiply_value: Option<u32>,
    /// Changes a material's `HEATDAM_POINT`, but only if it was not set to `NONE`. Not permitted in
    /// material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_HEATDAM_POINT")]
    pub if_exists_set_heatdam_point: Option<Choose<u32, NoneEnum>>,
    /// Changes a material's `COLDDAM_POINT`, but only if it was not set to `NONE`. Not permitted in
    /// material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_COLDDAM_POINT")]
    pub if_exists_set_colddam_point: Option<Choose<u32, NoneEnum>>,
    /// Changes a material's `IGNITE_POINT`, but only if it was not set to `NONE`. Not permitted in
    /// material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_IGNITE_POINT")]
    pub if_exists_set_ignite_point: Option<Choose<u32, NoneEnum>>,
    /// Changes a material's `MELTING_POINT`, but only if it was not set to `NONE`. Not permitted in
    /// material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_MELTING_POINT")]
    pub if_exists_set_melting_point: Option<Choose<u32, NoneEnum>>,
    /// Changes a material's `BOILING_POINT`, but only if it was not set to `NONE`. Not permitted in
    /// material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_BOILING_POINT")]
    pub if_exists_set_boiling_point: Option<Choose<u32, NoneEnum>>,
    /// Changes a material's `MAT_FIXED_TEMP`, but only if it was not set to `NONE`. Not permitted
    /// in material template definitions.
    #[df_token(token_name = "IF_EXISTS_SET_MAT_FIXED_TEMP")]
    pub if_exists_set_mat_fixed_temp: Option<Choose<u32, NoneEnum>>,
    // endregion ==================================================================================
    // region: MatDef but not allowed in INORGANIC ================================================
    /// The material forms "wafers" instead of "bars".
    #[df_token(token_name = "WAFERS")]
    pub wafers: Option<()>,
    /// Makes `BOULDER` acceptable as a reagent in reactions that require `METAL_ORE:MATERIAL_NAME`,
    /// as well as smelting directly into metal bars.
    ///
    /// Places the material under "Metal Ores" in Stone stockpiles.
    ///
    /// The specified value determines the probability for this product
    /// (see [Tetrahedrite](https://dwarffortresswiki.org/index.php/Tetrahedrite)
    /// or [Galena](https://dwarffortresswiki.org/index.php/Galena) for details).
    #[df_token(token_name = "METAL_ORE")]
    pub metal_ore: Option<(ReferenceTo<InorganicToken>, u32)>,
    /// Makes `BOULDER` items made of the material acceptable for strand extraction into threads;
    /// see also `STOCKPILE_THREAD_METAL`.
    ///
    /// The value presumably determines the probability of this product extracted.
    #[df_token(token_name = "THREAD_METAL")]
    pub thread_metal: Option<(ReferenceTo<InorganicToken>, u32)>,
    // endregion ==================================================================================
    // region: Material definition tokens =========================================================
    /// List of syndromes tied to the material.
    #[df_token(token_name = "SYNDROME")]
    pub syndrome: Vec<SyndromeToken>,
    /// Overrides the name of `BOULDER` items (i.e. mined-out stones) made of the material (used for
    /// native copper/silver/gold/platinum to make them be called "nuggets" instead of "boulders").
    #[df_token(token_name = "STONE_NAME")]
    pub stone_name: Option<String>,
    /// Used to indicate that said material is a gemstone - when tiles are mined out, rough gems
    /// will be yielded instead of boulders. Plural can be "STP" to automatically append an "s" to
    /// the singular form, and `OVERWRITE_SOLID` will override the relevant `STATE_NAME` and
    /// `STATE_ADJ` values.
    #[df_token(token_name = "IS_GEM")]
    pub is_gem: Option<(
        String,
        Choose<StandardPluralEnum, String>,
        Option<OverwriteSolidEnum>,
    )>,
    /// Specifies what the material should be treated as when drinking water contaminated by it, for
    /// generating unhappy thoughts.
    #[df_token(token_name = "TEMP_DIET_INFO")]
    pub temp_diet_info: Option<DietInfoEnum>,
    /// Allows the material to be used as dye, and defines color of dyed items.
    #[df_token(token_name = "POWDER_DYE")]
    pub powder_dye: Option<ReferenceTo<ColorToken>>,
    /// Specifies the tile that will be used to represent unmined tiles made of this material.
    /// Generally only used with stones. Defaults to 219 ('█').
    #[df_token(token_name = "TILE")]
    pub tile: Option<DFChar>,
    /// Specifies the tile that will be used to represent `BOULDER` items made of this material.
    /// Generally only used with stones. Defaults to 7 ('•').
    #[df_token(token_name = "ITEM_SYMBOL")]
    pub item_symbol: Option<DFChar>,
    /// The on-screen color of the material. Uses a standard 3-digit color token. Equivalent to
    /// `[TILE_COLOR:a:b:c]`, `[BUILD_COLOR:b:a:X]` (X = 1 if 'a' equals 'b', 0 otherwise), and
    /// `[BASIC_COLOR:a:c]`.
    #[df_token(token_name = "DISPLAY_COLOR")]
    pub display_color: Option<(u8, u8, u8)>,
    /// The color of objects made of this material which use both the foreground and background
    /// color: doors, floodgates, hatch covers, bins, barrels, and cages. Defaults to 7:7:1 (white).
    #[df_token(token_name = "BUILD_COLOR")]
    pub build_color: Option<(u8, u8, u8)>,
    /// The color of unmined tiles containing this material (for stone and soil), as well as
    /// engravings in this material. Defaults to 7:7:1 (white).
    #[df_token(token_name = "TILE_COLOR")]
    pub tile_color: Option<(u8, u8, u8)>,
    /// The color of objects made of this material which use only the foreground color, including
    /// workshops, floors and boulders, and smoothed walls. Defaults to 7:1 (white).
    #[df_token(token_name = "BASIC_COLOR")]
    pub basic_color: Option<(u8, u8)>,
    /// Determines the color of the material at the specified state. Colors come from
    /// `descriptor_color_standard.txt`. The nearest color value is used to display contaminants
    /// and body parts made of this material.
    #[df_token(token_name = "STATE_COLOR")]
    pub state_color: Vec<(
        Choose<MaterialStateEnum, AllOrAllSolidEnum>,
        ReferenceTo<ColorToken>,
    )>,
    /// Determines the name of the material at the specified state, as displayed in-game.
    #[df_token(token_name = "STATE_NAME")]
    pub state_name: Vec<(Choose<MaterialStateEnum, AllOrAllSolidEnum>, String)>,
    /// Like `STATE_NAME`, but used in different situations. Equipment made from the material uses
    /// the state adjective and not the state name.
    #[df_token(token_name = "STATE_ADJ")]
    pub state_adj: Vec<(Choose<MaterialStateEnum, AllOrAllSolidEnum>, String)>,
    /// Sets both `STATE_NAME` and `STATE_ADJ` at the same time.
    #[df_token(token_name = "STATE_NAME_ADJ")]
    pub state_name_adj: Vec<(Choose<MaterialStateEnum, AllOrAllSolidEnum>, String)>,
    /// The material's tendency to absorb liquids. Containers made of materials with nonzero
    /// absorption cannot hold liquids unless they have been glazed. Defaults to 0.
    #[df_token(token_name = "ABSORPTION")]
    pub absorption: Option<u32>,
    /// Specifies how hard of an impact (in kilopascals) the material can withstand before it will
    /// start deforming permanently. Used for blunt-force combat. Defaults to 10000.
    #[df_token(token_name = "IMPACT_YIELD")]
    pub impact_yield: Option<u32>,
    /// Specifies how hard of an impact the material can withstand before it will fail entirely.
    /// Used for blunt-force combat. Defaults to 10000.
    #[df_token(token_name = "IMPACT_FRACTURE")]
    pub impact_fracture: Option<u32>,
    /// Specifies how much the material will have given (in parts-per-100000) when the yield point
    /// is reached. Used for blunt-force combat. Defaults to 0.
    ///
    /// Apparently affects in combat whether the corresponding tissue is bruised (value >= 50000),
    /// torn (value between 25000 and 49999), or fractured (value <= 24999).
    #[df_token(token_name = "IMPACT_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "IMPACT_ELASTICITY")]
    pub impact_strain_at_yield: Option<u32>,
    /// Specifies how hard the material can be compressed before it will start deforming
    /// permanently. Determines a tissue's resistance to pinching and response to strangulation.
    /// Defaults to 10000.
    #[df_token(token_name = "COMPRESSIVE_YIELD")]
    pub compressive_yield: Option<u32>,
    /// Specifies how hard the material can be compressed before it will fail entirely. Determines a
    /// tissue's resistance to pinching and response to strangulation. Defaults to 10000.
    #[df_token(token_name = "COMPRESSIVE_FRACTURE")]
    pub compressive_fracture: Option<u32>,
    /// Specifies how much the material will have given when it has been compressed to its yield
    /// point. Determines a tissue's resistance to pinching and response to strangulation. Defaults
    /// to 0.
    #[df_token(token_name = "COMPRESSIVE_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "COMPRESSIVE_ELASTICITY")]
    pub compressive_strain_at_yield: Option<u32>,
    /// Specifies how hard the material can be stretched before it will start deforming permanently.
    /// Determines a tissue's resistance to a latching and tearing bite. Defaults to 10000.
    #[df_token(token_name = "TENSILE_YIELD")]
    pub tensile_yield: Option<u32>,
    /// Specifies how hard the material can be stretched before it will fail entirely. Determines a
    /// tissue's resistance to a latching and tearing bite. Defaults to 10000.
    #[df_token(token_name = "TENSILE_FRACTURE")]
    pub tensile_fracture: Option<u32>,
    /// Specifies how much the material will have given when it is stretched to its yield point.
    /// Determines a tissue's resistance to a latching and tearing bite. Defaults to 0.
    #[df_token(token_name = "TENSILE_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "TENSILE_ELASTICITY")]
    pub tensile_strain_at_yield: Option<u32>,
    /// Specifies how hard the material can be twisted before it will start deforming permanently.
    /// Used for latching and shaking with a blunt attack (no default creature has such an attack,
    /// but they can be modded in). Defaults to 10000.
    #[df_token(token_name = "TORSION_YIELD")]
    pub torsion_yield: Option<u32>,
    /// Specifies how hard the material can be twisted before it will fail entirely. Used for
    /// latching and shaking with a blunt attack (no default creature has such an attack, but they
    /// can be modded in). Defaults to 10000.
    #[df_token(token_name = "TORSION_FRACTURE")]
    pub torsion_fracture: Option<u32>,
    /// Specifies how much the material will have given when it is twisted to its yield point. Used
    /// for latching and shaking with a blunt attack (no default creature has such an attack, but
    /// they can be modded in). Defaults to 0.
    #[df_token(token_name = "TORSION_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "TORSION_ELASTICITY")]
    pub torsion_strain_at_yield: Option<u32>,
    /// Specifies how hard the material can be sheared before it will start deforming permanently.
    /// Used for cutting calculations. Defaults to 10000.
    #[df_token(token_name = "SHEAR_YIELD")]
    pub shear_yield: Option<u32>,
    /// Specifies how hard the material can be sheared before it will fail entirely. Used for
    /// cutting calculations. Defaults to 10000.
    #[df_token(token_name = "SHEAR_FRACTURE")]
    pub shear_fracture: Option<u32>,
    /// Specifies how much the material will have given when sheared to its yield point. Used for
    /// cutting calculations. Defaults to 0.
    #[df_token(token_name = "SHEAR_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "SHEAR_ELASTICITY")]
    pub shear_strain_at_yield: Option<u32>,
    /// Specifies how hard the material can be bent before it will start deforming permanently.
    /// Determines a tissue's resistance to being mangled with a joint lock. Defaults to 10000.
    #[df_token(token_name = "BENDING_YIELD")]
    pub bending_yield: Option<u32>,
    /// Specifies how hard the material can be bent before it will fail entirely. Determines a
    /// tissue's resistance to being mangled with a joint lock. Defaults to 10000.
    #[df_token(token_name = "BENDING_FRACTURE")]
    pub bending_fracture: Option<u32>,
    /// Specifies how much the material will have given when bent to its yield point. Determines a
    /// tissue's resistance to being mangled with a joint lock. Defaults to 0.
    #[df_token(token_name = "BENDING_STRAIN_AT_YIELD")]
    #[df_alias(token_name = "BENDING_ELASTICITY")]
    pub bending_strain_at_yield: Option<u32>,
    /// How sharp the material is. Used in cutting calculations. Does not allow an inferior metal to
    /// penetrate superior armor. Applying a value of at least 10000 to a stone will allow weapons
    /// to be made from that stone. Defaults to 10000.
    #[df_token(token_name = "MAX_EDGE")]
    pub max_edge: Option<u32>,
    /// Value modifier for the material. Defaults to 1. This number can be made negative by placing
    /// a "-" in front, resulting in things that you are paid to buy and must pay to sell.
    #[df_token(token_name = "MATERIAL_VALUE")]
    pub material_value: Option<i32>,
    /// Rate at which the material heats up or cools down (in joules/kilokelvin). If set to `NONE`,
    /// the temperature will be fixed at its initial value.
    /// See [Temperature](https://dwarffortresswiki.org/index.php/Temperature) for more information.
    /// Defaults to `NONE`.
    #[df_token(token_name = "SPEC_HEAT")]
    pub spec_heat: Option<Choose<u32, NoneEnum>>,
    /// Temperature above which the material takes damage from heat. May be set to `NONE`. If the
    /// material has an ignite point but no heatdam point, it will burn for a very long time (9
    /// months and 16.8 days). Defaults to `NONE`.
    #[df_token(token_name = "HEATDAM_POINT")]
    pub heatdam_point: Option<Choose<u32, NoneEnum>>,
    /// Temperature below which the material takes damage from cold. Defaults to `NONE`.
    #[df_token(token_name = "COLDDAM_POINT")]
    pub colddam_point: Option<Choose<u32, NoneEnum>>,
    /// Temperature at which the material will catch fire. Defaults to `NONE`.
    #[df_token(token_name = "IGNITE_POINT")]
    pub ignite_point: Option<Choose<u32, NoneEnum>>,
    /// Temperature at which the material melts. Defaults to `NONE`.
    #[df_token(token_name = "MELTING_POINT")]
    pub melting_point: Option<Choose<u32, NoneEnum>>,
    /// Temperature at which the material boils. Defaults to `NONE`.
    #[df_token(token_name = "BOILING_POINT")]
    pub boiling_point: Option<Choose<u32, NoneEnum>>,
    /// Items composed of this material will initially have this temperature. Used in conjunction
    /// with `[SPEC_HEAT:NONE]` to make material's temperature fixed at the specified value.
    /// Defaults to `NONE`.
    #[df_token(token_name = "MAT_FIXED_TEMP")]
    pub mat_fixed_temp: Option<Choose<u32, NoneEnum>>,
    /// Specifies the density (in kilograms per cubic meter) of the material when in solid form.
    /// Also affects combat calculations; affects blunt-force damage and ability of edged weapons to
    /// pierce tissue layers. Defaults to `NONE`.
    #[df_token(token_name = "SOLID_DENSITY")]
    pub solid_density: Option<Choose<u32, NoneEnum>>,
    /// Specifies the density of the material when in liquid form. Defaults to `NONE`.
    #[df_token(token_name = "LIQUID_DENSITY")]
    pub liquid_density: Option<Choose<u32, NoneEnum>>,
    /// Supposedly not used. Theoretically, should determine density (at given pressure) in
    /// gas state, on which in turn would depend (together with weight of vaporized material) on the
    /// volume covered by spreading vapors. Defaults to `NONE`.
    #[df_token(token_name = "MOLAR_MASS")]
    pub molar_mass: Option<Choose<u32, NoneEnum>>,
    /// Specifies the type of container used to store the material. Used in conjunction with the
    /// `[EXTRACT_BARREL]`, `[EXTRACT_VIAL]`, or `[EXTRACT_STILL_VIAL]` plant tokens. Defaults to
    /// `BARREL`.
    #[df_token(token_name = "EXTRACT_STORAGE")]
    pub extract_storage: Option<Reference>, // TODO: ref is container type
    /// Specifies the item type used for butchering results made of this material. Stock raws use
    /// `GLOB:NONE` for fat and `MEAT:NONE` for other meat materials.
    #[df_token(token_name = "BUTCHER_SPECIAL")]
    pub butcher_special: Option<ItemReferenceArg>,
    /// When a creature is butchered, meat yielded from organs made from this material will be named
    /// via this token.
    #[df_token(token_name = "MEAT_NAME")]
    pub meat_name: Option<(Choose<NoneEnum, String>, String, String)>,
    /// Specifies the name of blocks made from this material.
    #[df_token(token_name = "BLOCK_NAME")]
    pub block_name: Option<(String, Choose<StandardPluralEnum, String>)>,
    /// Used with reaction raws to associate a reagent material with a product material. The first
    /// argument is used by `HAS_MATERIAL_REACTION_PRODUCT` and `GET_MATERIAL_FROM_REAGENT` in
    /// reaction raws. The remainder is a material reference, generally `LOCAL_CREATURE_MAT:SUBTYPE`
    /// or `LOCAL_PLANT_MAT:SUBTYPE` or `INORGANIC:STONETYPE`.
    #[df_token(token_name = "MATERIAL_REACTION_PRODUCT")]
    pub material_reaction_product: Vec<(
        ReferenceTo<ReactionToken>,
        MaterialTokenArgWithLocalCreatureMat,
    )>,
    /// Used with reaction raws to associate a reagent material with a complete item. The first
    /// argument is used by `HAS_ITEM_REACTION_PRODUCT` and `GET_ITEM_DATA_FROM_REAGENT` in reaction
    /// raws. The rest refers to the type of item, then its material.
    #[df_token(token_name = "ITEM_REACTION_PRODUCT")]
    pub item_reaction_product: Vec<(
        Reference,
        ItemReferenceArg,
        MaterialTokenArgWithLocalCreatureMat,
    )>,
    /// Used to classify all items made of the material, so that reactions can use them as generic
    /// reagents.
    ///
    /// In default raws, the following classes are used:
    /// - `FAT`, `TALLOW`, `SOAP`, `PARCHMENT`, `PAPER_PLANT`, `PAPER_SLURRY`, `MILK`, `CHEESE`, `WAX`
    /// - `CAN_GLAZE` - items made from this material can be glazed.
    /// - `FLUX` - can be used as flux in pig iron and steel making.
    /// - `GYPSUM` - can be processed into gypsum plaster.
    /// - `CALCIUM_CARBONATE` - can be used in production of quicklime.
    #[df_token(token_name = "REACTION_CLASS")]
    pub reaction_class: Vec<Reference>,
    /// Allows the material to be used to make casts.
    #[df_token(token_name = "HARDENS_WITH_WATER")]
    pub hardens_with_water: Option<MaterialTokenArgWithLocalCreatureMat>,
    /// Soap has `[SOAP_LEVEL:2]`. Effects unknown. Defaults to 0.
    #[df_token(token_name = "SOAP_LEVEL")]
    pub soap_level: Option<u32>,
    // region: Material usage tokens (no args) ====================================================
    /// Lets the game know that an animal was likely killed in the production of this item. Entities
    /// opposed to killing animals (which currently does not include Elves) will refuse to accept
    /// these items in trade.
    #[df_token(token_name = "IMPLIES_ANIMAL_KILL")]
    pub implies_animal_kill: Option<()>,
    /// Classifies the material as plant-based alcohol, allowing its storage in food stockpiles
    /// under "Drink (Plant)".
    #[df_token(token_name = "ALCOHOL_PLANT")]
    pub alcohol_plant: Option<()>,
    /// Classifies the material as animal-based alcohol, allowing its storage in food stockpiles
    /// under "Drink (Animal)".
    #[df_token(token_name = "ALCOHOL_CREATURE")]
    pub alcohol_creature: Option<()>,
    /// Classifies the material as generic alcohol. Implied by both `ALCOHOL_PLANT` and
    /// `ALCOHOL_CREATURE`. Exact behavior unknown, possibly vestigial.
    #[df_token(token_name = "ALCOHOL")]
    pub alcohol: Option<()>,
    /// Classifies the material as plant-based cheese, allowing its storage in food stockpiles
    /// under "Cheese (Plant)".
    #[df_token(token_name = "CHEESE_PLANT")]
    pub cheese_plant: Option<()>,
    /// Classifies the material as animal-based cheese, allowing its storage in food stockpiles
    /// under "Cheese (Animal)".
    #[df_token(token_name = "CHEESE_CREATURE")]
    pub cheese_creature: Option<()>,
    /// Classifies the material as generic cheese. Implied by both `CHEESE_PLANT` and
    /// `CHEESE_CREATURE`. Exact behavior unknown, possibly vestigial.
    #[df_token(token_name = "CHEESE")]
    pub cheese: Option<()>,
    /// Classifies the material as plant powder, allowing its storage in food stockpiles under
    /// "Milled Plant".
    #[df_token(token_name = "POWDER_MISC_PLANT")]
    pub powder_misc_plant: Option<()>,
    /// Classifies the material as creature powder, allowing its storage in food stockpiles under
    /// "Bone Meal".
    #[df_token(token_name = "POWDER_MISC_CREATURE")]
    pub powder_misc_creature: Option<()>,
    /// Classifies the material as generic powder. Implied by both `POWDER_MISC_PLANT` and
    /// `POWDER_MISC_CREATURE`. Exact behavior unknown, possibly vestigial.
    #[df_token(token_name = "POWDER_MISC")]
    pub powder_misc: Option<()>,
    /// Permits globs of the material in solid form to be stored in food stockpiles under "Fat" -
    /// without it, dwarves will come by and "clean" the items, destroying them (unless
    /// `[DO_NOT_CLEAN_GLOB]` is also included).
    #[df_token(token_name = "STOCKPILE_GLOB")]
    #[df_alias(token_name = "STOCKPILE_GLOB_SOLID", discouraged)]
    pub stockpile_glob: Option<()>,
    /// Classifies the material as milled paste, allowing its storage in food stockpiles under
    /// "Paste".
    #[df_token(token_name = "STOCKPILE_GLOB_PASTE")]
    pub stockpile_glob_paste: Option<()>,
    /// Classifies the material as pressed goods, allowing its storage in food stockpiles under
    /// "Pressed Material".
    #[df_token(token_name = "STOCKPILE_GLOB_PRESSED")]
    pub stockpile_glob_pressed: Option<()>,
    /// Classifies the material as a plant growth (e.g. fruits, leaves), allowing its storage in
    /// food stockpiles under Plant Growth/Fruit.
    #[df_token(token_name = "STOCKPILE_PLANT_GROWTH")]
    pub stockpile_plant_growth: Option<()>,
    /// Classifies the material as a plant extract, allowing its storage in food stockpiles under
    /// "Extract (Plant)".
    #[df_token(token_name = "LIQUID_MISC_PLANT")]
    pub liquid_misc_plant: Option<()>,
    /// Classifies the material as a creature extract, allowing its storage in food stockpiles under
    /// "Extract (Animal)".
    #[df_token(token_name = "LIQUID_MISC_CREATURE")]
    pub liquid_misc_creature: Option<()>,
    /// Classifies the material as a miscellaneous liquid, allowing its storage in food stockpiles
    /// under "Misc. Liquid" along with lye.
    #[df_token(token_name = "LIQUID_MISC_OTHER")]
    pub liquid_misc_other: Option<()>,
    /// Classifies the material as a generic liquid. Implied by `LIQUID_MISC_PLANT`,
    /// `LIQUID_MISC_CREATURE`, and `LIQUID_MISC_OTHER`. Exact behavior unknown, possibly vestigial.
    #[df_token(token_name = "LIQUID_MISC")]
    pub liquid_misc: Option<()>,
    /// Classifies the material as a plant, allowing its storage in food stockpiles under "Plants".
    #[df_token(token_name = "STRUCTURAL_PLANT_MAT")]
    pub structural_plant_mat: Option<()>,
    /// Classifies the material as a plant seed, allowing its storage in food stockpiles under
    /// "Seeds".
    #[df_token(token_name = "SEED_MAT")]
    pub seed_mat: Option<()>,
    /// Classifies the material as bone, allowing its use for bone carvers and restriction from
    /// stockpiles by material.
    #[df_token(token_name = "BONE")]
    pub bone: Option<()>,
    /// Classifies the material as wood, allowing its use for carpenters and storage in wood
    /// stockpiles. Entities opposed to killing plants (i.e. Elves) will refuse to accept these
    /// items in trade.
    #[df_token(token_name = "WOOD")]
    pub wood: Option<()>,
    /// Classifies the material as plant fiber, allowing its use for clothiers and storage in cloth
    /// stockpiles under "Thread (Plant)" and "Cloth (Plant)".
    #[df_token(token_name = "THREAD_PLANT")]
    pub thread_plant: Option<()>,
    /// Classifies the material as tooth, allowing its use for bone carvers and restriction from
    /// stockpiles by material.
    #[df_token(token_name = "TOOTH")]
    pub tooth: Option<()>,
    /// Classifies the material as horn, allowing its use for bone carvers and restriction from
    /// stockpiles by material.
    #[df_token(token_name = "HORN")]
    pub horn: Option<()>,
    /// Classifies the material as pearl, allowing its use for bone carvers and restriction from
    /// stockpiles by material.
    #[df_token(token_name = "PEARL")]
    pub pearl: Option<()>,
    /// Classifies the material as shell, allowing its use for bone carvers and restriction from
    /// stockpiles by material.
    #[df_token(token_name = "SHELL")]
    pub shell: Option<()>,
    /// Classifies the material as leather, allowing its use for leatherworkers and storage in
    /// leather stockpiles.
    #[df_token(token_name = "LEATHER")]
    pub leather: Option<()>,
    /// Classifies the material as silk, allowing its use for clothiers and storage in cloth
    /// stockpiles under "Thread (Silk)" and "Cloth (Silk)".
    #[df_token(token_name = "SILK")]
    pub silk: Option<()>,
    /// Classifies the material as soap, allowing it to be used as a bath detergent and stored in
    /// bar/block stockpiles under "Bars: Other Materials".
    #[df_token(token_name = "SOAP")]
    pub soap: Option<()>,
    /// Material generates miasma when it rots.
    #[df_token(token_name = "GENERATES_MIASMA")]
    pub generates_miasma: Option<()>,
    /// Classifies the material as edible meat.
    #[df_token(token_name = "MEAT")]
    pub meat: Option<()>,
    /// Material will rot if not stockpiled appropriately. Currently only affects food and refuse,
    /// other items made of this material will not rot.
    #[df_token(token_name = "ROTS")]
    pub rots: Option<()>,
    /// Tells the game to classify contaminants of this material as being "blood" in Adventurer mode
    /// tile descriptions ("Here we have a Dwarf in a slurry of blood.").
    #[df_token(token_name = "BLOOD_MAP_DESCRIPTOR")]
    pub blood_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "ichor".
    #[df_token(token_name = "ICHOR_MAP_DESCRIPTOR")]
    pub ichor_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "goo".
    #[df_token(token_name = "GOO_MAP_DESCRIPTOR")]
    pub goo_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "slime".
    #[df_token(token_name = "SLIME_MAP_DESCRIPTOR")]
    pub slime_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "pus".
    #[df_token(token_name = "PUS_MAP_DESCRIPTOR")]
    pub pus_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "sweat".
    #[df_token(token_name = "SWEAT_MAP_DESCRIPTOR")]
    pub sweat_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "tears".
    #[df_token(token_name = "TEARS_MAP_DESCRIPTOR")]
    pub tears_map_descriptor: Option<()>,
    /// Tells the game to classify contaminants of this material as being "spit".
    #[df_token(token_name = "SPIT_MAP_DESCRIPTOR")]
    pub spit_map_descriptor: Option<()>,
    /// Contaminants composed of this material evaporate over time, slowly disappearing from the
    /// map. Used internally by water.
    #[df_token(token_name = "EVAPORATES")]
    pub evaporates: Option<()>,
    /// Used for materials which cause syndromes, causes it to enter the creature's blood instead of
    /// simply spattering on the surface.
    #[df_token(token_name = "ENTERS_BLOOD")]
    pub enters_blood: Option<()>,
    /// Can be eaten by vermin.
    #[df_token(token_name = "EDIBLE_VERMIN")]
    pub edible_vermin: Option<()>,
    /// Can be eaten raw.
    #[df_token(token_name = "EDIBLE_RAW")]
    pub edible_raw: Option<()>,
    /// Can be cooked and then eaten.
    #[df_token(token_name = "EDIBLE_COOKED")]
    pub edible_cooked: Option<()>,
    /// Prevents globs made of this material from being cleaned up and destroyed.
    #[df_token(token_name = "DO_NOT_CLEAN_GLOB")]
    pub do_not_clean_glob: Option<()>,
    /// Prevents the material from showing up in Stone stockpile settings.
    #[df_token(token_name = "NO_STONE_STOCKPILE")]
    pub no_stone_stockpile: Option<()>,
    /// Allows the creation of metal furniture at the metalsmith's forge.
    #[df_token(token_name = "ITEMS_METAL")]
    pub items_metal: Option<()>,
    /// Equivalent to `ITEMS_HARD`. Given to bone.
    #[df_token(token_name = "ITEMS_BARRED")]
    pub items_barred: Option<()>,
    /// Equivalent to `ITEMS_HARD`. Given to shell.
    #[df_token(token_name = "ITEMS_SCALED")]
    pub items_scaled: Option<()>,
    /// Equivalent to `ITEMS_SOFT`. Given to leather.
    #[df_token(token_name = "ITEMS_LEATHER")]
    pub items_leather: Option<()>,
    /// Random crafts made from this material cannot be made into rings, crowns, scepters or
    /// figurines. Given to plant fiber, silk and wool.
    #[df_token(token_name = "ITEMS_SOFT")]
    pub items_soft: Option<()>,
    /// Random crafts made from this material include all seven items. Given to stone, wood, bone,
    /// shell, chitin, claws, teeth, horns, hooves and beeswax. Hair, pearls and eggshells also have
    /// the tag.
    #[df_token(token_name = "ITEMS_HARD")]
    pub items_hard: Option<()>,
    /// Used to define that the material is a stone. Allows its usage in masonry and stonecrafting
    /// and storage in stone stockpiles, among other effects.
    #[df_token(token_name = "IS_STONE")]
    pub is_stone: Option<()>,
    /// Used for a stone that cannot be dug into.
    #[df_token(token_name = "UNDIGGABLE")]
    pub undiggable: Option<()>,
    /// Causes containers made of this material to be prefixed with "unglazed" if they have not yet
    /// been glazed.
    #[df_token(token_name = "DISPLAY_UNGLAZED")]
    pub display_unglazed: Option<()>,
    /// Classifies the material as yarn, allowing its use for clothiers and its storage in cloth
    /// stockpiles under "Thread (Yarn)" and "Cloth (Yarn)".
    #[df_token(token_name = "YARN")]
    pub yarn: Option<()>,
    /// Classifies the material as metal thread, permitting thread and cloth to be stored in cloth
    /// stockpiles under "Thread (Metal)" and "Cloth (Metal)".
    #[df_token(token_name = "STOCKPILE_THREAD_METAL")]
    pub stockpile_thread_metal: Option<()>,
    /// Defines the material as being metal, allowing it to be used at forges.
    #[df_token(token_name = "IS_METAL")]
    pub is_metal: Option<()>,
    /// Used internally by green glass, clear glass, and crystal glass.
    #[df_token(token_name = "IS_GLASS")]
    pub is_glass: Option<()>,
    /// Can be used in the production of crystal glass.
    #[df_token(token_name = "CRYSTAL_GLASSABLE")]
    pub crystal_glassable: Option<()>,
    /// Melee weapons can be made out of this material.
    #[df_token(token_name = "ITEMS_WEAPON")]
    pub items_weapon: Option<()>,
    /// Ranged weapons can be made out of this material.
    #[df_token(token_name = "ITEMS_WEAPON_RANGED")]
    pub items_weapon_ranged: Option<()>,
    /// Anvils can be made out of this material.
    #[df_token(token_name = "ITEMS_ANVIL")]
    pub items_anvil: Option<()>,
    /// Ammunition can be made out of this material.
    #[df_token(token_name = "ITEMS_AMMO")]
    pub items_ammo: Option<()>,
    /// Picks can be made out of this material.
    #[df_token(token_name = "ITEMS_DIGGER")]
    pub items_digger: Option<()>,
    /// Armor can be made out of this material.
    #[df_token(token_name = "ITEMS_ARMOR")]
    pub items_armor: Option<()>,
    /// Used internally by amber and coral. Functionally equivalent to `ITEMS_HARD`.
    #[df_token(token_name = "ITEMS_DELICATE")]
    pub items_delicate: Option<()>,
    /// Siege engine parts can be made out of this material. Does not appear to work.
    #[df_token(token_name = "ITEMS_SIEGE_ENGINE")]
    pub items_siege_engine: Option<()>,
    /// Querns and millstones can be made out of this material.
    #[df_token(token_name = "ITEMS_QUERN")]
    pub items_quern: Option<()>,
    // endregion ==================================================================================
    // endregion ==================================================================================
}
