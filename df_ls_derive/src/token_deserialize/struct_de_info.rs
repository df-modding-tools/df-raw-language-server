use crate::common::{self, opt_lit_to_string};
use quote::quote;

#[derive(Clone, Debug, Default)]
/// Combined info/metadata about the struct token.
pub struct StructDeInfo {
    pub token_name: Option<String>,
    pub second_par_check: bool,
    pub enum_value: bool,
}

pub fn get_struct_enum_de_info(ast: &syn::DeriveInput) -> (StructDeInfo, proc_macro2::TokenStream) {
    let mut struct_info = StructDeInfo::default();
    let mut errors = quote! {};
    // Loop over all the line of attributes (`#[...]`).
    for attr in &ast.attrs {
        if let Some((info, error)) = get_struct_de_info(attr) {
            // Add errors
            errors = quote! {
                #errors
                #error
            };
            if let Some(info) = info {
                struct_info = info;
            }
        }
    }
    (struct_info, errors)
}

// Parse `#[df_token(...)]` to `StructDeInfo`
fn get_struct_de_info(
    attr: &syn::Attribute,
) -> Option<(Option<StructDeInfo>, proc_macro2::TokenStream)> {
    let mut token_name: Option<String> = None;
    let mut second_par_check = false;
    let mut enum_value = false;

    let meta_items = common::get_meta_items(attr, "df_token").unwrap();
    // If an different argument was found, skip this.
    for meta_item in meta_items? {
        match meta_item {
            // Parse `#[df_token(token_name = "FOO")]`
            syn::NestedMeta::Meta(syn::Meta::NameValue(m)) if m.path.is_ident("token_name") => {
                token_name = opt_lit_to_string(Some(m.lit));
            }
            // Parse `#[df_token(second_par_check)]`
            syn::NestedMeta::Meta(syn::Meta::Path(m)) if m.is_ident("second_par_check") => {
                second_par_check = true;
            }
            // Parse `#[df_token(enum_value)]`
            syn::NestedMeta::Meta(syn::Meta::Path(m)) if m.is_ident("enum_value") => {
                enum_value = true;
            }
            _ => (),
        };
    }
    let errors = quote! {};
    Some((
        Some(StructDeInfo {
            token_name,
            second_par_check,
            enum_value,
        }),
        errors,
    ))
}
