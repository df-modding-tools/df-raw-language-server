/// Implement `TokenDeserialize` for a token with `[REF:ty]`
/// There `ty` is the type given in the argument
/// So it adds: `impl TokenDeserialize for ty {...}`
#[macro_export]
macro_rules! token_deserialize_unary_token {
    ( $x:ty ) => {
        /// Deserialize a token with following pattern: `[REF:ty]`
        impl TokenDeserialize for $x {
            fn deserialize_tokens(
                mut cursor: &mut $crate::TreeCursor,
                source: &str,
                mut diagnostics: &mut DiagnosticsInfo,
                config: &df_ls_core::DfLsConfig,
            ) -> Result<Box<Self>, ()> {
                // Get arguments from token
                let mut token = match $crate::Token::deserialize_tokens(
                    &mut cursor,
                    &source,
                    &mut diagnostics,
                    config,
                ) {
                    Ok(token) => token,
                    Err(err) => {
                        // Token could not be parsed, so we can consume it.
                        // Because this will always fail.
                        $crate::Token::consume_token(&mut cursor)?;
                        return Err(err);
                    }
                };
                $crate::Token::consume_token(&mut cursor)?;
                // Token Name
                token.check_token_name(source, &mut diagnostics, true)?;
                // Arg 0
                let result =
                    Self::try_from_argument_group(&mut token, source, &mut diagnostics, true);
                // Just check if all tokens are consumed if no other errors are present
                if result.is_ok() {
                    token.check_all_arg_consumed(source, &mut diagnostics, true)?;
                } else {
                    // In case of an error, mark other arguments as unchecked.
                    $crate::mark_rest_of_token_as_unchecked(diagnostics, &token);
                }
                Ok(Box::new(result?))
            }

            fn deserialize_general_token(
                _cursor: &mut $crate::TreeCursor,
                _source: &str,
                _diagnostics: &mut DiagnosticsInfo,
                _config: &df_ls_core::DfLsConfig,
                new_self: Box<Self>,
            ) -> ($crate::LoopControl, Box<Self>) {
                ($crate::LoopControl::DoNothing, new_self)
            }

            fn get_vec_loopcontrol() -> $crate::LoopControl {
                $crate::LoopControl::DoNothing
            }

            fn get_allowed_tokens(_config: &df_ls_core::DfLsConfig) -> Option<Vec<String>> {
                None
            }
        }
    };
    ( $x:ty, $t:tt ) => {
        /// Deserialize a token with following pattern: `[REF:ty]`
        impl<$t> TokenDeserialize for $x
        where
            $t: Default + $crate::TryFromArgumentGroup,
            $x: Default + $crate::TryFromArgumentGroup,
        {
            fn deserialize_tokens(
                mut cursor: &mut $crate::TreeCursor,
                source: &str,
                mut diagnostics: &mut DiagnosticsInfo,
                config: &df_ls_core::DfLsConfig,
            ) -> Result<Box<Self>, ()> {
                // Get arguments from token
                let mut token = match $crate::Token::deserialize_tokens(
                    &mut cursor,
                    &source,
                    &mut diagnostics,
                    config,
                ) {
                    Ok(token) => token,
                    Err(err) => {
                        // Token could not be parsed, so we can consume it.
                        // Because this will always fail.
                        $crate::Token::consume_token(&mut cursor)?;
                        return Err(err);
                    }
                };
                $crate::Token::consume_token(&mut cursor)?;
                // Token Name
                token.check_token_name(source, &mut diagnostics, true)?;
                // Arg 0
                let result =
                    Self::try_from_argument_group(&mut token, source, &mut diagnostics, true);
                // Just check if all tokens are consumed if no other errors are present
                if result.is_ok() {
                    token.check_all_arg_consumed(source, &mut diagnostics, true)?;
                } else {
                    // In case of an error, mark other arguments as unchecked.
                    $crate::mark_rest_of_token_as_unchecked(diagnostics, &token);
                }
                Ok(Box::new(result?))
            }

            fn deserialize_general_token(
                _cursor: &mut $crate::TreeCursor,
                _source: &str,
                _diagnostics: &mut DiagnosticsInfo,
                _config: &df_ls_core::DfLsConfig,
                new_self: Box<Self>,
            ) -> ($crate::LoopControl, Box<Self>) {
                ($crate::LoopControl::DoNothing, new_self)
            }

            fn get_vec_loopcontrol() -> $crate::LoopControl {
                $crate::LoopControl::DoNothing
            }

            fn get_allowed_tokens(_config: &df_ls_core::DfLsConfig) -> Option<Vec<String>> {
                None
            }
        }
    };
}
