/// Main config structure for providing global settings during RAW parsing.
///
/// This structure is static and can not be changed during the parsing of a file.
/// But it can be changed in between checks.
///
/// Changing setting might require the LS to be reloaded and/or caches to be invalidated.
#[derive(Debug, Clone)]
pub struct DfLsConfig {
    /// The minimum DF version number that the developer intends be use.
    /// Format should be "x.xx.xx" leading zeros should be added where needed.
    ///
    /// This is used to provide info about know bugs, not yet added or deprecated tokens.
    ///
    /// The DF RAW Language Server does not work with all versions.
    /// For more info see: https://gitlab.com/df-modding-tools/df-raw-language-server#supported-df-versions
    ///
    /// Example: `"0.50.01"`
    pub target_df_version: String,
}

impl Default for DfLsConfig {
    fn default() -> Self {
        Self {
            target_df_version: "0.47.05".to_owned(),
        }
    }
}
