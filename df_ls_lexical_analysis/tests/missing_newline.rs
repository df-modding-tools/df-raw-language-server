use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;

#[test]
fn test_missing_newline() {
    LexerTestBuilder::test_source("creature_domestic[OBJECT:CREATURE][CREATURE:DOG]")
        .add_test_s_exp(
            "(raw_file
  (header: `creature_domestic`)
  (token
    ([)
    (token_name: `OBJECT`)
    (token_arguments
      (:)
      (token_argument_reference: `CREATURE`)
    )
    (])
  )
  (token
    ([)
    (token_name: `CREATURE`)
    (token_arguments
      (:)
      (token_argument_reference: `DOG`)
    )
    (])
  )
  (EOF: ``)
)
",
        )
        .add_test_lexer_diagnostics_codes(vec!["missing_newline"])
        .add_test_lexer_diagnostics_ranges(vec![Range {
            start: Position {
                line: 0,
                character: 17,
            },
            end: Position {
                line: 0,
                character: 17,
            },
        }])
        .run_test();
}
