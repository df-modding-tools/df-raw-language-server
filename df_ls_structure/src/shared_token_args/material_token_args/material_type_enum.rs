use crate::{CreatureToken, InorganicToken, NoMatGlossEnum, NoneEnum, PlantToken};
use df_ls_core::{Choose, Reference, ReferenceTo};
use df_ls_diagnostics::hash_map;
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
/// The shape of the tissue
pub enum MaterialTypeEnum {
    /// Specifies a standalone inorganic material defined in the raws, generally a stone or metal.
    /// For example, `INORGANIC:IRON` refers to iron, and `INORGANIC:CERAMIC_PORCELAIN`
    /// refers to porcelain. The material name can be substituted with `USE_LAVA_STONE`
    /// to automatically select the local lava stone, which is normally obsidian.
    // #[df_token(token_name = "INORGANIC")]
    // #[df_alias(token_name = "STONE", discouraged)]
    // #[df_alias(token_name = "METAL", discouraged)]
    Inorganic(ReferenceTo<InorganicToken>),
    /// Specifies a material associated with a specific creature.
    /// Examples: `CREATURE_MAT:DWARF:SKIN` refers to dwarf skin.
    // #[df_token(token_name = "CREATURE_MAT")]
    CreatureMat((ReferenceTo<CreatureToken>, Reference)),
    /// Specifies a material associated with a specific plant.
    /// Example: `PLANT_MAT:BUSH_QUARRY:LEAF` refers to quarry bush leaves.
    // #[df_token(token_name = "PLANT_MAT")]
    PlantMat((ReferenceTo<PlantToken>, Reference)),
    //-----------Hardcoded materials--------
    /// Specifies one of the hardcoded materials.
    /// Amber is a type of material made from fossilized tree resin.
    // #[df_token(token_name = "AMBER")]
    Amber(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Coral is a type of material composed of the dead remains of corals,
    /// creatures that have not yet been implemented into the game.
    // #[df_token(token_name = "CORAL")]
    Coral(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Glass is produced at the glass furnace using fuel with either sand (green glass),
    /// sand and pearlash (clear glass), or rock crystal and pearlash (crystal glass).
    // #[df_token(token_name = "GLASS_GREEN")]
    GlassGreen(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Glass is produced at the glass furnace using fuel with either sand (green glass),
    /// sand and pearlash (clear glass), or rock crystal and pearlash (crystal glass).
    // #[df_token(token_name = "GLASS_CLEAR")]
    GlassClear(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Glass is produced at the glass furnace using fuel with either sand (green glass),
    /// sand and pearlash (clear glass), or rock crystal and pearlash (crystal glass).
    // #[df_token(token_name = "GLASS_CRYSTAL")]
    GlassCrystal(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Water, when placed in buckets or when mining out ice.
    // #[df_token(token_name = "WATER")]
    Water(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Specifies a material that can be used as fuel - charcoal or coke.
    /// Specifying `NO_MATGLOSS` (not `NONE`) will make it accept "refined coal" in general,
    /// which matches charcoal, coke, and generic refined coal.
    // #[df_token(token_name = "COAL")]
    Coal(CoalMaterialEnum),
    /// Specifies one of the hardcoded materials.
    /// Potash is a wood-based product which has applications in farming,
    /// as well as production of mid- and high-end glass products.
    // #[df_token(token_name = "POTASH")]
    Potash(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Ash is an intermediate good used to make potash, lye, or to glaze ceramics.
    // #[df_token(token_name = "ASH")]
    Ash(Option<Choose<NoneEnum, NoMatGlossEnum>>),
    /// Specifies one of the hardcoded materials.
    /// Pearlash is a wood-based product which is used primarily
    /// in the manufacture of clear and crystal glass.
    // #[df_token(token_name = "PEARLASH")]
    Pearlash(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Lye is a material used to make soap, and can also be used to make potash.
    // #[df_token(token_name = "LYE")]
    Lye(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Mud is a contaminant produced when an area is covered with water, and colors tiles brown.
    // #[df_token(token_name = "MUD")]
    Mud(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Under certain conditions, creatures (such as your dwarves) will vomit,
    /// creating a puddle of vomit.
    // #[df_token(token_name = "VOMIT")]
    Vomit(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Salt is a contaminant that makes oceanic water unsuitable for drinking.
    // #[df_token(token_name = "SALT")]
    Salt(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Filth comes in two varieties: solid brown (B) and liquid yellow (Y) filth.
    // #[df_token(token_name = "FILTH_B")]
    FilthB(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Filth comes in two varieties: solid brown (B) and liquid yellow (Y) filth.
    // #[df_token(token_name = "FILTH_Y")]
    FilthY(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Unknown substance is a hardcoded material that takes the form of a light gray liquid.
    /// No longer used in recent versions of game.
    // #[df_token(token_name = "UNKNOWN_SUBSTANCE")]
    UnknownSubstance(Option<NoneEnum>),
    /// Specifies one of the hardcoded materials.
    /// Grime is a brown-colored contaminant that makes water from murky pools disgusting to drink.
    // #[df_token(token_name = "GRIME")]
    Grime(Option<NoneEnum>),
}
impl Default for MaterialTypeEnum {
    fn default() -> Self {
        Self::Inorganic(ReferenceTo::new(String::default()))
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum CoalMaterialEnum {
    #[df_token(token_name = "CHARCOAL")]
    Charcoal,
    #[df_token(token_name = "COKE")]
    Coke,
    /// Make it accept "refined coal" in general,
    /// which matches charcoal, coke, and generic refined coal.
    #[df_token(token_name = "NO_MATGLOSS")]
    NoMatgloss,
}
impl Default for CoalMaterialEnum {
    fn default() -> Self {
        Self::Charcoal
    }
}
