#![allow(clippy::needless_update)]

use df_ls_debug_structure::*;
use df_ls_diagnostics::lsp_types::*;
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_char_byte_width() {
    let source = "h
    [MAIN:DEBUG]

    [DF_CHAR:'ð'][VEC:2:0:1]
    ";
    // Convert source from CP437 to UTF-8
    let source_bytes = df_cp437::convert_cp437_to_utf8(source.as_bytes());
    let source = String::from_utf8(source_bytes).expect("Non UTF-8 characters found");
    println!("{}", source);
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(source)
            .add_test_s_exp(
                "(raw_file
  (header: `h`)
  (comment)
  (token
    ([)
    (token_name: `MAIN`)
    (token_arguments
      (:)
      (token_argument_reference: `DEBUG`)
    )
    (])
  )
  (comment)
  (token
    ([)
    (token_name: `DF_CHAR`)
    (token_arguments
      (:)
      (token_argument_string: `'├░'`)
    )
    (])
  )
  (token
    ([)
    (token_name: `VEC`)
    (token_arguments
      (:)
      (token_argument_integer: `2`)
      (:)
      (token_argument_integer: `0`)
      (:)
      (token_argument_integer: `1`)
    )
    (])
  )
  (comment)
  (EOF: ``)
)
",
            )
            .add_test_lexer_diagnostics_codes(vec![])
            .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DebugRaw {
        header: "h".to_owned(),
        token_structure: vec![MainToken {
            debug: vec![DebugTokens {
                df_char: vec![],
                vec: vec![(vec![2, 0, 1],)],
                ..Default::default()
            }],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec!["wrong_arg_type"])
    .add_test_syntax_diagnostics_ranges(vec![Range {
        start: Position {
            line: 3,
            character: 13,
        },
        end: Position {
            line: 3,
            character: 17,
        },
    }])
    .run_test();
}
