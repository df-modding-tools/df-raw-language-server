use crate::{ItemReferenceArg, KeyBindEnum, LaborEnum, MaterialTokenArgWithNoneNone};
use df_ls_core::{Choose, Clamp, DFChar, Reference, ReferenceTo, Referenceable};
use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq, Referenceable)]
pub enum BuildingToken {
    #[df_token(token_name = "BUILDING_WORKSHOP")]
    Workshop(BuildingGeneralToken),
    #[df_token(token_name = "BUILDING_FURNACE")]
    Furnace(BuildingGeneralToken),
}
impl Default for BuildingToken {
    fn default() -> Self {
        Self::Workshop(BuildingGeneralToken::default())
    }
}

#[derive(
    Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq, Referenceable,
)]
pub struct BuildingGeneralToken {
    /// Argument 1 of `[BUILDING_WORKSHOP:...]` or `[BUILDING_FURNACE:...]`
    #[df_token(
        token_name = "BUILDING_WORKSHOP",
        on_duplicate_to_parent,
        primary_token
    )]
    #[df_alias(token_name = "BUILDING_FURNACE")]
    #[referenceable(self_reference)]
    pub reference: Option<ReferenceTo<Self>>,
    /// The name of the custom building.
    #[df_token(token_name = "NAME")]
    pub name: Option<String>,
    /// The color of the building's name when querying it.
    /// Seemingly ignored for furnaces, which are hardcoded to 4:0:1.
    ///
    /// Arguments: `[NAME_COLOR:fg:bg:bright]`
    #[df_token(token_name = "NAME_COLOR")]
    pub name_color: Option<(u8, u8, u8)>,
    /// The size of the custom building, in number of tiles.
    ///
    /// Arguments: `[DIM:width:height]`
    /// Defaults to 3x3.
    /// Maximum possible size is 31x31.
    #[df_token(token_name = "DIM")]
    pub dim: Option<(Clamp<u8, 0, 31>, Clamp<u8, 0, 31>)>,
    /// The tile (1:1 for upper-left) in which dwarves will stand when they are performing tasks.
    ///
    /// Arguments: `[WORK_LOCATION:x:y]`
    /// Defaults to 3:3 (bottom-right).
    #[df_token(token_name = "WORK_LOCATION")]
    pub work_location: Option<(Clamp<u8, 1, 31>, Clamp<u8, 1, 31>)>,
    /// The labor required to construct the custom building.
    /// If multiple `BUILD_LABOR` tokens are specified, then any of the indicated labors can
    /// be used to construct the building; if none are specified, then no labors are required.
    /// For furnaces, this labor does not come into play until after the
    /// workshop has been designed by an architect.
    #[df_token(token_name = "BUILD_LABOR")]
    pub build_labor: Vec<LaborEnum>,
    /// The shortcut key used in the Build menu for selecting the custom building.
    #[df_token(token_name = "BUILD_KEY")]
    pub build_key: Option<KeyBindEnum>,
    /// Specifies whether or not each workshop tile blocks movement.
    /// The first parameter is the row (1 = top), and each subsequent parameter
    /// is a 0 (nonblocking) or 1 (blocking) for each column, left to right.
    ///
    /// Arguments: `[BLOCK:row_nr:blocking_args]`
    #[df_token(token_name = "BLOCK")]
    pub block: Vec<(u8, bool, Vec<bool>)>,
    /// Specifies the characters used to represent the custom building.
    /// The first parameter is the building stage, varying from 0 (awaiting construction)
    /// to N (completed) where N is between 1 and 3, the 2nd parameter is the row number,
    /// and each subsequent parameter is a character number
    /// (or literal character enclosed in 'quotes').
    ///
    /// Arguments: `[TILE:building_stage:row_nr:df_chars]`
    #[df_token(token_name = "TILE")]
    pub tile: Vec<(
        Clamp<u8, 0, 3>, // Building stage
        u8,              // Row number
        DFChar,          // Building characters (at least 1 required)
        Vec<DFChar>,     // Building characters
    )>,
    /// Specifies the colors in which the custom building's tiles will be displayed.
    /// The first parameter is the building stage, the 2nd parameter is the row number,
    /// and subsequent parameters are either sets of 3 numbers (`foreground:background:brightness`)
    /// or the token `MAT` to use the color of the primary building material.
    /// `MAT` may not be available on `BUILDING_FURNACE`s.
    ///
    /// Arguments: `[COLOR:building_stage:row_nr:colors]`
    #[df_token(token_name = "COLOR")]
    pub color: Vec<(
        Clamp<u8, 0, 3>,                    // Building stage
        u8,                                 // Row number
        Choose<MatEnum, (u8, u8, u8)>,      // Building color (at least 1 required)
        Vec<Choose<MatEnum, (u8, u8, u8)>>, // Building color
    )>,
    /// Specifies one of the objects necessary to construct the custom building.
    /// Each `BUILD_ITEM` can be followed by zero or more modifiers.
    ///
    /// Arguments: `[BUILD_ITEM:quantity:item_token:material_token]`
    #[df_token(token_name = "BUILD_ITEM")]
    pub build_item: Vec<BuildItemToken>,
    /// Specifies that one of the building's tiles (other than the `WORK_LOCATION`)
    /// must be hanging over magma in order for the building to function.
    /// Buildings with this token also ignore the `[FUEL]` token in their reactions.
    #[df_token(token_name = "NEEDS_MAGMA")]
    pub needs_magma: Option<()>,
}

#[derive(Serialize, Deserialize, Clone, Debug, Default, TokenDeserialize, PartialEq, Eq)]
pub struct BuildItemToken {
    /// Argument 1 of `[BUILD_ITEM:...]`
    #[df_token(token_name = "BUILD_ITEM", on_duplicate_to_parent, primary_token)]
    pub build_item: Option<(
        u32,                          // quantity
        ItemReferenceArg,             // Item token
        MaterialTokenArgWithNoneNone, // Material token
    )>,
    //-------------------------------------------------------------
    // All tokens below are similar or the same as `ReagentToken`.
    //-------------------------------------------------------------
    /// Item material must have the `[BONE]` token.
    #[df_token(token_name = "ANY_BONE_MATERIAL")]
    pub any_bone_material: Option<()>,
    /// Item material must have the `[HORN]` token.
    #[df_token(token_name = "ANY_HORN_MATERIAL")]
    pub any_horn_material: Option<()>,
    /// Item material must have the `[LEATHER]` token.
    #[df_token(token_name = "ANY_LEATHER_MATERIAL")]
    pub any_leather_material: Option<()>,
    /// Item material must have the `[PEARL]` token.
    #[df_token(token_name = "ANY_PEARL_MATERIAL")]
    pub any_pearl_material: Option<()>,
    /// Item material must be subordinate to a `PLANT` object.
    #[df_token(token_name = "ANY_PLANT_MATERIAL")]
    pub any_plant_material: Option<()>,
    /// Item material must have the `[SHELL]` token.
    #[df_token(token_name = "ANY_SHELL_MATERIAL")]
    pub any_shell_material: Option<()>,
    /// Item material must have the `[SILK]` token.
    #[df_token(token_name = "ANY_SILK_MATERIAL")]
    pub any_silk_material: Option<()>,
    /// Item material must have the `[SOAP]` token.
    #[df_token(token_name = "ANY_SOAP_MATERIAL")]
    pub any_soap_material: Option<()>,
    /// Item is made of a tissue having `[TISSUE_SHAPE:STRANDS]`,
    /// intended for matching hair and wool.
    /// Must be used with `[USE_BODY_COMPONENT]`.
    #[df_token(token_name = "ANY_STRAND_TISSUE")]
    pub any_strand_tissue: Option<()>,
    /// Item material must have the `[TOOTH]` token.
    #[df_token(token_name = "ANY_TOOTH_MATERIAL")]
    pub any_tooth_material: Option<()>,
    /// Item material must have the `[YARN]` token.
    #[df_token(token_name = "ANY_YARN_MATERIAL")]
    pub any_yarn_material: Option<()>,
    /// Item has to be a bag. Intended to be used with an item type of `BOX`,
    /// to prevent chests, coffers, and other containers from being used instead.
    #[df_token(token_name = "BAG")]
    pub bag: Option<()>,
    /// Item is able to be used to build structures (Stone, Wood, Blocks, Bars?).
    #[df_token(token_name = "BUILDMAT")]
    pub build_material: Option<()>,
    /// Item can be an Artifact.
    #[df_token(token_name = "CAN_USE_ARTIFACT")]
    pub can_use_artifact: Option<()>,
    /// Item must be a `BARREL` or `TOOL` which contains at least one item of
    /// type `LIQUID_MISC` made of `LYE`.
    #[df_token(token_name = "CONTAINS_LYE")]
    #[df_alias(token_name = "POTASHABLE", discouraged)]
    pub contains_lye: Option<Reference>,
    /// If the item is a container, it must be empty.
    #[df_token(token_name = "EMPTY")]
    pub empty: Option<()>,
    /// Item material must be considered fire-safe (stable temperature below 11000 °U ).
    /// Only works with items of type `BAR`, `BLOCKS`, `BOULDER`, `WOOD`, and `ANVIL` -
    /// all others are considered unsafe.
    #[df_token(token_name = "FIRE_BUILD_SAFE")]
    pub fire_build_safe: Option<()>,
    /// Item material has `[IS_GLASS]`. All 3 types of glass have this token hardcoded.
    #[df_token(token_name = "GLASS_MATERIAL")]
    pub glass_material: Option<()>,
    /// Similar to `REACTION_CLASS`, but requires the reagents material to have a matching
    /// `MATERIAL_REACTION_PRODUCT` entry. Intended for reactions which transform one class of
    /// material into another, such as skin->leather and fat->tallow.
    #[df_token(token_name = "HAS_MATERIAL_REACTION_PRODUCT")]
    pub has_material_reaction_product: Option<Reference>, // TODO See REACTION_CLASS in MaterialDefinitionToken
    /// Item must be a tool with the specific `TOOL_USE` value.
    /// The item type must be `TOOL:NONE` for this to make any sense.
    #[df_token(token_name = "HAS_TOOL_USE")]
    pub has_tool_use: Option<Reference>, // TODO reference to Item ToolToken (must have `TOOL_USE`)
    /// Item material must be considered fire-safe (stable temperature below 12000 °U ).
    /// Only works with items of type `BAR`, `BLOCKS`, `BOULDER`, `WOOD`, and `ANVIL` -
    /// all others are considered unsafe.
    #[df_token(token_name = "MAGMA_BUILD_SAFE")]
    pub magma_build_safe: Option<()>,
    /// Item material must be an ore of the specified metal.
    #[df_token(token_name = "METAL_ORE")]
    pub metal_ore: Option<Reference>, // TODO reference to Inorganic material
    /// Item's item dimension must be at least this large. The reagent's item type must be
    /// `BAR`, `POWDER_MISC`, `LIQUID_MISC`, `DRINK`, `THREAD`, `CLOTH`, or `GLOB` for this to work.
    #[df_token(token_name = "MIN_DIMENSION")]
    pub min_dimension: Option<u32>,
    /// Item must not have an edge, so must be blunt.
    /// Sharp stones (produced using knapping) and most types of weapon/ammo
    /// can not be used with this token.
    #[df_token(token_name = "NO_EDGE_ALLOWED")]
    pub no_edge_allowed: Option<()>,
    /// If the item is a container, it must not contain lye or milk.
    /// Not necessary if specifying `[EMPTY]`.
    #[df_token(token_name = "NOT_CONTAIN_BARREL_ITEM")]
    pub not_contain_barrel_item: Option<()>,
    /// Item can not be engraved. For example, a memorial slab can not be engraved.
    #[df_token(token_name = "NOT_ENGRAVED")]
    pub not_engraved: Option<()>,
    /// Item must be "collected" - used with `THREAD:NONE` to exclude webs.
    #[df_token(token_name = "NOT_WEB")]
    pub not_web: Option<()>,
    /// Requires the reagents material to have a matching `REACTION_CLASS` entry.
    /// Intended for reactions which accept a variety of materials but where the input material
    /// does not determine the output material, such as `FLUX` (for making pig iron and steel)
    /// and `GYPSUM` (for producing plaster powder).
    #[df_token(token_name = "REACTION_CLASS")]
    pub reaction_class: Option<Reference>, // TODO reference to Reaction token
    /// Item must not be rotten, mainly for organic materials.
    #[df_token(token_name = "UNROTTEN")]
    pub unrotten: Option<()>,
    /// Item material must come off a creature's body (`CORPSE` or `CORPSEPIECE`).
    #[df_token(token_name = "USE_BODY_COMPONENT")]
    pub use_body_component: Option<()>,
    /// Item must be "undisturbed" - used with `THREAD:NONE` to gather webs.
    #[df_token(token_name = "WEB_ONLY")]
    pub web_only: Option<()>,
    /// Item is made of an non-economic stone.
    #[df_token(token_name = "WORTHLESS_STONE_ONLY")]
    pub worthless_stone_only: Option<()>,
}

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum MatEnum {
    #[df_token(token_name = "MAT")]
    Mat,
}

impl Default for MatEnum {
    fn default() -> Self {
        Self::Mat
    }
}
