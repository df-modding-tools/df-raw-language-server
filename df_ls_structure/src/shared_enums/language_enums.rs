use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
/// Plural shorthand alternatives
pub enum PluralEnum {
    /// No Plural
    #[df_token(token_name = "NP")]
    Np,
    /// Standard Plural, adds an 's' on the end
    #[df_token(token_name = "STP")]
    Stp,
}
impl Default for PluralEnum {
    fn default() -> Self {
        Self::Np
    }
}
