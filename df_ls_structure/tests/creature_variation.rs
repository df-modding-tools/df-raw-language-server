use df_ls_core::{
    AllowEmpty, Any, BangArgN, BangArgNOrValue, BangArgNSequence, Reference, ReferenceTo,
};
use df_ls_lexical_analysis::test_utils::LexerTestBuilder;
use df_ls_structure::*;
use df_ls_syntax_analysis::test_utils::SyntaxTestBuilder;

#[test]
fn test_creature_variation() {
    SyntaxTestBuilder::from_lexer_test_builder(
        LexerTestBuilder::test_source(
            "creature_variation_header

            [OBJECT:CREATURE_VARIATION]

            [CREATURE_VARIATION:ANIMAL_PERSON]
                [CV_REMOVE_TAG:NAME]
                [CV_CONVERT_TAG]
                    [CVCT_MASTER:BODY]
                    [CVCT_TARGET:SPIDER]
                    [CVCT_REPLACEMENT:HUMANOID_6ARMS:3FINGERS]
                [CV_NEW_TAG:LARGE_ROAMING]

            [CREATURE_VARIATION:ANIMAL_PERSON_OTHER]
                [CV_REMOVE_CTAG:1:YES:NAME]
                [CV_CONVERT_CTAG:4:AFFIRMATIVE]
                    [CVCT_MASTER:BODY]
                    [CVCT_TARGET:SPIDER]
                    [CVCT_REPLACEMENT:HUMANOID_6ARMS:3FINGERS]
                [CV_NEW_CTAG:3:YEP:LARGE_ROAMING]

            [CREATURE_VARIATION:STANDARD_BIPED_GAITS]
                [CV_NEW_TAG:GAIT:WALK:Sprint:!ARG4:10:3:!ARG2:50:LAYERS_SLOW:STRENGTH:AGILITY:STEALTH_SLOWS:50]
                [CV_NEW_TAG:GAIT:WALK:Run:!ARG3:5:3:!ARG2:10:LAYERS_SLOW:STRENGTH:AGILITY:STEALTH_SLOWS:20]

            ",
        )
        .add_test_lexer_diagnostics_codes(vec![])
        .add_test_lexer_diagnostics_ranges(vec![]),
    )
    .add_test_structure(DFRaw {
        header: "creature_variation_header".to_owned(),
        token_structure: vec![ObjectToken {
            creature_variation_tokens: vec![
                CreatureVariationToken {
                    reference: Some(ReferenceTo::new("ANIMAL_PERSON".to_owned())),
                    cv_new_tag: vec![(Reference("LARGE_ROAMING".to_owned()), None)],
                    cv_remove_tag: vec![(Reference("NAME".to_owned()), None)],
                    cv_convert_tag: vec![CvConvertTag {
                        cv_convert_tag: Some(()),
                        cvct_master: Some(AllowEmpty::Some((
                            Reference("BODY".to_owned()),
                            None
                        ))),
                        cvct_target: Some((vec![Any::Reference(Reference(
                            "SPIDER".to_owned()
                        ))],)),
                        cvct_replacement: Some((vec![
                            Any::Reference(Reference("HUMANOID_6ARMS".to_owned())),
                            Any::Reference(Reference("3FINGERS".to_owned())),
                        ],)),
                    }],
                    ..Default::default()
                },
                CreatureVariationToken {
                    reference: Some(ReferenceTo::new("ANIMAL_PERSON_OTHER".to_owned())),
                    cv_new_ctag: vec![(
                        3,
                        Reference("YEP".to_owned()),
                        Some((vec![Any::Reference(Reference("LARGE_ROAMING".to_owned()))],)),
                    )],
                    cv_remove_ctag: vec![(
                        1,
                        Reference("YES".to_owned()),
                        Some((vec![Any::Reference(Reference("NAME".to_owned()))],)),
                    )],
                    cv_convert_ctag: vec![CvConvertCTag {
                        cv_convert_ctag: Some((
                            4,
                            Any::Reference(Reference("AFFIRMATIVE".to_owned()))
                        ),),
                        cvct_master: Some(AllowEmpty::Some((
                            Reference("BODY".to_owned()),
                            None
                        ))),
                        cvct_target: Some((vec![Any::Reference(Reference(
                            "SPIDER".to_owned()
                        ))],)),
                        cvct_replacement: Some((vec![
                            Any::Reference(Reference("HUMANOID_6ARMS".to_owned())),
                            Any::Reference(Reference("3FINGERS".to_owned())),
                        ],)),
                    }],
                    ..Default::default()
                },
                CreatureVariationToken {
                    reference: Some(ReferenceTo::new("STANDARD_BIPED_GAITS".to_owned())),
                    cv_new_tag: vec![
                        (
                            Reference("GAIT".to_owned()),
                            Some((vec![
                                Any::Reference(Reference("WALK".to_owned())),
                                Any::String("Sprint".to_owned()),
                                Any::BangArgNSequence(BangArgNSequence(vec![
                                    BangArgNOrValue::BangArgN(BangArgN(4))
                                ])),
                                Any::Integer(10),
                                Any::Integer(3),
                                Any::BangArgNSequence(BangArgNSequence(vec![
                                    BangArgNOrValue::BangArgN(BangArgN(2))
                                ])),
                                Any::Integer(50),
                                Any::Reference(Reference("LAYERS_SLOW".to_owned())),
                                Any::Reference(Reference("STRENGTH".to_owned())),
                                Any::Reference(Reference("AGILITY".to_owned())),
                                Any::Reference(Reference("STEALTH_SLOWS".to_owned())),
                                Any::Integer(50),
                            ],)),
                        ),
                        (
                            Reference("GAIT".to_owned()),
                            Some((vec![
                                Any::Reference(Reference("WALK".to_owned())),
                                Any::String("Run".to_owned()),
                                Any::BangArgNSequence(BangArgNSequence(vec![
                                    BangArgNOrValue::BangArgN(BangArgN(3))
                                ])),
                                Any::Integer(5),
                                Any::Integer(3),
                                Any::BangArgNSequence(BangArgNSequence(vec![
                                    BangArgNOrValue::BangArgN(BangArgN(2))
                                ])),
                                Any::Integer(10),
                                Any::Reference(Reference("LAYERS_SLOW".to_owned())),
                                Any::Reference(Reference("STRENGTH".to_owned())),
                                Any::Reference(Reference("AGILITY".to_owned())),
                                Any::Reference(Reference("STEALTH_SLOWS".to_owned())),
                                Any::Integer(20),
                            ],)),
                        ),
                    ],
                    ..Default::default()
                },
            ],
            ..Default::default()
        }],
    })
    .add_test_syntax_diagnostics_codes(vec![])
    .add_test_syntax_diagnostics_ranges(vec![])
    .run_test();
}
