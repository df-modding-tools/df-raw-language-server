use df_ls_syntax_analysis::TokenDeserialize;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, TokenDeserialize, PartialEq, Eq)]
#[df_token(enum_value)]
pub enum LaborEnum {
    /// [Mining](https://dwarffortresswiki.org/index.php/Mining)
    #[df_token(token_name = "MINE")]
    Mine,
    /// [Stone hauling](https://dwarffortresswiki.org/index.php/Stone_hauling)
    #[df_token(token_name = "HAUL_STONE")]
    HaulStone,
    /// [Wood hauling](https://dwarffortresswiki.org/index.php/Wood_hauling)
    #[df_token(token_name = "HAUL_WOOD")]
    HaulWood,
    /// [Burial](https://dwarffortresswiki.org/index.php/Burial)
    #[df_token(token_name = "HAUL_BODY")]
    HaulBody,
    /// [Food hauling](https://dwarffortresswiki.org/index.php/Food_hauling)
    #[df_token(token_name = "HAUL_FOOD")]
    HaulFood,
    /// [Refuse hauling](https://dwarffortresswiki.org/index.php/Refuse_hauling)
    #[df_token(token_name = "HAUL_REFUSE")]
    HaulRefuse,
    /// [Item hauling](https://dwarffortresswiki.org/index.php/Item_hauling)
    #[df_token(token_name = "HAUL_ITEM")]
    HaulItem,
    /// [Furniture hauling](https://dwarffortresswiki.org/index.php/Furniture_hauling)
    #[df_token(token_name = "HAUL_FURNITURE")]
    HaulFurniture,
    /// [Animal hauling](https://dwarffortresswiki.org/index.php/Animal_hauling)
    #[df_token(token_name = "HAUL_ANIMALS")]
    HaulAnimals,
    /// [Cleaning](https://dwarffortresswiki.org/index.php/Cleaning)
    #[df_token(token_name = "CLEAN")]
    Clean,
    /// [Wood cutting](https://dwarffortresswiki.org/index.php/Wood_cutting)
    #[df_token(token_name = "CUTWOOD")]
    CutWood,
    /// [Carpentry](https://dwarffortresswiki.org/index.php/Carpentry)
    #[df_token(token_name = "CARPENTER")]
    Carpenter,
    /// [Stone detailing](https://dwarffortresswiki.org/index.php/Stone_detailing)
    #[df_token(token_name = "DETAIL")]
    Detail,
    /// [Masonry](https://dwarffortresswiki.org/index.php/Masonry)
    #[df_token(token_name = "MASON")]
    Mason,
    /// [Architecture](https://dwarffortresswiki.org/index.php/Architecture)
    #[df_token(token_name = "ARCHITECT")]
    Architect,
    /// [Animal training](https://dwarffortresswiki.org/index.php/Animal_training)
    #[df_token(token_name = "ANIMALTRAIN")]
    AnimalTrainer,
    /// [Animal care](https://dwarffortresswiki.org/index.php/Animal_care)
    #[df_token(token_name = "ANIMALCARE")]
    AnimalCare,
    /// [Diagnosis](https://dwarffortresswiki.org/index.php/Diagnosis)
    #[df_token(token_name = "DIAGNOSE")]
    Diagnoser,
    /// [Surgery](https://dwarffortresswiki.org/index.php/Surgery)
    #[df_token(token_name = "SURGERY")]
    Surgery,
    /// [Setting bones](https://dwarffortresswiki.org/index.php/Setting_bones)
    #[df_token(token_name = "BONE_SETTING")]
    BoneSetting,
    /// [Suturing](https://dwarffortresswiki.org/index.php/Suturing)
    #[df_token(token_name = "SUTURING")]
    Suturing,
    /// [Dressing wounds](https://dwarffortresswiki.org/index.php/Dressing_wounds)
    #[df_token(token_name = "DRESSING_WOUNDS")]
    DressingWounds,
    /// [Feed patients/prisoners](https://dwarffortresswiki.org/index.php/Feed_patients/prisoners)
    #[df_token(token_name = "FEED_WATER_CIVILIANS")]
    FeedWaterCivilians,
    /// [Recovering wounded](https://dwarffortresswiki.org/index.php/Recovering_wounded)
    #[df_token(token_name = "RECOVER_WOUNDED")]
    RecoveringWounded,
    /// [Butchery](https://dwarffortresswiki.org/index.php/Butchery)
    #[df_token(token_name = "BUTCHER")]
    Butchery,
    /// [Trapper](https://dwarffortresswiki.org/index.php/Trapper)
    #[df_token(token_name = "TRAPPER")]
    Trapper,
    /// [Small animal dissection](https://dwarffortresswiki.org/index.php/Small_animal_dissection)
    #[df_token(token_name = "DISSECT_VERMIN")]
    SmallAnimalDissection,
    /// [Leatherworking](https://dwarffortresswiki.org/index.php/Leatherworking)
    #[df_token(token_name = "LEATHER")]
    Leatherworker,
    /// [Tanning](https://dwarffortresswiki.org/index.php/Tanning)
    #[df_token(token_name = "TANNER")]
    Tanning,
    /// [Brewing](https://dwarffortresswiki.org/index.php/Brewing)
    #[df_token(token_name = "BREWER")]
    Brewing,
    /// [Alchemy](https://dwarffortresswiki.org/index.php/Alchemy)
    #[df_token(token_name = "ALCHEMIST")]
    Alchemy,
    /// [Soap making](https://dwarffortresswiki.org/index.php/Soap_making)
    #[df_token(token_name = "SOAP_MAKER")]
    SoapMaking,
    /// [Weaving](https://dwarffortresswiki.org/index.php/Weaving)
    #[df_token(token_name = "WEAVER")]
    Weaving,
    /// [Clothesmaking](https://dwarffortresswiki.org/index.php/Clothesmaking)
    #[df_token(token_name = "CLOTHESMAKER")]
    ClothesMaking,
    /// [Milling](https://dwarffortresswiki.org/index.php/Milling)
    #[df_token(token_name = "MILLER")]
    Milling,
    /// [Plant processing](https://dwarffortresswiki.org/index.php/Plant_processing)
    #[df_token(token_name = "PROCESS_PLANT")]
    PlantProcessing,
    /// [Cheese making](https://dwarffortresswiki.org/index.php/Cheese_making)
    #[df_token(token_name = "MAKE_CHEESE")]
    CheeseMaking,
    /// [Milking](https://dwarffortresswiki.org/index.php/Milking)
    #[df_token(token_name = "MILK")]
    Milking,
    /// [Cooking](https://dwarffortresswiki.org/index.php/Cooking)
    #[df_token(token_name = "COOK")]
    Cooking,
    /// [Farming (fields)](https://dwarffortresswiki.org/index.php/Farming_(fields))
    #[df_token(token_name = "PLANT")]
    Farming,
    /// [Plant gathering](https://dwarffortresswiki.org/index.php/Plant_gathering)
    #[df_token(token_name = "HERBALIST")]
    PlantGathering,
    /// [Fishing](https://dwarffortresswiki.org/index.php/Fishing)
    #[df_token(token_name = "FISH")]
    Fishing,
    /// [Fish cleaning](https://dwarffortresswiki.org/index.php/Fish_cleaning)
    #[df_token(token_name = "CLEAN_FISH")]
    FishCleaning,
    /// [Fish dissection](https://dwarffortresswiki.org/index.php/Fish_dissection)
    #[df_token(token_name = "DISSECT_FISH")]
    FishDissection,
    /// [Hunting](https://dwarffortresswiki.org/index.php/Hunting)
    #[df_token(token_name = "HUNT")]
    Hunting,
    /// [Furnace operating](https://dwarffortresswiki.org/index.php/Furnace_operating)
    #[df_token(token_name = "SMELT")]
    FurnaceOperating,
    /// [Weaponsmithing](https://dwarffortresswiki.org/index.php/Weaponsmithing)
    #[df_token(token_name = "FORGE_WEAPON")]
    WeaponSmithing,
    /// [Armoring](https://dwarffortresswiki.org/index.php/Armoring)
    #[df_token(token_name = "FORGE_ARMOR")]
    Armoring,
    /// [Blacksmithing](https://dwarffortresswiki.org/index.php/Blacksmithing)
    #[df_token(token_name = "FORGE_FURNITURE")]
    Blacksmithing,
    /// [Metalcrafting](https://dwarffortresswiki.org/index.php/Metalcrafting)
    #[df_token(token_name = "METAL_CRAFT")]
    MetalCrafting,
    /// [Gem cutting](https://dwarffortresswiki.org/index.php/Gem_cutting)
    #[df_token(token_name = "CUT_GEM")]
    GemCutting,
    /// [Gem setting](https://dwarffortresswiki.org/index.php/Gem_setting)
    #[df_token(token_name = "ENCRUST_GEM")]
    GemSetting,
    /// [Woodcrafting](https://dwarffortresswiki.org/index.php/Woodcrafting)
    #[df_token(token_name = "WOOD_CRAFT")]
    WoodCrafting,
    /// [Stonecrafting](https://dwarffortresswiki.org/index.php/Stonecrafting)
    #[df_token(token_name = "STONE_CRAFT")]
    StoneCrafting,
    /// [Bone carving](https://dwarffortresswiki.org/index.php/Bone_carving)
    #[df_token(token_name = "BONE_CARVE")]
    BoneCarving,
    /// [Glass making](https://dwarffortresswiki.org/index.php/Glassmaking)
    #[df_token(token_name = "GLASSMAKER")]
    GlassMaking,
    /// [Strand extraction](https://dwarffortresswiki.org/index.php/Strand_extraction)
    #[df_token(token_name = "EXTRACT_STRAND")]
    StrandExtraction,
    /// [Siege engineering](https://dwarffortresswiki.org/index.php/Siege_engineering)
    #[df_token(token_name = "SIEGECRAFT")]
    SiegeEngineering,
    /// [Siege operating](https://dwarffortresswiki.org/index.php/Siege_operating)
    #[df_token(token_name = "SIEGEOPERATE")]
    SiegeOperating,
    /// [Bowyer](https://dwarffortresswiki.org/index.php/Bowyer)
    #[df_token(token_name = "BOWYER")]
    Bowyer,
    /// [Mechanics](https://dwarffortresswiki.org/index.php/Mechanics)
    #[df_token(token_name = "MECHANIC")]
    Mechanics,
    /// [Potash making](https://dwarffortresswiki.org/index.php/Potash_making)
    #[df_token(token_name = "POTASH_MAKING")]
    PotashMaking,
    /// [Lye making](https://dwarffortresswiki.org/index.php/Lye_making)
    #[df_token(token_name = "LYE_MAKING")]
    LyeMaking,
    /// [Dyeing](https://dwarffortresswiki.org/index.php/Dyeing)
    #[df_token(token_name = "DYER")]
    Dyeing,
    /// [Wood burning](https://dwarffortresswiki.org/index.php/Wood_burning)
    #[df_token(token_name = "BURN_WOOD")]
    WoodBurning,
    /// [Pump operating](https://dwarffortresswiki.org/index.php/Pump_operating)
    #[df_token(token_name = "OPERATE_PUMP")]
    PumpOperating,
    /// [Shearing](https://dwarffortresswiki.org/index.php/Shearing)
    #[df_token(token_name = "SHEARER")]
    Shearing,
    /// [Spinning](https://dwarffortresswiki.org/index.php/Spinning)
    #[df_token(token_name = "SPINNER")]
    Spinning,
    /// [Pottery](https://dwarffortresswiki.org/index.php/Pottery)
    #[df_token(token_name = "POTTERY")]
    Pottery,
    /// [Glazing](https://dwarffortresswiki.org/index.php/Glazing)
    #[df_token(token_name = "GLAZING")]
    Glazing,
    /// [Pressing](https://dwarffortresswiki.org/index.php/Pressing)
    #[df_token(token_name = "PRESSING")]
    Pressing,
    /// [Beekeeping](https://dwarffortresswiki.org/index.php/Beekeeping)
    #[df_token(token_name = "BEEKEEPING")]
    Beekeeping,
    /// [Wax working](https://dwarffortresswiki.org/index.php/Wax_working)
    #[df_token(token_name = "WAX_WORKING")]
    WaxWorking,
    /// [Push/haul vehicles](https://dwarffortresswiki.org/index.php/Push/haul_vehicles)
    #[df_token(token_name = "HANDLE_VEHICLES")]
    HandleVehicles,
    /// [Trade](https://dwarffortresswiki.org/index.php/Trade)
    #[df_token(token_name = "HAUL_TRADE")]
    HaulTrade,
    /// [Lever](https://dwarffortresswiki.org/index.php/Lever)
    #[df_token(token_name = "PULL_LEVER")]
    PullLever,
    /// [Teardown and building disassembly](https://dwarffortresswiki.org/index.php/Construction)
    #[df_token(token_name = "REMOVE_CONSTRUCTION")]
    RemoveConstruction,
    /// Haul water from [activity zone](https://dwarffortresswiki.org/index.php/Activity_zone)
    #[df_token(token_name = "HAUL_WATER")]
    HaulWater,
    /// [Gelding](https://dwarffortresswiki.org/index.php/Gelding)
    #[df_token(token_name = "GELD")]
    Gelding,
    /// [Road](https://dwarffortresswiki.org/index.php/Road)
    #[df_token(token_name = "BUILD_ROAD")]
    BuildRoad,
    /// [Construction](https://dwarffortresswiki.org/index.php/Construction)
    #[df_token(token_name = "BUILD_CONSTRUCTION")]
    BuildConstruction,
}

impl Default for LaborEnum {
    fn default() -> Self {
        Self::Mine
    }
}
